//
//  MyView.h
//  Window
//
//  Created by Nilesh Mahajan on 27/05/18.
//

#import <UIKit/UIKit.h>

@interface MyView : UIView <UIGestureRecognizerDelegate>

@end
