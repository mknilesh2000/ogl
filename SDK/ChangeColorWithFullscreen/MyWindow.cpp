﻿
// Info: Assignment 2
//
// Uses File-IO for logging. This uses ChangeColorWithFullscreen.log created in the current directory
//
// Shows a message box on creation
// Supports processing keystrokes as well as mouse keys
//
// - Changing the window color
//   R-Key => RED
//   G Key => GREEN
//   B-Key => BLUE
//   M-Key => MAGENTA
//   Y-Key => YELLOW
//   V-Key => VIOLET
//   Mouse Left Button / Right Button Pressed => change color
//   Shows the cursor position on WM_MOUSEMOVE message
//
// Toggles fullscreen using -
//  F-Key with CAPS ON => uses changeDisplaySettings
//  F-Key without CAPS_ON => uses window position change

#include "MyWindow.h"

const TCHAR MyWindow::m_appName[] = TEXT("MyApp");
const TCHAR MyWindow::m_windowName[] = TEXT("Nilesh's Window");

LRESULT MyWindow::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_DESTROY:
        fprintf(m_logFile, "WM_DESTROY received\n");
        PostQuitMessage(0);
        break;

    case WM_PAINT:
    {
        PAINTSTRUCT ps = { 0 };
        HDC hDC = BeginPaint(m_hWnd, &ps);
        if (hDC != NULL)
        {
            HBRUSH hbrush = CreateSolidBrush(m_backgroundColor);
            if (hbrush != NULL)
            {
                FillRect(hDC, &ps.rcPaint, hbrush);
                DeleteObject(hbrush);
            }
            EndPaint(m_hWnd, &ps);
        }
    }
    break;

    case WM_KEYDOWN:
        ProcessKeyDownMsg(wParam, lParam);
        break;

    case WM_MOUSEMOVE:
        DrawCursorPosition(LOWORD(lParam), HIWORD(lParam));
        break;

    case WM_LBUTTONDOWN:
        fprintf(m_logFile, "WM_LBUTTONDOWN received\n");
        m_backgroundColor *= 2;
        InvalidateRect(m_hWnd, NULL, FALSE);
        break;

    case WM_RBUTTONDOWN:
        fprintf(m_logFile, "WM_RBUTTONDOWN received\n");
        m_backgroundColor *= 3;
        InvalidateRect(m_hWnd, NULL, FALSE);
        break;
    }
    return DefWindowProc(m_hWnd, message, wParam, lParam);
}

void MyWindow::DrawCursorPosition(int x, int y)
{
    HDC hDC = GetWindowDC(m_hWnd);
    HBRUSH hbrush = CreateSolidBrush(m_backgroundColor);
    wchar_t buffer[64] = { L'\0' };
    wsprintf(buffer, L"X=%d, Y=%d", x, y);
    RECT rect = { 100, 100, 800, 800 };
    FillRect(hDC, &rect, hbrush);
    DrawText(hDC, buffer, -1, &rect, DT_LEFT);
    ReleaseDC(m_hWnd, hDC);
}

// Toggles fullscreen using -
// changeDisplaySettings if caps lock is ON
// uses window position change otherwise
void MyWindow::ToggleFullscreen(bool useCDS)
{
    fprintf(m_logFile, "Fullscreen with CDS=%s\n", useCDS ? "YES" : "NO");
    if (!m_isFullscreen)
    {
        // capture window position
        m_lastWindowStyle = GetWindowLong(m_hWnd, GWL_STYLE);
        GetWindowPlacement(m_hWnd, &m_lastWindowPlacement);

        if (useCDS)
        {
            SetWindowLong(m_hWnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);

            RECT rect = { 0 };
            GetWindowRect(m_hWnd, &rect);

            DEVMODE devMode = { 0 };
            devMode.dmSize = sizeof(devMode);
            devMode.dmPelsWidth = rect.right - rect.left;
            devMode.dmPelsHeight = rect.bottom - rect.top;
            devMode.dmDisplayFlags = DM_PELSWIDTH | DM_PELSHEIGHT;

            m_isFullscreen = (DISP_CHANGE_SUCCESSFUL == ChangeDisplaySettings(&devMode, CDS_FULLSCREEN));
            ShowWindow(m_hWnd, SW_MAXIMIZE);
        }
        else
        {
            HMONITOR hMonitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
            if (hMonitor != NULL)
            {
                MONITORINFO monitorInfo = { 0 };
                monitorInfo.cbSize = sizeof(monitorInfo);
                GetMonitorInfo(hMonitor, &monitorInfo);

                // only keep the client area of the window and remove rest
                SetWindowLong(m_hWnd, GWL_STYLE, m_lastWindowStyle & ~WS_OVERLAPPEDWINDOW);

                // set window position to monitor co-ordinates
                SetWindowPos(m_hWnd,
                    HWND_TOPMOST,
                    monitorInfo.rcMonitor.left,
                    monitorInfo.rcMonitor.top,
                    monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left,
                    monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top,
                    SWP_FRAMECHANGED);

                m_isFullscreen = true;
            }
        }
    }
    else
    {
        // revert any temporary modes if set
        ChangeDisplaySettings(NULL, 0);
        // restore the last window position
        SetWindowPlacement(m_hWnd, &m_lastWindowPlacement);
        SetWindowLong(m_hWnd, GWL_STYLE, m_lastWindowStyle);
        m_isFullscreen = false;
    }
}

void MyWindow::ProcessKeyDownMsg(WPARAM wParam, LPARAM lParam)
{
    // only press when last state was up
    if ((lParam & 0x40000000) == 0)
    {
        switch (wParam)
        {
        case R_KEY:
            fprintf(m_logFile, "R key received\n");
            m_backgroundColor = RGB(255, 0, 0);
            InvalidateRect(m_hWnd, NULL, FALSE);
            break;
        case G_KEY:
            fprintf(m_logFile, "G key received\n");
            m_backgroundColor = RGB(0, 255, 0);
            InvalidateRect(m_hWnd, NULL, FALSE);
            break;
        case B_KEY:
            fprintf(m_logFile, "B key received\n");
            m_backgroundColor = RGB(0, 0, 255);
            InvalidateRect(m_hWnd, NULL, FALSE);
            break;
        case Y_KEY:
            fprintf(m_logFile, "Y key received\n");
            m_backgroundColor = RGB(255, 255, 0);
            InvalidateRect(m_hWnd, NULL, FALSE);
            break;
        case M_KEY:
            fprintf(m_logFile, "M key received\n");
            m_backgroundColor = RGB(255, 0, 255);
            InvalidateRect(m_hWnd, NULL, FALSE);
            break;
        case V_KEY:
            fprintf(m_logFile, "V key received\n");
            m_backgroundColor = RGB(0, 255, 255);
            InvalidateRect(m_hWnd, NULL, FALSE);
            break;
        case VK_ESCAPE:
            fprintf(m_logFile, "ESC key received\n");
            DestroyWindow(m_hWnd);
            break;
        case F_KEY:
            {
                fprintf(m_logFile, "F key received\n");
                ToggleFullscreen(GetKeyState(VK_CAPITAL) != 0);
                break;
            }
        }
    }
}

int MyWindow::Main(HINSTANCE hInstance, int nCmdShow)
{
    if (fopen_s(&m_logFile, "ChangeColorWithFullscreen.txt", "w+") != 0)
        return -1;

    fprintf(m_logFile, "In %s\n", __FUNCTION__);

    WNDCLASSEX wndclass = { 0 };
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.lpfnWndProc = MyWindow::WndProcFunc;
    wndclass.hInstance = hInstance;
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground = (HBRUSH)CreateSolidBrush(m_backgroundColor);
    wndclass.lpszClassName = m_appName;
    wndclass.lpszMenuName = NULL;
    wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

    // register above class
    RegisterClassEx(&wndclass);

    // create window
    m_hWnd = CreateWindowEx(WS_EX_APPWINDOW,
                        m_appName,
                        m_windowName,
                        WS_OVERLAPPEDWINDOW,
                        CW_USEDEFAULT,
                        CW_USEDEFAULT,
                        CW_USEDEFAULT,
                        CW_USEDEFAULT,
                        NULL,
                        NULL,
                        hInstance,
                        NULL);

    if (m_hWnd != NULL)
    {
        SetWindowLongPtr(m_hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
        ShowWindow(m_hWnd, nCmdShow);
        UpdateWindow(m_hWnd);

        // message loop
        MSG msg;
        while (GetMessage(&msg, NULL, 0, 0))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        return ((int)msg.wParam);
    }

    return -1;
}

LRESULT CALLBACK MyWindow::WndProcFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (message == WM_CREATE)
    {
        // This message comes before we set window Long parameters so handle here.
        ::MessageBox(NULL, TEXT("Window Created"), TEXT("Info"), MB_OK);
    }
    else
    {
        MyWindow* pThis = reinterpret_cast<MyWindow*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
        if (pThis != NULL)
        {
            return pThis->WndProc(message, wParam, lParam);
        }
    }
    return DefWindowProc(hWnd, message, wParam, lParam);
}

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
    MyWindow window;
    return window.Main(hInstance, nCmdShow);
}
