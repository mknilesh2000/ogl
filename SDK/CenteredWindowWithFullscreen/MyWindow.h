#pragma once

#include <Windows.h>
#include <stdio.h>

#define R_KEY 0x52
#define G_KEY 0x47
#define B_KEY 0x42
#define Y_KEY 0x59
#define M_KEY 0x4D
#define V_KEY 0x56
#define F_KEY 0x46

class MyWindow
{
public:
    MyWindow()
        : m_hWnd(NULL),
          m_backgroundColor(RGB(0,0,0)),
          m_isFullscreen(false),
          m_lastWindowStyle(0l),
          m_logFile(NULL)
    {
        ZeroMemory(&m_lastWindowPlacement, sizeof(m_lastWindowPlacement));
    }

    ~MyWindow()
    {
        if (m_logFile)
            fclose(m_logFile);
    }

public:
    static LRESULT CALLBACK WndProcFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
    int Main(HINSTANCE hInstance, int nCmdShow);

private:
    LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam);
    void ProcessKeyDownMsg(WPARAM wParam, LPARAM lParam);
    void ToggleFullscreen(bool useCDS);
    void DrawCursorPosition(int x, int y);

    HWND    m_hWnd;
    COLORREF m_backgroundColor;
    bool m_isFullscreen;
    LONG m_lastWindowStyle;
    WINDOWPLACEMENT m_lastWindowPlacement;
    FILE *m_logFile;

    static const TCHAR m_appName[];
    static const TCHAR m_windowName[];
};
