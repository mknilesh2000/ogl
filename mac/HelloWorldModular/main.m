#import "AppDelegate.h"
#import <Foundation/Foundation.h>

// Entry point function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
    
    // get application object
    NSApp = [NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    // Execure runLoop
    [NSApp run];

    [pPool release];

    return (0);
}