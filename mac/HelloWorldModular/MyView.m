#import "MyView.h"

@implementation MyView
{
    NSString *centralText;
}

- (id) initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];

    if (self)
    {
        [[self window] setContentView:self];
        centralText = @"Hello World";
    }

    return (self);
}

- (void) drawRect:(NSRect)dirtyRect
{
    // black background
    NSColor *fillColor = [NSColor blackColor];
    [fillColor set];
    NSRectFill(dirtyRect);

    // dictionary with kvc
    NSDictionary *dictionaryForTextAttributes = 
        [NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Helvetica" size:32],
            NSFontAttributeName,
            [NSColor greenColor],
            NSForegroundColorAttributeName,
            nil];
    
    NSSize textSize = [centralText sizeWithAttributes:dictionaryForTextAttributes];

    NSPoint point;
    point.x = (dirtyRect.size.width/2) - (textSize.width/2);
    point.y = (dirtyRect.size.height/2) - (textSize.height/2) + 12;

    [centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
}

// called for handling keyDown event
- (BOOL) acceptsFirstResponder
{
    [[self window] makeFirstResponder:self];
    return (YES);
}

- (void) keyDown:(NSEvent *)theEvent
{
    int key = (int) [[theEvent characters] characterAtIndex:0];
    switch (key)
    {
        case 27: // Esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            centralText=@"'F' or 'f' Key is pressed";
            [[self window] toggleFullScreen:self]; //repainting occurs automatically
            break;

        default:
            break;
    }
}

- (void) mouseDown:(NSEvent*)theEvent
{
    centralText = @"Left mouse button is clicked";
    [self setNeedsDisplay:YES]; // repainting
}

- (void) mouseDragged:(NSEvent*)theEvent
{

}

- (void) rightMouseDown:(NSEvent*)theEvent
{
    centralText = @"Right mouse button is clicked";
    [self setNeedsDisplay:YES]; // repainting
}

- (void) dealloc
{
    [super dealloc];
}
@end
