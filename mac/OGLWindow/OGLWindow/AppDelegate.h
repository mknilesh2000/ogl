//
//  AppDelegate.h
//  OGLWindow
//
//  Created by Nilesh Mahajan on 30/07/18.
//  Copyright © 2018 Nilesh Mahajan. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

