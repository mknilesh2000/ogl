//
//  GLESView.m
//  Window
//
//  Created by Nilesh Mahajan on 27/05/18.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import <glm/gtx/transform.hpp>
#import <glm/gtc/type_ptr.hpp>

#import "GLESView.h"

enum ATTRIBUTE
{
    VERTEX = 0,
    COLOR = 1,
    NORMAL = 2,
    TEXTURE0 = 3
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Draw Members
    GLuint m_vertexShaderObject;
    GLuint m_fragmentShaderObject;
    GLuint m_shaderProgramObject;
    GLuint m_vaoPyramid;
    GLuint m_vboPyramid[4];
    GLuint m_vaoCube;
    GLuint m_vboCube[4];
    GLuint m_MVPuniform;
    glm::mat4x4 m_projectionMatrix;
}

- (GLint) CheckObjectStatus:(GLuint)object forOperation:(GLenum)operation forObjectName:(const char *)objectName
{
    GLint status = GL_FALSE;
    bool isLinkOperation = operation == GL_LINK_STATUS;
    isLinkOperation ? glGetProgramiv(object, operation, &status) :
    glGetShaderiv(object, operation, &status);
    
    if (status == GL_FALSE)
    {
        assert(0);
        printf("%s failed\n", objectName);
        GLint shaderInfoLogLength = 0;
        isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &status) :
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &status);
        if (shaderInfoLogLength > 0)
        {
            GLchar *szInfoLog = (GLchar*) malloc(shaderInfoLogLength * sizeof(GLchar));
            if (szInfoLog != NULL)
            {
                memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                GLsizei written = 0;
                isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                printf("%s info Log :\n %s", objectName, szInfoLog);
                free(szInfoLog);
            }
        }
    }
    return status;
}

- (void) LoadPyramidData
{
    const GLfloat pyramidVertices[] = {
        // front
        0.0f, 0.5f, 0.0f,
        -0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        // back
        0.0f, 0.5f, 0.0f,
        0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        // left
        0.0f, 0.5f, 0.0f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, 0.5f,
        // right
        0.0f, 0.5f, 0.0f,
        0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, -0.5f
    };
    
    const GLfloat pyramidColors[] = {
        // front face
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        // back
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        // left
        0.0f, 0.0f, 1.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 1.0f,
        // right
        0.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f, 1.0f,
    };
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoPyramid);
    glBindVertexArray(m_vaoPyramid);
    
    // prepare array buffer
    glGenBuffers(4, m_vboPyramid);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboPyramid[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboPyramid[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidColors), pyramidColors, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::COLOR, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) LoadCubeData
{
    const GLfloat cubeVertices[] =
    {
        // front
        0.5f, 0.5f, 0.5f,
        -0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        // right
        0.5f, 0.5f, -0.5f,
        0.5f, 0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,
        // left
        -0.5f, 0.5f, 0.5f,
        -0.5f, 0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, 0.5f,
        // back
        -0.5f, 0.5f, -0.5f,
        0.5f, 0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        // top
        0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, 0.5f,
        0.5f, 0.5f, 0.5f,
        // bottom
        0.5f, -0.5f, 0.5f,
        -0.5f, -0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,
        0.5f, -0.5f, -0.5f
    };
    
    const GLfloat CubeColors[] =
    {
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        
        0.0f, 0.0f, 1.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 1.0f,
        
        1.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f,
        
        0.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f, 1.0f,
        
        1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 1.0f
    };
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoCube);
    glBindVertexArray(m_vaoCube);
    
    // prepare array buffer
    glGenBuffers(4, m_vboCube);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(CubeColors), CubeColors, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::COLOR, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) initialize
{
    m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSource =
    "#version 300 es\n"                       \
    "in vec4 vPosition;"                                        \
    "in vec4 vColor;"                                           \
    "out vec4 outColor;"                                        \
    "uniform mat4 u_mvp_matrix;"                                \
    "void main(void)"                                           \
    "{"                                                         \
    "   gl_Position = u_mvp_matrix * vPosition;"                \
    "   outColor = vColor;"                                     \
    "}";
    
    glShaderSource(m_vertexShaderObject, 1, &vertexShaderSource, NULL);
    glCompileShader(m_vertexShaderObject);
    [self CheckObjectStatus:m_vertexShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Vertex Shader"];
    
    m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentShaderSource =
    "#version 300 es\n"                             \
    "precision highp float;"                        \
    "out vec4 FragColor;"                               \
    "in vec4 outColor;"                                 \
    "void main(void)"                                   \
    "{"                                                 \
    "   FragColor = outColor;"                          \
    "}";
    glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSource, NULL);
    glCompileShader(m_fragmentShaderObject);
    [self CheckObjectStatus:m_fragmentShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Fragment Shader"];
    
    m_shaderProgramObject = glCreateProgram();
    glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
    glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::COLOR, "vColor");
    glLinkProgram(m_shaderProgramObject);
    [self CheckObjectStatus:m_shaderProgramObject forOperation:GL_LINK_STATUS forObjectName:"Shader Program"];
    
    m_MVPuniform = glGetUniformLocation(m_shaderProgramObject, "u_mvp_matrix");
    
    [self LoadPyramidData];
    [self LoadCubeData];
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        // initialize drawing objects
        [self initialize];
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    // code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    static GLfloat angle = 0.0f;
    angle += 0.01f;
    
    glUseProgram(m_shaderProgramObject);
    
    // Draw Pyramid
    {
        glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(-1.5f, 0.0f, -4.0f));
        glm::mat4x4 modelViewProjectionMatrix =
        m_projectionMatrix * glm::rotate(translationMatrix, angle, glm::vec3(0.0f, 1.0f, 0.0f));
        glUniformMatrix4fv(m_MVPuniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
        
        glBindVertexArray(m_vaoPyramid);
        glDrawArrays(GL_TRIANGLES, 0, 12);
        glBindVertexArray(0);
    }
    
    // Draw Cube
    {
        glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(1.5f, 0.0f, -4.0f));
        glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(), angle, glm::vec3(1.0f, 0.0f, 0.0f))
        * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 1.0f, 0.0f))
        * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 0.0f, 1.0f));
        glm::mat4x4 scaleMatrix = glm::scale(glm::mat4x4(), glm::vec3(0.75f, 0.75f, 0.75f));
        
        glm::mat4x4 modelViewProjectionMatrix =
        m_projectionMatrix * translationMatrix * rotationMatrix * scaleMatrix;
        
        glUniformMatrix4fv(m_MVPuniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));

        glBindVertexArray(m_vaoCube);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
        glBindVertexArray(0);
    }
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    GLfloat aspect = (GLfloat)width / (GLfloat)height;
    m_projectionMatrix = glm::perspective(45.0f, aspect, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    // code
    if (m_vaoPyramid)
    {
        glDeleteVertexArrays(1, &m_vaoPyramid);
        m_vaoPyramid = 0;
    }
    
    if (m_vboPyramid[0] != 0)
    {
        glDeleteBuffers(4, m_vboPyramid);
        memset(m_vboPyramid, 0, sizeof(m_vboPyramid));
    }
    
    if (m_vaoCube)
    {
        glDeleteVertexArrays(1, &m_vaoCube);
        m_vaoCube = 0;
    }
    
    if (m_vboCube[0] != 0)
    {
        glDeleteBuffers(4, m_vboCube);
        memset(m_vboCube, 0, sizeof(m_vboCube));
    }
    
    if (m_shaderProgramObject)
    {
        glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
        glDeleteProgram(m_shaderProgramObject);
        m_shaderProgramObject = 0;
    }
    
    if (m_fragmentShaderObject)
    {
        glDeleteShader(m_fragmentShaderObject);
        m_fragmentShaderObject = 0;
    }
    
    if (m_vertexShaderObject)
    {
        glDeleteShader(m_vertexShaderObject);
        m_vertexShaderObject = 0;
    }

    if (depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if (colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if (defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if ([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}
@end
