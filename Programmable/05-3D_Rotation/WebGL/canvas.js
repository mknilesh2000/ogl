// global variables
var canvas = null;
var gl = null;      // webgl context
var bFullScreen = false;
var canvasOriginalWidth;
var canvasOriginalHeight;

const WebGLMacros = 
{
    VERTEX: 0,
    COLOR: 1,
    NORMAL: 2,
    TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var mvpUniform;
var projectionMatrix;

// To start animation
var requestAnimationFrame = 
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

// To stop animation
var cancelAnimationFrame = 
    window.cancelAnimationFrame ||
    window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

class Model3D
{
    constructor()
    {
        this.vao = null;
        this.vboPosition = null;
        this.vboColor = null;
        this.vertices = null;
        this.colors = null;
    }

    initialize(vertices, colors)
    {
        this.vao = gl.createVertexArray();
        gl.bindVertexArray(this.vao);

        this.vboPosition = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vboPosition);
        gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        this.vboColor = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vboColor);
        gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.COLOR, 4, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.COLOR);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);

        this.vertices = vertices;
        this.colors = colors;
    }

    drawOverride()
    {

    }

    draw()
    {
        gl.bindVertexArray(this.vao);
        this.drawOverride();
        gl.bindVertexArray(null);
    }

    uninitialize()
    {
        if (this.vao)
        {
            gl.deleteVertexArray(this.vao);
            this.vao = null;
        }
    
        if (this.vboPosition)
        {
            gl.deleteBuffer(this.vboPosition);
            this.vboPosition = null;
        }
    }
};

class Pyramid extends Model3D
{
    drawOverride()
    {
        gl.drawArrays(gl.TRIANGLES, 0, this.vertices.length / 3);
    }
};

class Cube extends Model3D
{
    drawOverride()
    {
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
    }
}

var pyramid = new Pyramid();
var cube = new Cube();

// onload function
function main()
{
    canvas = document.getElementById("WGLCanvas");
    if (!canvas)
        console.log("Obtaining canvas failed");
    else
        console.log("Canvas Obtained");

    canvasOriginalWidth = canvas.width;
    canvasOriginalHeight = canvas.height;

    // register event handlers
    window.addEventListener("keydown", onKeyDown, false);
    window.addEventListener("click", onClick, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function onKeyDown(event)
{
    switch (event.keyCode)
    {
        case 27: // escape
            uninitialize();
            window.close();
            break;

        case 70: // for 'F' or 'f'
            toggleFullscreen();
            break;
    }
}

function onClick()
{
    // code
}

function resize()
{
    if (bFullScreen)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvasOriginalWidth;
        canvas.height = canvasOriginalHeight;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    var aspect = canvas.width / canvas.height;
    if (aspect > 1.0)
    {
        mat4.perspective(projectionMatrix, 45.0, aspect, 0.1, 100);
    }
    else
    {
        mat4.perspective(projectionMatrix, 45.0, aspect, 0.1, 100);
    }
}

function toggleFullscreen()
{
    var fullscreen_element = 
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    if (fullscreen_element == null)
    {
        // not fullscreen
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.requestFullscreen();

        bFullScreen = true;
    }
    else
    {
        // in-fullscreen
        if (document.exitFullscreen)
            document.exitFullScreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msCancelFullscreen)
            document.msCancelFullscreen();

        bFullScreen = false;
    }
}

function init()
{
    // get WeGL 2.0 context
    gl = canvas.getContext("webgl2");

    if (gl == null)
    {
        console.log("Failed to get rendering context for WebGL 2.0");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    var vertexShaderSourceCode = document.getElementById("vertex-shader");
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode.firstChild.data);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    // fragment shader
    var fragmentShaderSourceCode = document.getElementById("fragment-shader");
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode.firstChild.data);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    // create program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // bind attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.COLOR, "vColor");

    // link program
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    // configure for 3D
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
    gl.enable(gl.CULL_FACE);

    // vertices, colors, shader attribs, vao, vbo initialization
    var pyramidVertices = new Float32Array([
        // front
        0.0, 0.5, 0.0,
        -0.5, -0.5, 0.5,
        0.5, -0.5, 0.5,
        // back
        0.0, 0.5, 0.0,
        0.5, -0.5, -0.5,
        -0.5, -0.5, -0.5,
        // left
        0.0, 0.5, 0.0,
        -0.5, -0.5, -0.5,
        -0.5, -0.5, 0.5,
        // right
        0.0, 0.5, 0.0,
        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5
    ]);
    var pyramidColors = new Float32Array([
        // front face
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0,
        // back
        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
        // left
        0.0, 0.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,
        // right
        0.0, 1.0, 1.0, 1.0,
        0.0, 1.0, 1.0, 1.0,
        0.0, 1.0, 1.0, 1.0,
    ]);
    pyramid.initialize(pyramidVertices, pyramidColors);

    var cubeVertices = new Float32Array([
        // front
        0.5, 0.5, 0.5,
        -0.5, 0.5, 0.5,
        -0.5, -0.5, 0.5,
        0.5, -0.5, 0.5,
        // right
        0.5, 0.5, -0.5,
        0.5, 0.5, 0.5,
        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5,
        // left
        -0.5, 0.5, 0.5,
        -0.5, 0.5, -0.5,
        -0.5, -0.5, -0.5,
        -0.5, -0.5, 0.5,
        // back
        -0.5, 0.5, -0.5,
        0.5, 0.5, -0.5,
        0.5, -0.5, -0.5,
        -0.5, -0.5, -0.5,
        // top
        0.5, 0.5, -0.5,
        -0.5, 0.5, -0.5,
        -0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        // bottom
        0.5, -0.5, 0.5,
        -0.5, -0.5, 0.5,
        -0.5, -0.5, -0.5,
        0.5, -0.5, -0.5
    ]);
    var cubeColors = new Float32Array([
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0,

        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,

        0.0, 0.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,

        1.0, 1.0, 0.0, 1.0,
        1.0, 1.0, 0.0, 1.0,
        1.0, 1.0, 0.0, 1.0,
        1.0, 1.0, 0.0, 1.0,

        0.0, 1.0, 1.0, 1.0,
        0.0, 1.0, 1.0, 1.0,
        0.0, 1.0, 1.0, 1.0,
        0.0, 1.0, 1.0, 1.0,

        1.0, 0.0, 1.0, 1.0,
        1.0, 0.0, 1.0, 1.0,
        1.0, 0.0, 1.0, 1.0,
        1.0, 0.0, 1.0, 1.0
    ]);
    cube.initialize(cubeVertices, cubeColors);

    // set color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    projectionMatrix = mat4.create();
}

function uninitialize()
{
    cube.uninitialize();
    pyramid.uninitialize();

    if (shaderProgramObject)
    {
        if (fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject)
        {
            gl.deleteShader(vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject = null;
        }

        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}

var angleDeg = 0.0;
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    angleDeg += 1.0;

    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    var angleRad = angleDeg * 3.14 / 180.0;

    // draw pyramid
    mat4.translate(modelViewMatrix, mat4.create(), vec3.fromValues(-1.0, 0.0, -3.0));
    mat4.rotate(modelViewMatrix, modelViewMatrix, angleRad, vec3.fromValues(0.0, 1.0, 0.0));
    mat4.multiply(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    pyramid.draw();

    // draw cube
    mat4.translate(modelViewMatrix, mat4.create(), vec3.fromValues(1.0, 0.0, -3.0));
    mat4.rotate(modelViewMatrix, modelViewMatrix, angleRad, vec3.fromValues(1.0, 0.0, 0.0));
    mat4.rotate(modelViewMatrix, modelViewMatrix, angleRad, vec3.fromValues(0.0, 1.0, 0.0));
    mat4.rotate(modelViewMatrix, modelViewMatrix, angleRad, vec3.fromValues(0.0, 0.0, 1.0));
    mat4.multiply(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    cube.draw();

    gl.useProgram(null);

    // animation loop
    requestAnimationFrame(draw, canvas);
}