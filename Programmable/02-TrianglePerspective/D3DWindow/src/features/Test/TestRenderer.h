#pragma once

#include <Renderer.h>
#include <stdio.h>

#include <d3d11.h>
#include <d3dcompiler.h>
#pragma warning(disable: 4838)
#include <XNAMath\xnamath.h>

namespace Features
{
    using namespace Interfaces;

    class TestRenderer : public IRenderer
    {
        struct CBUFFER
        {
            XMMATRIX WorldViewProjectionMatrix;
        };

    private:
        FILE*                       m_fp;
        float                       m_clearColor[4];
        IDXGISwapChain*             m_pIDxgiSwapChain;
        ID3D11Device*               m_pID3dDevice;
        ID3D11DeviceContext*        m_pID3dDeviceContext;
        ID3D11RenderTargetView*     m_pID3d11RenderTargetView;

        ID3D11VertexShader*         m_pID3d11VertexShader;
        ID3D11PixelShader*          m_pID3d11PixelShader;
        ID3D11Buffer*               m_pID3d11Buffer_VertexBuffer;
        ID3D11InputLayout*          m_pID3d11InputLayout;
        ID3D11Buffer*               m_pID3d11Buffer_ConstantBuffer;

        ID3DBlob*                   m_pID3DBlobVertexShader;
        ID3DBlob*                   m_pID3DBlobPixelShader;

        XMMATRIX                    m_projectionMatrix;
    public:
        /// Method for retrieving name of the renderer
        /// @param rendererName buffer to be filled with the renderer name
        /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
        ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
        const char* GetName() override;

        /// Method for performing one-time renderer initialization.
        /// Renderer can initialize global/static instances as part of this method
        /// @param window identifier of the window where drawing is directed
        /// @return RENDERER_RESULT_SUCCESS if succeded
        ///         RENDERER_RESULT_ERROR if failed.
        RendererResult Initialize(Window window) override;

        /// Method for performing one-time renderer un-initialization before it is unloaded
        /// Renderer can perform global cleanup as part of this method
        void Uninitialize(void) override;

        /// Method for performing scene-specific initialization
        /// This method will be called by the host before rendering a scene to the active renderer.
        /// Renderer should do initialization of scene specific things as part of this method
        /// @param scene Identifier of a scene to be initialized
        /// @return RENDERER_RESULT_SUCCESS if succeded
        ///         RENDERER_RESULT_ERROR if failed.
        RendererResult InitializeScene(SceneType scene) override;

        /// Method for performing scene-specific initialization
        /// This method will be called by the host after rendering a scene to the active renderer
        /// Renderer should do cleanup of scene specific things done as part of scene initialize.
        /// @param scene Identifier of a scene to be cleaned-up
        void UninitializeScene(SceneType scene) override;

        /// Method for rendering a frame in a scene
        /// This method will be called by the host per frame of a scene only to the active renderer
        /// @param params describes the parameters curresponding to this render
        /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
        ///         RENDERER_RESULT_ERROR if failed in building the frame
        ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
        ///                                   in such cases no further frame calls would be made for this scene
        ///                                   to the renderer.
        virtual RendererResult Render(const RenderParams &params) override;

        /// Generic method to notify active renderer about a message posted to host window. The message can be
        /// from system or initiated by the host itself.
        /// @param message OS dependent structure that describes the system message being processed.
        void OnMessage(const Message &message) override;

        /// Generic method to notify active renderer about the change in the dimensions of the host window
        /// @param width New width of the window
        /// @param height New height of the window
        void OnResize(unsigned int width, unsigned int height) override;

        // constructor
        TestRenderer();

        // destructor
        ~TestRenderer();

    private:
        void RefreshLogFile();
        HRESULT CreateVertexShader();
        HRESULT CreatePixelShader();
        HRESULT ConfigureGeometry();

        void CleanupGeometry();
        void CleanupVertexShader();
        void CleanupPixelShader();
    };

}
