#version 410 core

// from Vertex shader
in vec3 phong_ads_color;
in vec3 transformed_normals;
in vec3 light_direction;
in vec3 viewer_vector;

// lighting variables
uniform int u_lightMode;

// light
vec3 u_La = vec3(0.0, 0.0, 0.0);
vec3 u_Ld = vec3(1.0, 1.0, 1.0);
vec3 u_Ls = vec3(1.0, 1.0, 1.0);
uniform vec3 u_light_position;

// material constants
uniform vec3 u_Ka;                  // ambient reflective color intensity
uniform vec3 u_Kd;                  // diffused reflective color intensity
uniform vec3 u_Ks;                  // specular reflective color intensity
uniform float u_material_shininess; // shininess

// final color after lighting calculations applied
out vec4 FragColor;

void main(void)
{
    if (u_lightMode == 2)
    {
        // per-fragment lighting
        vec3 normalized_transformed_normals = normalize(transformed_normals);
        vec3 normalized_viewer_vector = normalize(viewer_vector);

        // calculate effective light components
        {
            vec3 normalized_light_direction = normalize(light_direction);
            vec3 ambient = u_La * u_Ka;

            float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);
            vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;

            vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);
            vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);

            FragColor = vec4(ambient + diffuse + specular, 1.0);
        }
    }
    else
    {
        FragColor = vec4(phong_ads_color, 1.0);
    }
}
