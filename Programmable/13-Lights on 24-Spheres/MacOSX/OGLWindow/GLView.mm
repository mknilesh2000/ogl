//
//  GLView.m
//  OGLWindow
//
//  Created by Nilesh Mahajan on 30/07/18.
//  Copyright © 2018 Nilesh Mahajan. All rights reserved.
//

#import "GLView.h"
#import <CoreVideo/CVDisplayLink.h>
#import <glm/gtx/transform.hpp>
#import <glm/gtc/type_ptr.hpp>
#import "Utils.h"
#import "Sphere.h"
#import "Material.h"

extern FILE* gpFile;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext);

enum ATTRIBUTE
{
    VERTEX = 0,
    COLOR,
    NORMAL,
    TEXTURE0,
    ELEMENT,
    LAST
};

enum RotationAxis
{
    X,
    Y,
    Z
};

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    GLuint          m_vertexShaderObject;
    GLuint          m_fragmentShaderObject;
    GLuint          m_shaderProgramObject;
    
    GLuint          m_vaoSphere;
    GLuint          m_vboSphere[ATTRIBUTE::LAST];
    glm::mat4x4     m_perspectiveProjectionMatrix;
    
    // MVP
    GLuint          m_ModelMatrixUniform;
    GLuint          m_ViewMatrixUniform;
    GLuint          m_ProjectionMatrixUniform;
    
    // Light
    GLuint          m_lightEnableStateUniform;
    GLuint          m_lightPositionUniform;
    
    // Material constants
    GLuint          m_KaUniform;
    GLuint          m_KdUniform;
    GLuint          m_KsUniform;
    GLuint          m_ShininessUniform;
    
    bool            m_enableAnimation;
    GLuint          m_LightMode;
    unsigned int    m_winWidth;
    unsigned int    m_winHeight;
    RotationAxis    m_rotationAxis;
    Sphere          m_sphere;
}

- (id) initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    if (self != NULL)
    {
        [[self window] setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            // Specify a display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0       // last 0 is a must
        };
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if (pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format available. Exitting... ");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; // this will automatically releases the older context, if present and sets the newer one
    }
    return (self);
}

- (CVReturn) getFrameForTime:(const CVTimeStamp*)pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [self drawView];
    [pool release];
    return kCVReturnSuccess;
}

- (GLint) CheckObjectStatus:(GLuint)object forOperation:(GLenum)operation forObjectName:(const char *)objectName
{
    GLint status = GL_FALSE;
    bool isLinkOperation = operation == GL_LINK_STATUS;
    isLinkOperation ? glGetProgramiv(object, operation, &status) :
    glGetShaderiv(object, operation, &status);
    
    if (status == GL_FALSE)
    {
        assert(0);
        fprintf(gpFile, "%s failed\n", objectName);
        GLint shaderInfoLogLength = 0;
        isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &status) :
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &status);
        if (shaderInfoLogLength > 0)
        {
            GLchar *szInfoLog = (GLchar*) malloc(shaderInfoLogLength * sizeof(GLchar));
            if (szInfoLog != NULL)
            {
                memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                GLsizei written = 0;
                isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                fprintf(gpFile, "%s info Log :\n %s", objectName, szInfoLog);
                free(szInfoLog);
            }
        }
    }
    return status;
}

- (void) LoadSphereData
{
    float sphereVertices[1146];
    float sphereNormals[1146];
    float sphere_textures[764];
    short sphereElements[2280];
    
    m_sphere.getSphereVertexData(sphereVertices, sphereNormals, sphere_textures, sphereElements);
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoSphere);
    glBindVertexArray(m_vaoSphere);
    
    // prepare array buffer
    glGenBuffers(ATTRIBUTE::LAST, m_vboSphere);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphereVertices), sphereVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::NORMAL]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphereNormals), sphereNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::ELEMENT]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphereElements), sphereElements, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) initialize
{
    m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    ShaderFile vertexShader("shader.vert");
    glShaderSource(m_vertexShaderObject, 1, vertexShader.GetShaderSource(), nullptr);
    glCompileShader(m_vertexShaderObject);
    [self CheckObjectStatus:m_vertexShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Vertex Shader"];
    
    m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    ShaderFile fragmentShader("shader.frag");
    glShaderSource(m_fragmentShaderObject, 1, fragmentShader.GetShaderSource(), nullptr);
    glCompileShader(m_fragmentShaderObject);
    [self CheckObjectStatus:m_fragmentShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Fragment Shader"];
    
    m_shaderProgramObject = glCreateProgram();
    glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
    glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::NORMAL, "vNormal");
    glLinkProgram(m_shaderProgramObject);
    [self CheckObjectStatus:m_shaderProgramObject forOperation:GL_LINK_STATUS forObjectName:"Shader Program"];
    
    // get uniform location
    m_ModelMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_model_matrix");
    m_ViewMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_view_matrix");
    m_ProjectionMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_projection_matrix");
    
    m_lightEnableStateUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
    m_lightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_light_position");
    
    m_KaUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ka");
    m_KdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Kd");
    m_KsUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ks");
    m_ShininessUniform = glGetUniformLocation(m_shaderProgramObject, "u_material_shininess");
    
    [self LoadSphereData];
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

- (void) prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    GLint swapInt;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    [self initialize];
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext =(CGLContextObj) [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

- (void) reshape
{
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    if (height == 0.0f)
    {
        height = 1.0f;
    }
    m_winWidth = width;
    m_winHeight = height;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    m_perspectiveProjectionMatrix = glm::perspective(45.0f, width / height, 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    [self drawView];
}

- (void) drawView
{
    [[self openGLContext] makeCurrentContext];
    CGLContextObj contextObj = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLLockContext(contextObj);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(m_shaderProgramObject);
    
    // Draw Sphere
    {
        glm::mat4x4 modelMatrix = glm::translate(glm::mat4x4(), glm::vec3(0.0f, 0.0f, -1.3f));
        glm::mat4x4 viewMatrix;
        
        // calculate perspective projection matrix w.r.t. the x and y pixels
        unsigned int xPixels = m_winWidth / 4;
        unsigned int yPixels = m_winHeight / 6;
        m_perspectiveProjectionMatrix = glm::perspective(45.0f, (GLfloat)xPixels / (GLfloat)yPixels, 0.1f, 100.0f);
        
        glUniformMatrix4fv(m_ModelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));
        glUniformMatrix4fv(m_ViewMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniformMatrix4fv(m_ProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(m_perspectiveProjectionMatrix));
        glUniform1i(m_lightEnableStateUniform, m_LightMode);
        
        if (m_LightMode > 0)
        {
            static GLfloat angle = 0.0f;
            if (m_enableAnimation)
            {
                angle += 0.01f;
            }
            
            GLfloat varying1 = 1000.0f * cos(angle);
            GLfloat varying2 = 1000.0f * sin(angle);
            
            // set light components
            switch (m_rotationAxis)
            {
                case RotationAxis::X:
                    glUniform3f(m_lightPositionUniform, 0.0f, varying1, varying2);
                    break;
                    
                case RotationAxis::Y:
                    glUniform3f(m_lightPositionUniform, varying1, 0.0f, varying2);
                    break;
                    
                case RotationAxis::Z:
                    glUniform3f(m_lightPositionUniform, varying1, varying2, 0.0f);
                    break;
            }
        }
        
        glBindVertexArray(m_vaoSphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::ELEMENT]);
        for (size_t sphereId = 0; sphereId < 24; ++sphereId)
        {
            glViewport(g_spheres[sphereId].colMultiplier * xPixels, g_spheres[sphereId].rowMultiplier * yPixels, xPixels, yPixels);
            
            // set material components
            glUniform3fv(m_KaUniform, 1, g_spheres[sphereId].ambientMaterial);
            glUniform3fv(m_KdUniform, 1, g_spheres[sphereId].diffuseMaterial);
            glUniform3fv(m_KsUniform, 1, g_spheres[sphereId].specularMaterial);
            glUniform1f(m_ShininessUniform, g_spheres[sphereId].shinyness);
            
            glDrawElements(GL_TRIANGLES, m_sphere.getNumberOfSphereElements(), GL_UNSIGNED_SHORT, 0);
        }
        glBindVertexArray(0);
    }
    
    glUseProgram(0);

    CGLFlushDrawable(contextObj);
    CGLUnlockContext(contextObj);
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

- (void) keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: // Esc key
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        case 'L':
        case 'l':
            m_LightMode = (m_LightMode + 1) % 3;
            break;
            
        case 'A':
        case 'a':
            m_enableAnimation = !m_enableAnimation;
            break;
            
        case 'X':
        case 'x':
            m_rotationAxis = RotationAxis::X;
            break;
            
        case 'Y':
        case 'y':
            m_rotationAxis = RotationAxis::Y;
            break;
            
        case 'Z':
        case 'z':
            m_rotationAxis = RotationAxis::Z;
            break;
            
        default:
            break;
    }
}

- (void) mouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) mouseDragged:(NSEvent *)theEvent
{
    // code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) dealloc
{
    if (m_vaoSphere)
    {
        glDeleteVertexArrays(1, &m_vaoSphere);
        m_vaoSphere = 0;
    }
    
    if (m_vboSphere[0] != 0)
    {
        glDeleteBuffers(ATTRIBUTE::LAST, m_vboSphere);
        memset(m_vboSphere, 0, sizeof(m_vboSphere));
    }
    
    if (m_shaderProgramObject)
    {
        glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
        glDeleteProgram(m_shaderProgramObject);
        m_shaderProgramObject = 0;
    }
    
    if (m_fragmentShaderObject)
    {
        glDeleteShader(m_fragmentShaderObject);
        m_fragmentShaderObject = 0;
    }
    
    if (m_vertexShaderObject)
    {
        glDeleteShader(m_vertexShaderObject);
        m_vertexShaderObject = 0;
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return result;
}
