package com.rtr.nmahajan.MaterialSpheres;

/**
 * Created by nmahajan on 15-04-2018.
 */

public final class Material {
    public float ambientMaterial[] = new float[4];
    public float diffuseMaterial[] = new float[4];
    public float specularMaterial[] = new float[4];
    public float shinyness;
    public int rowMultiplier;
    public int colMultiplier;

    Material(float ambientMaterialR, float ambientMaterialG, float ambientMaterialB, float ambientMaterialA,
             float diffusedMaterialR, float diffusedMaterialG, float diffusedMaterialB, float diffusedMaterialA,
             float specularMaterialR, float specularMaterialG, float specularMaterialB, float specularMaterialA,
             float shinyness, int colMultiplier, int rowMultiplier)
    {
        this.ambientMaterial[0] = ambientMaterialR;
        this.ambientMaterial[1] = ambientMaterialG;
        this.ambientMaterial[2] = ambientMaterialB;
        this.ambientMaterial[3] = ambientMaterialA;

        this.diffuseMaterial[0] = diffusedMaterialR;
        this.diffuseMaterial[1] = diffusedMaterialG;
        this.diffuseMaterial[2] = diffusedMaterialB;
        this.diffuseMaterial[3] = diffusedMaterialA;

        this.specularMaterial[0] = specularMaterialR;
        this.specularMaterial[1] = specularMaterialG;
        this.specularMaterial[2] = specularMaterialB;
        this.specularMaterial[3] = specularMaterialA;

        this.shinyness = shinyness;
        this.rowMultiplier = rowMultiplier;
        this.colMultiplier = colMultiplier;
    }
}

