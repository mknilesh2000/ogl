#pragma once

#include "TestRenderer.h"
#include <glm/gtc/type_ptr.hpp>
#include <Utils.h>
#include <Sphere.h>
#include "Material.h"

namespace Features
{
    using namespace Interfaces;

#define EXIT_IF_ERROR(x) if (x == GL_FALSE) { __debugbreak(); return RENDERER_RESULT_ERROR; }
#define CALL_AND_RESET_IF_VALID(x, y) if (x != NULL) { y;  x = NULL; }

    TestRenderer::TestRenderer() :
        m_fp(nullptr),
        m_vertexShaderObject(0),
        m_fragmentShaderObject(0),
        m_shaderProgramObject(0),
        m_vaoSphere(0),
        m_ModelMatrixUniform(0),
        m_ViewMatrixUniform(0),
        m_ProjectionMatrixUniform(0),
        m_lightEnableStateUniform(0),
        m_lightPositionUniform(0),
        m_KaUniform(0),
        m_KdUniform(0),
        m_KsUniform(0),
        m_ShininessUniform(0),
        m_enableAnimation(false),
        m_LightMode(0),
        m_winWidth(0),
        m_winHeight(0),
        m_rotationAxis(RotationAxis::X)
    {
        memset(m_vboSphere, 0, sizeof(m_vboSphere));
        fopen_s(&m_fp, "output.log", "w+");
        if (m_fp == nullptr)
            __debugbreak();
        fprintf(m_fp, "Opened log-file\n");
    }

    TestRenderer::~TestRenderer()
    {
        if (m_fp != nullptr)
        {
            fprintf(m_fp, "Closing log-file\n");
            fclose(m_fp);
            m_fp = nullptr;
        }
    }

    /// Method for retrieving name of the renderer
    /// @param rendererName buffer to be filled with the renderer name
    /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
    ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
    const char* TestRenderer::GetName()
    {
        static const char name[] = "TestRenderer";
        return name;
    }

    void TestRenderer::LoadSphereData()
    {
        float sphereVertices[1146];
        float sphereNormals[1146];
        float sphere_textures[764];
        unsigned short sphereElements[2280];

        getSphereVertexData(sphereVertices, sphereNormals, sphere_textures, sphereElements);

        // generate and start filling list
        glGenVertexArrays(1, &m_vaoSphere);
        glBindVertexArray(m_vaoSphere);

        // prepare array buffer
        glGenBuffers(ATTRIBUTE::LAST, m_vboSphere);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::VERTEX]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphereVertices), sphereVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::NORMAL]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphereNormals), sphereNormals, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::ELEMENT]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphereElements), sphereElements, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // reset array buffer and vertex array
        glBindVertexArray(0);
    }

    /// Method for performing one-time renderer initialization.
    /// Renderer can initialize global/static instances as part of this method
    /// @param window identifier of the window where drawing is directed
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::Initialize(Window window)
    {
        // vertex shader
        m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        ShaderFile vertexShader("./shaders/TestRenderer/shader.vert");
        glShaderSource(m_vertexShaderObject, 1, vertexShader.GetShaderSource(), nullptr);
        glCompileShader(m_vertexShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_vertexShaderObject, GL_COMPILE_STATUS, "m_vertexShaderObject"));

        // fragment shader
        m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        ShaderFile fragmentShader("./shaders/TestRenderer/shader.frag");
        glShaderSource(m_fragmentShaderObject, 1, fragmentShader.GetShaderSource(), nullptr);

        glCompileShader(m_fragmentShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_fragmentShaderObject, GL_COMPILE_STATUS, "m_fragmentShaderObject"));

        // shader program
        m_shaderProgramObject = glCreateProgram();
        glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
        glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
        glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::NORMAL, "vNormal");
        glLinkProgram(m_shaderProgramObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_shaderProgramObject, GL_LINK_STATUS, "m_shaderProgramObject"));

        // get uniform location
        m_ModelMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_model_matrix");
        m_ViewMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_view_matrix");
        m_ProjectionMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_projection_matrix");

        m_lightEnableStateUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
        m_lightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_light_position");

        m_KaUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ka");
        m_KdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Kd");
        m_KsUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ks");
        m_ShininessUniform = glGetUniformLocation(m_shaderProgramObject, "u_material_shininess");

        LoadSphereData();

        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing one-time renderer un-initialization before it is unloaded
    /// Renderer can perform global cleanup as part of this method
    void TestRenderer::Uninitialize(void)
    {
        CALL_AND_RESET_IF_VALID(m_vaoSphere, glDeleteVertexArrays(1, &m_vaoSphere));
        glDeleteBuffers(1, m_vboSphere);
        CALL_AND_RESET_IF_VALID(m_vertexShaderObject, glDetachShader(m_shaderProgramObject, m_vertexShaderObject));
        CALL_AND_RESET_IF_VALID(m_fragmentShaderObject, glDetachShader(m_shaderProgramObject, m_fragmentShaderObject));
        CALL_AND_RESET_IF_VALID(m_shaderProgramObject, glDeleteProgram(m_shaderProgramObject));
        glUseProgram(0);
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host before rendering a scene to the active renderer.
    /// Renderer should do initialization of scene specific things as part of this method
    /// @param scene Identifier of a scene to be initialized
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::InitializeScene(SceneType scene)
    {
        // general initializations
        glShadeModel(GL_SMOOTH);
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_CULL_FACE);
        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host after rendering a scene to the active renderer
    /// Renderer should do cleanup of scene specific things done as part of scene initialize.
    /// @param scene Identifier of a scene to be cleaned-up
    void TestRenderer::UninitializeScene(SceneType scene)
    {
    }

    /// Method for rendering a frame in a scene
    /// This method will be called by the host per frame of a scene only to the active renderer
    /// @param params describes the parameters curresponding to this render
    /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
    ///         RENDERER_RESULT_ERROR if failed in building the frame
    ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
    ///                                   in such cases no further frame calls would be made for this scene
    ///                                   to the renderer.
    RendererResult TestRenderer::Render(const RenderParams &params)
    {
        RendererResult result = RENDERER_RESULT_FINISHED;

        // render for 5 secs before transitioning to next scene
        if (params.scene == SCENE_TYPE_TEST0)
        {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glUseProgram(m_shaderProgramObject);

            // Draw Sphere
            {
                glm::mat4x4 modelMatrix = glm::translate(glm::mat4x4(), glm::vec3(0.0f, 0.0f, -1.3f));
                glm::mat4x4 viewMatrix;

                // calculate perspective projection matrix w.r.t. the x and y pixels
                unsigned int xPixels = m_winWidth / 4;
                unsigned int yPixels = m_winHeight / 6;
                m_perspectiveProjectionMatrix = glm::perspective(45.0f, (GLfloat)xPixels / (GLfloat)yPixels, 0.1f, 100.0f);

                glUniformMatrix4fv(m_ModelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));
                glUniformMatrix4fv(m_ViewMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewMatrix));
                glUniformMatrix4fv(m_ProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(m_perspectiveProjectionMatrix));
                glUniform1i(m_lightEnableStateUniform, m_LightMode);

                if (m_LightMode > 0)
                {
                    static GLfloat angle = 0.0f;
                    if (m_enableAnimation)
                    {
                        angle += 0.1f;
                    }

                    GLfloat varying1 = 1000.0f * cos(angle);
                    GLfloat varying2 = 1000.0f * sin(angle);

                    // set light components
                    switch (m_rotationAxis)
                    {
                    case RotationAxis::X:
                        glUniform3f(m_lightPositionUniform, 0.0f, varying1, varying2);
                        break;

                    case RotationAxis::Y:
                        glUniform3f(m_lightPositionUniform, varying1, 0.0f, varying2);
                        break;

                    case RotationAxis::Z:
                        glUniform3f(m_lightPositionUniform, varying1, varying2, 0.0f);
                        break;
                    }
                }

                glBindVertexArray(m_vaoSphere);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::ELEMENT]);
                    for (size_t sphereId = 0; sphereId < _countof(g_spheres); ++sphereId)
                    {
                        glViewport(g_spheres[sphereId].colMultiplier * xPixels, g_spheres[sphereId].rowMultiplier * yPixels, xPixels, yPixels);

                        // set material components
                        glUniform3fv(m_KaUniform, 1, g_spheres[sphereId].ambientMaterial);
                        glUniform3fv(m_KdUniform, 1, g_spheres[sphereId].diffuseMaterial);
                        glUniform3fv(m_KsUniform, 1, g_spheres[sphereId].specularMaterial);
                        glUniform1f(m_ShininessUniform, g_spheres[sphereId].shinyness);

                        glDrawElements(GL_TRIANGLES, getNumberOfSphereElements(), GL_UNSIGNED_SHORT, 0);
                    }
                glBindVertexArray(0);
            }

            glUseProgram(0);

            result = RENDERER_RESULT_SUCCESS;
        }
        return result;
    }

    /// Generic method to notify active renderer about a message posted to host window. The message can be
    /// from system or initiated by the host itself.
    /// @param message OS dependent structure that describes the system message being processed.
    void TestRenderer::OnMessage(const Message &message)
    {
        switch (message.messageId)
        {
        case WM_CHAR:
            switch (message.wParam)
            {
            case 'L':
            case 'l':
                m_LightMode = (m_LightMode + 1) % 3;
                break;

            case 'A':
            case 'a':
                m_enableAnimation = !m_enableAnimation;
                break;

            case 'X':
            case 'x':
                m_rotationAxis = RotationAxis::X;
                break;

            case 'Y':
            case 'y':
                m_rotationAxis = RotationAxis::Y;
                break;

            case 'Z':
            case 'z':
                m_rotationAxis = RotationAxis::Z;
                break;
            }
            break;
        }
    }

    /// Generic method to notify active renderer about the change in the dimensions of the host window
    /// @param width New width of the window
    /// @param height New height of the window
    void TestRenderer::OnResize(unsigned int width, unsigned int height)
    {
        if (height == 0)
            height = 1;
        m_winWidth = width;
        m_winHeight = height;
    }

    GLint TestRenderer::CheckObjectStatus(GLuint object, GLenum operation, const char *objectName)
    {
        GLint status = GL_FALSE;
        bool isLinkOperation = operation == GL_LINK_STATUS;
        isLinkOperation ? glGetProgramiv(object, operation, &status) :
            glGetShaderiv(object, operation, &status);

        if (status == GL_FALSE)
        {
            fprintf(m_fp, "%s failed\n", objectName);
            GLint shaderInfoLogLength = 0;
            isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength) :
                glGetShaderiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);
            if (shaderInfoLogLength > 0)
            {
                GLchar *szInfoLog = new GLchar[shaderInfoLogLength];
                if (szInfoLog != nullptr)
                {
                    memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                    GLsizei written = 0;
                    isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                        glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                    fprintf(m_fp, "%s info Log :\n %s", objectName, szInfoLog);
                    fflush(m_fp);
                    delete[]szInfoLog;
                }
            }
        }
        return status;
    }
}
