cbuffer ConstantBuffer
{
    float4x4 world_matrix;
    float4x4 view_matrix;
    float4x4 projection_matrix;

    uint light_mode;

    float4 La;
    float4 Ld;
    float4 Ls;
    float4 light_position;

    float4 Ka;
    float4 Kd;
    float4 Ks;
    float material_shininess;
};

struct vertex_shader_output
{
    float4 phong_ads_color : VOUT1;
    float3 transformed_normals : VOUT2;
    float3 light_direction : VOUT3;
    float3 viewer_vector : VOUT6;
    float4 position : SV_POSITION;
};

vertex_shader_output main(float3 vPosition : POSITION, float3 vNormal : NORMAL)
{
    vertex_shader_output vsout;
    if (light_mode == 1 || light_mode == 2)
    {
        // per-vertex lighting
        float4 eye_coordinates = mul(view_matrix, mul(world_matrix, float4(vPosition, 1.0)));
        vsout.transformed_normals = normalize(mul((float3x3)(mul(view_matrix, world_matrix)), vNormal));
        vsout.viewer_vector = normalize(-eye_coordinates.xyz);

        // calculate  light components
        vsout.light_direction = normalize((float3)(light_position) - eye_coordinates.xyz);

        // calculate effective light components
        float4 ambient = La * Ka;

        float tn_dot_ld = max(dot(vsout.transformed_normals, vsout.light_direction), 0.0);
        float4 diffuse = Ld * Kd * tn_dot_ld;

        float3 reflection_vector = reflect(-vsout.light_direction, vsout.transformed_normals);
        float4 specular = Ls * Ks * pow(max(dot(reflection_vector, vsout.viewer_vector), 0.0), material_shininess);

        vsout.phong_ads_color = ambient + diffuse + specular;
    }
    else
    {
        vsout.phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);
    }

    vsout.position = mul(projection_matrix, mul(view_matrix, mul(world_matrix, float4(vPosition, 1.0))));
    return vsout;
}