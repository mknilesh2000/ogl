//
//  GLView.m
//  OGLWindow
//
//  Created by Nilesh Mahajan on 30/07/18.
//  Copyright © 2018 Nilesh Mahajan. All rights reserved.
//

#import "GLView.h"
#import <CoreVideo/CVDisplayLink.h>
#import <glm/gtx/transform.hpp>
#import <glm/gtc/type_ptr.hpp>

extern FILE* gpFile;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext);

enum ATTRIBUTE
{
    VERTEX = 0,
    TEXTURE0,
    COLOR,
    NORMAL
};

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    // Draw Members
    GLuint m_vertexShaderObject;
    GLuint m_fragmentShaderObject;
    GLuint m_shaderProgramObject;
    GLuint m_vaoSquare;
    GLuint m_vboSquare[4];
    GLuint m_MVPuniform;
    glm::mat4x4 m_perspectiveProjectionMatrix;
    
    GLuint m_textureSquare;
    GLuint m_TexSampler2DUniform;
    GLfloat m_squareTexCords[8];
}

- (id) initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    if (self != NULL)
    {
        [[self window] setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            // Specify a display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0       // last 0 is a must
        };
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if (pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format available. Exitting... ");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; // this will automatically releases the older context, if present and sets the newer one
    }
    return (self);
}

- (CVReturn) getFrameForTime:(const CVTimeStamp*)pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [self drawView];
    [pool release];
    return kCVReturnSuccess;
}

- (GLint) CheckObjectStatus:(GLuint)object forOperation:(GLenum)operation forObjectName:(const char *)objectName
{
    GLint status = GL_FALSE;
    bool isLinkOperation = operation == GL_LINK_STATUS;
    isLinkOperation ? glGetProgramiv(object, operation, &status) :
    glGetShaderiv(object, operation, &status);
    
    if (status == GL_FALSE)
    {
        assert(0);
        fprintf(gpFile, "%s failed\n", objectName);
        GLint shaderInfoLogLength = 0;
        isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &status) :
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &status);
        if (shaderInfoLogLength > 0)
        {
            GLchar *szInfoLog = (GLchar*) malloc(shaderInfoLogLength * sizeof(GLchar));
            if (szInfoLog != NULL)
            {
                memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                GLsizei written = 0;
                isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                fprintf(gpFile, "%s info Log :\n %s", objectName, szInfoLog);
                free(szInfoLog);
            }
        }
    }
    return status;
}

- (void) LoadSquareData
{
    const GLfloat squareVertices[] =
    {
        0.5f, 0.5f, 0.0f,
        -0.5f, 0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
    };
    
    // load default tex-coords
    m_squareTexCords[0] = 1.0f;  m_squareTexCords[1] = 0.0f;
    m_squareTexCords[2] = 0.0f;  m_squareTexCords[3] = 0.0f;
    m_squareTexCords[4] = 0.0f;  m_squareTexCords[5] = 1.0f;
    m_squareTexCords[6] = 1.0f;  m_squareTexCords[7] = 1.0f;
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoSquare);
    glBindVertexArray(m_vaoSquare);
    
    // prepare array buffer
    glGenBuffers(2, m_vboSquare);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(m_squareTexCords), m_squareTexCords, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::TEXTURE0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
    
    m_textureSquare = [self LoadTextureFromBMPResourceFile:"Smiley.bmp"];
}

- (GLuint) LoadTextureFromBMPResourceFile:(const char *)texFileName
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *textureFileNameWithPath=[NSString stringWithFormat:@"%@/Contents/Resources/%s", appDirName, texFileName];
    
    NSImage *bmpImage=[[NSImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if (!bmpImage)
    {
        NSLog(@"can't find %@", textureFileNameWithPath);
        return(0);
    }
    
    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];
    
    int w = (int)CGImageGetWidth(cgImage);
    int h = (int)CGImageGetHeight(cgImage);
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    void* pixels = (void *)CFDataGetBytePtr(imageData);
    
    GLuint bmpTexture;
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4, for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    // Create mipmaps for this texture for better image quality
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    return(bmpTexture);
}

- (void) initialize
{
    m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSource =
        "#version 410 core\n"                                       \
        "in vec4 vPosition;"                                        \
        "in vec2 vTex0Coord;"                                       \
        "out vec2 vTex0Coord2FS;"                                   \
        "uniform mat4 u_mvp_matrix;"                                \
        "void main(void)"                                           \
        "{"                                                         \
        "   gl_Position = u_mvp_matrix * vPosition;"                \
        "   vTex0Coord2FS = vTex0Coord;"                            \
        "}";
    
    glShaderSource(m_vertexShaderObject, 1, &vertexShaderSource, NULL);
    glCompileShader(m_vertexShaderObject);
    [self CheckObjectStatus:m_vertexShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Vertex Shader"];
    
    m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentShaderSource =
    "#version 410 core\n"                           \
    "in vec2 vTex0Coord2FS;"                                    \
    "out vec4 FragColor;"                                       \
    "uniform sampler2D u_texture0_sampler;"                     \
    "void main(void)"                                           \
    "{"                                                         \
    "   FragColor = texture(u_texture0_sampler, vTex0Coord2FS);" \
    "}";
    glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSource, NULL);
    glCompileShader(m_fragmentShaderObject);
    [self CheckObjectStatus:m_fragmentShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Fragment Shader"];
    
    m_shaderProgramObject = glCreateProgram();
    glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
    glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::TEXTURE0, "vTex0Coord");
    glLinkProgram(m_shaderProgramObject);
    [self CheckObjectStatus:m_shaderProgramObject forOperation:GL_LINK_STATUS forObjectName:"Shader Program"];
    
    m_MVPuniform = glGetUniformLocation(m_shaderProgramObject, "u_mvp_matrix");
    m_TexSampler2DUniform = glGetUniformLocation(m_shaderProgramObject, "u_texture0_sampler");
    
    [self LoadSquareData];
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

- (void) prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    GLint swapInt;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    [self initialize];
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext =(CGLContextObj) [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

- (void) reshape
{
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    if (height == 0.0f)
    {
        height = 1.0f;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    m_perspectiveProjectionMatrix = glm::perspective(45.0f, width / height, 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    [self drawView];
}

- (void) drawView
{
    [[self openGLContext] makeCurrentContext];
    CGLContextObj contextObj = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLLockContext(contextObj);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(m_shaderProgramObject);
    
    // Draw Square
    {
        glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(0.0f, 0.0f, -1.0f));
        glm::mat4x4 modelViewProjectionMatrix =
        m_perspectiveProjectionMatrix * translationMatrix;
        
        glUniformMatrix4fv(m_MVPuniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
        
        // bind with square texture
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_textureSquare);
        glUniform1i(m_TexSampler2DUniform, 0);
        
        glBindVertexArray(m_vaoSquare);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
    }
    
    glUseProgram(0);

    CGLFlushDrawable(contextObj);
    CGLUnlockContext(contextObj);
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

- (void) keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: // Esc key
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
            // original smiley
        case '0':
        case '4':
            glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[1]);
            m_squareTexCords[0] = 1.0f;  m_squareTexCords[1] = 0.0f;
            m_squareTexCords[2] = 0.0f;  m_squareTexCords[3] = 0.0f;
            m_squareTexCords[4] = 0.0f;  m_squareTexCords[5] = 1.0f;
            m_squareTexCords[6] = 1.0f;  m_squareTexCords[7] = 1.0f;
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(m_squareTexCords), m_squareTexCords);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            break;
            
            // quarter smiley
        case '1':
            glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[1]);
            m_squareTexCords[0] = 0.5f;  m_squareTexCords[1] = 0.0f;
            m_squareTexCords[2] = 0.0f;  m_squareTexCords[3] = 0.0f;
            m_squareTexCords[4] = 0.0f;  m_squareTexCords[5] = 0.5f;
            m_squareTexCords[6] = 0.5f;  m_squareTexCords[7] = 0.5f;
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(m_squareTexCords), m_squareTexCords);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            break;
            
            // 4-smileys
        case '2':
            glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[1]);
            m_squareTexCords[0] = 2.0f;  m_squareTexCords[1] = 0.0f;
            m_squareTexCords[2] = 0.0f;  m_squareTexCords[3] = 0.0f;
            m_squareTexCords[4] = 0.0f;  m_squareTexCords[5] = 2.0f;
            m_squareTexCords[6] = 2.0f;  m_squareTexCords[7] = 2.0f;
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(m_squareTexCords), m_squareTexCords);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            break;
            
            // center texel
        case '3':
            glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[1]);
            m_squareTexCords[0] = 0.5f;  m_squareTexCords[1] = 0.5f;
            m_squareTexCords[2] = 0.5f;  m_squareTexCords[3] = 0.5f;
            m_squareTexCords[4] = 0.5f;  m_squareTexCords[5] = 0.5f;
            m_squareTexCords[6] = 0.5f;  m_squareTexCords[7] = 0.5f;
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(m_squareTexCords), m_squareTexCords);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            break;
            
        default:
            break;
    }
}

- (void) mouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) mouseDragged:(NSEvent *)theEvent
{
    // code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) dealloc
{
    if (m_vaoSquare)
    {
        glDeleteVertexArrays(1, &m_vaoSquare);
        m_vaoSquare = 0;
    }
    
    if (m_vboSquare[0] != 0)
    {
        glDeleteBuffers(4, m_vboSquare);
        memset(m_vboSquare, 0, sizeof(m_vboSquare));
    }
    
    if (m_shaderProgramObject)
    {
        glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
        glDeleteProgram(m_shaderProgramObject);
        m_shaderProgramObject = 0;
    }
    
    if (m_fragmentShaderObject)
    {
        glDeleteShader(m_fragmentShaderObject);
        m_fragmentShaderObject = 0;
    }
    
    if (m_vertexShaderObject)
    {
        glDeleteShader(m_vertexShaderObject);
        m_vertexShaderObject = 0;
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return result;
}
