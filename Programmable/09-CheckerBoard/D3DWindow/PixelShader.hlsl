struct vertex_shader_output
{
   float4 position: SV_POSITION;
};

float modfunc(float x, float y)
{
    return (x - y * floor(x / y));
}

float4 main(vertex_shader_output vsoutput) : SV_TARGET
{
   float4 outcolor;

   // convert to 0-1 range\n
   float x = 0.5 * (vsoutput.position[0] + 1.0);
   float y = 0.5 * (vsoutput.position[1] + 1.0);
   
   if ((modfunc(4.0 * x, 1.0) < 0.5) || (modfunc(4.0 * y, 1.0) < 0.5))
   {
       outcolor = float4(0.0, 0.0, 0.0, 1.0);
   }
   else
   {
       outcolor = float4(1.0, 1.0, 1.0, 0.0);
   }

   return outcolor;
}
