cbuffer ConstantBuffer
{
   float4x4 worldViewProjectionMatrix;
};

struct vertex_shader_output
{
   float4 position: SV_POSITION;
};

vertex_shader_output main(float4 pos : POSITION)
{
   float4 position = mul(worldViewProjectionMatrix, pos);
   vertex_shader_output vsout;
   vsout.position = position;
   return vsout;
}