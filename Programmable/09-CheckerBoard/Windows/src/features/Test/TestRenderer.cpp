#pragma once

#include "TestRenderer.h"
#include <glm/gtc/type_ptr.hpp>

namespace Features
{
    enum ATTRIBUTE
    {
        VERTEX = 0,
        COLOR,
        NORMAL,
        TEXTURE0
    };

    using namespace Interfaces;

#define EXIT_IF_ERROR(x) if (x == GL_FALSE) { __debugbreak(); return RENDERER_RESULT_ERROR; }
#define CALL_AND_RESET_IF_VALID(x, y) if (x != NULL) { y;  x = NULL; }

    TestRenderer::TestRenderer() :
        m_fp(nullptr),
        m_vertexShaderObject(0),
        m_fragmentShaderObject(0),
        m_shaderProgramObject(0),
        m_vaoSquare(0)
    {
        memset(m_vboSquare, 0, sizeof(m_vboSquare));
        fopen_s(&m_fp, "output.log", "w+");
        if (m_fp == nullptr)
            __debugbreak();
        fprintf(m_fp, "Opened log-file\n");
    }

    TestRenderer::~TestRenderer()
    {
        if (m_fp != nullptr)
        {
            fprintf(m_fp, "Closing log-file\n");
            fclose(m_fp);
            m_fp = nullptr;
        }
    }

    /// Method for retrieving name of the renderer
    /// @param rendererName buffer to be filled with the renderer name
    /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
    ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
    const char* TestRenderer::GetName()
    {
        static const char name[] = "TestRenderer";
        return name;
    }

    void TestRenderer::LoadSquareData()
    {
        const GLfloat squareVertices[] =
        {
            1.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
        };

        // generate and start filling list
        glGenVertexArrays(1, &m_vaoSquare);
        glBindVertexArray(m_vaoSquare);

        // prepare array buffer
        glGenBuffers(4, m_vboSquare);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // reset array buffer and vertex array
        glBindVertexArray(0);
    }

    /// Method for performing one-time renderer initialization.
    /// Renderer can initialize global/static instances as part of this method
    /// @param window identifier of the window where drawing is directed
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::Initialize(Window window)
    {
        // vertex shader
        m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexShaderSourceCode =
            "#version 450 core"                                         \
            "\n"                                                        \
            "in vec4 vPosition;"                                        \
            "out vec4 vPosition2FS;"                                    \
            "uniform mat4 u_mvp_matrix;"                                \
            "void main(void)"                                           \
            "{"                                                         \
            "   gl_Position = u_mvp_matrix * vPosition;"                \
            "   vPosition2FS = vPosition;"                              \
            "}";
        glShaderSource(m_vertexShaderObject, 1, &vertexShaderSourceCode, nullptr);
        glCompileShader(m_vertexShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_vertexShaderObject, GL_COMPILE_STATUS, "m_vertexShaderObject"));

        // fragment shader
        m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
            "#version 450 core"                                             \
            "\n"                                                            \
            "out vec4 FragColor;"                                           \
            "in vec4 vPosition2FS;"                                         \
            "void main(void)"                                               \
            "{"                                                             \
            "   // convert to 0-1 range\n"                                  \
            "   float x = 0.5 * (vPosition2FS.x + 1.0);"                    \
            "   float y = 0.5 * (vPosition2FS.y + 1.0);"                    \
            "   if ((mod(4.0 * x, 1.0) < 0.5) ^^ (mod(4.0 * y, 1.0) < 0.5))"\
            "       FragColor = vec4(0.0, 0.0, 0.0, 1.0);"                  \
            "   else"                                                       \
            "       FragColor = vec4(1.0, 1.0, 1.0, 0.0);"                  \
            "}";
        glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSourceCode, nullptr);
        glCompileShader(m_fragmentShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_fragmentShaderObject, GL_COMPILE_STATUS, "m_fragmentShaderObject"));

        // shader program
        m_shaderProgramObject = glCreateProgram();
        glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
        glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
        glLinkProgram(m_shaderProgramObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_shaderProgramObject, GL_LINK_STATUS, "m_shaderProgramObject"));

        // get uniform location
        m_MVPuniform = glGetUniformLocation(m_shaderProgramObject, "u_mvp_matrix");

        LoadSquareData();

        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing one-time renderer un-initialization before it is unloaded
    /// Renderer can perform global cleanup as part of this method
    void TestRenderer::Uninitialize(void)
    {
        CALL_AND_RESET_IF_VALID(m_vaoSquare, glDeleteVertexArrays(1, &m_vaoSquare));
        glDeleteBuffers(1, m_vboSquare);
        CALL_AND_RESET_IF_VALID(m_vertexShaderObject, glDetachShader(m_shaderProgramObject, m_vertexShaderObject));
        CALL_AND_RESET_IF_VALID(m_fragmentShaderObject, glDetachShader(m_shaderProgramObject, m_fragmentShaderObject));
        CALL_AND_RESET_IF_VALID(m_shaderProgramObject, glDeleteProgram(m_shaderProgramObject));
        glUseProgram(0);
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host before rendering a scene to the active renderer.
    /// Renderer should do initialization of scene specific things as part of this method
    /// @param scene Identifier of a scene to be initialized
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::InitializeScene(SceneType scene)
    {
        // general initializations
        glShadeModel(GL_SMOOTH);
        glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host after rendering a scene to the active renderer
    /// Renderer should do cleanup of scene specific things done as part of scene initialize.
    /// @param scene Identifier of a scene to be cleaned-up
    void TestRenderer::UninitializeScene(SceneType scene)
    {
    }

    /// Method for rendering a frame in a scene
    /// This method will be called by the host per frame of a scene only to the active renderer
    /// @param params describes the parameters curresponding to this render
    /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
    ///         RENDERER_RESULT_ERROR if failed in building the frame
    ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
    ///                                   in such cases no further frame calls would be made for this scene
    ///                                   to the renderer.
    RendererResult TestRenderer::Render(const RenderParams &params)
    {
        RendererResult result = RENDERER_RESULT_FINISHED;

        // render for 5 secs before transitioning to next scene
        if (params.scene == SCENE_TYPE_TEST0)
        {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glUseProgram(m_shaderProgramObject);
            // Draw Square
            {
                glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(-1.0f, 0.0f, -3.0f));
                glm::mat4x4 modelViewProjectionMatrix =
                    m_perspectiveProjectionMatrix * translationMatrix;

                glUniformMatrix4fv(m_MVPuniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
                glBindVertexArray(m_vaoSquare);
                glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
                glBindVertexArray(0);
            }

            // Draw tilted square
            {
                glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(1.0f, 0.0f, -3.0f));
                glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(), 30.0f, glm::vec3(0.0f, 1.0f, 0.0f));

                glm::mat4x4 modelViewProjectionMatrix =
                    m_perspectiveProjectionMatrix * translationMatrix * rotationMatrix;

                glUniformMatrix4fv(m_MVPuniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
                glBindVertexArray(m_vaoSquare);
                glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
                glBindVertexArray(0);
            }

            glUseProgram(0);

            result = RENDERER_RESULT_SUCCESS;
        }
        return result;
    }

    /// Generic method to notify active renderer about a message posted to host window. The message can be
    /// from system or initiated by the host itself.
    /// @param message OS dependent structure that describes the system message being processed.
    void TestRenderer::OnMessage(const Message &message)
    {
    }

    /// Generic method to notify active renderer about the change in the dimensions of the host window
    /// @param width New width of the window
    /// @param height New height of the window
    void TestRenderer::OnResize(unsigned int width, unsigned int height)
    {
        if (height == 0)
            height = 1;
        glViewport(0, 0, (GLsizei)width, (GLsizei)height);
        m_perspectiveProjectionMatrix = glm::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    }

    GLint TestRenderer::CheckObjectStatus(GLuint object, GLenum operation, const char *objectName)
    {
        GLint status = GL_FALSE;
        bool isLinkOperation = operation == GL_LINK_STATUS;
        isLinkOperation ? glGetProgramiv(object, operation, &status) :
            glGetShaderiv(object, operation, &status);

        if (status == GL_FALSE)
        {
            fprintf(m_fp, "%s failed\n", objectName);
            GLint shaderInfoLogLength = 0;
            isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &status) :
                glGetShaderiv(object, GL_INFO_LOG_LENGTH, &status);
            if (shaderInfoLogLength > 0)
            {
                GLchar *szInfoLog = new GLchar[shaderInfoLogLength];
                if (szInfoLog != nullptr)
                {
                    memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                    GLsizei written = 0;
                    isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                        glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                    fprintf(m_fp, "%s info Log :\n %s", objectName, szInfoLog);
                    delete[]szInfoLog;
                }
            }
        }
        return status;
    }
}
