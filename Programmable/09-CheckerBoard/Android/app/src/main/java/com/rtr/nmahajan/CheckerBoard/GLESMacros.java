package com.rtr.nmahajan.CheckerBoard;

/**
 * Created by nmahajan on 28-01-2018.
 */

public class GLESMacros
{
    // attribute index
    public static final int ATTRIBUTE_VERTEX = 0;
    public static final int ATTRIBUTE_COLOR = 1;
    public static final int ATTRIBUTE_NORMAL = 2;
    public static final int ATTRIBUTE_TEXTURE0 = 3;
}
