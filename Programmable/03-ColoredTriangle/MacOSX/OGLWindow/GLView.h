//
//  GLView.h
//  OGLWindow
//
//  Created by Nilesh Mahajan on 30/07/18.
//  Copyright © 2018 Nilesh Mahajan. All rights reserved.
//

#ifndef GLView_h
#define GLView_h

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

@interface GLView : NSOpenGLView
@end

#endif /* GLView_h */
