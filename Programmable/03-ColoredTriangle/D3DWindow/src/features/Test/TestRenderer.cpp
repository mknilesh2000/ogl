#pragma once

#include "TestRenderer.h"

namespace Features
{
    using namespace Interfaces;

#define EXIT_IF_ERROR(x) if (x == GL_FALSE) { __debugbreak(); return RENDERER_RESULT_ERROR; }
#define CALL_AND_RESET_IF_VALID(x, y) if (x != NULL) { y;  x = NULL; }

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

    TestRenderer::TestRenderer() :
        m_fp(nullptr),
        m_pIDxgiSwapChain(nullptr),
        m_pID3dDevice(nullptr),
        m_pID3dDeviceContext(nullptr),
        m_pID3d11RenderTargetView(nullptr),
        m_pID3d11VertexShader(nullptr),
        m_pID3d11PixelShader(nullptr),
        m_pID3d11Buffer_VertexBuffer{},
        m_pID3d11InputLayout(nullptr),
        m_pID3d11Buffer_ConstantBuffer(nullptr),
        m_projectionMatrix{},
        m_clearColor{0.0f, 0.0f, 0.0f, 1.0f}
    {
        fopen_s(&m_fp, "output.log", "w+");
        if (m_fp == nullptr)
            __debugbreak();
        fprintf(m_fp, "Opened log-file\n");
    }

    void TestRenderer::RefreshLogFile()
    {
        if (m_fp != nullptr)
        {
            fclose(m_fp);
            fopen_s(&m_fp, "output.log", "a+");
            if (m_fp == nullptr)
                __debugbreak();
        }
    }

    TestRenderer::~TestRenderer()
    {
        if (m_fp != nullptr)
        {
            fprintf(m_fp, "Closing log-file\n");
            fclose(m_fp);
            m_fp = nullptr;
        }
    }

    /// Method for retrieving name of the renderer
    /// @param rendererName buffer to be filled with the renderer name
    /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
    ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
    const char* TestRenderer::GetName()
    {
        static const char name[] = "TestRenderer";
        return name;
    }

    HRESULT TestRenderer::CreateVertexShader()
    {
        const char* vertexShaderSourceCode =
            "cbuffer ConstantBuffer"
            "{"
            "   float4x4 worldViewProjectionMatrix;"
            "};"
            "struct vertex_shader_output"
            "{"
            "   float4 position: SV_POSITION;"
            "   float4 color: COLOR;"
            "};"
            "vertex_shader_output main(float4 pos : POSITION, float4 col : COLOR)"
            "{"
            "   float4 position = mul(worldViewProjectionMatrix, pos);"
            "   vertex_shader_output vsout;"
            "   vsout.position = position;"
            "   vsout.color = col;"
            "   return vsout;"
            "}";
        ID3DBlob* pID3DBlob_Error = nullptr;
        HRESULT hr = D3DCompile(vertexShaderSourceCode,
            lstrlenA(vertexShaderSourceCode) + 1,
            "VS",
            nullptr,
            D3D_COMPILE_STANDARD_FILE_INCLUDE,
            "main",
            "vs_5_0",
            0,
            0,
            &m_pID3DBlobVertexShader,
            &pID3DBlob_Error);
        fprintf_s(m_fp, "D3DCompile for Vertex Shader returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        if (FAILED(hr))
        {
            fprintf_s(m_fp, "D3DCompile for Vertex Shader log %s", (char*)pID3DBlob_Error->GetBufferPointer());
        }
        RefreshLogFile();

        if (SUCCEEDED(hr))
        {
            hr = m_pID3dDevice->CreateVertexShader(m_pID3DBlobVertexShader->GetBufferPointer(),
                m_pID3DBlobVertexShader->GetBufferSize(),
                nullptr,
                &m_pID3d11VertexShader);
            fprintf_s(m_fp, "CreateVertexShader returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
            RefreshLogFile();

            if (SUCCEEDED(hr))
            {
                m_pID3dDeviceContext->VSSetShader(m_pID3d11VertexShader, nullptr, 0);
            }
            else
            {
                if (pID3DBlob_Error != nullptr)
                {
                    fprintf_s(m_fp, "D3DCompile failed for Vertex SHader : %s \n", (char*)pID3DBlob_Error->GetBufferPointer());
                    RefreshLogFile();
                    pID3DBlob_Error->Release();
                }
            }
        }
        return hr;
    }

    HRESULT TestRenderer::CreatePixelShader()
    {
        const char* pixelShaderSource =
            "struct vertex_shader_output"
            "{"
            "   float4 position: SV_POSITION;"
            "   float4 color: COLOR;"
            "};"
            "float4 main(vertex_shader_output vsoutput) : SV_TARGET"
            "{"
            "   return vsoutput.color;"
            "}";
        ID3DBlob* pID3DBlob_Error = nullptr;
        HRESULT hr = D3DCompile(pixelShaderSource,
            lstrlenA(pixelShaderSource),
            "PS",
            nullptr,
            D3D_COMPILE_STANDARD_FILE_INCLUDE,
            "main",
            "ps_5_0",
            0,
            0,
            &m_pID3DBlobPixelShader,
            &pID3DBlob_Error);
        fprintf_s(m_fp, "D3DCompile for Pixel Shader returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        if (FAILED(hr))
        {
            fprintf_s(m_fp, "D3DCompile for Pixel Shader log %s\n", (char*)pID3DBlob_Error->GetBufferPointer());
        }
        RefreshLogFile();

        if (SUCCEEDED(hr))
        {
            hr = m_pID3dDevice->CreatePixelShader(m_pID3DBlobPixelShader->GetBufferPointer(),
                m_pID3DBlobPixelShader->GetBufferSize(),
                nullptr,
                &m_pID3d11PixelShader);
            fprintf_s(m_fp, "CreateVertexShader returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
            RefreshLogFile();

            if (SUCCEEDED(hr))
            {
                m_pID3dDeviceContext->PSSetShader(m_pID3d11PixelShader, nullptr, 0);
            }
        }
        else
        {
            if (pID3DBlob_Error != nullptr)
            {
                fprintf_s(m_fp, "D3DCompile failed for Pixel Shader : %s \n", (char*)pID3DBlob_Error->GetBufferPointer());
                RefreshLogFile();
                pID3DBlob_Error->Release();
            }
        }
        return hr;
    }

#define RETURN_IF_FAIL(hr)          \
    if (FAILED(hr))                 \
        return hr;

#define CHECK_AND_RETURN(hr)        \
    if (FAILED(hr))                 \
        return;

    HRESULT TestRenderer::ConfigureGeometry()
    {
        HRESULT hr = S_FALSE;

        // Create and set input layout
        D3D11_INPUT_ELEMENT_DESC inputElementDesc[ATTRIBUTE::MAX];
        ZeroMemory(inputElementDesc, sizeof(inputElementDesc));

        // ATTRIBUTE::POSITION => 0
        inputElementDesc[ATTRIBUTE::POSITION].AlignedByteOffset = 0;
        inputElementDesc[ATTRIBUTE::POSITION].Format = DXGI_FORMAT_R32G32B32_FLOAT;
        inputElementDesc[ATTRIBUTE::POSITION].InputSlot = ATTRIBUTE::POSITION; // 0
        inputElementDesc[ATTRIBUTE::POSITION].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
        inputElementDesc[ATTRIBUTE::POSITION].InstanceDataStepRate = 0;
        inputElementDesc[ATTRIBUTE::POSITION].SemanticIndex = 0;
        inputElementDesc[ATTRIBUTE::POSITION].SemanticName = "POSITION";
        // ATTRIBUTE::COLOR => 1
        inputElementDesc[ATTRIBUTE::COLOR].AlignedByteOffset = 0;
        inputElementDesc[ATTRIBUTE::COLOR].Format = DXGI_FORMAT_R32G32B32_FLOAT;
        inputElementDesc[ATTRIBUTE::COLOR].InputSlot = ATTRIBUTE::COLOR; // 1
        inputElementDesc[ATTRIBUTE::COLOR].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
        inputElementDesc[ATTRIBUTE::COLOR].InstanceDataStepRate = 0;
        inputElementDesc[ATTRIBUTE::COLOR].SemanticIndex = 0;
        inputElementDesc[ATTRIBUTE::COLOR].SemanticName = "COLOR";

        hr = m_pID3dDevice->CreateInputLayout(inputElementDesc,
            _countof(inputElementDesc),
            m_pID3DBlobVertexShader->GetBufferPointer(),
            m_pID3DBlobVertexShader->GetBufferSize(),
            &m_pID3d11InputLayout);
        fprintf_s(m_fp, "CreateInputLayout returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        RETURN_IF_FAIL(hr);

        // set input layout
        m_pID3dDeviceContext->IASetInputLayout(m_pID3d11InputLayout);

        // Order of vertices is clockwise
        float vertices[] =
        {
            0.0f, 0.5f, 0.0f,              // apex
            0.5f, -0.5f, 0.0f,            // right
            -0.5f, -0.5f, 0.0f            // left
        };

        // create vertex buffer
        D3D11_BUFFER_DESC bufferDesc_VertexBuffer;
        ZeroMemory(&bufferDesc_VertexBuffer, sizeof(D3D11_BUFFER_DESC));
        bufferDesc_VertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        bufferDesc_VertexBuffer.ByteWidth = sizeof(float) * ARRAYSIZE(vertices);
        bufferDesc_VertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        bufferDesc_VertexBuffer.MiscFlags = 0;
        bufferDesc_VertexBuffer.StructureByteStride = 0;
        bufferDesc_VertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
        hr = m_pID3dDevice->CreateBuffer(&bufferDesc_VertexBuffer, nullptr, &m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::POSITION]);
        fprintf_s(m_fp, "CreateBuffer returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        RETURN_IF_FAIL(hr);

        // copy vertices into above buffer
        D3D11_MAPPED_SUBRESOURCE mappedSubResource;
        ZeroMemory(&mappedSubResource, sizeof(mappedSubResource));
        hr = m_pID3dDeviceContext->Map(m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::POSITION], 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
        fprintf_s(m_fp, "Map m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::POSITION] returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        RETURN_IF_FAIL(hr);

        memcpy(mappedSubResource.pData, vertices, sizeof(vertices));
        (void)m_pID3dDeviceContext->Unmap(m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::POSITION], 0);

        // Order of vertices is clockwise
        float colors[] =
        {
            1.0f, 0.0f, 0.0f,             // apex
            0.0f, 0.0f, 1.0f,            // right
            0.0f, 1.0f, 0.0f            // left
        };

        // create color buffer
        D3D11_BUFFER_DESC bufferDesc_ColorBuffer;
        ZeroMemory(&bufferDesc_ColorBuffer, sizeof(D3D11_BUFFER_DESC));
        bufferDesc_ColorBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        bufferDesc_ColorBuffer.ByteWidth = sizeof(float) * ARRAYSIZE(colors);
        bufferDesc_ColorBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        bufferDesc_ColorBuffer.MiscFlags = 0;
        bufferDesc_ColorBuffer.StructureByteStride = 0;
        bufferDesc_ColorBuffer.Usage = D3D11_USAGE_DYNAMIC;
        hr = m_pID3dDevice->CreateBuffer(&bufferDesc_ColorBuffer, nullptr, &m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::COLOR]);
        fprintf_s(m_fp, "CreateBuffer returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        RETURN_IF_FAIL(hr);

        // copy vertices into above buffer
        ZeroMemory(&mappedSubResource, sizeof(mappedSubResource));
        hr = m_pID3dDeviceContext->Map(m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::COLOR], 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
        fprintf_s(m_fp, "Map m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::COLOR] returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        RETURN_IF_FAIL(hr);

        memcpy(mappedSubResource.pData, colors, sizeof(colors));
        (void)m_pID3dDeviceContext->Unmap(m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::COLOR], 0);

        // define and set constant buffer
        D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
        ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
        bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
        // bufferDesc_ConstantBuffer.CPUAccessFlags = 0;
        // bufferDesc_ConstantBuffer.MiscFlags = 0;
        // bufferDesc_ConstantBuffer.StructureByteStride = 0;
        bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT; // GPU_RW | NO CPU Access
        hr = m_pID3dDevice->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &m_pID3d11Buffer_ConstantBuffer);
        fprintf_s(m_fp, "CreateBuffer for bufferDesc_ConstantBuffer returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        RETURN_IF_FAIL(hr);

        m_pID3dDeviceContext->VSSetConstantBuffers(0, 1, &m_pID3d11Buffer_ConstantBuffer);

        return hr;
    }

    void TestRenderer::CleanupGeometry()
    {
        if (m_pID3d11Buffer_ConstantBuffer != nullptr)
        {
            m_pID3d11Buffer_ConstantBuffer->Release();
            m_pID3d11Buffer_ConstantBuffer = nullptr;
        }

        if (m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::COLOR] != nullptr)
        {
            m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::COLOR]->Release();
            m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::COLOR] = nullptr;
        }

        if (m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::POSITION] != nullptr)
        {
            m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::POSITION]->Release();
            m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::POSITION] = nullptr;
        }

        if (m_pID3d11InputLayout != nullptr)
        {
            m_pID3d11InputLayout->Release();
            m_pID3d11InputLayout = nullptr;
        }
    }

    void TestRenderer::CleanupVertexShader()
    {
        if (m_pID3DBlobVertexShader != nullptr)
        {
            m_pID3DBlobVertexShader->Release();
            m_pID3DBlobVertexShader = nullptr;
        }

        if (m_pID3d11VertexShader != nullptr)
        {
            m_pID3d11VertexShader->Release();
            m_pID3d11VertexShader = nullptr;
        }
    }

    void TestRenderer::CleanupPixelShader()
    {
        if (m_pID3DBlobPixelShader != nullptr)
        {
            m_pID3DBlobPixelShader->Release();
            m_pID3DBlobPixelShader = nullptr;
        }

        if (m_pID3d11PixelShader != nullptr)
        {
            m_pID3d11PixelShader->Release();
            m_pID3d11PixelShader = nullptr;
        }
    }

    /// Method for performing one-time renderer initialization.
    /// Renderer can initialize global/static instances as part of this method
    /// @param window identifier of the window where drawing is directed
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::Initialize(Window window)
    {
        RendererResult result = RENDERER_RESULT_ERROR;
        HRESULT hr = S_FALSE;
        D3D_DRIVER_TYPE d3dDriverType = {};
        D3D_DRIVER_TYPE d3dDriverTypes[] = {
            D3D_DRIVER_TYPE_HARDWARE,
            D3D_DRIVER_TYPE_WARP,
            D3D_DRIVER_TYPE_REFERENCE
        };
        D3D_FEATURE_LEVEL d3dFeatureLevelRequired = D3D_FEATURE_LEVEL_11_0;
        D3D_FEATURE_LEVEL d3dFeatureLevelAcquired = D3D_FEATURE_LEVEL_10_0;
        UINT createDeviceflags = 0;
        UINT numDriverTypes = _countof(d3dDriverTypes);
        UINT numFeatureLevels = 1;

        DXGI_SWAP_CHAIN_DESC dxgiSwapcChainDesc = {};
        dxgiSwapcChainDesc.BufferCount = 1;
        dxgiSwapcChainDesc.BufferDesc.Width = WIN_WIDTH;
        dxgiSwapcChainDesc.BufferDesc.Height = WIN_HEIGHT;
        dxgiSwapcChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        dxgiSwapcChainDesc.BufferDesc.RefreshRate.Numerator = 60;
        dxgiSwapcChainDesc.BufferDesc.RefreshRate.Denominator = 1;
        dxgiSwapcChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        dxgiSwapcChainDesc.OutputWindow = window;
        dxgiSwapcChainDesc.SampleDesc.Count = 1;
        dxgiSwapcChainDesc.SampleDesc.Quality = 0;
        // dxgiSwapcChainDesc.SwapEffect = ;
        dxgiSwapcChainDesc.Windowed = TRUE;

        for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; ++driverTypeIndex)
        {
            d3dDriverType = d3dDriverTypes[driverTypeIndex];
            hr = D3D11CreateDeviceAndSwapChain(
                NULL,   // pAdapter
                d3dDriverType,
                NULL,   // Software
                createDeviceflags,
                &d3dFeatureLevelRequired,
                numFeatureLevels,
                D3D11_SDK_VERSION,
                &dxgiSwapcChainDesc,
                &m_pIDxgiSwapChain,
                &m_pID3dDevice,
                &d3dFeatureLevelAcquired,
                &m_pID3dDeviceContext);
            if (SUCCEEDED(hr))
            {
                fprintf_s(m_fp, "D3D11CreateDeviceAndSwapChain succeded \n");
                fprintf_s(m_fp, "The chosen driver is of: ");
                switch (d3dDriverType)
                {
                case D3D_DRIVER_TYPE_HARDWARE:
                    fprintf_s(m_fp, "Hardware Type \n");
                    break;

                case D3D_DRIVER_TYPE_WARP:
                    fprintf_s(m_fp, "Warp Type \n");
                    break;

                case D3D_DRIVER_TYPE_REFERENCE:
                    fprintf_s(m_fp, "Reference Type \n");
                    break;

                default:
                    fprintf_s(m_fp, "Unknown Type \n");
                    break;
                }
                fprintf_s(m_fp, "Supported Highest feature level is: ");
                switch (d3dFeatureLevelAcquired)
                {
                case D3D_FEATURE_LEVEL_11_0:
                    fprintf_s(m_fp, "11.0 \n");
                    break;

                case D3D_FEATURE_LEVEL_10_0:
                    fprintf_s(m_fp, "10.0 \n");
                    break;

                default:
                    fprintf_s(m_fp, "Unknown \n");
                    break;
                }
                RefreshLogFile();
                break;
            }
        }

        if (SUCCEEDED(hr))
        {
            result = RENDERER_RESULT_SUCCESS;
        }
        else
        {
            fprintf_s(m_fp, "D3D11CreateDeviceAndSwapChain failed with %u \n", hr);
            RefreshLogFile();
        }

        return result;
    }

    /// Method for performing one-time renderer un-initialization before it is unloaded
    /// Renderer can perform global cleanup as part of this method
    void TestRenderer::Uninitialize(void)
    {
        if (m_pID3d11RenderTargetView != nullptr)
        {
            m_pID3d11RenderTargetView->Release();
            m_pID3d11RenderTargetView = nullptr;
        }

        if (m_pID3dDeviceContext != nullptr)
        {
            m_pID3dDeviceContext->Release();
            m_pID3dDeviceContext = nullptr;
        }

        if (m_pID3dDevice != nullptr)
        {
            m_pID3dDevice->Release();
            m_pID3dDevice = nullptr;
        }

        if (m_pIDxgiSwapChain != nullptr)
        {
            m_pIDxgiSwapChain->Release();
            m_pIDxgiSwapChain = nullptr;
        }
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host before rendering a scene to the active renderer.
    /// Renderer should do initialization of scene specific things as part of this method
    /// @param scene Identifier of a scene to be initialized
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::InitializeScene(SceneType scene)
    {
        // initialize shaders, input layouts, constant buffers etc.
        RendererResult result = RENDERER_RESULT_ERROR;
        HRESULT hr = CreateVertexShader();
        if (SUCCEEDED(hr))
        {
            hr = CreatePixelShader();
            if (SUCCEEDED(hr))
            {
                hr = ConfigureGeometry();
                if (SUCCEEDED(hr))
                {
                    m_projectionMatrix = XMMatrixIdentity();
                    result = RENDERER_RESULT_SUCCESS;
                }
            }
        }
        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host after rendering a scene to the active renderer
    /// Renderer should do cleanup of scene specific things done as part of scene initialize.
    /// @param scene Identifier of a scene to be cleaned-up
    void TestRenderer::UninitializeScene(SceneType scene)
    {
        CleanupGeometry();
        CleanupPixelShader();
        CleanupVertexShader();
    }

    /// Method for rendering a frame in a scene
    /// This method will be called by the host per frame of a scene only to the active renderer
    /// @param params describes the parameters curresponding to this render
    /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
    ///         RENDERER_RESULT_ERROR if failed in building the frame
    ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
    ///                                   in such cases no further frame calls would be made for this scene
    ///                                   to the renderer.
    RendererResult TestRenderer::Render(const RenderParams &params)
    {
        // clear render target with the specified color
        m_pID3dDeviceContext->ClearRenderTargetView(m_pID3d11RenderTargetView, m_clearColor);

        // select which vertex buffer to display
        UINT stride = sizeof(float) * 3;
        UINT offset = 0;
        m_pID3dDeviceContext->IASetVertexBuffers(ATTRIBUTE::POSITION, 1, &m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::POSITION], &stride, &offset);
        m_pID3dDeviceContext->IASetVertexBuffers(ATTRIBUTE::COLOR, 1, &m_pID3d11Buffer_VertexBuffer[ATTRIBUTE::COLOR], &stride, &offset);

        // select geometry primitive
        m_pID3dDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

        // translation is concerned with world matrix transformation
        XMMATRIX worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 2.0f);
        XMMATRIX viewMatrix = XMMatrixIdentity();

        // final worldViewProjectionMatrix
        XMMATRIX wvpMatrix = worldMatrix * viewMatrix * m_projectionMatrix;

        // load this data into CBUFFER
        CBUFFER constantBuffer = {};
        constantBuffer.WorldViewProjectionMatrix = wvpMatrix;
        m_pID3dDeviceContext->UpdateSubresource(m_pID3d11Buffer_ConstantBuffer, 0, nullptr, &constantBuffer, 0, 0);

        // draw vertex buffer to render target
        m_pID3dDeviceContext->Draw(3, 0);

        // switch between front and back buffers
        m_pIDxgiSwapChain->Present(0, 0);

        return RENDERER_RESULT_SUCCESS;
    }

    /// Generic method to notify active renderer about a message posted to host window. The message can be
    /// from system or initiated by the host itself.
    /// @param message OS dependent structure that describes the system message being processed.
    void TestRenderer::OnMessage(const Message &message)
    {
    }

    /// Generic method to notify active renderer about the change in the dimensions of the host window
    /// @param width New width of the window
    /// @param height New height of the window
    void TestRenderer::OnResize(unsigned int width, unsigned int height)
    {
        HRESULT hr = S_OK;
        if (m_pID3d11RenderTargetView != nullptr)
        {
            m_pID3d11RenderTargetView->Release();
            m_pID3d11RenderTargetView = nullptr;
        }

        // resize buffers according to requested width and height
        hr = m_pIDxgiSwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
        fprintf_s(m_fp, "ResizeBuffers returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        CHECK_AND_RETURN(hr);

        // get the back buffer from the swapchain
        ID3D11Texture2D *pID3D11Texture2D_BackBuffer = nullptr;
        hr = m_pIDxgiSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);
        fprintf_s(m_fp, "GetBuffer pID3D11Texture2D_BackBuffer returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        CHECK_AND_RETURN(hr);

        // get render target view from d3d11 device using above back buffer
        hr = m_pID3dDevice->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, nullptr, &m_pID3d11RenderTargetView);
        fprintf_s(m_fp, "CreateRenderTargetView pID3D11Texture2D_BackBuffer returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        pID3D11Texture2D_BackBuffer->Release();
        CHECK_AND_RETURN(hr);

        m_pID3dDeviceContext->OMSetRenderTargets(1, &m_pID3d11RenderTargetView, nullptr);

        // set viewport
        D3D11_VIEWPORT d3dViewport;
        ZeroMemory(&d3dViewport, sizeof(d3dViewport));
        d3dViewport.TopLeftX = 0;
        d3dViewport.TopLeftY = 0;
        d3dViewport.Width = (FLOAT)width;
        d3dViewport.Height = (FLOAT)height;
        d3dViewport.MinDepth = 0.0f;
        d3dViewport.MaxDepth = 1.0f;
        m_pID3dDeviceContext->RSSetViewports(1, &d3dViewport);

        // set projection matrix
        m_projectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width/(float)height, 0.1f, 100.0f);
    }
}
