#version 300 es

in vec4 vPosition;
in vec3 vNormal;
uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;

// lighting variables
uniform mediump int u_lightMode;
uniform vec3 u_La;                  // ambient component
uniform vec3 u_Ld;                  // diffused component
uniform vec3 u_Ls;                  // specular component
uniform vec3 u_light_position;      // light position

// material constants
uniform vec3 u_Ka;                  // ambient reflective color intensity
uniform vec3 u_Kd;                  // diffused reflective color intensity
uniform vec3 u_Ks;                  // specular reflective color intensity
uniform float u_material_shininess; // shininess

// out parameters to fragment shader
out vec3 phong_ads_color;
out vec3 transformed_normals;
out vec3 light_direction;
out vec3 viewer_vector;

void main(void)
{
    if (u_lightMode == 1 || u_lightMode == 2)
    {
        // per-vertex lighting
        vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;
        transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);
        light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);

        // calculate effective light components
        vec3 ambient = u_La * u_Ka;

        float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);
        vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;

        vec3 reflection_vector = reflect(-light_direction, transformed_normals);
        viewer_vector = normalize(-eye_coordinates.xyz);
        vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);

        phong_ads_color = ambient + diffuse + specular;
    }
    else
    {
        phong_ads_color = vec3(1.0, 1.0, 1.0);
    }

    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;
}

