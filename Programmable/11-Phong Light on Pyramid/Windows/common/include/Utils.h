#pragma once

#include <fstream>
using namespace std;

class ShaderFile
{
public:
    ShaderFile(char * const shaderFile) :
        m_fileLength(0l),
        m_shaderSource(nullptr)
    {
        LoadShaderFromFile(shaderFile);
    }

    ~ShaderFile()
    {
        if (m_shaderSource)
            delete[]m_shaderSource;

        if (m_fileLength != 0)
            m_fileLength = 0;

        if (m_fileStream)
            m_fileStream.close();
    }

    unsigned long GetLength() const
    {
        return m_fileLength;
    }

    const char * const * GetShaderSource() const
    {
        return &m_shaderSource;
    }

private:
    void LoadShaderFromFile(char * const shaderFile)
    {
        // read as ascii
        m_fileStream.open(shaderFile, ios::in);
        if (m_fileStream)
        {
            if (m_fileStream.good())
            {
                // get shader file length
                auto startPos = m_fileStream.tellg();
                // seek to end
                m_fileStream.seekg(0, ios::end);
                m_fileLength = (unsigned long)(m_fileStream.tellg() - startPos);
                // seek to begin for processing
                m_fileStream.seekg(ios::beg);
            }

            if (m_fileLength != 0)
            {
                m_shaderSource = new char[m_fileLength + 1];
                m_shaderSource[m_fileLength] = 0;

                auto counter = 0;
                while (m_fileStream.good())
                {
                    m_shaderSource[counter] = m_fileStream.get();
                    if (!m_fileStream.eof())
                        ++counter;
                }
                // terminate the file with null
                m_shaderSource[counter] = 0;
            }
        }
    }

    ifstream m_fileStream;
    unsigned long m_fileLength;
    char *m_shaderSource;
};
