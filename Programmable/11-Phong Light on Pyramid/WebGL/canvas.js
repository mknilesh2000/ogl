//onload function
var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light0_ambient = [0.0, 0.0, 0.0];
var light0_diffuse = [1.0, .0, 0.0];
var light0_specular = [1.0, 0.0, 0.0];
var light0_position = [100.0, 100.0, 100.0, 1.0];

var light1_ambient = [0.0, 0.0, 0.0];
var light1_diffuse = [0.0, 0.0, 1.0];
var light1_specular = [0.0, 0.0, 1.0];
var light1_position = [-100.0, 100.0, 100.0, 1.0];

var material_ambient = [0.0, 0.0, 0.0];
var material_diffuse = [1.0, 1.0, 1.0];
var material_specular = [1.0, 1.0, 1.0];
var material_shininess = 50.0;

var vao_pyramid;
var vbo_pyramid_position;
var vbo_pyramid_normal;

var perspectiveProjectionMatrix;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var laUniform0;
var ldUniform0;
var lsUniform0;
var lightPositionUniform0;

var laUniform1;
var ldUniform1;
var lsUniform1;
var lightPositionUniform1;

var kaUniform, kdUniform, ksUniform, materialShininessUniform;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame || 
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

var angle = 0.0;	
	
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed :( \n");
	else 
		console.log("Obtaining Canvas Passes :) \n");
	
	//print canvas width and height on console
	console.log("Canvas width::" +canvas.width+ " and Canvas height::" +canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initailize webGL
	init();
	
	resize();
	draw();
}

function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement || 
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document. msFullscreenElement||
		null;
		
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullScreen = false;
	}
}

function init()
{
	//code 
	//get webGL 2.0 contextgl 
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	
	var vertexShaderSourceCode = 
	"#version 300 es"							+
	"\n"										+
	"in vec4 vPosition;"						+
	"in vec3 vNormal;"							+		
	"uniform mat4 u_model_matrix;"				+
	"uniform mat4 u_view_matrix;"				+
	"uniform mat4 u_projection_matrix;"			+
	"uniform mediump int u_LKeyPressed;"		+	
	"uniform vec3 u_La[2];"						+
	"uniform vec3 u_Ld[2];"						+
	"uniform vec3 u_Ls[2];"						+
	"uniform vec4 u_light_position[2];"			+
	"uniform vec3 u_Ka;"						+
	"uniform vec3 u_Kd;"						+
	"uniform vec3 u_Ks;"						+
	"uniform float u_material_shininess;"		+
	"out vec3 phong_ads_color;"					+
	"void main(void)"							+
	"{"											+
	"int i;"									+
	"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" 						+
	"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"		+
	"vec3 viewer_vector = normalize(-eye_coordinates.xyz);"	+
	"vec3 light_direction;"						+
	"vec3 phong_ads[2];"						+	
	"float tn_dot_ld;"							+
	"vec3 ambient, diffuse, reflection_vector, specular;"	+
	"for(i = 0; i < 2; i++)"					+
	"{"											+
	"light_direction = normalize(vec3(u_light_position[i]) - eye_coordinates.xyz);"			+
	"tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);"					+
	"ambient = u_La[i] * u_Ka;"							+
	"diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"				+
	"reflection_vector = reflect(-light_direction, transformed_normals);"					+
	"specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
	"phong_ads[i] = ambient + diffuse + specular;"		+
	"}"														+
	"phong_ads_color = phong_ads[0] + phong_ads[1];"	+
	"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"						+
	"}";
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject)
		if(error.length > 0)
		{
			alert("vertex shader");
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	"#version 300 es"			+
	"\n"						+
	"precision highp float;"	+
	"in vec3 phong_ads_color;"	+
	"out vec4 FragColor;"		+
	"void main(void)"			+
	"{"							+
	"FragColor = vec4(phong_ads_color, 1.0);" +
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject)
		if(error.length > 0)
		{
			alert("fragment shader");
			alert(error);
			uninitialize();
		}
	}

	//shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

    modelMatrixUniform =gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
    viewMatrixUniform =gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
    projectionMatrixUniform =gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
		
	laUniform0 = gl.getUniformLocation(shaderProgramObject, "u_La[0]");
	ldUniform0 = gl.getUniformLocation(shaderProgramObject, "u_Ld[0]");
	lsUniform0 = gl.getUniformLocation(shaderProgramObject, "u_Ls[0]");
	lightPositionUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_position[0]");

	laUniform1 = gl.getUniformLocation(shaderProgramObject, "u_La[1]");
	ldUniform1 = gl.getUniformLocation(shaderProgramObject, "u_Ld[1]");
	lsUniform1 = gl.getUniformLocation(shaderProgramObject, "u_Ls[1]");
	lightPositionUniform1 = gl.getUniformLocation(shaderProgramObject, "u_light_position[1]");

	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");
										
	var pyramidVertices = new Float32Array([
											0.0, 1.0, 0.0, //right
											1.0, -1.0, 1.0,
											1.0, -1.0, -1.0,

											0.0, 1.0, 0.0, //ront
											1.0, -1.0, 1.0,
											-1.0, -1.0, 1.0,

											0.0, 1.0, 0.0, //let
											-1.0, -1.0, 1.0,
											-1.0, -1.0, -1.0,

											0.0, 1.0, 0.0, //back
											-1.0, -1.0, -1.0,
											1.0, -1.0, -1.0
										]);

	var pyramidNormals = new Float32Array([
											0.894427, 0.447214, 0.0, //right
											0.894427, 0.447214, 0.0,
											0.894427, 0.447214, 0.0,

											0.0, 0.447214, 0.894427, //ront
											0.0, 0.447214, 0.894427,
											0.0, 0.447214, 0.894427,

											-0.894427, 0.447214, 0.0, //let
											-0.894427, 0.447214, 0.0,
											-0.894427, 0.447214, 0.0,

											0.0, 0.447214, -0.894427, //back
											0.0, 0.447214, -0.894427,
											0.0, 0.447214, -0.894427,
										]);

	vao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);
		vbo_pyramid_position = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_position);
			gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
									3,
									gl.FLOAT,
									false, 0, 0);
			gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		vbo_pyramid_normal = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_normal);
			gl.bufferData(gl.ARRAY_BUFFER, pyramidNormals, gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_NORMAL,
									3,
									gl.FLOAT,
									false, 0, 0);
			gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_NORMAL);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	//gl.enable(gl.CULL_FACE);
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code 
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0, perspectiveProjectionMatrix);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	gl.uniform3fv(laUniform0, light0_ambient);
	gl.uniform3fv(ldUniform0, light0_diffuse);
	gl.uniform3fv(lsUniform0, light0_specular);
	gl.uniform4fv(lightPositionUniform0, light0_position);

	gl.uniform3fv(laUniform1, light1_ambient);
	gl.uniform3fv(ldUniform1, light1_diffuse);
	gl.uniform3fv(lsUniform1, light1_specular);
	gl.uniform4fv(lightPositionUniform1, light1_position);
	
	gl.uniform3fv(kaUniform, material_ambient);
	gl.uniform3fv(kdUniform, material_diffuse);
	gl.uniform3fv(ksUniform, material_specular);
	gl.uniform1f(materialShininessUniform, material_shininess);	
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var rotateMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	mat4.rotateY(rotateMatrix, rotateMatrix, angle);
	
	mat4.multiply(modelMatrix, modelMatrix, rotateMatrix);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

	gl.bindVertexArray(vao_pyramid);
	
		gl.drawArrays(gl.TRIANGLES, 0, 12);
	
	gl.bindVertexArray(null);

	gl.useProgram(null);

	if(angle > 2 * 3.145)
		angle = 0.0;
	
	angle += 0.01;
	
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(vao_pyramid)
	{
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid = null;
	}
	
	if(vbo_pyramid_position)
	{
		gl.deleteBuffer(vbo_pyramid_position);
		vbo_pyramid_position = null;
	}
	
	if(vbo_pyramid_color)
	{
		gl.deleteBuffer(vbo_pyramid_color);
		vbo_pyramid_color = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		case 70:
			toggleFullScreen();
			break;	
		case 76:
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else
				bLKeyPressed = false;
				
			break;

	}
}

function mouseDown()
{
	//code 
}