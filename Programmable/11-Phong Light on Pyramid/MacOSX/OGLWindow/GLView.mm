//
//  GLView.m
//  OGLWindow
//
//  Created by Nilesh Mahajan on 30/07/18.
//  Copyright © 2018 Nilesh Mahajan. All rights reserved.
//

#import "GLView.h"
#import <CoreVideo/CVDisplayLink.h>
#import <glm/gtx/transform.hpp>
#import <glm/gtc/type_ptr.hpp>
#import "Utils.h"

extern FILE* gpFile;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext);

enum ATTRIBUTE
{
    VERTEX = 0,
    COLOR = 1,
    NORMAL = 2,
    TEXTURE0 = 3
};

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    // Draw Members
    GLuint m_vertexShaderObject;
    GLuint m_fragmentShaderObject;
    GLuint m_shaderProgramObject;
    GLuint m_vaoPyramid;
    GLuint m_vboPyramid[4];
    glm::mat4x4 m_perspectiveProjectionMatrix;
    GLuint m_ModelMatrixUniform;
    GLuint m_ViewMatrixUniform;
    GLuint m_ProjectionMatrixUniform;
    GLuint m_lightEnableStateUniform;
    GLuint m_LaUniform;
    GLuint m_LdUniform;
    GLuint m_LsUniform;
    GLuint m_lightPositionUniform;
    GLuint m_KaUniform;
    GLuint m_KdUniform;
    GLuint m_KsUniform;
    GLuint m_ShininessUniform;
    GLuint m_enableAnimation;
    GLuint m_LightMode;
}

- (id) initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    if (self != NULL)
    {
        [[self window] setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            // Specify a display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0       // last 0 is a must
        };
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if (pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format available. Exitting... ");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; // this will automatically releases the older context, if present and sets the newer one
    }
    return (self);
}

- (CVReturn) getFrameForTime:(const CVTimeStamp*)pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [self drawView];
    [pool release];
    return kCVReturnSuccess;
}

- (GLint) CheckObjectStatus:(GLuint)object forOperation:(GLenum)operation forObjectName:(const char *)objectName
{
    GLint status = GL_FALSE;
    bool isLinkOperation = operation == GL_LINK_STATUS;
    isLinkOperation ? glGetProgramiv(object, operation, &status) :
    glGetShaderiv(object, operation, &status);
    
    if (status == GL_FALSE)
    {
        assert(0);
        fprintf(gpFile, "%s failed\n", objectName);
        GLint shaderInfoLogLength = 0;
        isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &status) :
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &status);
        if (shaderInfoLogLength > 0)
        {
            GLchar *szInfoLog = (GLchar*) malloc(shaderInfoLogLength * sizeof(GLchar));
            if (szInfoLog != NULL)
            {
                memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                GLsizei written = 0;
                isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                fprintf(gpFile, "%s info Log :\n %s", objectName, szInfoLog);
                free(szInfoLog);
            }
        }
    }
    return status;
}

- (void) LoadPyramidData
{
    const GLfloat pyramidVertices[] =
    {
        // front
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        // back
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        // left
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        // right
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f
    };
    
    const GLfloat pyramidNormals[] =
    {
        0.0f, 0.447214f, 0.894427f,
        0.0f, 0.447214f, 0.894427f,
        0.0f, 0.447214f, 0.894427f,
        
        0.0f, 0.447214f, -0.894427f,
        0.0f, 0.447214f, -0.894427f,
        0.0f, 0.447214f, -0.894427f,
        
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f,
        
        0.894427f, 0.447214f, 0.0f,
        0.894427f, 0.447214f, 0.0f,
        0.894427f, 0.447214f, 0.0f
    };
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoPyramid);
    glBindVertexArray(m_vaoPyramid);
    
    // prepare array buffer
    glGenBuffers(4, m_vboPyramid);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboPyramid[ATTRIBUTE::VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboPyramid[ATTRIBUTE::NORMAL]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) initialize
{
    m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    ShaderFile vertexShader("vertex.glsl");
    glShaderSource(m_vertexShaderObject, 1, vertexShader.GetShaderSource(), nullptr);
    glCompileShader(m_vertexShaderObject);
    [self CheckObjectStatus:m_vertexShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Vertex Shader"];
    
    m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    ShaderFile fragmentShader("fragment.glsl");
    glShaderSource(m_fragmentShaderObject, 1, fragmentShader.GetShaderSource(), nullptr);
    glCompileShader(m_fragmentShaderObject);
    [self CheckObjectStatus:m_fragmentShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Fragment Shader"];
    
    m_shaderProgramObject = glCreateProgram();
    glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
    glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::NORMAL, "vNormal");
    glLinkProgram(m_shaderProgramObject);
    [self CheckObjectStatus:m_shaderProgramObject forOperation:GL_LINK_STATUS forObjectName:"Shader Program"];
    
    // get uniform location
    m_ModelMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_model_matrix");
    m_ViewMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_view_matrix");
    m_ProjectionMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_projection_matrix");
    
    m_lightEnableStateUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
    m_LaUniform = glGetUniformLocation(m_shaderProgramObject, "u_La");
    m_LdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ld");
    m_LsUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ls");
    m_lightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_light_position");
    
    m_KaUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ka");
    m_KdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Kd");
    m_KsUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ks");
    m_ShininessUniform = glGetUniformLocation(m_shaderProgramObject, "u_material_shininess");
    
    [self LoadPyramidData];
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

- (void) prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    GLint swapInt;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    [self initialize];
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext =(CGLContextObj) [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

- (void) reshape
{
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    if (height == 0.0f)
    {
        height = 1.0f;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    m_perspectiveProjectionMatrix = glm::perspective(45.0f, width / height, 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    [self drawView];
}

- (void) drawView
{
    [[self openGLContext] makeCurrentContext];
    CGLContextObj contextObj = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLLockContext(contextObj);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    static GLfloat angle = 0.0f;
    angle += 0.01f;
    
    glUseProgram(m_shaderProgramObject);
    
    // Draw Pyramid
    {
        glm::mat4x4 modelMatrix = glm::translate(glm::mat4x4(), glm::vec3(0.0f, 0.0f, -4.0f));
        glm::mat4x4 viewMatrix;
        
        if (m_enableAnimation)
        {
            static GLfloat angle = 0.0f;
            angle += 0.01f;
            glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 1.0f, 0.0f));
            modelMatrix *= rotationMatrix;
        }
        
        glUniformMatrix4fv(m_ModelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));
        glUniformMatrix4fv(m_ViewMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniformMatrix4fv(m_ProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(m_perspectiveProjectionMatrix));
        glUniform1i(m_lightEnableStateUniform, m_LightMode);
        
        if (m_LightMode > 0)
        {
            // set light components
            glUniform3f(m_LaUniform, 0.0f, 0.0f, 0.0f);
            glUniform3f(m_LdUniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(m_LsUniform, 1.0f, 1.0f, 1.0f);
            glUniform4f(m_lightPositionUniform, 0.0f, 1.5f, 5.0f, 1.0f);
            
            // set material components
            glUniform3f(m_KaUniform, 0.0f, 0.0f, 0.0f);
            glUniform3f(m_KdUniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(m_KsUniform, 1.0f, 1.0f, 1.0f);
            glUniform1f(m_ShininessUniform, 50.0f);
        }
        
        glBindVertexArray(m_vaoPyramid);
        glDrawArrays(GL_TRIANGLES, 0, 12);
        glBindVertexArray(0);
    }
    
    glUseProgram(0);

    CGLFlushDrawable(contextObj);
    CGLUnlockContext(contextObj);
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

- (void) keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: // Esc key
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        case 'L':
        case 'l':
            m_LightMode = (m_LightMode + 1) % 3;
            break;
            
        case 'A':
        case 'a':
            m_enableAnimation = !m_enableAnimation;
            break;
        default:
            break;
    }
}

- (void) mouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) mouseDragged:(NSEvent *)theEvent
{
    // code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) dealloc
{
    if (m_vaoPyramid)
    {
        glDeleteVertexArrays(1, &m_vaoPyramid);
        m_vaoPyramid = 0;
    }
    
    if (m_vboPyramid[0] != 0)
    {
        glDeleteBuffers(4, m_vboPyramid);
        memset(m_vboPyramid, 0, sizeof(m_vboPyramid));
    }
    
    if (m_shaderProgramObject)
    {
        glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
        glDeleteProgram(m_shaderProgramObject);
        m_shaderProgramObject = 0;
    }
    
    if (m_fragmentShaderObject)
    {
        glDeleteShader(m_fragmentShaderObject);
        m_fragmentShaderObject = 0;
    }
    
    if (m_vertexShaderObject)
    {
        glDeleteShader(m_vertexShaderObject);
        m_vertexShaderObject = 0;
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return result;
}
