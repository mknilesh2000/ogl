cbuffer ConstantBuffer
{
    float4x4 WorldMatrix;
    float4x4 ViewMatrix;
    float4x4 ProjectionMatrix;

    uint LightMode;

    // ligting variables
    float4 La;
    float4 Ld;
    float4 Ls;
    float4 LightPosition;

    // material constants
    float4 Ka;
    float4 Kd;
    float4 Ks;
    float Shininess;
};

struct vertex_output
{
    float3 phong_ads_color : VERT1;
    float3 transformed_normals: VERT2;
    float3 light_direction: VERT3;
    float3 viewer_vector : VERT4;
    float4 pos : SV_POSITION;
};

float4 main(vertex_output inputFromVS) : SV_TARGET
{
    float4 outcolor = float4(1.0, 1.0, 1.0, 1.0);
    if (LightMode == 1)
    {
        outcolor = float4(inputFromVS.phong_ads_color, 1.0);
    }
    else if (LightMode == 2)
    {
        // per-fragment lighting
        float3 normalized_transformed_normals = normalize(inputFromVS.transformed_normals);
        float3 normalized_light_direction = normalize(inputFromVS.light_direction);
        float3 normalized_viewer_vector = normalize(inputFromVS.viewer_vector);

        // calculate effective light components
        float3 ambient = (float3)(La * Ka);

        float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);
        float3 diffuse = (float3)(Ld * Kd * tn_dot_ld);

        float3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);
        float3 specular = (float3)(Ls * Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), Shininess));

        outcolor = float4(ambient + diffuse + specular, 1.0);
    }
    return outcolor;
}
