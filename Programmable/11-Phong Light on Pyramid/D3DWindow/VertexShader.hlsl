cbuffer ConstantBuffer
{
    float4x4 WorldMatrix;
    float4x4 ViewMatrix;
    float4x4 ProjectionMatrix;

    uint LightMode;

    // ligting variables
    float4 La;
    float4 Ld;
    float4 Ls;
    float4 LightPosition;

    // material constants
    float4 Ka;
    float4 Kd;
    float4 Ks;
    float Shininess;
};

struct vertex_output
{
    float4 phong_ads_color : VERT1;
    float3 transformed_normals: VERT2;
    float3 light_direction: VERT3;
    float3 viewer_vector : VERT4;
    float4 pos : SV_POSITION;
};

vertex_output main(float4 pos:POSITION, float4 normal:NORMAL)
{
    vertex_output toPS;
    if (LightMode == 1 || LightMode == 2)
    {
        // per-vertex lighting
        float4 eye_coordinates = mul(ViewMatrix, mul(WorldMatrix, pos));
        toPS.transformed_normals = normalize(mul((float3x3)mul(ViewMatrix, WorldMatrix), (float3)normal));
        toPS.light_direction = normalize((float3)(LightPosition) - eye_coordinates.xyz);

        // calculate effective light components
        float4 ambient = La * Ka;

        float tn_dot_ld = max(dot(toPS.light_direction, toPS.transformed_normals), 0.0);
        float4 diffuse = Ld * Kd * tn_dot_ld;

        float3 reflection_vector = reflect(-toPS.light_direction, toPS.transformed_normals);
        toPS.viewer_vector = normalize(-eye_coordinates.xyz);
        float4 specular = Ls * Ks * pow(max(dot(reflection_vector, toPS.viewer_vector), 0.0), Shininess);

        toPS.phong_ads_color = ambient + diffuse + specular;
    }
    else
    {
        toPS.phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);
        toPS.transformed_normals = float3(0.0, 0.0, 0.0);
        toPS.light_direction = float3(0.0, 0.0, 0.0);
        toPS.viewer_vector = float3(0.0, 0.0, 0.0);
    }

    toPS.pos = mul(ProjectionMatrix, mul(ViewMatrix, mul(WorldMatrix, pos)));
    return toPS;
}