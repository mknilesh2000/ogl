//
//  GLView.m
//  OGLWindow
//
//  Created by Nilesh Mahajan on 30/07/18.
//  Copyright © 2018 Nilesh Mahajan. All rights reserved.
//

#import "GLView.h"
#import <CoreVideo/CVDisplayLink.h>
#import <glm/gtx/transform.hpp>
#import <glm/gtc/type_ptr.hpp>

extern FILE* gpFile;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext);

enum ATTRIBUTE
{
    VERTEX = 0,
    COLOR = 1,
    NORMAL = 2,
    TEXTURE0 = 3
};

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    // Draw Members
    GLuint m_vertexShaderObject;
    GLuint m_fragmentShaderObject;
    GLuint m_shaderProgramObject;
    GLuint m_vaoTriangle;
    GLuint m_vboTriangle[4];
    GLuint m_vaoSquare;
    GLuint m_vboSquare[4];
    GLuint m_MVPUniform;
    glm::mat4x4 m_projectionMatrix;
}

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    if (self != NULL)
    {
        [[self window] setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            // Specify a display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0       // last 0 is a must
        };
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if (pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format available. Exitting... ");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; // this will automatically releases the older context, if present and sets the newer one
    }
    return (self);
}

- (CVReturn) getFrameForTime:(const CVTimeStamp*)pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [self drawView];
    [pool release];
    return kCVReturnSuccess;
}

- (GLint) CheckObjectStatus:(GLuint)object forOperation:(GLenum)operation forObjectName:(const char *)objectName
{
    GLint status = GL_FALSE;
    bool isLinkOperation = operation == GL_LINK_STATUS;
    isLinkOperation ? glGetProgramiv(object, operation, &status) :
    glGetShaderiv(object, operation, &status);
    
    if (status == GL_FALSE)
    {
        assert(0);
        fprintf(gpFile, "%s failed\n", objectName);
        GLint shaderInfoLogLength = 0;
        isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &status) :
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &status);
        if (shaderInfoLogLength > 0)
        {
            GLchar *szInfoLog = (GLchar*) malloc(shaderInfoLogLength * sizeof(GLchar));
            if (szInfoLog != NULL)
            {
                memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                GLsizei written = 0;
                isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                fprintf(gpFile, "%s info Log :\n %s", objectName, szInfoLog);
                free(szInfoLog);
            }
        }
    }
    return status;
}

- (void) LoadTriangleData
{
    const GLfloat triangleVertices[] = {
        0.0f, 0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
    };
    
    const GLfloat TriangleColors[] = {
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
    };
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoTriangle);
    glBindVertexArray(m_vaoTriangle);
    
    // prepare array buffer
    glGenBuffers(4, m_vboTriangle);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboTriangle[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboTriangle[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(TriangleColors), TriangleColors, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::COLOR, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) LoadSquareData
{
    const GLfloat squareVertices[] =
    {
        0.5f, 0.5f, 0.0f,
        -0.5f, 0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
    };
    
    const GLfloat SquareColors[] =
    {
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
    };
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoSquare);
    glBindVertexArray(m_vaoSquare);
    
    // prepare array buffer
    glGenBuffers(4, m_vboSquare);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboSquare[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(SquareColors), SquareColors, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::COLOR, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) initialize
{
    m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSource =
        "#version 410 core\n"                                       \
        "in vec4 vPosition;"                                        \
        "in vec4 vColor;"                                           \
        "out vec4 outColor;"                                        \
        "uniform mat4 u_mvp_matrix;"                                \
        "void main(void)"                                           \
        "{"                                                         \
        "   gl_Position = u_mvp_matrix * vPosition;"                \
        "   outColor = vColor;"                                     \
        "}";
    
    glShaderSource(m_vertexShaderObject, 1, &vertexShaderSource, NULL);
    glCompileShader(m_vertexShaderObject);
    [self CheckObjectStatus:m_vertexShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Vertex Shader"];
    
    m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentShaderSource =
    "#version 410 core\n"                           \
    "out vec4 FragColor;"                               \
    "in vec4 outColor;"                                 \
    "void main(void)"                                   \
    "{"                                                 \
    "   FragColor = outColor;"                          \
    "}";
    glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSource, NULL);
    glCompileShader(m_fragmentShaderObject);
    [self CheckObjectStatus:m_fragmentShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Fragment Shader"];
    
    m_shaderProgramObject = glCreateProgram();
    glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
    glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::COLOR, "vColor");
    glLinkProgram(m_shaderProgramObject);
    [self CheckObjectStatus:m_shaderProgramObject forOperation:GL_LINK_STATUS forObjectName:"Shader Program"];
    
    m_MVPUniform = glGetUniformLocation(m_shaderProgramObject, "u_mvp_matrix");
    
    [self LoadTriangleData];
    [self LoadSquareData];
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

- (void) prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    GLint swapInt;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    [self initialize];
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext =(CGLContextObj) [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

- (void) reshape
{
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    if (height == 0.0f)
    {
        height = 1.0f;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    m_projectionMatrix = glm::perspective(45.0f, width / height, 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    [self drawView];
}

- (void) drawView
{
    [[self openGLContext] makeCurrentContext];
    CGLContextObj contextObj = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLLockContext(contextObj);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    static GLfloat angle = 0.0f;
    angle += 0.01f;
    
    glUseProgram(m_shaderProgramObject);
    
    // Draw Triangle
    {
        glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(-1.0f, 0.0f, -3.0f));
        translationMatrix = glm::rotate(translationMatrix, angle, glm::vec3(0.0f, 1.0f, 0.0f));
        
        glm::mat4x4 modelViewProjectionMatrix = m_projectionMatrix * translationMatrix;
        
        glUniformMatrix4fv(m_MVPUniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
        glBindVertexArray(m_vaoTriangle);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);
    }
    
    // Draw Square
    {
        glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(1.0f, 0.0f, -3.0f));
        glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(), angle, glm::vec3(1.0f, 0.0f, 0.0f));
        glm::mat4x4 modelViewProjectionMatrix =
        m_projectionMatrix * translationMatrix * rotationMatrix;
        
        glUniformMatrix4fv(m_MVPUniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
        glBindVertexArray(m_vaoSquare);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
    }
    
    glUseProgram(0);

    CGLFlushDrawable(contextObj);
    CGLUnlockContext(contextObj);
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

- (void) keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: // Esc key
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        default:
            break;
    }
}

- (void) mouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) mouseDragged:(NSEvent *)theEvent
{
    // code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) dealloc
{
    if (m_vaoTriangle)
    {
        glDeleteVertexArrays(1, &m_vaoTriangle);
        m_vaoTriangle = 0;
    }
    
    if (m_vboTriangle[0] != 0)
    {
        glDeleteBuffers(4, m_vboTriangle);
        memset(m_vboTriangle, 0, sizeof(m_vboTriangle));
    }
    
    if (m_vaoSquare)
    {
        glDeleteVertexArrays(1, &m_vaoSquare);
        m_vaoSquare = 0;
    }
    
    if (m_vboSquare[0] != 0)
    {
        glDeleteBuffers(4, m_vboSquare);
        memset(m_vboSquare, 0, sizeof(m_vboSquare));
    }
    
    if (m_shaderProgramObject)
    {
        glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
        glDeleteProgram(m_shaderProgramObject);
        m_shaderProgramObject = 0;
    }
    
    if (m_fragmentShaderObject)
    {
        glDeleteShader(m_fragmentShaderObject);
        m_fragmentShaderObject = 0;
    }
    
    if (m_vertexShaderObject)
    {
        glDeleteShader(m_vertexShaderObject);
        m_vertexShaderObject = 0;
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return result;
}
