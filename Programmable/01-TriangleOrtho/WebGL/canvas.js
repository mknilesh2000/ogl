// global variables
var canvas = null;
var gl = null;      // webgl context
var bFullScreen = false;
var canvasOriginalWidth;
var canvasOriginalHeight;

const WebGLMacros = 
{
    VERTEX: 0,
    COLOR: 1,
    NORMAL: 2,
    TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo;
var mvpUniform;
var projectionMatrix;

// To start animation
var requestAnimationFrame = 
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

// To stop animation
var cancelAnimationFrame = 
    window.cancelAnimationFrame ||
    window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

// onload function
function main()
{
    canvas = document.getElementById("WGLCanvas");
    if (!canvas)
        console.log("Obtaining canvas failed");
    else
        console.log("Canvas Obtained");

    canvasOriginalWidth = canvas.width;
    canvasOriginalHeight = canvas.height;

    // register event handlers
    window.addEventListener("keydown", onKeyDown, false);
    window.addEventListener("click", onClick, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function onKeyDown(event)
{
    switch (event.keyCode)
    {
        case 27: // escape
            uninitialize();
            window.close();
            break;

        case 70: // for 'F' or 'f'
            toggleFullscreen();
            break;
    }
}

function onClick()
{
    // code
}

function resize()
{
    if (bFullScreen)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvasOriginalWidth;
        canvas.height = canvasOriginalHeight;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    var aspect = canvas.width / canvas.height;
    if (aspect > 1.0)
    {
        mat4.ortho(projectionMatrix, -100.0 * aspect, 100.0 * aspect, -100.0, 100.0, -100.0, 100.0);
    }
    else
    {
        mat4.ortho(projectionMatrix, -100.0, 100.0, -100.0 / aspect, 100.0 / aspect, -100.0, 100.0);
    }
}

function toggleFullscreen()
{
    var fullscreen_element = 
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    if (fullscreen_element == null)
    {
        // not fullscreen
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.requestFullscreen();

        bFullScreen = true;
    }
    else
    {
        // in-fullscreen
        if (document.exitFullscreen)
            document.exitFullScreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msCancelFullscreen)
            document.msCancelFullscreen();

        bFullScreen = false;
    }
}

function init()
{
    // get WeGL 2.0 context
    gl = canvas.getContext("webgl2");

    if (gl == null)
    {
        console.log("Failed to get rendering context for WebGL 2.0");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    var vertexShaderSourceCode = document.getElementById("vertex-shader");
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode.firstChild.data);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    // fragment shader
    var fragmentShaderSourceCode = document.getElementById("fragment-shader");
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode.firstChild.data);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    // create program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // bind attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VERTEX, "vPosition");

    // link program
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    // vertices, colors, shader attribs, vao, vbo initialization
    var triangleVertices = new Float32Array([
        0.0, 50.0, 0.0,
        -50.0, -50.0, 0.0,
        50.0, -50.0, 0.0
    ]);

    vao = gl.createVertexArray();
    gl.bindVertexArray(vao);

    vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
    gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    // set color
    gl.clearColor(0.0, 0.0, 1.0, 1.0);
    projectionMatrix = mat4.create();
}

function uninitialize()
{
    if (vao)
    {
        gl.deleteVertexArray(vao);
        vao = null;
    }

    if (vbo)
    {
        gl.deleteBuffer(vbo);
        vbo = null;
    }

    if (shaderProgramObject)
    {
        if (fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject)
        {
            gl.deleteShader(vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject = null;
        }

        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}

function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    mat4.multiply(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    
    gl.bindVertexArray(vao);
    gl.drawArrays(gl.TRIANGLES, 0, 3);
    gl.bindVertexArray(null);

    gl.useProgram(null);

    // animation loop
    requestAnimationFrame(draw, canvas);
}