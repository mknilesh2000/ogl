#pragma once

#include "TestRenderer.h"
#include <glm/gtc/type_ptr.hpp>
#include <SOIL.h>

namespace Features
{
    enum ATTRIBUTE
    {
        VERTEX = 0,
        TEXTURE0,
        COLOR,
        NORMAL
    };

    using namespace Interfaces;

#define EXIT_IF_ERROR(x) if (x == GL_FALSE) { __debugbreak(); return RENDERER_RESULT_ERROR; }
#define CALL_AND_RESET_IF_VALID(x, y) if (x != NULL) { y;  x = NULL; }

    TestRenderer::TestRenderer() :
        m_fp(nullptr),
        m_vertexShaderObject(0),
        m_fragmentShaderObject(0),
        m_shaderProgramObject(0),
        m_vaoPyramid(0),
        m_textureCube(0),
        m_vaoCube(0),
        m_texturePyramid(0),
        m_TexSampler2DUniform(0)
    {
        memset(m_vboPyramid, 0, sizeof(m_vboPyramid));
        memset(m_vboCube, 0, sizeof(m_vboCube));
        fopen_s(&m_fp, "output.log", "w+");
        if (m_fp == nullptr)
            __debugbreak();
        fprintf(m_fp, "Opened log-file\n");
    }

    TestRenderer::~TestRenderer()
    {
        if (m_fp != nullptr)
        {
            fprintf(m_fp, "Closing log-file\n");
            fclose(m_fp);
            m_fp = nullptr;
        }
    }

    /// Method for retrieving name of the renderer
    /// @param rendererName buffer to be filled with the renderer name
    /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
    ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
    const char* TestRenderer::GetName()
    {
        static const char name[] = "TestRenderer";
        return name;
    }

    void TestRenderer::LoadTexture(GLuint &texture, char *szPath)
    {
        int iTextureWidth, iTextureHeight;
        unsigned char *pImageData = NULL;
        pImageData = SOIL_load_image((const char*)szPath, &iTextureWidth, &iTextureHeight, 0, SOIL_LOAD_RGB);
        if (pImageData != NULL)
        {
            glGenTextures(1, &texture);
            glBindTexture(GL_TEXTURE_2D, texture);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // set 1 rather than default 4, for better performance
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexImage2D(GL_TEXTURE_2D,
                0,
                GL_RGB,
                iTextureWidth,
                iTextureHeight,
                0,
                GL_RGB,
                GL_UNSIGNED_BYTE,
                pImageData);
            glGenerateMipmap(GL_TEXTURE_2D);

            SOIL_free_image_data(pImageData); // free the imageData
        }
        else
        {
            __debugbreak();
        }
    }

    void TestRenderer::LoadPyramidData()
    {
        const GLfloat pyramidVertices[] = {
            // front
            0.0f, 0.5f, 0.0f,
            -0.5f, -0.5f, 0.5f,
            0.5f, -0.5f, 0.5f,
            // back
            0.0f, 0.5f, 0.0f,
            0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            // left
            0.0f, 0.5f, 0.0f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, 0.5f,
            // right
            0.0f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.5f,
            0.5f, -0.5f, -0.5f
        };

        const GLfloat pyramidTexCords[] =
        {
            0.5f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,

            0.5f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,

            0.5f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,

            0.5f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
        };

        // generate and start filling list
        glGenVertexArrays(1, &m_vaoPyramid);
        glBindVertexArray(m_vaoPyramid);

        // prepare array buffer
        glGenBuffers(2, m_vboPyramid);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboPyramid[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, m_vboPyramid[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexCords), pyramidTexCords, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::TEXTURE0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // reset array buffer and vertex array
        glBindVertexArray(0);

        LoadTexture(m_texturePyramid, "res\\Test\\Pyramid.jpg");
    }

    void TestRenderer::LoadCubeData()
    {
        const GLfloat cubeVertices[] =
        {
            // front
            0.5f, 0.5f, 0.5f,
            -0.5f, 0.5f, 0.5f,
            -0.5f, -0.5f, 0.5f,
            0.5f, -0.5f, 0.5f,
            // right
            0.5f, 0.5f, -0.5f,
            0.5f, 0.5f, 0.5f,
            0.5f, -0.5f, 0.5f,
            0.5f, -0.5f, -0.5f,
            // left
            -0.5f, 0.5f, 0.5f,
            -0.5f, 0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, 0.5f,
            // back
            -0.5f, 0.5f, -0.5f,
            0.5f, 0.5f, -0.5f,
            0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            // top
            0.5f, 0.5f, -0.5f,
            -0.5f, 0.5f, -0.5f,
            -0.5f, 0.5f, 0.5f,
            0.5f, 0.5f, 0.5f,
            // bottom
            0.5f, -0.5f, 0.5f,
            -0.5f, -0.5f, 0.5f,
            -0.5f, -0.5f, -0.5f,
            0.5f, -0.5f, -0.5f
        };

        const GLfloat cubeTexCords[] =
        {
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,

            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,

            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,

            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,

            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,

            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,
        };

        // generate and start filling list
        glGenVertexArrays(1, &m_vaoCube);
        glBindVertexArray(m_vaoCube);

        // prepare array buffer
        glGenBuffers(2, m_vboCube);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCords), cubeTexCords, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::TEXTURE0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // reset array buffer and vertex array
        glBindVertexArray(0);

        LoadTexture(m_textureCube, "res\\Test\\Cube.png");
    }

    /// Method for performing one-time renderer initialization.
    /// Renderer can initialize global/static instances as part of this method
    /// @param window identifier of the window where drawing is directed
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::Initialize(Window window)
    {
        // vertex shader
        m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexShaderSourceCode =
            "#version 450 core"                                         \
            "\n"                                                        \
            "in vec4 vPosition;"                                        \
            "in vec2 vTex0Coord;"                                       \
            "out vec2 vTex0Coord2FS;"                                   \
            "uniform mat4 u_mvp_matrix;"                                \
            "void main(void)"                                           \
            "{"                                                         \
            "   gl_Position = u_mvp_matrix * vPosition;"                \
            "   vTex0Coord2FS = vTex0Coord;"                            \
            "}";
        glShaderSource(m_vertexShaderObject, 1, &vertexShaderSourceCode, nullptr);
        glCompileShader(m_vertexShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_vertexShaderObject, GL_COMPILE_STATUS, "m_vertexShaderObject"));

        // fragment shader
        m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
            "#version 450 core"                                         \
            "\n"                                                        \
            "in vec2 vTex0Coord2FS;"                                    \
            "out vec4 FragColor;"                                       \
            "uniform sampler2D u_texture0_sampler;"                     \
            "void main(void)"                                           \
            "{"                                                         \
            "   FragColor = texture(u_texture0_sampler, vTex0Coord2FS);" \
            "}";
        glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSourceCode, nullptr);
        glCompileShader(m_fragmentShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_fragmentShaderObject, GL_COMPILE_STATUS, "m_fragmentShaderObject"));

        // shader program
        m_shaderProgramObject = glCreateProgram();
        glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
        glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
        glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::TEXTURE0, "vTex0Coord");
        glLinkProgram(m_shaderProgramObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_shaderProgramObject, GL_LINK_STATUS, "m_shaderProgramObject"));

        // get uniform location
        m_MVPuniform = glGetUniformLocation(m_shaderProgramObject, "u_mvp_matrix");
        m_TexSampler2DUniform = glGetUniformLocation(m_shaderProgramObject, "u_texture0_sampler");

        LoadPyramidData();
        LoadCubeData();

        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing one-time renderer un-initialization before it is unloaded
    /// Renderer can perform global cleanup as part of this method
    void TestRenderer::Uninitialize(void)
    {
        CALL_AND_RESET_IF_VALID(m_vaoPyramid, glDeleteVertexArrays(1, &m_vaoPyramid));
        CALL_AND_RESET_IF_VALID(m_vaoCube, glDeleteVertexArrays(1, &m_vaoCube));
        glDeleteBuffers(2, m_vboPyramid);
        glDeleteBuffers(2, m_vboCube);
        CALL_AND_RESET_IF_VALID(m_vertexShaderObject, glDetachShader(m_shaderProgramObject, m_vertexShaderObject));
        CALL_AND_RESET_IF_VALID(m_fragmentShaderObject, glDetachShader(m_shaderProgramObject, m_fragmentShaderObject));
        CALL_AND_RESET_IF_VALID(m_shaderProgramObject, glDeleteProgram(m_shaderProgramObject));
        glUseProgram(0);
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host before rendering a scene to the active renderer.
    /// Renderer should do initialization of scene specific things as part of this method
    /// @param scene Identifier of a scene to be initialized
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::InitializeScene(SceneType scene)
    {
        // general initializations
        glShadeModel(GL_SMOOTH);
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_CULL_FACE);
        glEnable(GL_TEXTURE_2D);
        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host after rendering a scene to the active renderer
    /// Renderer should do cleanup of scene specific things done as part of scene initialize.
    /// @param scene Identifier of a scene to be cleaned-up
    void TestRenderer::UninitializeScene(SceneType scene)
    {
    }

    /// Method for rendering a frame in a scene
    /// This method will be called by the host per frame of a scene only to the active renderer
    /// @param params describes the parameters curresponding to this render
    /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
    ///         RENDERER_RESULT_ERROR if failed in building the frame
    ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
    ///                                   in such cases no further frame calls would be made for this scene
    ///                                   to the renderer.
    RendererResult TestRenderer::Render(const RenderParams &params)
    {
        RendererResult result = RENDERER_RESULT_FINISHED;

        // render for 5 secs before transitioning to next scene
        if (params.scene == SCENE_TYPE_TEST0)
        {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glUseProgram(m_shaderProgramObject);

            // Draw Pyramid
            {
                glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(-1.5f, 0.0f, -4.0f));
                GLfloat angle = params.frameIdFromEpicStart * 0.1f;
                glm::mat4x4 modelViewProjectionMatrix =
                    m_perspectiveProjectionMatrix * glm::rotate(translationMatrix, angle, glm::vec3(0.0f, 1.0f, 0.0f));
                glUniformMatrix4fv(m_MVPuniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));

                glBindVertexArray(m_vaoPyramid);

                // bind with pyramid texture
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, m_texturePyramid);
                glUniform1i(m_TexSampler2DUniform, 0);

                glDrawArrays(GL_TRIANGLES, 0, 12);
                glBindVertexArray(0);
            }

            // Draw Cube
            {
                glm::mat4x4 translationMatrix = glm::translate(glm::mat4x4(), glm::vec3(1.5f, 0.0f, -4.0f));
                GLfloat angle = params.frameIdFromEpicStart * 0.05f;
                glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(), angle, glm::vec3(1.0f, 0.0f, 0.0f))
                    * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 1.0f, 0.0f))
                    * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 0.0f, 1.0f));
                glm::mat4x4 scaleMatrix = glm::scale(glm::mat4x4(), glm::vec3(0.75f, 0.75f, 0.75f));

                glm::mat4x4 modelViewProjectionMatrix =
                    m_perspectiveProjectionMatrix * translationMatrix * rotationMatrix * scaleMatrix;

                glUniformMatrix4fv(m_MVPuniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));

                // bind with cube texture
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, m_textureCube);
                glUniform1i(m_TexSampler2DUniform, 0);

                glBindVertexArray(m_vaoCube);
                glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
                glBindVertexArray(0);
            }

            glUseProgram(0);

            result = RENDERER_RESULT_SUCCESS;
        }
        return result;
    }

    /// Generic method to notify active renderer about a message posted to host window. The message can be
    /// from system or initiated by the host itself.
    /// @param message OS dependent structure that describes the system message being processed.
    void TestRenderer::OnMessage(const Message &message)
    {
    }

    /// Generic method to notify active renderer about the change in the dimensions of the host window
    /// @param width New width of the window
    /// @param height New height of the window
    void TestRenderer::OnResize(unsigned int width, unsigned int height)
    {
        if (height == 0)
            height = 1;
        glViewport(0, 0, (GLsizei)width, (GLsizei)height);
        m_perspectiveProjectionMatrix = glm::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    }

    GLint TestRenderer::CheckObjectStatus(GLuint object, GLenum operation, const char *objectName)
    {
        GLint status = GL_FALSE;
        bool isLinkOperation = operation == GL_LINK_STATUS;
        isLinkOperation ? glGetProgramiv(object, operation, &status) :
            glGetShaderiv(object, operation, &status);

        if (status == GL_FALSE)
        {
            fprintf(m_fp, "%s failed\n", objectName);
            GLint shaderInfoLogLength = 0;
            isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &status) :
                glGetShaderiv(object, GL_INFO_LOG_LENGTH, &status);
            if (shaderInfoLogLength > 0)
            {
                GLchar *szInfoLog = new GLchar[shaderInfoLogLength];
                if (szInfoLog != nullptr)
                {
                    memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                    GLsizei written = 0;
                    isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                        glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                    fprintf(m_fp, "%s info Log :\n %s", objectName, szInfoLog);
                    delete[]szInfoLog;
                }
            }
        }
        return status;
    }
}
