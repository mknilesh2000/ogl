// global variables
var canvas = null;
var gl = null;      // webgl context
var bFullScreen = false;
var canvasOriginalWidth;
var canvasOriginalHeight;

const WebGLMacros = 
{
    VERTEX: 0,
    COLOR: 1,
    NORMAL: 2,
    TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var mvpUniform;
var projectionMatrix;
var texSampler2DUniform;

// To start animation
var requestAnimationFrame = 
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

// To stop animation
var cancelAnimationFrame = 
    window.cancelAnimationFrame ||
    window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;


function isPowerOf2(value)
{
    return (value & (value - 1)) == 0;
}

//
// Initialize a texture and load an image.
// When the image finished loading copy it into the texture.
//
function loadTexture(url)
{
    const texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);

    texture.image = new Image();
    texture.image.src = url;
    texture.image.crossOrigin = "anonymous";
    texture.image.onload = function()
    {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        //gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);

        if (isPowerOf2(texture.image.width) && isPowerOf2(texture.image.height))
        {
            // Yes, it's a power of 2. Generate mips.
            gl.generateMipmap(gl.TEXTURE_2D);
        }
        else
        {
            // No, it's not a power of 2. Turn of mips and set
            // wrapping to clamp to edge
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
         }

        gl.bindTexture(gl.TEXTURE_2D, null);
    };

    return texture;
}

class Model3D
{
    constructor()
    {
        this.vao = null;
        this.vboPosition = null;
        this.vboTexCoord0 = null;
        this.vertices = null;
        this.texCoords = null;
        this.texture = null;
    }

    initialize(vertices, texCoords, textureUrl)
    {
        this.vao = gl.createVertexArray();
        gl.bindVertexArray(this.vao);

        this.vboPosition = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vboPosition);
        gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        this.vboTexCoord0 = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vboTexCoord0);
        gl.bufferData(gl.ARRAY_BUFFER, texCoords, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);

        this.vertices = vertices;
        this.texCoords = texCoords;
        this.texture = loadTexture(textureUrl);
    }

    drawOverride()
    {

    }

    draw()
    {
        // bind with pyramid texture
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        gl.bindVertexArray(this.vao);
        this.drawOverride();
        gl.bindVertexArray(null);
    }

    uninitialize()
    {
        if (this.texture)
        {
            gl.deleteTexture(this.texture);
            this.texture = null;
        }

        if (this.vboTexCoord0)
        {
            gl.deleteBuffer(this.vboTexCoord0);
            this.vboTexCoord0 = null;
        }
            
        if (this.vboPosition)
        {
            gl.deleteBuffer(this.vboPosition);
            this.vboPosition = null;
        }

        if (this.vao)
        {
            gl.deleteVertexArray(this.vao);
            this.vao = null;
        }
    }
};

class Pyramid extends Model3D
{
    drawOverride()
    {
        gl.drawArrays(gl.TRIANGLES, 0, this.vertices.length / 3);
    }
};

class Cube extends Model3D
{
    drawOverride()
    {
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
    }
}

var pyramid = new Pyramid();
var cube = new Cube();

// onload function
function main()
{
    canvas = document.getElementById("WGLCanvas");
    if (!canvas)
        console.log("Obtaining canvas failed");
    else
        console.log("Canvas Obtained");

    canvasOriginalWidth = canvas.width;
    canvasOriginalHeight = canvas.height;

    // register event handlers
    window.addEventListener("keydown", onKeyDown, false);
    window.addEventListener("click", onClick, false);
    window.addEventListener("resize", resize, false);

    // initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function onKeyDown(event)
{
    switch (event.keyCode)
    {
        case 27: // escape
            uninitialize();
            window.close();
            break;

        case 70: // for 'F' or 'f'
            toggleFullscreen();
            break;
    }
}

function onClick()
{
    // code
}

function resize()
{
    if (bFullScreen)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvasOriginalWidth;
        canvas.height = canvasOriginalHeight;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    var aspect = canvas.width / canvas.height;
    if (aspect > 1.0)
    {
        mat4.perspective(projectionMatrix, 45.0, aspect, 0.1, 100);
    }
    else
    {
        mat4.perspective(projectionMatrix, 45.0, aspect, 0.1, 100);
    }
}

function toggleFullscreen()
{
    var fullscreen_element = 
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    if (fullscreen_element == null)
    {
        // not fullscreen
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.requestFullscreen();

        bFullScreen = true;
    }
    else
    {
        // in-fullscreen
        if (document.exitFullscreen)
            document.exitFullScreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msCancelFullscreen)
            document.msCancelFullscreen();

        bFullScreen = false;
    }
}

function init()
{
    // get WeGL 2.0 context
    gl = canvas.getContext("webgl2");

    if (gl == null)
    {
        console.log("Failed to get rendering context for WebGL 2.0");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    var vertexShaderSourceCode = document.getElementById("vertex-shader");
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode.firstChild.data);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    // fragment shader
    var fragmentShaderSourceCode = document.getElementById("fragment-shader");
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode.firstChild.data);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    // create program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // bind attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.TEXTURE0, "vTexCoord");

    // link program
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
            return;
        }
    }

    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
    texSampler2DUniform = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");

    // configure for 3D
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
    gl.enable(gl.CULL_FACE);
        
    // vertices, colors, shader attribs, vao, vbo initialization
    var pyramidVertices = new Float32Array([
        // front
        0.0, 0.5, 0.0,
        -0.5, -0.5, 0.5,
        0.5, -0.5, 0.5,
        // back
        0.0, 0.5, 0.0,
        0.5, -0.5, -0.5,
        -0.5, -0.5, -0.5,
        // left
        0.0, 0.5, 0.0,
        -0.5, -0.5, -0.5,
        -0.5, -0.5, 0.5,
        // right
        0.0, 0.5, 0.0,
        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5
    ]);
    var pyramidTexCoords = new Float32Array([
        0.5, 1.0,
        1.0, 0.0,
        0.0, 0.0,

        0.5, 1.0,
        1.0, 0.0,
        0.0, 0.0,

        0.5, 1.0,
        1.0, 0.0,
        0.0, 0.0,

        0.5, 1.0,
        1.0, 0.0,
        0.0, 0.0,
    ]);
    pyramid.initialize(pyramidVertices, pyramidTexCoords, 'Pyramid.jpg');

    var cubeVertices = new Float32Array([
        // front
        0.5, 0.5, 0.5,
        -0.5, 0.5, 0.5,
        -0.5, -0.5, 0.5,
        0.5, -0.5, 0.5,
        // right
        0.5, 0.5, -0.5,
        0.5, 0.5, 0.5,
        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5,
        // left
        -0.5, 0.5, 0.5,
        -0.5, 0.5, -0.5,
        -0.5, -0.5, -0.5,
        -0.5, -0.5, 0.5,
        // back
        -0.5, 0.5, -0.5,
        0.5, 0.5, -0.5,
        0.5, -0.5, -0.5,
        -0.5, -0.5, -0.5,
        // top
        0.5, 0.5, -0.5,
        -0.5, 0.5, -0.5,
        -0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        // bottom
        0.5, -0.5, 0.5,
        -0.5, -0.5, 0.5,
        -0.5, -0.5, -0.5,
        0.5, -0.5, -0.5
    ]);
    var cubeTexCoords = new Float32Array([
        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0,

        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0,

        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0,

        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0,

        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0,

        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0
    ]);
    cube.initialize(cubeVertices, cubeTexCoords, 'Cube.png');

    // set color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    projectionMatrix = mat4.create();
}

function uninitialize()
{
    cube.uninitialize();
    pyramid.uninitialize();

    if (shaderProgramObject)
    {
        if (fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject)
        {
            gl.deleteShader(vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject = null;
        }

        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}

var angleDeg = 0.0;
function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    angleDeg += 1.0;

    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    var angleRad = angleDeg * 3.14 / 180.0;

    // draw pyramid
    mat4.translate(modelViewMatrix, mat4.create(), vec3.fromValues(-1.0, 0.0, -3.0));
    mat4.rotate(modelViewMatrix, modelViewMatrix, angleRad, vec3.fromValues(0.0, 1.0, 0.0));
    mat4.multiply(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
    
    gl.activeTexture(gl.TEXTURE0);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    gl.uniform1i(texSampler2DUniform, 0);
    pyramid.draw();

    // draw pyramid
    mat4.translate(modelViewMatrix, mat4.create(), vec3.fromValues(1.0, 0.0, -3.0));
    mat4.rotate(modelViewMatrix, modelViewMatrix, angleRad, vec3.fromValues(1.0, 0.0, 0.0));
    mat4.rotate(modelViewMatrix, modelViewMatrix, angleRad, vec3.fromValues(0.0, 1.0, 0.0));
    mat4.rotate(modelViewMatrix, modelViewMatrix, angleRad, vec3.fromValues(0.0, 0.0, 1.0));
    mat4.multiply(modelViewProjectionMatrix, projectionMatrix, modelViewMatrix);
    gl.activeTexture(gl.TEXTURE0);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    gl.uniform1i(texSampler2DUniform, 0);
    cube.draw();

    gl.useProgram(null);

    // animation loop
    requestAnimationFrame(draw, canvas);
}