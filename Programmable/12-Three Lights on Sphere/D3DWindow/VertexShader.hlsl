cbuffer ConstantBuffer
{
    float4x4 world_matrix;
    float4x4 view_matrix;
    float4x4 projection_matrix;

    uint light_mode;

    float4 red_La;
    float4 red_Ld;
    float4 red_Ls;
    float4 red_light_position;

    float4 green_La;
    float4 green_Ld;
    float4 green_Ls;
    float3 green_light_position;

    float4 blue_La;
    float4 blue_Ld;
    float4 blue_Ls;
    float3 blue_light_position;

    float4 Ka;
    float4 Kd;
    float4 Ks;
    float material_shininess;
};

struct vertex_shader_output
{
    float4 phong_ads_color : VOUT1;
    float3 transformed_normals : VOUT2;
    float3 red_light_direction : VOUT3;
    float3 green_light_direction : VOUT4;
    float3 blue_light_direction : VOUT5;
    float3 viewer_vector : VOUT6;
    float4 position : SV_POSITION;
};

vertex_shader_output main(float3 vPosition : POSITION, float3 vNormal : NORMAL)
{
    vertex_shader_output vsout;
    if (light_mode == 1 || light_mode == 2)
    {
        // per-vertex lighting
        float4 eye_coordinates = mul(view_matrix, mul(world_matrix, float4(vPosition, 1.0)));
        vsout.transformed_normals = normalize(mul((float3x3)(mul(view_matrix, world_matrix)), vNormal));
        vsout.viewer_vector = normalize(-eye_coordinates.xyz);

        vsout.phong_ads_color = float4(0.0, 0.0, 0.0, 1.0);

        // calculate red light components
        {
            vsout.red_light_direction = normalize((float3)(red_light_position) - eye_coordinates.xyz);

            // calculate effective light components
            float4 ambient = red_La * Ka;

            float tn_dot_ld = max(dot(vsout.transformed_normals, vsout.red_light_direction), 0.0);
            float4 diffuse = red_Ld * Kd * tn_dot_ld;

            float3 reflection_vector = reflect(-vsout.red_light_direction, vsout.transformed_normals);
            float4 specular = red_Ls * Ks * pow(max(dot(reflection_vector, vsout.viewer_vector), 0.0), material_shininess);

            vsout.phong_ads_color += ambient + diffuse + specular;
        }
        // calculate green light components
        {
            vsout.green_light_direction = normalize(float3(green_light_position) - eye_coordinates.xyz);

            // calculate effective light components
            float4 ambient = green_La * Ka;

            float tn_dot_ld = max(dot(vsout.transformed_normals, vsout.green_light_direction), 0.0);
            float4 diffuse = green_Ld * Kd * tn_dot_ld;

            float3 reflection_vector = reflect(-vsout.green_light_direction, vsout.transformed_normals);
            float4 specular = green_Ls * Ks * pow(max(dot(reflection_vector, vsout.viewer_vector), 0.0), material_shininess);

            vsout.phong_ads_color += (ambient + diffuse + specular);
        }
        // calculate blue light components
        {
            vsout.blue_light_direction = normalize(float3(blue_light_position) - eye_coordinates.xyz);

            // calculate effective light components
            float4 ambient = blue_La * Ka;

            float tn_dot_ld = max(dot(vsout.transformed_normals, vsout.blue_light_direction), 0.0);
            float4 diffuse = blue_Ld * Kd * tn_dot_ld;

            float3 reflection_vector = reflect(-vsout.blue_light_direction, vsout.transformed_normals);
            float4 specular = blue_Ls * Ks * pow(max(dot(reflection_vector, vsout.viewer_vector), 0.0), material_shininess);

            vsout.phong_ads_color += (ambient + diffuse + specular);
        }
    }
    else
    {
        vsout.phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);
    }

    vsout.position = mul(projection_matrix, mul(view_matrix, mul(world_matrix, float4(vPosition, 1.0))));
    return vsout;
}