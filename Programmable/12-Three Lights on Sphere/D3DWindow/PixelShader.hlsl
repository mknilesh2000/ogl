cbuffer ConstantBuffer
{
    float4x4 world_matrix;
    float4x4 view_matrix;
    float4x4 projection_matrix;

    uint light_mode;

    float4 red_La;
    float4 red_Ld;
    float4 red_Ls;
    float4 red_light_position;

    float4 green_La;
    float4 green_Ld;
    float4 green_Ls;
    float3 green_light_position;

    float4 blue_La;
    float4 blue_Ld;
    float4 blue_Ls;
    float3 blue_light_position;

    float4 Ka;
    float4 Kd;
    float4 Ks;
    float material_shininess;
};

struct vertex_shader_output
{
    float4 phong_ads_color : VOUT1;
    float3 transformed_normals : VOUT2;
    float3 red_light_direction : VOUT3;
    float3 green_light_direction : VOUT4;
    float3 blue_light_direction : VOUT5;
    float3 viewer_vector : VOUT6;
    float4 position : SV_POSITION;
};

float4 main(vertex_shader_output inputFromVS) : SV_TARGET
{
    float4 outcolor = float4(1.0, 1.0, 1.0, 1.0);
    if (light_mode == 1)
    {
        outcolor = inputFromVS.phong_ads_color;
    }
    else if (light_mode == 2)
    {
        {
            // per-fragment lighting
            float3 normalized_transformed_normals = normalize(inputFromVS.transformed_normals);
            float3 normalized_light_direction = normalize(inputFromVS.red_light_direction);
            float3 normalized_viewer_vector = normalize(inputFromVS.viewer_vector);

            // calculate effective light components
            float3 ambient = (float3)(red_La * Ka);

            float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);
            float3 diffuse = (float3)(red_Ld * Kd * tn_dot_ld);

            float3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);
            float3 specular = (float3)(red_Ls * Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), material_shininess));

            outcolor = float4(ambient + diffuse + specular, 1.0);
        }
        {
            // per-fragment lighting
            float3 normalized_transformed_normals = normalize(inputFromVS.transformed_normals);
            float3 normalized_light_direction = normalize(inputFromVS.green_light_direction);
            float3 normalized_viewer_vector = normalize(inputFromVS.viewer_vector);

            // calculate effective light components
            float3 ambient = (float3)(green_La * Ka);

            float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);
            float3 diffuse = (float3)(green_Ld * Kd * tn_dot_ld);

            float3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);
            float3 specular = (float3)(green_Ls * Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), material_shininess));

            outcolor += float4(ambient + diffuse + specular, 1.0);
        }
        {
            // per-fragment lighting
            float3 normalized_transformed_normals = normalize(inputFromVS.transformed_normals);
            float3 normalized_light_direction = normalize(inputFromVS.blue_light_direction);
            float3 normalized_viewer_vector = normalize(inputFromVS.viewer_vector);

            // calculate effective light components
            float3 ambient = (float3)(blue_La * Ka);

            float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);
            float3 diffuse = (float3)(blue_Ld * Kd * tn_dot_ld);

            float3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);
            float3 specular = (float3)(blue_Ls * Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), material_shininess));

            outcolor += float4(ambient + diffuse + specular, 1.0);
        }
    }
    return outcolor;
}
