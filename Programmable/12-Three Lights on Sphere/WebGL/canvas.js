//onload function
var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObjectPerVertex;
var fragmentShaderObjectPerVertex;
var shaderProgramObjectPerVertex;

var vertexShaderObjectPerFragment;
var fragmentShaderObjectPerFragment;
var shaderProgramObjectPerFragment;

var light_ambient = [0.0, 0.0, 0.0];

var light_diffuse0 = [1.0, 0.0, 0.0];
var light_specular0 = [1.0, 0.0, 0.0];
var light_position0 = [0.0, 100.0, 100.0, 1.0];

var light_diffuse1 = [0.0, 1.0, 0.0];
var light_specular1 = [0.0, 1.0, 0.0];
var light_position1 = [100.0, 0.0, 100.0, 1.0];

var light_diffuse2 = [0.0, 0.0, 1.0];
var light_specular2 = [0.0, 0.0, 1.0];
var light_position2 = [100.0, 100.0, 0.0, 1.0];

var material_ambient = [0.0, 0.0, 0.0];
var material_diffuse = [1.0, 1.0, 1.0];
var material_specular = [1.0, 1.0, 1.0];
var material_shininess = 50.0;

var sphere = null;

var perspectiveProjectionMatrix;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var laUniform0, ldUniform0, lsUniform0, lightPositionUniform0;
var laUniform1, ldUniform1, lsUniform1, lightPositionUniform1;
var laUniform2, ldUniform2, lsUniform2, lightPositionUniform2;

var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var LKeyPressedUniform;

var bLKeyPressed = false;
var bFKeyPressed = false;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame || 
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

var angle = 0.0;	
	
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed :( \n");
	else 
		console.log("Obtaining Canvas Passes :) \n");
	
	//print canvas width and height on console
	console.log("Canvas width::" +canvas.width+ " and Canvas height::" +canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initailize webGL
	init();
	
	resize();
	draw();
}

function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement || 
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document. msFullscreenElement||
		null;
		
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullScreen = false;
	}
}

function init()
{
	//code 
	//get webGL 2.0 contextgl 
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	
	//var vertexShaderSourceCodePerVertex = 
	//"#version 300 es"							+
	//"\n"										+
	//"in vec4 vPosition;"						+
	//"in vec3 vNormal;"							+		
	//"uniform mat4 u_model_matrix;"				+
	//"uniform mat4 u_view_matrix;"				+
	//"uniform mat4 u_projection_matrix;"			+
	//"uniform mediump int u_LKeyPressed;"		+	
	//"uniform vec3 u_La[3];"						+
	//"uniform vec3 u_Ld[3];"						+
	//"uniform vec3 u_Ls[3];"						+
	//"uniform vec4 u_light_position[3];"			+
	//"uniform vec3 u_Ka;"						+
	//"uniform vec3 u_Kd;"						+
	//"uniform vec3 u_Ks;"						+
	//"uniform float u_material_shininess;"		+
	//"out vec3 phong_ads_color;"					+
	//"void main(void)"							+
	//"{"											+
	//"if(u_LKeyPressed == 1)"					+
	//"{"											+
	//"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" 						+
	//"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"		+
	//"int i = 0;"																			+
	//"vec3 light_direction;"																	+
	//"float tn_dot_ld;"																		+
	//"vec3 ambient;"																			+
	//"vec3 diffuse;"																			+
	//"vec3 reflection_vector;"																+
	//"vec3 viewer_vector;"																	+
	//"vec3 specular;"																		+
	//"vec3 phong_ads[3];"																	+
	//"for(i = 0; i < 3; i++)"																+
	//"{"																						+
	//"light_direction = normalize(vec3(u_light_position[i]) - eye_coordinates.xyz);"			+
	//"tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);"						+
	//"ambient = u_La[i] * u_Ka;"																+
	//"diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"													+
	//"reflection_vector = reflect(-light_direction, transformed_normals);"					+
	//"viewer_vector = normalize(-eye_coordinates.xyz);"										+
	//"specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"+
	//"phong_ads[i] = ambient + diffuse + specular;"											+
	//"}"																						+
	//"phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];"							+
	//"}"																						+
	//"else"																					+
	//"{"																						+
	//"phong_ads_color = vec3(1.0, 1.0, 1.0);"												+
	//"}"																						+
	//"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"		+
	//"}";

	//vertexShaderObjectPerVertex = gl.createShader(gl.VERTEX_SHADER);
	//gl.shaderSource(vertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
	//gl.compileShader(vertexShaderObjectPerVertex);
	
	//if(gl.getShaderParameter(vertexShaderObjectPerVertex, gl.COMPILE_STATUS) == false)
	//{
	//	var error = gl.getShaderInfoLog(vertexShaderObjectPerVertex)
	//	if(error.length > 0)
	//	{
	//		alert("vertex shader");
	//		alert(error);
	//		uninitialize();
	//	}
	//}

	//var fragmentShaderSourceCodePerVertex = 
	//"#version 300 es"			+
	//"\n"						+
	//"precision highp float;"	+
	//"in vec3 phong_ads_color;"	+
	//"out vec4 FragColor;"		+
	//"void main(void)"			+
	//"{"							+
	//"FragColor = vec4(phong_ads_color, 1.0);" +
	//"}";
	
	//fragmentShaderObjectPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
	//gl.shaderSource(fragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
	//gl.compileShader(fragmentShaderObjectPerVertex);
	
	//if(gl.getShaderParameter(fragmentShaderObjectPerVertex, gl.COMPILE_STATUS) == false)
	//{
	//	var error = gl.getShaderInfoLog(fragmentShaderObjectPerVertex)
	//	if(error.length > 0)
	//	{
	//		alert("fragment shader");
	//		alert(error);
	//		uninitialize();
	//	}
	//}

	////shader program
	//shaderProgramObjectPerVertex = gl.createProgram();
	//gl.attachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
	//gl.attachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
	
	//gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	//gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

	//gl.linkProgram(shaderProgramObjectPerVertex);
	//if(!gl.getProgramParameter(shaderProgramObjectPerVertex, gl.LINK_STATUS))
	//{
	//	var error = gl.getProgramInfoLog(shaderProgramObjectPerVertex);
	//	if(error.length > 0)
	//	{
	//		alert(error);
	//		uninitialize();
	//	}
	//}
    //modelMatrixUniform =gl.getUniformLocation(shaderProgramObjectPerVertex,"u_model_matrix");
    //viewMatrixUniform =gl.getUniformLocation(shaderProgramObjectPerVertex,"u_view_matrix");
    //projectionMatrixUniform =gl.getUniformLocation(shaderProgramObjectPerVertex,"u_projection_matrix");
		
	//LKeyPressedUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_LKeyPressed");

	//laUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[0]");
	//ldUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[0]");
	//lsUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[0]");
	//lightPositionUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[0]");

	//laUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[1]");
	//ldUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[1]");
	//lsUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[1]");
	//lightPositionUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[1]");

	//laUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[2]");
	//ldUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[2]");
	//lsUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[2]");
	//lightPositionUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[2]");

	//kaUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ka");
	//kdUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Kd");
	//ksUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ks");
	//materialShininessUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_material_shininess");


	var vertexShaderSourceCodePerFragment = 
	"#version 300 es"							+
	"\n"										+
	"in vec4 vPosition;"						+
	"in vec3 vNormal;"							+		
	"uniform mat4 u_model_matrix;"				+
	"uniform mat4 u_view_matrix;"				+
	"uniform mat4 u_projection_matrix;"			+
	"uniform mediump int u_LKeyPressed;"		+	
	"uniform vec4 u_light_position[3];"			+
	"out vec3 transformed_normals;"				+
	"out vec3 light_direction[3];"				+
	"out vec3 viewer_vector;"					+
	"void main(void)"							+
	"{"											+
	"if(u_LKeyPressed == 1)"				+
	"{"											+
	"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"					+
	"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"					+
	"int i;"																				+
	"for(i = 0;i<3;i++)"																	+
	"{"																						+
	"light_direction[i] = u_light_position[i].xyz - eye_coordinates.xyz;"					+
	"}"																						+
	"viewer_vector = -eye_coordinates.xyz;"													+
	"}"																						+
	"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"		+
	"}";

	vertexShaderObjectPerFragment = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObjectPerFragment, vertexShaderSourceCodePerFragment);
	gl.compileShader(vertexShaderObjectPerFragment);
	
	if(gl.getShaderParameter(vertexShaderObjectPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObjectPerFragment)
		if(error.length > 0)
		{
			alert("vertex shader");
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCodePerFragment = 
	"#version 300 es"			+
	"\n"						+
	"precision highp float;"	+
	"in vec3 phong_ads_color;"	+
	"in vec3 transformed_normals;"				+
	"in vec3 light_direction[3];"				+
	"in vec3 viewer_vector;"					+
	"out vec4 FragColor;"						+
	"uniform int u_LKeyPressed;"				+
	"uniform vec3 u_La[3];"						+
	"uniform vec3 u_Ld[3];"						+
	"uniform vec3 u_Ls[3];"						+
	"uniform vec3 u_Ka;"						+
	"uniform vec3 u_Kd;"						+
	"uniform vec3 u_Ks;"						+
	"uniform float u_material_shininess;"		+
	"void main(void)"												+
	"{"																+
	"if(u_LKeyPressed == 1)"									+
	"{"																+
	"vec3 normalized_transformed_normals = normalize(transformed_normals);"			+
	"vec3 normalized_light_direction[3];"											+
	"int i;"																		+
	"vec3 ambient;"													+
	"float tn_dot_ld;"		+
	"vec3 diffuse;"									+
	"vec3 reflection_vector;"														+
	"vec3 specular;"																+
	"vec3 phong_ads[3];"															+
	"for(i=0;i<3;i++)"																+
	"{"																				+
	"normalized_light_direction[i] = normalize(light_direction[i]);"				+
	"}"																				+
	"vec3 normalized_viewer_vector = normalize(viewer_vector);"						+
	"for(i=0;i<3;i++)"																+
	"{"																				+
	"ambient = u_La[i] * u_Ka;"														+
	"tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction[i]), 0.0);"		+
	"vec3 diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"									+
	"vec3 reflection_vector = reflect(-normalized_light_direction[i], normalized_transformed_normals);"	+
	"vec3 specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
	"phong_ads[i] = ambient + diffuse + specular;"									+
	"}"																				+
	"vec3 phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];"			+
	"FragColor = vec4(phong_ads_color, 1.0);"										+
	"}"																				+
	"else"																			+
	"{"																				+
	"FragColor = vec4(1.0,1.0,1.0,1.0);"											+
	"}"																				+
	"}";
	
	fragmentShaderObjectPerFragment = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObjectPerFragment, fragmentShaderSourceCodePerFragment);
	gl.compileShader(fragmentShaderObjectPerFragment);
	
	if(gl.getShaderParameter(fragmentShaderObjectPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObjectPerFragment)
		if(error.length > 0)
		{
			alert("fragment shader");
			alert(error);
			uninitialize();
		}
	}

	//shader program
	shaderProgramObjectPerFragment = gl.createProgram();
	gl.attachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
	gl.attachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);
	
	gl.bindAttribLocation(shaderProgramObjectPerFragment, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObjectPerFragment, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

	gl.linkProgram(shaderProgramObjectPerFragment);
	if(!gl.getProgramParameter(shaderProgramObjectPerFragment, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

    modelMatrixUniform =gl.getUniformLocation(shaderProgramObjectPerFragment,"u_model_matrix");
    viewMatrixUniform =gl.getUniformLocation(shaderProgramObjectPerFragment,"u_view_matrix");
    projectionMatrixUniform =gl.getUniformLocation(shaderProgramObjectPerFragment,"u_projection_matrix");
		
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_LKeyPressed");

	laUniform0 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_La[0]");
	ldUniform0 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ld[0]");
	lsUniform0 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ls[0]");
	lightPositionUniform0 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position[0]");

	laUniform1 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_La[1]");
	ldUniform1 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ld[1]");
	lsUniform1 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ls[1]");
	lightPositionUniform1 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position[1]");

	laUniform2 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_La[2]");
	ldUniform2 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ld[2]");
	lsUniform2 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ls[2]");
	lightPositionUniform2 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position[2]");

	kaUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_material_shininess");


	var vertexShaderSourceCodePerVertex =
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec3 vNormal;" +
	"uniform mat4 u_model_matrix;" +
	"uniform mat4 u_view_matrix;" +
	"uniform mat4 u_projection_matrix;" +
	"uniform mediump int u_LKeyPressed;" +
	"uniform vec3 u_La[3];" +
	"uniform vec3 u_Ld[3];" +
	"uniform vec3 u_Ls[3];" +
	"uniform vec4 u_light_position[3];" +
	"uniform vec3 u_Ka;" +
	"uniform vec3 u_Kd;" +
	"uniform vec3 u_Ks;" +
	"uniform float u_material_shininess;" +
	"out vec3 phong_ads_color;" +
	"void main(void)" +
	"{" +
	"if(u_LKeyPressed == 1)" +
	"{" +
	"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
	"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
	"int i = 0;" +
	"vec3 light_direction;" +
	"float tn_dot_ld;" +
	"vec3 ambient;" +
	"vec3 diffuse;" +
	"vec3 reflection_vector;" +
	"vec3 viewer_vector;" +
	"vec3 specular;" +
	"vec3 phong_ads[3];" +
	"for(i = 0; i < 3; i++)" +
	"{" +
	"light_direction = normalize(vec3(u_light_position[i]) - eye_coordinates.xyz);" +
	"tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" +
	"ambient = u_La[i] * u_Ka;" +
	"diffuse = u_Ld[i] * u_Kd * tn_dot_ld;" +
	"reflection_vector = reflect(-light_direction, transformed_normals);" +
	"viewer_vector = normalize(-eye_coordinates.xyz);" +
	"specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" +
	"phong_ads[i] = ambient + diffuse + specular;" +
	"}" +
	"phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];" +
	"}" +
	"else" +
	"{" +
	"phong_ads_color = vec3(1.0, 1.0, 1.0);" +
	"}" +
	"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
	"}";

	vertexShaderObjectPerVertex = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
	gl.compileShader(vertexShaderObjectPerVertex);

	if (gl.getShaderParameter(vertexShaderObjectPerVertex, gl.COMPILE_STATUS) == false) {
	    var error = gl.getShaderInfoLog(vertexShaderObjectPerVertex)
	    if (error.length > 0) {
	        alert("vertex shader");
	        alert(error);
	        uninitialize();
	    }
	}

	var fragmentShaderSourceCodePerVertex =
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec3 phong_ads_color;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
	"FragColor = vec4(phong_ads_color, 1.0);" +
	"}";

	fragmentShaderObjectPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
	gl.compileShader(fragmentShaderObjectPerVertex);

	if (gl.getShaderParameter(fragmentShaderObjectPerVertex, gl.COMPILE_STATUS) == false) {
	    var error = gl.getShaderInfoLog(fragmentShaderObjectPerVertex)
	    if (error.length > 0) {
	        alert("fragment shader");
	        alert(error);
	        uninitialize();
	    }
	}

    //shader program
	shaderProgramObjectPerVertex = gl.createProgram();
	gl.attachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
	gl.attachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

	gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

	gl.linkProgram(shaderProgramObjectPerVertex);
	if (!gl.getProgramParameter(shaderProgramObjectPerVertex, gl.LINK_STATUS)) {
	    var error = gl.getProgramInfoLog(shaderProgramObjectPerVertex);
	    if (error.length > 0) {
	        alert(error);
	        uninitialize();
	    }
	}

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_projection_matrix");

	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_LKeyPressed");

	laUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[0]");
	ldUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[0]");
	lsUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[0]");
	lightPositionUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[0]");

	laUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[1]");
	ldUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[1]");
	lsUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[1]");
	lightPositionUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[1]");

	laUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[2]");
	ldUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[2]");
	lsUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[2]");
	lightPositionUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[2]");

	kaUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_material_shininess");

	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);
	
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code 
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0, perspectiveProjectionMatrix);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	 if(bFKeyPressed)
		gl.useProgram(shaderProgramObjectPerFragment);
	 else
    	 gl.useProgram(shaderProgramObjectPerVertex);
		
	if(bLKeyPressed == true)
	{
		gl.uniform1i(LKeyPressedUniform, 1);
		
		gl.uniform3fv(laUniform0, light_ambient);
		gl.uniform3fv(ldUniform0, light_diffuse0);
		gl.uniform3fv(lsUniform0, light_specular0);
		gl.uniform4fv(lightPositionUniform0, light_position0);

		gl.uniform3fv(laUniform1, light_ambient);
		gl.uniform3fv(ldUniform1, light_diffuse1);
		gl.uniform3fv(lsUniform1, light_specular1);
		gl.uniform4fv(lightPositionUniform1, light_position1);
		
		gl.uniform3fv(laUniform2, light_ambient);
		gl.uniform3fv(ldUniform2, light_diffuse2);
		gl.uniform3fv(lsUniform2, light_specular2);
		gl.uniform4fv(lightPositionUniform2, light_position2);

		gl.uniform3fv(kaUniform, material_ambient);
		gl.uniform3fv(kdUniform, material_diffuse);
		gl.uniform3fv(ksUniform, material_specular);
		gl.uniform1f(materialShininessUniform, material_shininess);	
	}
	else
	{
		gl.uniform1i(LKeyPressedUniform, 0);
	}
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

	sphere.draw();
	
	gl.useProgram(null);

	angle += 0.01;
	if (angle >= 2*3.145)
	{
		angle = 0.0;
	}
	light_position0[1] = 100.0 * Math.cos(angle);
	light_position0[2] = 100.0 * Math.sin(angle);

	light_position1[0] = 100.0 * Math.cos(angle);
	light_position1[2] = 100.0 * Math.sin(angle);

	light_position2[0] = 100.0 * Math.cos(angle);
	light_position2[1] = 100.0 * Math.sin(angle);
	
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}

	if(shaderProgramObjectPerVertex)
	{
		if(fragmentShaderObjectPerVertex)
		{
			gl.detachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
			gl.deleteShader(fragmentShaderObjectPerVertex);
			fragmentShaderObjectPerVertex = null;
		}
		
		if(vertexShaderObjectPerVertex)
		{
			gl.detachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
			gl.deleteShader(vertexShaderObjectPerVertex);
			vertexShaderObjectPerVertex = null;
		}
		
		gl.deleteProgram(shaderProgramObjectPerVertex);
		shaderProgramObjectPerVertex = null;
	}
	
	if(shaderProgramObjectPerFragment)
	{
		if(fragmentShaderObjectPerFragment)
		{
			gl.detachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);
			gl.deleteShader(fragmentShaderObjectPerFragment);
			fragmentShaderObjectPerFragment = null;
		}
		
		if(vertexShaderObjectPerFragment)
		{
			gl.detachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
			gl.deleteShader(vertexShaderObjectPerFragment);
			vertexShaderObjectPerFragment = null;
		}
		
		gl.deleteProgram(shaderProgramObjectPerFragment);
		shaderProgramObjectPerFragment = null;
	}
}

function keyDown(event)
{
	//code
    switch(event.keyCode)
    {
        case 81://q
            uninitialize();
            window.close();
            break;
        case 27:
            toggleFullScreen();
            break;	
        case 76:
            if(bLKeyPressed == false)
                bLKeyPressed = true;
            else
                bLKeyPressed = false;
            break;
        case 70:
            if (bFKeyPressed == false) {
                modelMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_model_matrix");
                viewMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_view_matrix");
                projectionMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_projection_matrix");

                LKeyPressedUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_LKeyPressed");

                laUniform0 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_La[0]");
                ldUniform0 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ld[0]");
                lsUniform0 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ls[0]");
                lightPositionUniform0 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position[0]");

                laUniform1 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_La[1]");
                ldUniform1 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ld[1]");
                lsUniform1 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ls[1]");
                lightPositionUniform1 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position[1]");

                laUniform2 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_La[2]");
                ldUniform2 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ld[2]");
                lsUniform2 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ls[2]");
                lightPositionUniform2 = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position[2]");

                kaUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ka");
                kdUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Kd");
                ksUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_Ks");
                materialShininessUniform = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_material_shininess");

                bFKeyPressed = true;
            }
            else {
                modelMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_model_matrix");
                viewMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_view_matrix");
                projectionMatrixUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_projection_matrix");

                LKeyPressedUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_LKeyPressed");

                laUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[0]");
                ldUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[0]");
                lsUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[0]");
                lightPositionUniform0 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[0]");

                laUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[1]");
                ldUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[1]");
                lsUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[1]");
                lightPositionUniform1 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[1]");

                laUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_La[2]");
                ldUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ld[2]");
                lsUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ls[2]");
                lightPositionUniform2 = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position[2]");

                kaUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ka");
                kdUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Kd");
                ksUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_Ks");
                materialShininessUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_material_shininess");

                bFKeyPressed = false;
            }
            break;
		

	}
}

function mouseDown()
{
	//code 
}