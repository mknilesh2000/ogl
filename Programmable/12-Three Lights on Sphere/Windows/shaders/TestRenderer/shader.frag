#version 450 core

// from Vertex shader
in vec3 phong_ads_color;
in vec3 transformed_normals;
in vec3 red_light_direction;
in vec3 green_light_direction;
in vec3 blue_light_direction;
in vec3 viewer_vector;

// lighting variables
uniform int u_lightMode;

// red light
vec3 u_red_La = vec3(0.0, 0.0, 0.0);
vec3 u_red_Ld = vec3(1.0, 0.0, 0.0);
vec3 u_red_Ls = vec3(1.0, 0.0, 0.0);

// green light
vec3 u_green_La = vec3(0.0, 0.0, 0.0);
vec3 u_green_Ld = vec3(0.0, 1.0, 0.0);
vec3 u_green_Ls = vec3(0.0, 1.0, 0.0);

// blue light
vec3 u_blue_La = vec3(0.0, 0.0, 0.0);
vec3 u_blue_Ld = vec3(0.0, 0.0, 1.0);
vec3 u_blue_Ls = vec3(0.0, 0.0, 1.0);

// material constants
uniform vec3 u_Ka;                  // ambient reflective color intensity
uniform vec3 u_Kd;                  // diffused reflective color intensity
uniform vec3 u_Ks;                  // specular reflective color intensity
uniform float u_material_shininess; // shininess

// final color after lighting calculations applied
out vec4 FragColor;

void main(void)
{
    if (u_lightMode == 2)
    {
        // per-fragment lighting
        vec3 normalized_transformed_normals = normalize(transformed_normals);
        vec3 normalized_viewer_vector = normalize(viewer_vector);

        // calculate effective light components
        {
            vec3 normalized_light_direction = normalize(red_light_direction);
            vec3 ambient = u_red_La * u_Ka;

            float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);
            vec3 diffuse = u_red_Ld * u_Kd * tn_dot_ld;

            vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);
            vec3 specular = u_red_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);

            FragColor = vec4(ambient + diffuse + specular, 1.0);
        }
        {
            vec3 normalized_light_direction = normalize(green_light_direction);
            vec3 ambient = u_green_La * u_Ka;

            float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);
            vec3 diffuse = u_green_Ld * u_Kd * tn_dot_ld;

            vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);
            vec3 specular = u_green_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);

            FragColor += vec4(ambient + diffuse + specular, 1.0);
        }

        {
            vec3 normalized_light_direction = normalize(blue_light_direction);
            vec3 ambient = u_blue_La * u_Ka;

            float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);
            vec3 diffuse = u_blue_Ld * u_Kd * tn_dot_ld;

            vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);
            vec3 specular = u_blue_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);

            FragColor += vec4(ambient + diffuse + specular, 1.0);
        }
    }
    else
    {
        FragColor = vec4(phong_ads_color, 1.0);
    }
}