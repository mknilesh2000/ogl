#version 300 es

in vec4 vPosition;
in vec3 vNormal;
uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;

// lighting variables
uniform mediump int u_lightMode;

// red light
vec3 u_red_La = vec3(0.0, 0.0, 0.0);
vec3 u_red_Ld = vec3(1.0, 0.0, 0.0);
vec3 u_red_Ls = vec3(1.0, 0.0, 0.0);
uniform vec3 u_red_light_position;

// green light
vec3 u_green_La = vec3(0.0, 0.0, 0.0);
vec3 u_green_Ld = vec3(0.0, 1.0, 0.0);
vec3 u_green_Ls = vec3(0.0, 1.0, 0.0);
uniform vec3 u_green_light_position;

// blue light
vec3 u_blue_La = vec3(0.0, 0.0, 0.0);
vec3 u_blue_Ld = vec3(0.0, 0.0, 1.0);
vec3 u_blue_Ls = vec3(0.0, 0.0, 1.0);
uniform vec3 u_blue_light_position;

// material constants
uniform vec3 u_Ka;                  // ambient reflective color intensity
uniform vec3 u_Kd;                  // diffused reflective color intensity
uniform vec3 u_Ks;                  // specular reflective color intensity
uniform float u_material_shininess; // shininess

// out parameters to fragment shader
out vec3 phong_ads_color;
out vec3 transformed_normals;
out vec3 red_light_direction;
out vec3 green_light_direction;
out vec3 blue_light_direction;
out vec3 viewer_vector;

void main(void)
{
    if (u_lightMode == 1 || u_lightMode == 2)
    {
        // per-vertex lighting
        vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;
        transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);
        viewer_vector = normalize(-eye_coordinates.xyz);

        phong_ads_color = vec3(0.0, 0.0, 0.0);

        // calculate red light components
        {
            red_light_direction = normalize(vec3(u_red_light_position) - eye_coordinates.xyz);

            // calculate effective light components
            vec3 ambient = u_red_La * u_Ka;

            float tn_dot_ld = max(dot(transformed_normals, red_light_direction), 0.0);
            vec3 diffuse = u_red_Ld * u_Kd * tn_dot_ld;

            vec3 reflection_vector = reflect(-red_light_direction, transformed_normals);
            vec3 specular = u_red_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);

            phong_ads_color += ambient + diffuse + specular;
        }
        // calculate green light components
        {
            green_light_direction = normalize(vec3(u_green_light_position) - eye_coordinates.xyz);

            // calculate effective light components
            vec3 ambient = u_green_La * u_Ka;

            float tn_dot_ld = max(dot(transformed_normals, green_light_direction), 0.0);
            vec3 diffuse = u_green_Ld * u_Kd * tn_dot_ld;

            vec3 reflection_vector = reflect(-green_light_direction, transformed_normals);
            vec3 specular = u_green_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);

            phong_ads_color += (ambient + diffuse + specular);
        }
        // calculate blue light components
        {
            blue_light_direction = normalize(vec3(u_blue_light_position) - eye_coordinates.xyz);

            // calculate effective light components
            vec3 ambient = u_blue_La * u_Ka;

            float tn_dot_ld = max(dot(transformed_normals, blue_light_direction), 0.0);
            vec3 diffuse = u_blue_Ld * u_Kd * tn_dot_ld;

            vec3 reflection_vector = reflect(-blue_light_direction, transformed_normals);
            vec3 specular = u_blue_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);

            phong_ads_color += (ambient + diffuse + specular);
        }
    }
    else
    {
        phong_ads_color = vec3(1.0, 1.0, 1.0);
    }

    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;
}

