//
//  GLESView.m
//  Window
//
//  Created by Nilesh Mahajan on 27/05/18.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import <glm/gtx/transform.hpp>
#import <glm/gtc/type_ptr.hpp>
#import "Utils.h"
#import "Sphere.h"

#import "GLESView.h"

enum ATTRIBUTE
{
    VERTEX = 0,
    COLOR,
    NORMAL,
    TEXTURE0,
    ELEMENT,
    LAST
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Draw Members
    GLuint          m_vertexShaderObject;
    GLuint          m_fragmentShaderObject;
    GLuint          m_shaderProgramObject;
    
    GLuint          m_vaoSphere;
    GLuint          m_vboSphere[ATTRIBUTE::LAST];
    glm::mat4x4     m_perspectiveProjectionMatrix;
    
    // MVP
    GLuint          m_ModelMatrixUniform;
    GLuint          m_ViewMatrixUniform;
    GLuint          m_ProjectionMatrixUniform;
    
    // Light
    GLuint          m_lightEnableStateUniform;
    GLuint          m_redLightPositionUniform;
    GLuint          m_greenLightPositionUniform;
    GLuint          m_blueLightPositionUniform;
    
    // Material constants
    GLuint          m_KaUniform;
    GLuint          m_KdUniform;
    GLuint          m_KsUniform;
    GLuint          m_ShininessUniform;
    
    bool            m_enableAnimation;
    GLuint          m_LightMode;
    Sphere          m_sphere;
}

- (GLint) CheckObjectStatus:(GLuint)object forOperation:(GLenum)operation forObjectName:(const char *)objectName
{
    GLint status = GL_FALSE;
    bool isLinkOperation = operation == GL_LINK_STATUS;
    isLinkOperation ? glGetProgramiv(object, operation, &status) :
    glGetShaderiv(object, operation, &status);
    
    if (status == GL_FALSE)
    {
        printf("%s failed\n", objectName);
        GLint shaderInfoLogLength = 0;
        isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength) :
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);
        if (shaderInfoLogLength > 0)
        {
            GLchar *szInfoLog = (GLchar*) malloc(shaderInfoLogLength * sizeof(GLchar));
            if (szInfoLog != NULL)
            {
                memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                GLsizei written = 0;
                isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                printf("%s info Log :\n %s", objectName, szInfoLog);
                free(szInfoLog);
            }
        }
    }
    return status;
}

- (void) LoadSphereData
{
    float sphereVertices[1146];
    float sphereNormals[1146];
    float sphere_textures[764];
    short sphereElements[2280];
    m_sphere.getSphereVertexData(sphereVertices, sphereNormals, sphere_textures, sphereElements);
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoSphere);
    glBindVertexArray(m_vaoSphere);
    
    // prepare array buffer
    glGenBuffers(ATTRIBUTE::LAST, m_vboSphere);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphereVertices), sphereVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::NORMAL]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphereNormals), sphereNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::ELEMENT]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphereElements), sphereElements, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) initialize
{
    m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    ShaderFile vertexShader("shader.vert");
    glShaderSource(m_vertexShaderObject, 1, vertexShader.GetShaderSource(), nullptr);
    glCompileShader(m_vertexShaderObject);
    [self CheckObjectStatus:m_vertexShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Vertex Shader"];
    
    m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    ShaderFile fragmentShader("shader.frag");
    glShaderSource(m_fragmentShaderObject, 1, fragmentShader.GetShaderSource(), nullptr);
    glCompileShader(m_fragmentShaderObject);
    [self CheckObjectStatus:m_fragmentShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Fragment Shader"];
    
    m_shaderProgramObject = glCreateProgram();
    glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
    glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::NORMAL, "vNormal");
    glLinkProgram(m_shaderProgramObject);
    [self CheckObjectStatus:m_shaderProgramObject forOperation:GL_LINK_STATUS forObjectName:"Shader Program"];
    
    // get uniform location
    m_ModelMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_model_matrix");
    m_ViewMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_view_matrix");
    m_ProjectionMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_projection_matrix");
    
    m_lightEnableStateUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
    m_redLightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_red_light_position");
    m_greenLightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_green_light_position");
    m_blueLightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_blue_light_position");
    
    m_KaUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ka");
    m_KdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Kd");
    m_KsUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ks");
    m_ShininessUniform = glGetUniformLocation(m_shaderProgramObject, "u_material_shininess");
    
    [self LoadSphereData];
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        // initialize drawing objects
        [self initialize];
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    // code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(m_shaderProgramObject);
    
    // Draw Sphere
    {
        glm::mat4x4 modelMatrix = glm::translate(glm::mat4x4(), glm::vec3(0.0f, 0.0f, -2.0f));
        glm::mat4x4 viewMatrix;
        
        static GLfloat angle = 0.0f;
        if (m_enableAnimation)
        {
            angle += 0.01f;
        }
        
        glUniformMatrix4fv(m_ModelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));
        glUniformMatrix4fv(m_ViewMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniformMatrix4fv(m_ProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(m_perspectiveProjectionMatrix));
        glUniform1i(m_lightEnableStateUniform, m_LightMode);
        
        if (m_LightMode > 0)
        {
            GLfloat varying1 = 1000.0f * cos(angle);
            GLfloat varying2 = 1000.0f * sin(angle);
            
            // set light components
            glUniform3f(m_redLightPositionUniform, varying1, 0.0f, varying2);
            glUniform3f(m_greenLightPositionUniform, 0.0f, varying1, varying2);
            glUniform3f(m_blueLightPositionUniform, varying1, varying2, 0.0f);
            
            // set material components
            glUniform3f(m_KaUniform, 0.0f, 0.0f, 0.0f);
            glUniform3f(m_KdUniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(m_KsUniform, 1.0f, 1.0f, 1.0f);
            glUniform1f(m_ShininessUniform, 50.0f);
        }
        
        glBindVertexArray(m_vaoSphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboSphere[ATTRIBUTE::ELEMENT]);
        glDrawElements(GL_TRIANGLES, m_sphere.getNumberOfSphereElements(), GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
    }
    
    glUseProgram(0);

    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    GLfloat aspect = (GLfloat)width / (GLfloat)height;
    m_perspectiveProjectionMatrix = glm::perspective(45.0f, aspect, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    m_LightMode = (m_LightMode + 1) % 3;
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    m_enableAnimation = !m_enableAnimation;
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    // code
    if (m_vaoSphere)
    {
        glDeleteVertexArrays(1, &m_vaoSphere);
        m_vaoSphere = 0;
    }
    
    if (m_vboSphere[0] != 0)
    {
        glDeleteBuffers(ATTRIBUTE::LAST, m_vboSphere);
        memset(m_vboSphere, 0, sizeof(m_vboSphere));
    }
    
    if (m_shaderProgramObject)
    {
        glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
        glDeleteProgram(m_shaderProgramObject);
        m_shaderProgramObject = 0;
    }
    
    if (m_fragmentShaderObject)
    {
        glDeleteShader(m_fragmentShaderObject);
        m_fragmentShaderObject = 0;
    }
    
    if (m_vertexShaderObject)
    {
        glDeleteShader(m_vertexShaderObject);
        m_vertexShaderObject = 0;
    }

    if (depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if (colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if (defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if ([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}
@end
