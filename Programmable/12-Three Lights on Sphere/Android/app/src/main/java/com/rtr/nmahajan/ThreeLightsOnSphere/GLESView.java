package com.rtr.nmahajan.ThreeLightsOnSphere;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.view.GestureDetector;
import android.view.MotionEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by nmahajan on 28-01-2018.
 */

class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,
        GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener

{
    private final Context m_context;

    private GestureDetector m_gestureDetector;

    private int m_vertexShaderObject;
    private int m_fragmentShaderObject;
    private int m_shaderProgramObject;

    private int[] m_vaoSphere = new int[1];
    private int[] m_vboSphere = new int[3];

    private double m_angleSphere;

    private int m_modelMatrixUniform;
    private int m_viewMatrixUniform;
    private int m_projectionMatrixUniform;

    // lights related uniforms
    private int m_lightingModeUniform;
    private int m_LaUniform;
    private int m_LdUniform;
    private int m_LsUniform;

    private int m_KaUniform;
    private int m_KdUniform;
    private int m_KsUniform;
    private int m_shininessUniform;

    private int m_redLightPositionUniform;
    private int m_greenLightPositionUniform;
    private int m_blueLightPositionUniform;

    private float m_perspectiveProjectionMatrix[] = new float[16];
    private int m_lightingMode;
    private boolean m_enableAnimation;

    private Sphere m_sphere = new Sphere();

    /**
     * Standard View constructor. In order to render something, you
     * must call {@link #setRenderer} to register a renderer.
     *
     * @param context
     */
    public GLESView(Context context)
    {
        super(context);
        m_context = context;

        // set EGLContext to current supported version of OpenGL-ES
        setEGLContextClientVersion(3);

        // set renderer for drawing on GLSurfaceView
        setRenderer(this);

        // render the view only when there is change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        m_gestureDetector = new GestureDetector(context, this, null, false);
        m_gestureDetector.setOnDoubleTapListener(this);

        m_angleSphere = 0.0f;
        m_modelMatrixUniform = 0;
        m_projectionMatrixUniform = 0;

        m_lightingMode = 0;

        m_LaUniform = 0;
        m_LdUniform = 0;
        m_LsUniform = 0;

        m_KaUniform = 0;
        m_KdUniform = 0;
        m_KsUniform = 0;
        m_shininessUniform = 0;

        m_lightingModeUniform = 0;
        m_redLightPositionUniform = 0;
        m_greenLightPositionUniform = 0;
        m_blueLightPositionUniform = 0;

        m_enableAnimation = false;
    }

    /**
     * Called when the surface is created or recreated.
     * <p>
     * Called when the rendering thread
     * starts and whenever the EGL context is lost. The EGL context will typically
     * be lost when the Android device awakes after going to sleep.
     * <p>
     * Since this method is called at the beginning of rendering, as well as
     * every time the EGL context is lost, this method is a convenient place to put
     * code to create resources that need to be created when the rendering
     * starts, and that need to be recreated when the EGL context is lost.
     * Textures are an example of a resource that you might want to create
     * here.
     * <p>
     * Note that when the EGL context is lost, all OpenGL resources associated
     * with that context will be automatically deleted. You do not need to call
     * the corresponding "glDelete" methods such as glDeleteTextures to
     * manually delete these lost resources.
     * <p>
     *
     * @param gl     the GL interface. Use <code>instanceof</code> to
     *               test if the interface supports GL11 or higher interfaces.
     * @param config the EGLConfig of the created surface. Can be used
     */
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("OpenGL: " + glesVersion);

        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("GLSL: " + glslVersion);

        initialize(gl);
    }

    /**
     * Called when the surface changed size.
     * <p>
     * Called after the surface is created and whenever
     * the OpenGL ES surface size changes.
     * <p>
     * Typically you will set your viewport here. If your camera
     * is fixed then you could also set your projection matrix here:
     * <pre class="prettyprint">
     * void onSurfaceChanged(GL10 gl, int width, int height) {
     * gl.glViewport(0, 0, width, height);
     * // for a fixed camera, set the projection too
     * float ratio = (float) width / height;
     * gl.glMatrixMode(GL10.GL_PROJECTION);
     * gl.glLoadIdentity();
     * gl.glFrustumf(-ratio, ratio, -1, 1, 1, 10);
     * }
     * </pre>
     *
     * @param gl     the GL interface. Use <code>instanceof</code> to
     *               test if the interface supports GL11 or higher interfaces.
     * @param width
     * @param height
     */
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        resize(width, height);
    }

    /**
     * Called to draw the current frame.
     * <p>
     * This method is responsible for drawing the current frame.
     * <p>
     * The implementation of this method typically looks like this:
     * <pre class="prettyprint">
     * void onDrawFrame(GL10 gl) {
     * gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
     * //... other gl calls to render the scene ...
     * }
     * </pre>
     *
     * @param gl the GL interface. Use <code>instanceof</code> to
     *           test if the interface supports GL11 or higher interfaces.
     */
    @Override
    public void onDrawFrame(GL10 gl)
    {
        display();
    }

    // Handling onTouchEvent is very important since it triggers all gesture
    // related events
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        // forward the touch event to m_gestureDetector
        int eventAction = e.getAction();
        if (!m_gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);

        return true;
    }

    /**
     * Notified when a single-tap occurs.
     * <p>
     * Unlike {@link OnGestureListener#onSingleTapUp(MotionEvent)}, this
     * will only be called after the detector is confident that the user's
     * first tap is not followed by a second tap leading to a double-tap
     * gesture.
     *
     * @param e The down motion event of the single-tap.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        m_lightingMode = (m_lightingMode + 1) % 3;

        return true;
    }

    /**
     * Notified when a double-tap occurs.
     *
     * @param e The down motion event of the first tap of the double-tap.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // by default handled
        return true;
    }

    /**
     * Notified when an event within a double-tap gesture occurs, including
     * the down, move, and up events.
     *
     * @param e The motion event that occurred during the double-tap gesture.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        m_enableAnimation = !m_enableAnimation;
        return true;
    }

    /**
     * Notified when a tap occurs with the down {@link MotionEvent}
     * that triggered it. This will be triggered immediately for
     * every down event. All other events should be preceded by this.
     *
     * @param e The down motion event.
     */
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do not write anything here since we've already written onSingleTapConfirmed.
        return true;
    }

    /**
     * The user has performed a down {@link MotionEvent} and not performed
     * a move or up yet. This event is commonly used to provide visual
     * feedback to the user to let them know that their action has been
     * recognized i.e. highlight an element.
     *
     * @param e The down motion event
     */
    @Override
    public void onShowPress(MotionEvent e)
    {

    }

    /**
     * Notified when a tap occurs with the up {@link MotionEvent}
     * that triggered it.
     *
     * @param e The up motion event that completed the first tap
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        // by default handled
        return true;
    }

    /**
     * Notified when a scroll occurs with the initial on down {@link MotionEvent} and the
     * current move {@link MotionEvent}. The distance in x and y is also supplied for
     * convenience.
     *
     * @param e1        The first down motion event that started the scrolling.
     * @param e2        The move motion event that triggered the current onScroll.
     * @param distanceX The distance along the X axis that has been scrolled since the last
     *                  call to onScroll. This is NOT the distance between {@code e1}
     *                  and {@code e2}.
     * @param distanceY The distance along the Y axis that has been scrolled since the last
     *                  call to onScroll. This is NOT the distance between {@code e1}
     *                  and {@code e2}.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        // exit the app on scroll
        return true;
    }

    /**
     * Notified when a long press occurs with the initial on down {@link MotionEvent}
     * that trigged it.
     *
     * @param e The initial on down motion event that started the longpress.
     */
    @Override
    public void onLongPress(MotionEvent e)
    {
        uninitialize();
        System.exit(0);
    }

    /**
     * Notified of a fling event when it occurs with the initial on down {@link MotionEvent}
     * and the matching up {@link MotionEvent}. The calculated velocity is supplied along
     * the x and y axis in pixels per second.
     *
     * @param e1        The first down motion event that started the fling.
     * @param e2        The move motion event that triggered the current onFling.
     * @param velocityX The velocity of this fling measured in pixels per second
     *                  along the x axis.
     * @param velocityY The velocity of this fling measured in pixels per second
     *                  along the y axis.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return true;
    }

    private int loadGLTexture(int imageFileResourceId)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        // read the resource
        Bitmap bitmap = BitmapFactory.decodeResource(m_context.getResources(), imageFileResourceId, options);

        int[] texture = new int[1];

        // create a texture object to apply to model
        GLES32.glGenTextures(1, texture, 0);

        // indicate that pixel rows are tightly packed
        // defauls to stride of 4 which is kind of only good for RGBA or FLOAT types
        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);

        //bind with the texture
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);

        //set up filter and wrap modes for this texture object
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

        // load the bitmap into the bound texture
        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);

        // generate mipmap
        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);

        return texture[0];
    }

    private String getShaderSourceFromResource(int resourceId)
    {
        Resources res = m_context.getResources();
        StringBuilder source = new StringBuilder();

        String line;
        try
        {
            BufferedReader r = new BufferedReader(new InputStreamReader(res.openRawResource(resourceId)));
            while ((line = r.readLine()) != null)
            {
                source.append(line).append('\n');
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return source.toString();
    }

    private void initialize(GL10 gl)
    {
        //  ------------------------ Vertex Shader ------------------------------------- //
        m_vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        // provide shader source code
        GLES32.glShaderSource(m_vertexShaderObject, getShaderSourceFromResource(R.raw.vertexshader));
        // compile shader and check for errors
        GLES32.glCompileShader(m_vertexShaderObject);
        int[] iShaderCompilationStatus = new int[1];
        GLES32.glGetShaderiv(m_vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if (iShaderCompilationStatus[0] == GLES32.GL_FALSE)
        {
            int[] iShaderInfoLogLength = new int[1];
            GLES32.glGetShaderiv(m_vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iShaderInfoLogLength, 0);
            if (iShaderInfoLogLength[0] > 0)
            {
                System.out.println("Vertex shader compilation log");
                System.out.println(GLES32.glGetShaderInfoLog(m_vertexShaderObject));
                uninitialize();
                System.exit(0);
            }
        }

        //  ------------------------ Fragment Shader ------------------------------------- //
        m_fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        GLES32.glShaderSource(m_fragmentShaderObject, getShaderSourceFromResource(R.raw.fragshader));
        GLES32.glCompileShader(m_fragmentShaderObject);
        GLES32.glGetShaderiv(m_fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if (iShaderCompilationStatus[0] == GLES32.GL_FALSE)
        {
            int[] iShaderInfoLength = new int[1];
            GLES32.glGetShaderiv(m_fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iShaderInfoLength, 0);
            if (iShaderInfoLength[0] > 0)
            {
                System.out.println("Fragment shader compilation log:");
                System.out.println(GLES32.glGetShaderInfoLog(m_fragmentShaderObject));
                uninitialize();
                System.exit(0);
            }
        }

        //  ------------------------ Shader Program ------------------------------------- //
        m_shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
        GLES32.glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);

        GLES32.glBindAttribLocation(m_shaderProgramObject, GLESMacros.ATTRIBUTE_VERTEX, "vPosition");
        GLES32.glBindAttribLocation(m_shaderProgramObject, GLESMacros.ATTRIBUTE_NORMAL, "vNormal");

        // link program
        GLES32.glLinkProgram(m_shaderProgramObject);
        int[] iProgramLinkStatus = new int[1];
        GLES32.glGetProgramiv(m_shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            int[] iProgramInfoLength = new int[1];
            GLES32.glGetProgramiv(m_shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iProgramInfoLength, 0);
            if (iProgramInfoLength[0] > 0)
            {
                System.out.println("Shader program link failed. Link status");
                System.out.println(GLES32.glGetProgramInfoLog(m_shaderProgramObject));
                uninitialize();
                System.exit(0);
            }
        }

        // capture uniforms location for handy use
        m_modelMatrixUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_model_matrix");
        m_viewMatrixUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_view_matrix");
        m_projectionMatrixUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_projection_matrix");

        m_lightingModeUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
        m_LaUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_La");
        m_LdUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_Ld");
        m_LsUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_Ls");
        m_redLightPositionUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_red_light_position");
        m_greenLightPositionUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_green_light_position");
        m_blueLightPositionUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_blue_light_position");

        m_KaUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_Ka");
        m_KdUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_Kd");
        m_KsUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_Ks");
        m_shininessUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_material_shininess");

        // ---------------------- vertices, colors, shader attribs, vbo, vao initializations -----//
        loadSphereData();

        // ------------------------------- Drawing initialization ------------------------------- //
        // enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // enable backface culling to improve performance
        GLES32.glEnable(GLES20.GL_CULL_FACE);
        // set background color
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // initialize projection matrix to identity
        Matrix.setIdentityM(m_perspectiveProjectionMatrix, 0);
    }

    private void loadSphereData()
    {
        float sphereVertices[]=new float[1146];
        float sphereNormals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        m_sphere.getSphereVertexData(sphereVertices, sphereNormals, sphere_textures, sphere_elements);

        GLES32.glGenVertexArrays(1, m_vaoSphere, 0);
        GLES32.glBindVertexArray(m_vaoSphere[0]);

        GLES32.glGenBuffers(3, m_vboSphere, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, m_vboSphere[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphereVertices.length * 4); // zeroed-out buffer
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphereVertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphereVertices.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_VERTEX);

        // unbind vbo 0
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // bind vbo 1 which is a normals buffer
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, m_vboSphere[1]);

        byteBuffer = ByteBuffer.allocateDirect(sphereNormals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer normalsBuffer = byteBuffer.asFloatBuffer();
        normalsBuffer.put(sphereNormals);
        normalsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphereNormals.length * 4, normalsBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_NORMAL);

        GLES32.glBindVertexArray(0);

        // element vbo
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, m_vboSphere[2]);

        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());

        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                sphere_elements.length * 2,
                elementsBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

        // unbind vbo 0 followed by vao
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);
    }

    private void uninitialize()
    {
        // destroy vao
        if (m_vaoSphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, m_vaoSphere, 0);
            m_vaoSphere[0] = 0;
        }

        // delete vbo
        if (m_vboSphere[2] != 0)
        {
            GLES32.glDeleteBuffers(1, m_vboSphere, 2);
            m_vboSphere[2] = 0;
        }

        // delete vbo
        if (m_vboSphere[1] != 0)
        {
            GLES32.glDeleteBuffers(1, m_vboSphere, 1);
            m_vboSphere[1] = 0;
        }

        // delete vbo
        if (m_vboSphere[0] != 0)
        {
            GLES32.glDeleteBuffers(1, m_vboSphere, 0);
            m_vboSphere[0] = 0;
        }

        if (m_shaderProgramObject != 0)
        {
            // detach and destroy shaders
            GLES32.glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
            GLES32.glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);

            // delete shader program
            GLES32.glDeleteProgram(m_shaderProgramObject);
            m_shaderProgramObject = 0;
        }

        if (m_fragmentShaderObject != 0)
        {
            GLES32.glDeleteShader(m_fragmentShaderObject);
            m_fragmentShaderObject = 0;
        }

        if (m_vertexShaderObject != 0)
        {
            GLES32.glDeleteShader(m_vertexShaderObject);
            m_vertexShaderObject = 0;
        }
    }

    private void resize(int width, int height)
    {
        GLES32.glViewport(0, 0, width, height);

        if (height == 0)
            height = 1;

        float aspect = (float)width / (float)height;
        Matrix.perspectiveM(
                m_perspectiveProjectionMatrix,
                0,
                45.0f,
                aspect,
                0.1f,
                100.0f);
    }

    private void drawSphere()
    {
        float modelMatrix[] = new float[16];
        Matrix.setIdentityM(modelMatrix, 0);

        float viewMatrix[] = new float[16];
        Matrix.setIdentityM(viewMatrix, 0);

        // translate matrix
        Matrix.translateM(modelMatrix, 0,0.0f, 0.0f, -2.0f);

        if (m_enableAnimation)
        {
            m_angleSphere += 0.05f;
            if (m_angleSphere == 360.0f)
                m_angleSphere = 0.0f;
        }

        GLES32.glUniformMatrix4fv(m_modelMatrixUniform, 1, false, modelMatrix, 0);
        GLES32.glUniformMatrix4fv(m_viewMatrixUniform, 1, false, viewMatrix, 0);
        GLES32.glUniformMatrix4fv(m_projectionMatrixUniform, 1, false, m_perspectiveProjectionMatrix, 0);

        // lighting specific variables
        GLES32.glUniform1i(m_lightingModeUniform, m_lightingMode);

        if (m_lightingMode > 0)
        {
            float varying1 = (float)(1000.0f * Math.cos(m_angleSphere));
            float varying2 = (float)(1000.0f * Math.sin(m_angleSphere));

            // set light components
            GLES32.glUniform3f(m_redLightPositionUniform, varying1, 0.0f, varying2);
            GLES32.glUniform3f(m_greenLightPositionUniform, 0.0f, varying1, varying2);
            GLES32.glUniform3f(m_blueLightPositionUniform, varying1, varying2, 0.0f);

            // set material components
            GLES32.glUniform3f(m_KaUniform, 0.0f, 0.0f, 0.0f);
            GLES32.glUniform3f(m_KdUniform, 1.0f, 1.0f, 1.0f);
            GLES32.glUniform3f(m_KsUniform, 1.0f, 1.0f, 1.0f);
            GLES32.glUniform1f(m_shininessUniform, 50.0f);
        }

        GLES32.glBindVertexArray(m_vaoSphere[0]);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, m_vboSphere[2]);

        //draw either by glDrawArrays, glDrawTraiangles or glDrawElements
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, m_sphere.getNumberOfSphereElements(), GLES32.GL_UNSIGNED_SHORT, 0);

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(m_shaderProgramObject);

        drawSphere();

        GLES32.glUseProgram(0);

        // render/flush
        requestRender();
    }
}
