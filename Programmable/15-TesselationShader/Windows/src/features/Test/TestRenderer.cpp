#pragma once

#include "TestRenderer.h"
#include <glm/gtc/type_ptr.hpp>

namespace Features
{
    enum ATTRIBUTE
    {
        VERTEX = 0,
        COLOR,
        NORMAL,
        TEXTURE0
    };

    using namespace Interfaces;

#define EXIT_IF_ERROR(x) if (x == GL_FALSE) { __debugbreak(); return RENDERER_RESULT_ERROR; }
#define CALL_AND_RESET_IF_VALID(x, y) if (x != NULL) { y;  x = NULL; }

    TestRenderer::TestRenderer() :
        m_fp(nullptr),
        m_vertexShaderObject(0),
        m_fragmentShaderObject(0),
        m_shaderProgramObject(0),
        m_tesselationControlShaderObject(0),
        m_tesselationEvaluationShaderObject(0),
        m_numberOfStripsUniform(0),
        m_numberOfSegmentsUniform(0),
        m_numOfSegments(0),
        m_vao(0)
    {
        memset(m_vbo, 0, sizeof(m_vbo));
        fopen_s(&m_fp, "output.log", "w+");
        if (m_fp == nullptr)
            __debugbreak();
        fprintf(m_fp, "Opened log-file\n");
    }

    TestRenderer::~TestRenderer()
    {
        if (m_fp != nullptr)
        {
            fprintf(m_fp, "Closing log-file\n");
            fclose(m_fp);
            m_fp = nullptr;
        }
    }

    /// Method for retrieving name of the renderer
    /// @param rendererName buffer to be filled with the renderer name
    /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
    ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
    const char* TestRenderer::GetName()
    {
        static const char name[] = "TestRenderer";
        return name;
    }

    /// Method for performing one-time renderer initialization.
    /// Renderer can initialize global/static instances as part of this method
    /// @param window identifier of the window where drawing is directed
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::Initialize(Window window)
    {
        // vertex shader
        m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexShaderSourceCode =
            "#version 440" \
            "\n" \
            "in vec2 vPosition;" \
            "void main(void)" \
            "{" \
                "gl_Position = vec4(vPosition, 0.0, 1.0);" \
            "}";
        glShaderSource(m_vertexShaderObject, 1, &vertexShaderSourceCode, nullptr);
        glCompileShader(m_vertexShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_vertexShaderObject, GL_COMPILE_STATUS, "m_vertexShaderObject"));

        m_tesselationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);
        const GLchar *tesselationControlShaderSourceCode =
            "#version 440" \
            "\n" \
            "layout(vertices=4)out;" \
            "uniform int numberOfSegments;" \
            "uniform int numberOfStrips;" \
            "void main(void)" \
            "{" \
                "gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" \
                "gl_TessLevelOuter[0] = float(numberOfStrips);" \
                "gl_TessLevelOuter[1] = float(numberOfSegments);" \
            "}";
        glShaderSource(m_tesselationControlShaderObject, 1, &tesselationControlShaderSourceCode, nullptr);
        glCompileShader(m_tesselationControlShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_tesselationControlShaderObject, GL_COMPILE_STATUS, "m_tesselationControlShaderObject"));

        m_tesselationEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);
        const GLchar *tesselationEvaluationShaderSourceCode =
            "#version 440" \
            "\n" \
            "layout(isolines)in;" \
            "uniform mat4 u_mvp_matrix;" \
            "void main(void)" \
            "{" \
                "float u = gl_TessCoord.x;" \
                "vec3 p0 = gl_in[0].gl_Position.xyz;" \
                "vec3 p1 = gl_in[1].gl_Position.xyz;" \
                "vec3 p2 = gl_in[2].gl_Position.xyz;" \
                "vec3 p3 = gl_in[3].gl_Position.xyz;" \
                "float u1 = (1.0 - u);" \
                "float u2 = u * u;" \
                "float b3 = u2 * u;" \
                "float b2 = 3.0 * u2 * u1;" \
                "float b1 = 3.0 * u * u1 * u1;" \
                "float b0 = u1 * u1 * u1;" \
                "vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;" \
                "gl_Position = u_mvp_matrix * vec4(p, 1.0);" \
            "}";
        glShaderSource(m_tesselationEvaluationShaderObject, 1, &tesselationEvaluationShaderSourceCode, nullptr);
        glCompileShader(m_tesselationEvaluationShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_tesselationEvaluationShaderObject, GL_COMPILE_STATUS, "m_tesselationEvaluationShaderObject"));

        // fragment shader
        m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
            "#version 440" \
            "\n" \
            "uniform vec4 lineColor;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
            "FragColor = lineColor;" \
            "}";
        glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSourceCode, nullptr);
        glCompileShader(m_fragmentShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_fragmentShaderObject, GL_COMPILE_STATUS, "m_fragmentShaderObject"));

        // shader program
        m_shaderProgramObject = glCreateProgram();
        glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
        glAttachShader(m_shaderProgramObject, m_tesselationControlShaderObject);
        glAttachShader(m_shaderProgramObject, m_tesselationEvaluationShaderObject);
        glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);

        glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
        glLinkProgram(m_shaderProgramObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_shaderProgramObject, GL_LINK_STATUS, "m_shaderProgramObject"));

        // get uniform location
        m_MVPuniform = glGetUniformLocation(m_shaderProgramObject, "u_mvp_matrix");
        m_numberOfSegmentsUniform = glGetUniformLocation(m_shaderProgramObject, "numberOfSegments");
        m_numberOfStripsUniform = glGetUniformLocation(m_shaderProgramObject, "numberOfStrips");
        m_lineColorUniform = glGetUniformLocation(m_shaderProgramObject, "lineColor");

        float vertices[] = { -1.0f, -1.0f, -0.5f, 1.0f, 0.5f, -1.0f, 1.0f, 1.0f };

        // generate and start filling list
        glGenVertexArrays(1, &m_vao);
        glBindVertexArray(m_vao);

        // prepare array buffer
        glGenBuffers(4, m_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::VERTEX, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // reset array buffer and vertex array
        glBindVertexArray(0);
        glLineWidth(3.0f);
        m_numOfSegments = 1;

        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing one-time renderer un-initialization before it is unloaded
    /// Renderer can perform global cleanup as part of this method
    void TestRenderer::Uninitialize(void)
    {
        CALL_AND_RESET_IF_VALID(m_vao, glDeleteVertexArrays(1, &m_vao));
        //CALL_AND_RESET_IF_VALID(m_vbo, glDeleteBuffers(1, &m_vbo));
        CALL_AND_RESET_IF_VALID(m_vertexShaderObject, glDetachShader(m_shaderProgramObject, m_vertexShaderObject));
        CALL_AND_RESET_IF_VALID(m_fragmentShaderObject, glDetachShader(m_shaderProgramObject, m_fragmentShaderObject));
        CALL_AND_RESET_IF_VALID(m_shaderProgramObject, glDeleteProgram(m_shaderProgramObject));
        glUseProgram(0);
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host before rendering a scene to the active renderer.
    /// Renderer should do initialization of scene specific things as part of this method
    /// @param scene Identifier of a scene to be initialized
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::InitializeScene(SceneType scene)
    {
        // general initializations
        glShadeModel(GL_SMOOTH);
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_CULL_FACE);
        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host after rendering a scene to the active renderer
    /// Renderer should do cleanup of scene specific things done as part of scene initialize.
    /// @param scene Identifier of a scene to be cleaned-up
    void TestRenderer::UninitializeScene(SceneType scene)
    {
    }

    /// Method for rendering a frame in a scene
    /// This method will be called by the host per frame of a scene only to the active renderer
    /// @param params describes the parameters curresponding to this render
    /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
    ///         RENDERER_RESULT_ERROR if failed in building the frame
    ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
    ///                                   in such cases no further frame calls would be made for this scene
    ///                                   to the renderer.
    RendererResult TestRenderer::Render(const RenderParams &params)
    {
        RendererResult result = RENDERER_RESULT_FINISHED;
        if (params.scene == SCENE_TYPE_TEST0)
        {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glUseProgram(m_shaderProgramObject);

            glm::mat4x4 modelMatrix;
            modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, 0.0f, -5.0f));
            glm::mat4x4 modelViewProjectionMatrix = m_projectionMatrix * modelMatrix;

            glUniformMatrix4fv(m_MVPuniform, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));

            // send other uniforms
            glUniform1i(m_numberOfSegmentsUniform, m_numOfSegments);
            glUniform1i(m_numberOfStripsUniform, 1);
            glUniform4fv(m_lineColorUniform, 1, glm::value_ptr(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f)));

            glBindVertexArray(m_vao);
                glPatchParameteri(GL_PATCH_VERTICES, 4);
                glDrawArrays(GL_PATCHES, 0, 4);
            glBindVertexArray(0);

            glUseProgram(0);

            result = RENDERER_RESULT_SUCCESS;
        }
        return result;
    }

    /// Generic method to notify active renderer about a message posted to host window. The message can be
    /// from system or initiated by the host itself.
    /// @param message OS dependent structure that describes the system message being processed.
    void TestRenderer::OnMessage(const Message &message)
    {
        switch (message.messageId)
        {
        case WM_KEYDOWN:
            switch (message.wParam)
            {
            case VK_UP:
                m_numOfSegments++;
                if (m_numOfSegments >= 50)
                    m_numOfSegments = 50;
                break;

            case VK_DOWN:
                m_numOfSegments--;
                if (m_numOfSegments <= 0)
                    m_numOfSegments = 1;
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
    }

    /// Generic method to notify active renderer about the change in the dimensions of the host window
    /// @param width New width of the window
    /// @param height New height of the window
    void TestRenderer::OnResize(unsigned int width, unsigned int height)
    {
        if (height == 0)
            height = 1;
        glViewport(0, 0, (GLsizei)width, (GLsizei)height);
        m_projectionMatrix = glm::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    }

    GLint TestRenderer::CheckObjectStatus(GLuint object, GLenum operation, const char *objectName)
    {
        GLint status = GL_FALSE;
        bool isLinkOperation = operation == GL_LINK_STATUS;
        isLinkOperation ? glGetProgramiv(object, operation, &status) :
            glGetShaderiv(object, operation, &status);

        if (status == GL_FALSE)
        {
            fprintf(m_fp, "%s failed\n", objectName);
            GLint shaderInfoLogLength = 0;
            isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength) :
                glGetShaderiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);
            if (shaderInfoLogLength > 0)
            {
                GLchar *szInfoLog = new GLchar[shaderInfoLogLength];
                if (szInfoLog != nullptr)
                {
                    memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                    GLsizei written = 0;
                    isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                        glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                    fprintf(m_fp, "%s info Log :\n %s", objectName, szInfoLog);
                    delete[]szInfoLog;
                }
            }
        }
        return status;
    }
}
