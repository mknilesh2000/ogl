//
//  GLESView.m
//  Window
//
//  Created by Nilesh Mahajan on 27/05/18.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import <glm/gtx/transform.hpp>
#import <glm/gtc/type_ptr.hpp>

#import "GLESView.h"

enum ATTRIBUTE
{
    VERTEX = 0,
    COLOR = 1,
    NORMAL = 2,
    TEXTURE0 = 3
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    // Draw Members
    GLuint m_vertexShaderObject;
    GLuint m_fragmentShaderObject;
    GLuint m_shaderProgramObject;
    GLuint m_vaoCube;
    GLuint m_vboCube[4];
    GLuint m_MVPuniform;
    glm::mat4x4 m_perspectiveProjectionMatrix;
    GLuint m_lightEnableStateUniform;
    GLuint m_LdUniform;
    GLuint m_KdUniform;
    GLuint m_lightPositionUniform;
    GLuint m_enableAnimation;
    GLuint m_LightMode;
    GLuint m_ModelViewMatrixUniform;
    GLuint m_ProjectionMatrixUniform;
}

- (GLint) CheckObjectStatus:(GLuint)object forOperation:(GLenum)operation forObjectName:(const char *)objectName
{
    GLint status = GL_FALSE;
    bool isLinkOperation = operation == GL_LINK_STATUS;
    isLinkOperation ? glGetProgramiv(object, operation, &status) :
    glGetShaderiv(object, operation, &status);
    
    if (status == GL_FALSE)
    {
        printf("%s failed\n", objectName);
        GLint shaderInfoLogLength = 0;
        isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength) :
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);
        if (shaderInfoLogLength > 0)
        {
            GLchar *szInfoLog = (GLchar*) malloc(shaderInfoLogLength * sizeof(GLchar));
            if (szInfoLog != NULL)
            {
                memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                GLsizei written = 0;
                isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                printf("%s info Log :\n %s", objectName, szInfoLog);
                free(szInfoLog);
            }
        }
    }
    return status;
}

- (void) LoadCubeData
{
    const GLfloat cubeVertices[] =
    {
        // front
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        // right
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        // left
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        // back
        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        // top
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        // bottom
        1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f
    };
    
    const GLfloat cubeNormals[] =
    {
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
    };
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoCube);
    glBindVertexArray(m_vaoCube);
    
    // prepare array buffer
    glGenBuffers(4, m_vboCube);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[ATTRIBUTE::VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[ATTRIBUTE::NORMAL]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) initialize
{
    m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSource =
    "#version 300 es\n"                       \
    "in vec4 vPosition;\n"                                                        \
    "in vec3 vNormal;\n"                                                          \
    "uniform mat4 u_modelView_matrix;\n"                                          \
    "uniform mat4 u_projectionMatrix;\n"                                          \
    "uniform vec3 u_Ld;\n"                                                         \
    "uniform vec3 u_Kd;\n"                                                         \
    "uniform mediump int u_lightMode;\n"                                                \
    "uniform vec4 u_lightPosition;\n"                                             \
    "out vec3 diffused_light;\n"                                                  \
    "out vec4 vPosition2FS;\n"                                                      \
    "out vec3 vNormal2FS;\n"                                                    \
    "void main(void)\n"                                                           \
    "{\n"                                                                         \
    "   if (u_lightMode == 1)\n"                                                \
    "   {\n"                                                                      \
    "       vec4 eyeCoordinates = u_modelView_matrix * vPosition;\n"              \
    "       mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));\n"   \
    "       vec3 tnorm = normalize(normal_matrix * vNormal);\n"        \
    "       vec3 s = normalize(vec3(u_lightPosition - eyeCoordinates));\n"        \
    "       diffused_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);\n"            \
    "   }\n"                                                                      \
    "   gl_Position = u_projectionMatrix * u_modelView_matrix * vPosition;\n"   \
    "   vPosition2FS = vPosition;\n"\
    "   vNormal2FS = vNormal\n;"\
    "}\n";
    
    glShaderSource(m_vertexShaderObject, 1, &vertexShaderSource, NULL);
    glCompileShader(m_vertexShaderObject);
    [self CheckObjectStatus:m_vertexShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Vertex Shader"];
    
    m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentShaderSource =
    "#version 300 es\n"                             \
    "precision highp float;"                        \
    "in vec3 diffused_light;\n"                           \
    "in vec4 vPosition2FS;\n"                           \
    "in vec3 vNormal2FS;\n"                                                    \
    "uniform mat4 u_modelView_matrix;\n"                                          \
    "uniform mat4 u_projectionMatrix;\n"                                          \
    "uniform vec3 u_Ld;\n"                                                         \
    "uniform vec3 u_Kd;\n"                                                         \
    "uniform mediump int u_lightMode;\n"                                                \
    "uniform vec4 u_lightPosition;\n"                                             \
    "out vec4 FragColor;\n"                               \
    "void main(void)"                                   \
    "{"                                                 \
    "   if (u_lightMode == 1)"                        \
    "   {"                                              \
    "       FragColor = vec4(diffused_light, 1.0);\n"   \
    "   }"                                              \
    "   else if (u_lightMode == 2)\n"                                                \
    "   {\n"                                                                      \
    "       vec4 eyeCoordinates = u_modelView_matrix * vPosition2FS;\n"              \
    "       mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));\n"   \
    "       vec3 tnorm = normalize(normal_matrix * vNormal2FS);\n"        \
    "       vec3 s = normalize(vec3(u_lightPosition - eyeCoordinates));\n"        \
    "       FragColor = vec4(vec3(1.0, 0.0, 0.0) * u_Kd * max(dot(s, tnorm), 0.0), 1.0);\n"            \
    "   }\n"                                                                      \
    "   else\n"                                         \
    "   {"                                              \
    "       FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"      \
    "   }"                                              \
    "}";
    glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSource, NULL);
    glCompileShader(m_fragmentShaderObject);
    [self CheckObjectStatus:m_fragmentShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Fragment Shader"];
    
    m_shaderProgramObject = glCreateProgram();
    glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
    glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::NORMAL, "vNormal");
    glLinkProgram(m_shaderProgramObject);
    [self CheckObjectStatus:m_shaderProgramObject forOperation:GL_LINK_STATUS forObjectName:"Shader Program"];
    
    // get uniform location
    m_ModelViewMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_modelView_matrix");
    m_ProjectionMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_projectionMatrix");
    m_LdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ld");
    m_KdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Kd");
    m_lightEnableStateUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
    m_lightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightPosition");
    
    [self LoadCubeData];
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        // initialize drawing objects
        [self initialize];
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    // code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    static GLfloat angle = 0.0f;
    angle += 0.01f;
    
    glUseProgram(m_shaderProgramObject);
    
    // Draw Cube
    {
        static GLfloat angle = 0.0f;
        angle += 0.01f;
        
        glm::mat4x4 modelMatrix = glm::translate(glm::mat4x4(), glm::vec3(0.0f, 0.0f, -4.0f));
        glm::mat4x4 modelViewMatrix;
        if (m_enableAnimation)
        {
            glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(), angle, glm::vec3(1.0f, 0.0f, 0.0f))
            * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 1.0f, 0.0f))
            * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 0.0f, 1.0f));
            modelViewMatrix = modelMatrix * rotationMatrix;
        }
        else
        {
            modelViewMatrix = modelMatrix;
        }
        
        if (m_LightMode > 0)
        {
            glUniform3f(m_LdUniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(m_KdUniform, 0.5f, 0.5f, 0.5f);
            
            GLfloat lightPosition[] = { 0.0f, 0.0f, 5.0f, 1.0f };
            glUniform4fv(m_lightPositionUniform, 1, lightPosition);
        }
        glUniformMatrix4fv(m_ModelViewMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
        glUniformMatrix4fv(m_ProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(m_perspectiveProjectionMatrix));
        glUniform1i(m_lightEnableStateUniform, m_LightMode);
        
        glBindVertexArray(m_vaoCube);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
        glBindVertexArray(0);
    }
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    GLfloat aspect = (GLfloat)width / (GLfloat)height;
    m_perspectiveProjectionMatrix = glm::perspective(45.0f, aspect, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    m_LightMode = (m_LightMode + 1) % 3;
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    m_enableAnimation = !m_enableAnimation;
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    // code
    if (m_vaoCube)
    {
        glDeleteVertexArrays(1, &m_vaoCube);
        m_vaoCube = 0;
    }
    
    if (m_vboCube[0] != 0)
    {
        glDeleteBuffers(4, m_vboCube);
        memset(m_vboCube, 0, sizeof(m_vboCube));
    }
    
    if (m_shaderProgramObject)
    {
        glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
        glDeleteProgram(m_shaderProgramObject);
        m_shaderProgramObject = 0;
    }
    
    if (m_fragmentShaderObject)
    {
        glDeleteShader(m_fragmentShaderObject);
        m_fragmentShaderObject = 0;
    }
    
    if (m_vertexShaderObject)
    {
        glDeleteShader(m_vertexShaderObject);
        m_vertexShaderObject = 0;
    }

    if (depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if (colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if (defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if ([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}
@end
