#pragma once

#include <Renderer.h>
#include <stdio.h>
#include <glew.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include <glm/gtc/matrix_transform.hpp>

namespace Features
{
    using namespace Interfaces;

    class TestRenderer : public IRenderer
    {
    private:
        FILE            *m_fp;
        GLuint          m_vertexShaderObject;
        GLuint          m_fragmentShaderObject;
        GLuint          m_shaderProgramObject;

        GLuint          m_vaoCube;
        GLuint          m_vboCube[4];
        glm::mat4x4     m_perspectiveProjectionMatrix;

        GLuint          m_ModelViewMatrixUniform;
        GLuint          m_ProjectionMatrixUniform;
        GLuint          m_lightEnableStateUniform;
        GLuint          m_LdUniform;
        GLuint          m_KdUniform;
        GLuint          m_lightPositionUniform;

        bool            m_enableAnimation;
        GLuint          m_LightMode;

        GLint CheckObjectStatus(GLuint object, GLenum operation, const char *ObjectName);
        void LoadCubeData();

    public:
        /// Method for retrieving name of the renderer
        /// @param rendererName buffer to be filled with the renderer name
        /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
        ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
        const char* GetName() override;

        /// Method for performing one-time renderer initialization.
        /// Renderer can initialize global/static instances as part of this method
        /// @param window identifier of the window where drawing is directed
        /// @return RENDERER_RESULT_SUCCESS if succeded
        ///         RENDERER_RESULT_ERROR if failed.
        RendererResult Initialize(Window window) override;

        /// Method for performing one-time renderer un-initialization before it is unloaded
        /// Renderer can perform global cleanup as part of this method
        void Uninitialize(void) override;

        /// Method for performing scene-specific initialization
        /// This method will be called by the host before rendering a scene to the active renderer.
        /// Renderer should do initialization of scene specific things as part of this method
        /// @param scene Identifier of a scene to be initialized
        /// @return RENDERER_RESULT_SUCCESS if succeded
        ///         RENDERER_RESULT_ERROR if failed.
        RendererResult InitializeScene(SceneType scene) override;

        /// Method for performing scene-specific initialization
        /// This method will be called by the host after rendering a scene to the active renderer
        /// Renderer should do cleanup of scene specific things done as part of scene initialize.
        /// @param scene Identifier of a scene to be cleaned-up
        void UninitializeScene(SceneType scene) override;

        /// Method for rendering a frame in a scene
        /// This method will be called by the host per frame of a scene only to the active renderer
        /// @param params describes the parameters curresponding to this render
        /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
        ///         RENDERER_RESULT_ERROR if failed in building the frame
        ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
        ///                                   in such cases no further frame calls would be made for this scene
        ///                                   to the renderer.
        virtual RendererResult Render(const RenderParams &params) override;

        /// Generic method to notify active renderer about a message posted to host window. The message can be
        /// from system or initiated by the host itself.
        /// @param message OS dependent structure that describes the system message being processed.
        void OnMessage(const Message &message) override;

        /// Generic method to notify active renderer about the change in the dimensions of the host window
        /// @param width New width of the window
        /// @param height New height of the window
        void OnResize(unsigned int width, unsigned int height) override;

        // constructor
        TestRenderer();

        // destructor
        ~TestRenderer();
    };

}
