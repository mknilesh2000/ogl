#pragma once

#include "TestRenderer.h"
#include <glm/gtc/type_ptr.hpp>

namespace Features
{
    enum ATTRIBUTE
    {
        VERTEX = 0,
        COLOR,
        NORMAL,
        TEXTURE0
    };

    using namespace Interfaces;

#define EXIT_IF_ERROR(x) if (x == GL_FALSE) { __debugbreak(); return RENDERER_RESULT_ERROR; }
#define CALL_AND_RESET_IF_VALID(x, y) if (x != NULL) { y;  x = NULL; }

    TestRenderer::TestRenderer() :
        m_fp(nullptr),
        m_vertexShaderObject(0),
        m_fragmentShaderObject(0),
        m_shaderProgramObject(0),
        m_vaoCube(0),
        m_ModelViewMatrixUniform(0),
        m_ProjectionMatrixUniform(0),
        m_lightEnableStateUniform(0),
        m_LdUniform(0),
        m_KdUniform(0),
        m_lightPositionUniform(0),
        m_enableAnimation(false),
        m_LightMode(0)
    {
        memset(m_vboCube, 0, sizeof(m_vboCube));
        fopen_s(&m_fp, "output.log", "w+");
        if (m_fp == nullptr)
            __debugbreak();
        fprintf(m_fp, "Opened log-file\n");
    }

    TestRenderer::~TestRenderer()
    {
        if (m_fp != nullptr)
        {
            fprintf(m_fp, "Closing log-file\n");
            fclose(m_fp);
            m_fp = nullptr;
        }
    }

    /// Method for retrieving name of the renderer
    /// @param rendererName buffer to be filled with the renderer name
    /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
    ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
    const char* TestRenderer::GetName()
    {
        static const char name[] = "TestRenderer";
        return name;
    }

    void TestRenderer::LoadCubeData()
    {
        const GLfloat cubeVertices[] =
        {
            // front
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            // right
            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            // left
            -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            // back
            -1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            // top
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            // bottom
            1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f
        };

        const GLfloat cubeNormals[] =
        {
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,

            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,

            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,

            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,

            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,

            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
        };

        // generate and start filling list
        glGenVertexArrays(1, &m_vaoCube);
        glBindVertexArray(m_vaoCube);

        // prepare array buffer
        glGenBuffers(4, m_vboCube);
        glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[ATTRIBUTE::VERTEX]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[ATTRIBUTE::NORMAL]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(ATTRIBUTE::NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // reset array buffer and vertex array
        glBindVertexArray(0);
    }

    /// Method for performing one-time renderer initialization.
    /// Renderer can initialize global/static instances as part of this method
    /// @param window identifier of the window where drawing is directed
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::Initialize(Window window)
    {
        // vertex shader
        m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexShaderSourceCode =
            "#version 450 core\n"                                                         \
            "in vec4 vPosition;\n"                                                        \
            "in vec3 vNormal;\n"                                                          \
            "uniform mat4 u_modelView_matrix;\n"                                          \
            "uniform mat4 u_projectionMatrix;\n"                                          \
            "uniform vec3 u_Ld;\n"                                                         \
            "uniform vec3 u_Kd;\n"                                                         \
            "uniform int u_lightMode;\n"                                                \
            "uniform vec4 u_lightPosition;\n"                                             \
            "out vec3 diffused_light;\n"                                                  \
            "out vec4 vPosition2FS;\n"                                                      \
            "out vec3 vNormal2FS;\n"                                                    \
            "void main(void)\n"                                                           \
            "{\n"                                                                         \
            "   if (u_lightMode == 1)\n"                                                \
            "   {\n"                                                                      \
            "       vec4 eyeCoordinates = u_modelView_matrix * vPosition;\n"              \
            "       mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));\n"   \
            "       vec3 tnorm = normalize(normal_matrix * vNormal);\n"        \
            "       vec3 s = normalize(vec3(u_lightPosition - eyeCoordinates));\n"        \
            "       diffused_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);\n"            \
            "   }\n"                                                                      \
            "   gl_Position = u_projectionMatrix * u_modelView_matrix * vPosition;\n"   \
            "   vPosition2FS = vPosition;\n"\
            "   vNormal2FS = vNormal\n;"\
            "}\n";
        glShaderSource(m_vertexShaderObject, 1, &vertexShaderSourceCode, nullptr);
        glCompileShader(m_vertexShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_vertexShaderObject, GL_COMPILE_STATUS, "m_vertexShaderObject"));
        // fragment shader
        m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
            "#version 450 core"                                 \
            "\n"                                                \
            "in vec3 diffused_light;\n"                           \
            "in vec4 vPosition2FS;\n"                           \
            "in vec3 vNormal2FS;\n"                                                    \
            "uniform mat4 u_modelView_matrix;\n"                                          \
            "uniform mat4 u_projectionMatrix;\n"                                          \
            "uniform vec3 u_Ld;\n"                                                         \
            "uniform vec3 u_Kd;\n"                                                         \
            "uniform int u_lightMode;\n"                                                \
            "uniform vec4 u_lightPosition;\n"                                             \
            "out vec4 FragColor;\n"                               \
            "void main(void)"                                   \
            "{"                                                 \
            "   if (u_lightMode == 1)"                        \
            "   {"                                              \
            "       FragColor = vec4(diffused_light, 1.0);\n"   \
            "   }"                                              \
            "   else if (u_lightMode == 2)\n"                                                \
            "   {\n"                                                                      \
            "       vec4 eyeCoordinates = u_modelView_matrix * vPosition2FS;\n"              \
            "       mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));\n"   \
            "       vec3 tnorm = normalize(normal_matrix * vNormal2FS);\n"        \
            "       vec3 s = normalize(vec3(u_lightPosition - eyeCoordinates));\n"        \
            "       FragColor = vec4(vec3(1.0, 0.0, 0.0) * u_Kd * max(dot(s, tnorm), 0.0), 1.0);\n"            \
            "   }\n"                                                                      \
            "   else\n"                                         \
            "   {"                                              \
            "       FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"      \
            "   }"                                              \
            "}";
        glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSourceCode, nullptr);
        glCompileShader(m_fragmentShaderObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_fragmentShaderObject, GL_COMPILE_STATUS, "m_fragmentShaderObject"));

        // shader program
        m_shaderProgramObject = glCreateProgram();
        glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
        glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
        glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::NORMAL, "vNormal");
        glLinkProgram(m_shaderProgramObject);
        EXIT_IF_ERROR(CheckObjectStatus(m_shaderProgramObject, GL_LINK_STATUS, "m_shaderProgramObject"));

        // get uniform location
        m_ModelViewMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_modelView_matrix");
        m_ProjectionMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_projectionMatrix");
        m_LdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ld");
        m_KdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Kd");
        m_lightEnableStateUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
        m_lightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightPosition");

        LoadCubeData();

        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing one-time renderer un-initialization before it is unloaded
    /// Renderer can perform global cleanup as part of this method
    void TestRenderer::Uninitialize(void)
    {
        CALL_AND_RESET_IF_VALID(m_vaoCube, glDeleteVertexArrays(1, &m_vaoCube));
        glDeleteBuffers(1, m_vboCube);
        CALL_AND_RESET_IF_VALID(m_vertexShaderObject, glDetachShader(m_shaderProgramObject, m_vertexShaderObject));
        CALL_AND_RESET_IF_VALID(m_fragmentShaderObject, glDetachShader(m_shaderProgramObject, m_fragmentShaderObject));
        CALL_AND_RESET_IF_VALID(m_shaderProgramObject, glDeleteProgram(m_shaderProgramObject));
        glUseProgram(0);
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host before rendering a scene to the active renderer.
    /// Renderer should do initialization of scene specific things as part of this method
    /// @param scene Identifier of a scene to be initialized
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::InitializeScene(SceneType scene)
    {
        // general initializations
        glShadeModel(GL_SMOOTH);
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_CULL_FACE);
        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host after rendering a scene to the active renderer
    /// Renderer should do cleanup of scene specific things done as part of scene initialize.
    /// @param scene Identifier of a scene to be cleaned-up
    void TestRenderer::UninitializeScene(SceneType scene)
    {
    }

    /// Method for rendering a frame in a scene
    /// This method will be called by the host per frame of a scene only to the active renderer
    /// @param params describes the parameters curresponding to this render
    /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
    ///         RENDERER_RESULT_ERROR if failed in building the frame
    ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
    ///                                   in such cases no further frame calls would be made for this scene
    ///                                   to the renderer.
    RendererResult TestRenderer::Render(const RenderParams &params)
    {
        RendererResult result = RENDERER_RESULT_FINISHED;

        // render for 5 secs before transitioning to next scene
        if (params.scene == SCENE_TYPE_TEST0)
        {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glUseProgram(m_shaderProgramObject);

            // Draw Cube
            {
                glm::mat4x4 modelMatrix = glm::translate(glm::mat4x4(), glm::vec3(0.0f, 0.0f, -4.0f));
                glm::mat4x4 modelViewMatrix;

                if (m_enableAnimation)
                {
                    GLfloat angle = params.frameIdFromEpicStart * 0.001f;
                    glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(), angle, glm::vec3(1.0f, 0.0f, 0.0f))
                        * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 1.0f, 0.0f))
                        * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 0.0f, 1.0f));
                    modelViewMatrix = modelMatrix * rotationMatrix;
                }
                else
                {
                    modelViewMatrix = modelMatrix;
                }

                if (m_LightMode > 0)
                {
                    glUniform3f(m_LdUniform, 1.0f, 1.0f, 1.0f);
                    glUniform3f(m_KdUniform, 0.5f, 0.5f, 0.5f);

                    GLfloat lightPosition[] = { 0.0f, 0.0f, 5.0f, 1.0f };
                    glUniform4fv(m_lightPositionUniform, 1, lightPosition);
                }
                glUniformMatrix4fv(m_ModelViewMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
                glUniformMatrix4fv(m_ProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(m_perspectiveProjectionMatrix));
                glUniform1i(m_lightEnableStateUniform, m_LightMode);

                glBindVertexArray(m_vaoCube);
                glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
                glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
                glBindVertexArray(0);
            }

            glUseProgram(0);

            result = RENDERER_RESULT_SUCCESS;
        }
        return result;
    }

    /// Generic method to notify active renderer about a message posted to host window. The message can be
    /// from system or initiated by the host itself.
    /// @param message OS dependent structure that describes the system message being processed.
    void TestRenderer::OnMessage(const Message &message)
    {
        switch (message.messageId)
        {
        case WM_CHAR:
            switch (message.wParam)
            {
            case 'L':
            case 'l':
                m_LightMode = (m_LightMode + 1) % 3;
                break;

            case 'A':
            case 'a':
                m_enableAnimation = !m_enableAnimation;
                break;
            }
            break;
        }
    }

    /// Generic method to notify active renderer about the change in the dimensions of the host window
    /// @param width New width of the window
    /// @param height New height of the window
    void TestRenderer::OnResize(unsigned int width, unsigned int height)
    {
        if (height == 0)
            height = 1;
        glViewport(0, 0, (GLsizei)width, (GLsizei)height);
        m_perspectiveProjectionMatrix = glm::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    }

    GLint TestRenderer::CheckObjectStatus(GLuint object, GLenum operation, const char *objectName)
    {
        GLint status = GL_FALSE;
        bool isLinkOperation = operation == GL_LINK_STATUS;
        isLinkOperation ? glGetProgramiv(object, operation, &status) :
            glGetShaderiv(object, operation, &status);

        if (status == GL_FALSE)
        {
            fprintf(m_fp, "%s failed\n", objectName);
            GLint shaderInfoLogLength = 0;
            isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength) :
                glGetShaderiv(object, GL_INFO_LOG_LENGTH, &shaderInfoLogLength);
            if (shaderInfoLogLength > 0)
            {
                GLchar *szInfoLog = new GLchar[shaderInfoLogLength];
                if (szInfoLog != nullptr)
                {
                    memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                    GLsizei written = 0;
                    isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                        glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                    fprintf(m_fp, "%s info Log :\n %s", objectName, szInfoLog);
                    fflush(m_fp);
                    delete[]szInfoLog;
                }
            }
        }
        return status;
    }
}
