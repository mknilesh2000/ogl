//onload function
var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	ATTRIBUTE_VERTEX:0,
	ATTRIBUTE_COLOR:1,
	ATTRIBUTE_NORMAL:2,
	ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;
var vbo_cube_position;
var vbo_cube_normal;

var modelViewMatrixUniform;
var projectionMatrixUniform;

var ldUniform;
var kdUniform;
var lightPositionUniform;
var LKeyPressedUniform;

var bLKeyPressed = false;

var perspectiveProjectionMatrix;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame || 
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

var angle = 0.0;	
	
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed :( \n");
	else 
		console.log("Obtaining Canvas Passes :) \n");
	
	//print canvas width and height on console
	console.log("Canvas width::" +canvas.width+ " and Canvas height::" +canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initailize webGL
	init();
	
	resize();
	draw();
}

function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement || 
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document. msFullscreenElement||
		null;
		
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullScreen = false;
	}
}

function init()
{
	//code 
	//get webGL 2.0 contextgl 
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	
    var vertexShaderSourceCode = document.getElementById("vertex-shader");
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode.firstChild.data);
    gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject)
		if(error.length > 0)
		{
			alert("vertex shader");
			alert(error);
			uninitialize();
		}
	}

    var fragmentShaderSourceCode = document.getElementById("fragment-shader");
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode.firstChild.data);
    gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject)
		if(error.length > 0)
		{
			alert("fragment shader");
			alert(error);
			uninitialize();
		}
	}

	//shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.ATTRIBUTE_NORMAL, "vNormal");

	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

    modelViewMatrixUniform =gl.getUniformLocation(shaderProgramObject,"u_model_view_matrix");
    projectionMatrixUniform =gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
		
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");

	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
										
	var cubeVertices = new Float32Array([
											1.0, 1.0, 1.0, //ront
											1.0, -1.0, 1.0,
											-1.0, -1.0, 1.0,
											-1.0, 1.0, 1.0,

											1.0, 1.0, -1.0, //back
											1.0, -1.0, -1.0,
											-1.0, -1.0, -1.0,
											-1.0, 1.0, -1.0,

											1.0, 1.0, 1.0, //right
											1.0, 1.0, -1.0,
											1.0, -1.0, -1.0,
											1.0, -1.0, 1.0,

											-1.0, 1.0, 1.0, //let
											-1.0, 1.0, -1.0,
											-1.0, -1.0, -1.0,
											-1.0, -1.0, 1.0,

											1.0, 1.0, 1.0, //top
											-1.0, 1.0, 1.0,
											-1.0, 1.0, -1.0,
											1.0, 1.0, -1.0,

											1.0, -1.0, 1.0, //bottom
											-1.0, -1.0, 1.0,
											-1.0, -1.0, -1.0,
											1.0, -1.0, -1.0
											]);

	var cubeNormals = new Float32Array([
											0.0, 0.0, 1.0, 
											0.0, 0.0, 1.0,
											0.0, 0.0, 1.0,
											0.0, 0.0, 1.0,

											0.0, 0.0, -1.0, 
											0.0, 0.0, -1.0,
											0.0, 0.0, -1.0,
											0.0, 0.0, -1.0,
											
											1.0, 0.0, 0.0, 
											1.0, 0.0, 0.0,
											1.0, 0.0, 0.0,
											1.0, 0.0, 0.0,

											-1.0, 0.0, 0.0, 
											-1.0, 0.0, 0.0,
											-1.0, 0.0, 0.0,
											-1.0, 0.0, 0.0,

											0.0, 1.0, 0.0, 
											0.0, 1.0, 0.0,
											0.0, 1.0, 0.0,
											0.0, 1.0, 0.0,

											0.0, -1.0, 0.0, 
											0.0, -1.0, 0.0,
											0.0, -1.0, 0.0,
											0.0, -1.0, 0.0
										]);

	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);	
		vbo_cube_position = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_position);
			gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_VERTEX,
									3,
									gl.FLOAT,
									false, 0, 0);
			gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);	
	
		vbo_cube_normal = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_normal);
			gl.bufferData(gl.ARRAY_BUFFER, cubeNormals, gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_NORMAL,
									3,
									gl.FLOAT,
									false, 0, 0);
			gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_NORMAL);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);	
	
	gl.bindVertexArray(null);
	
	gl.enable(gl.DEPTH_TEST);
//	gl.enable(gl.CULL_FACE);
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code 
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0, perspectiveProjectionMatrix);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);

	if(bLKeyPressed == true)
	{
		gl.uniform1i(LKeyPressedUniform, 1);
		
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5);
		var lightPosition = [0.0, 0.0, 2.0, 1.0];
		gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
	{
		gl.uniform1i(LKeyPressedUniform, 0);
	}
	
	var modelViewMatrix = mat4.create();
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
	mat4.rotateX(modelViewMatrix, modelViewMatrix, angle);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, angle);
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, angle);
	
	gl.uniformMatrix4fv(modelViewMatrixUniform, false, modelViewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

	
	gl.bindVertexArray(vao_cube);
	
		gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	
	gl.bindVertexArray(null);
	
	gl.useProgram(null);

	if(angle>= 2 * 3.145)
		angle = 0.0;
	angle += 0.01;
	
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}

	if(vbo_cube_position)
	{
		gl.deleteBuffer(vbo_cube_position);
		vbo_cube_position = null;
	}
		
	if(vbo_cube_normal)
	{
		gl.deleteBuffer(vbo_cube_normal);
		vbo_cube_normal = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		case 70:
			toggleFullScreen();
			break;	
		case 76:
			bLKeyPressed = true;
			break;

	}
}

function mouseDown()
{
	//code 
}