//
//  AppDelegate.m
//  OGLWindow
//
//  Created by Nilesh Mahajan on 30/07/18.
//  Copyright © 2018 Nilesh Mahajan. All rights reserved.
//

#import "AppDelegate.h"
#import "GLView.h"

FILE* gpFile = NULL;

@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

- (void) createFileForLogging
{
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", NSTemporaryDirectory()];
    const char* pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile = fopen(pszLogFileNameWithPath,"w");
    if (gpFile == NULL)
    {
        printf("Cannot create log file. Exiting\n");
        [self release];
        [NSApp terminate:self];
    }
    printf("created file %s", pszLogFileNameWithPath);
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [self createFileForLogging];
    fprintf(gpFile, "Program started successfully\n");
    
    // window
    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    
    // create a simple window
    window = [[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];
    [window setTitle:@"macOS OpenGL Window"];
    [window center];
    
    glView = [[GLView alloc] initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    if (gpFile != NULL)
    {
        fprintf(gpFile, "Program has terminated successfully\n");
        
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void) windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}

@end
