//
//  GLView.m
//  OGLWindow
//
//  Created by Nilesh Mahajan on 30/07/18.
//  Copyright © 2018 Nilesh Mahajan. All rights reserved.
//

#import "GLView.h"
#import <CoreVideo/CVDisplayLink.h>
#import <glm/gtx/transform.hpp>
#import <glm/gtc/type_ptr.hpp>

extern FILE* gpFile;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext);

enum ATTRIBUTE
{
    VERTEX = 0,
    COLOR = 1,
    NORMAL = 2,
    TEXTURE0 = 3
};

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    // Draw Members
    GLuint m_vertexShaderObject;
    GLuint m_fragmentShaderObject;
    GLuint m_shaderProgramObject;
    GLuint m_vaoCube;
    GLuint m_vboCube[4];
    GLuint m_MVPuniform;
    glm::mat4x4 m_perspectiveProjectionMatrix;
    GLuint m_lightEnableStateUniform;
    GLuint m_LdUniform;
    GLuint m_KdUniform;
    GLuint m_lightPositionUniform;
    GLuint m_enableAnimation;
    GLuint m_LightMode;
    GLuint m_ModelViewMatrixUniform;
    GLuint m_ProjectionMatrixUniform;
}

- (id) initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    if (self != NULL)
    {
        [[self window] setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[] =
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            // Specify a display ID to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0       // last 0 is a must
        };
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
        if (pixelFormat == nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel format available. Exitting... ");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; // this will automatically releases the older context, if present and sets the newer one
    }
    return (self);
}

- (CVReturn) getFrameForTime:(const CVTimeStamp*)pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [self drawView];
    [pool release];
    return kCVReturnSuccess;
}

- (GLint) CheckObjectStatus:(GLuint)object forOperation:(GLenum)operation forObjectName:(const char *)objectName
{
    GLint status = GL_FALSE;
    bool isLinkOperation = operation == GL_LINK_STATUS;
    isLinkOperation ? glGetProgramiv(object, operation, &status) :
    glGetShaderiv(object, operation, &status);
    
    if (status == GL_FALSE)
    {
        assert(0);
        fprintf(gpFile, "%s failed\n", objectName);
        GLint shaderInfoLogLength = 0;
        isLinkOperation ? glGetProgramiv(object, GL_INFO_LOG_LENGTH, &status) :
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &status);
        if (shaderInfoLogLength > 0)
        {
            GLchar *szInfoLog = (GLchar*) malloc(shaderInfoLogLength * sizeof(GLchar));
            if (szInfoLog != NULL)
            {
                memset(szInfoLog, 0, shaderInfoLogLength * sizeof(GLchar));
                GLsizei written = 0;
                isLinkOperation ? glGetProgramInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog) :
                glGetShaderInfoLog(object, shaderInfoLogLength * sizeof(GLchar), &written, szInfoLog);
                fprintf(gpFile, "%s info Log :\n %s", objectName, szInfoLog);
                free(szInfoLog);
            }
        }
    }
    return status;
}

- (void) LoadCubeData
{
    const GLfloat cubeVertices[] =
    {
        // front
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        // right
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        // left
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        // back
        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        // top
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        // bottom
        1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f
    };
    
    const GLfloat cubeNormals[] =
    {
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        0.0f, 0.0f, -1.0f,
        
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
    };
    
    // generate and start filling list
    glGenVertexArrays(1, &m_vaoCube);
    glBindVertexArray(m_vaoCube);
    
    // prepare array buffer
    glGenBuffers(4, m_vboCube);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[ATTRIBUTE::VERTEX]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::VERTEX, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vboCube[ATTRIBUTE::NORMAL]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(ATTRIBUTE::NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // reset array buffer and vertex array
    glBindVertexArray(0);
}

- (void) initialize
{
    m_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSource =
        "#version 410 core\n"                                       \
        "in vec4 vPosition;\n"                                                        \
        "in vec3 vNormal;\n"                                                          \
        "uniform mat4 u_modelView_matrix;\n"                                          \
        "uniform mat4 u_projectionMatrix;\n"                                          \
        "uniform vec3 u_Ld;\n"                                                         \
        "uniform vec3 u_Kd;\n"                                                         \
        "uniform int u_lightMode;\n"                                                \
        "uniform vec4 u_lightPosition;\n"                                             \
        "out vec3 diffused_light;\n"                                                  \
        "out vec4 vPosition2FS;\n"                                                      \
        "out vec3 vNormal2FS;\n"                                                    \
        "void main(void)\n"                                                           \
        "{\n"                                                                         \
        "   if (u_lightMode == 1)\n"                                                \
        "   {\n"                                                                      \
        "       vec4 eyeCoordinates = u_modelView_matrix * vPosition;\n"              \
        "       mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));\n"   \
        "       vec3 tnorm = normalize(normal_matrix * vNormal);\n"        \
        "       vec3 s = normalize(vec3(u_lightPosition - eyeCoordinates));\n"        \
        "       diffused_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);\n"            \
        "   }\n"                                                                      \
        "   gl_Position = u_projectionMatrix * u_modelView_matrix * vPosition;\n"   \
        "   vPosition2FS = vPosition;\n"\
        "   vNormal2FS = vNormal\n;"\
        "}\n";
    
    glShaderSource(m_vertexShaderObject, 1, &vertexShaderSource, NULL);
    glCompileShader(m_vertexShaderObject);
    [self CheckObjectStatus:m_vertexShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Vertex Shader"];
    
    m_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentShaderSource =
    "#version 410 core\n"                           \
    "in vec3 diffused_light;\n"                           \
    "in vec4 vPosition2FS;\n"                           \
    "in vec3 vNormal2FS;\n"                                                    \
    "uniform mat4 u_modelView_matrix;\n"                                          \
    "uniform mat4 u_projectionMatrix;\n"                                          \
    "uniform vec3 u_Ld;\n"                                                         \
    "uniform vec3 u_Kd;\n"                                                         \
    "uniform int u_lightMode;\n"                                                \
    "uniform vec4 u_lightPosition;\n"                                             \
    "out vec4 FragColor;\n"                               \
    "void main(void)"                                   \
    "{"                                                 \
    "   if (u_lightMode == 1)"                        \
    "   {"                                              \
    "       FragColor = vec4(diffused_light, 1.0);\n"   \
    "   }"                                              \
    "   else if (u_lightMode == 2)\n"                                                \
    "   {\n"                                                                      \
    "       vec4 eyeCoordinates = u_modelView_matrix * vPosition2FS;\n"              \
    "       mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));\n"   \
    "       vec3 tnorm = normalize(normal_matrix * vNormal2FS);\n"        \
    "       vec3 s = normalize(vec3(u_lightPosition - eyeCoordinates));\n"        \
    "       FragColor = vec4(vec3(1.0, 0.0, 0.0) * u_Kd * max(dot(s, tnorm), 0.0), 1.0);\n"            \
    "   }\n"                                                                      \
    "   else\n"                                         \
    "   {"                                              \
    "       FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"      \
    "   }"                                              \
    "}";
    glShaderSource(m_fragmentShaderObject, 1, &fragmentShaderSource, NULL);
    glCompileShader(m_fragmentShaderObject);
    [self CheckObjectStatus:m_fragmentShaderObject forOperation:GL_COMPILE_STATUS forObjectName:"Fragment Shader"];
    
    m_shaderProgramObject = glCreateProgram();
    glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
    glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::VERTEX, "vPosition");
    glBindAttribLocation(m_shaderProgramObject, ATTRIBUTE::NORMAL, "vNormal");
    glLinkProgram(m_shaderProgramObject);
    [self CheckObjectStatus:m_shaderProgramObject forOperation:GL_LINK_STATUS forObjectName:"Shader Program"];
    
    // get uniform location
    m_ModelViewMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_modelView_matrix");
    m_ProjectionMatrixUniform = glGetUniformLocation(m_shaderProgramObject, "u_projectionMatrix");
    m_LdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Ld");
    m_KdUniform = glGetUniformLocation(m_shaderProgramObject, "u_Kd");
    m_lightEnableStateUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
    m_lightPositionUniform = glGetUniformLocation(m_shaderProgramObject, "u_lightPosition");
    
    [self LoadCubeData];
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

- (void) prepareOpenGL
{
    fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext] makeCurrentContext];
    
    GLint swapInt;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    [self initialize];
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext =(CGLContextObj) [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

- (void) reshape
{
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    NSRect rect = [self bounds];
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    if (height == 0.0f)
    {
        height = 1.0f;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    m_perspectiveProjectionMatrix = glm::perspective(45.0f, width / height, 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    [self drawView];
}

- (void) drawView
{
    [[self openGLContext] makeCurrentContext];
    CGLContextObj contextObj = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLLockContext(contextObj);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    static GLfloat angle = 0.0f;
    angle += 0.01f;
    
    glUseProgram(m_shaderProgramObject);
    
    // Draw Cube
    {
        static GLfloat angle = 0.0f;
        angle += 0.01f;
        
        glm::mat4x4 modelMatrix = glm::translate(glm::mat4x4(), glm::vec3(0.0f, 0.0f, -4.0f));
        glm::mat4x4 modelViewMatrix;
        if (m_enableAnimation)
        {
            glm::mat4x4 rotationMatrix = glm::rotate(glm::mat4x4(), angle, glm::vec3(1.0f, 0.0f, 0.0f))
            * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 1.0f, 0.0f))
            * glm::rotate(glm::mat4x4(), angle, glm::vec3(0.0f, 0.0f, 1.0f));
            modelViewMatrix = modelMatrix * rotationMatrix;
        }
        else
        {
            modelViewMatrix = modelMatrix;
        }
        
        if (m_LightMode > 0)
        {
            glUniform3f(m_LdUniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(m_KdUniform, 0.5f, 0.5f, 0.5f);
            
            GLfloat lightPosition[] = { 0.0f, 0.0f, 5.0f, 1.0f };
            glUniform4fv(m_lightPositionUniform, 1, lightPosition);
        }
        glUniformMatrix4fv(m_ModelViewMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
        glUniformMatrix4fv(m_ProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(m_perspectiveProjectionMatrix));
        glUniform1i(m_lightEnableStateUniform, m_LightMode);
        
        glBindVertexArray(m_vaoCube);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
        glBindVertexArray(0);
    }
    
    glUseProgram(0);

    CGLFlushDrawable(contextObj);
    CGLUnlockContext(contextObj);
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

- (void) keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: // Esc key
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        case 'L':
        case 'l':
            m_LightMode = (m_LightMode + 1) % 3;
            break;
            
        case 'A':
        case 'a':
            m_enableAnimation = !m_enableAnimation;
            break;
        default:
            break;
    }
}

- (void) mouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) mouseDragged:(NSEvent *)theEvent
{
    // code
}

- (void) rightMouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) dealloc
{
    if (m_vaoCube)
    {
        glDeleteVertexArrays(1, &m_vaoCube);
        m_vaoCube = 0;
    }
    
    if (m_vboCube[0] != 0)
    {
        glDeleteBuffers(4, m_vboCube);
        memset(m_vboCube, 0, sizeof(m_vboCube));
    }
    
    if (m_shaderProgramObject)
    {
        glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);
        glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
        glDeleteProgram(m_shaderProgramObject);
        m_shaderProgramObject = 0;
    }
    
    if (m_fragmentShaderObject)
    {
        glDeleteShader(m_fragmentShaderObject);
        m_fragmentShaderObject = 0;
    }
    
    if (m_vertexShaderObject)
    {
        glDeleteShader(m_vertexShaderObject);
        m_vertexShaderObject = 0;
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void* pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return result;
}
