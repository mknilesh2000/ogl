cbuffer ConstantBuffer
{
    float4x4 WorldViewMatrix;
    float4x4 ProjectionMatrix;
    float4 Ld;
    float4 Kd;
    float4 LightPosition;
    uint LightMode;
};

float4 main(float4 pos:SV_POSITION, float4 diffused_light:COLOR, float4 normal : NORMALTOPS) : SV_TARGET
{
    float4 outcolor = float4(1.0, 1.0, 1.0, 1.0);
    if (LightMode == 1)
    {
        outcolor = diffused_light;
    }
    else if (LightMode == 2)
    {
        float4 eye_coord = mul(WorldViewMatrix, pos);
        float3 tnorm = normalize(mul((float3x3)WorldViewMatrix, (float3)normal));
        float3 s = (float3)(normalize(LightPosition - eye_coord));
        outcolor = float4(float3(1.0, 0.0, 0.0) * Ld * Kd * max(dot(s, tnorm), 0.0), 1.0);
    }
    return outcolor;
}
