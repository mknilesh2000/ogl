cbuffer ConstantBuffer
{
    float4x4 WorldViewMatrix;
    float4x4 ProjectionMatrix;
    float4 Ld;
    float4 Kd;
    float4 LightPosition;
    uint LightMode;
};

struct vertex_output
{
    float4 position: SV_POSITION;
    float4 diffused_light:COLOR;
    float4 normal:NORMALTOPS;
};

vertex_output main(float4 pos:POSITION, float4 normal:NORMAL)
{
    vertex_output output;
    if (LightMode == 1)
    {
        float4 eye_coord = mul(WorldViewMatrix, pos);
        float3 tnorm = normalize(mul((float3x3)WorldViewMatrix, (float3)normal));
        float3 s = (float3)(normalize(LightPosition - eye_coord));
        output.diffused_light = Ld * Kd * max(dot(tnorm, s), 0.0);
    }
    else
    {
        output.diffused_light = float4(1.0, 1.0, 1.0, 1.0);
    }
    float4 temp = mul(WorldViewMatrix, pos);
    output.position = mul(ProjectionMatrix, temp);
    output.normal = normal;

    return(output);
}