package com.rtr.nmahajan.LightsOnCube;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.view.GestureDetector;
import android.view.MotionEvent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by nmahajan on 28-01-2018.
 */

class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,
        GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener

{
    private final Context m_context;

    private GestureDetector m_gestureDetector;

    private int m_vertexShaderObject;
    private int m_fragmentShaderObject;
    private int m_shaderProgramObject;

    private int[] m_vaoCube = new int[1];
    private int[] m_vboCube = new int[2];
    private float m_angleCube;

    private int m_modelViewMatrixUniform;
    private int m_projectionMatrixUniform;

    // lights related uniforms
    private int m_lightingModeUniform;
    private int m_LdUniform;
    private int m_KdUniform;
    private int m_lightPositionUniform;

    private float m_perspectiveProjectionMatrix[] = new float[16];
    private int m_lightingMode;
    private boolean m_enableAnimation;

    /**
     * Standard View constructor. In order to render something, you
     * must call {@link #setRenderer} to register a renderer.
     *
     * @param context
     */
    public GLESView(Context context)
    {
        super(context);
        m_context = context;

        // set EGLContext to current supported version of OpenGL-ES
        setEGLContextClientVersion(3);

        // set renderer for drawing on GLSurfaceView
        setRenderer(this);

        // render the view only when there is change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        m_gestureDetector = new GestureDetector(context, this, null, false);
        m_gestureDetector.setOnDoubleTapListener(this);

        m_angleCube = 0.0f;
        m_modelViewMatrixUniform = 0;
        m_projectionMatrixUniform = 0;

        m_lightingMode = 0;
        m_LdUniform = 0;
        m_KdUniform = 0;
        m_lightingModeUniform = 0;
        m_lightPositionUniform = 0;

        m_enableAnimation = false;
    }

    /**
     * Called when the surface is created or recreated.
     * <p>
     * Called when the rendering thread
     * starts and whenever the EGL context is lost. The EGL context will typically
     * be lost when the Android device awakes after going to sleep.
     * <p>
     * Since this method is called at the beginning of rendering, as well as
     * every time the EGL context is lost, this method is a convenient place to put
     * code to create resources that need to be created when the rendering
     * starts, and that need to be recreated when the EGL context is lost.
     * Textures are an example of a resource that you might want to create
     * here.
     * <p>
     * Note that when the EGL context is lost, all OpenGL resources associated
     * with that context will be automatically deleted. You do not need to call
     * the corresponding "glDelete" methods such as glDeleteTextures to
     * manually delete these lost resources.
     * <p>
     *
     * @param gl     the GL interface. Use <code>instanceof</code> to
     *               test if the interface supports GL11 or higher interfaces.
     * @param config the EGLConfig of the created surface. Can be used
     */
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("OpenGL: " + glesVersion);

        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("GLSL: " + glslVersion);

        initialize(gl);
    }

    /**
     * Called when the surface changed size.
     * <p>
     * Called after the surface is created and whenever
     * the OpenGL ES surface size changes.
     * <p>
     * Typically you will set your viewport here. If your camera
     * is fixed then you could also set your projection matrix here:
     * <pre class="prettyprint">
     * void onSurfaceChanged(GL10 gl, int width, int height) {
     * gl.glViewport(0, 0, width, height);
     * // for a fixed camera, set the projection too
     * float ratio = (float) width / height;
     * gl.glMatrixMode(GL10.GL_PROJECTION);
     * gl.glLoadIdentity();
     * gl.glFrustumf(-ratio, ratio, -1, 1, 1, 10);
     * }
     * </pre>
     *
     * @param gl     the GL interface. Use <code>instanceof</code> to
     *               test if the interface supports GL11 or higher interfaces.
     * @param width
     * @param height
     */
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        resize(width, height);
    }

    /**
     * Called to draw the current frame.
     * <p>
     * This method is responsible for drawing the current frame.
     * <p>
     * The implementation of this method typically looks like this:
     * <pre class="prettyprint">
     * void onDrawFrame(GL10 gl) {
     * gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
     * //... other gl calls to render the scene ...
     * }
     * </pre>
     *
     * @param gl the GL interface. Use <code>instanceof</code> to
     *           test if the interface supports GL11 or higher interfaces.
     */
    @Override
    public void onDrawFrame(GL10 gl)
    {
        display();
    }

    // Handling onTouchEvent is very important since it triggers all gesture
    // related events
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        // forward the touch event to m_gestureDetector
        int eventAction = e.getAction();
        if (!m_gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);

        return true;
    }

    /**
     * Notified when a single-tap occurs.
     * <p>
     * Unlike {@link OnGestureListener#onSingleTapUp(MotionEvent)}, this
     * will only be called after the detector is confident that the user's
     * first tap is not followed by a second tap leading to a double-tap
     * gesture.
     *
     * @param e The down motion event of the single-tap.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        m_lightingMode = (m_lightingMode + 1) % 3;

        return true;
    }

    /**
     * Notified when a double-tap occurs.
     *
     * @param e The down motion event of the first tap of the double-tap.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        // by default handled
        return true;
    }

    /**
     * Notified when an event within a double-tap gesture occurs, including
     * the down, move, and up events.
     *
     * @param e The motion event that occurred during the double-tap gesture.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        m_enableAnimation = !m_enableAnimation;
        return true;
    }

    /**
     * Notified when a tap occurs with the down {@link MotionEvent}
     * that triggered it. This will be triggered immediately for
     * every down event. All other events should be preceded by this.
     *
     * @param e The down motion event.
     */
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do not write anything here since we've already written onSingleTapConfirmed.
        return true;
    }

    /**
     * The user has performed a down {@link MotionEvent} and not performed
     * a move or up yet. This event is commonly used to provide visual
     * feedback to the user to let them know that their action has been
     * recognized i.e. highlight an element.
     *
     * @param e The down motion event
     */
    @Override
    public void onShowPress(MotionEvent e)
    {

    }

    /**
     * Notified when a tap occurs with the up {@link MotionEvent}
     * that triggered it.
     *
     * @param e The up motion event that completed the first tap
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        // by default handled
        return true;
    }

    /**
     * Notified when a scroll occurs with the initial on down {@link MotionEvent} and the
     * current move {@link MotionEvent}. The distance in x and y is also supplied for
     * convenience.
     *
     * @param e1        The first down motion event that started the scrolling.
     * @param e2        The move motion event that triggered the current onScroll.
     * @param distanceX The distance along the X axis that has been scrolled since the last
     *                  call to onScroll. This is NOT the distance between {@code e1}
     *                  and {@code e2}.
     * @param distanceY The distance along the Y axis that has been scrolled since the last
     *                  call to onScroll. This is NOT the distance between {@code e1}
     *                  and {@code e2}.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        // exit the app on scroll
        return true;
    }

    /**
     * Notified when a long press occurs with the initial on down {@link MotionEvent}
     * that trigged it.
     *
     * @param e The initial on down motion event that started the longpress.
     */
    @Override
    public void onLongPress(MotionEvent e)
    {
        uninitialize();
        System.exit(0);
    }

    /**
     * Notified of a fling event when it occurs with the initial on down {@link MotionEvent}
     * and the matching up {@link MotionEvent}. The calculated velocity is supplied along
     * the x and y axis in pixels per second.
     *
     * @param e1        The first down motion event that started the fling.
     * @param e2        The move motion event that triggered the current onFling.
     * @param velocityX The velocity of this fling measured in pixels per second
     *                  along the x axis.
     * @param velocityY The velocity of this fling measured in pixels per second
     *                  along the y axis.
     * @return true if the event is consumed, else false
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return true;
    }

    private int loadGLTexture(int imageFileResourceId)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        // read the resource
        Bitmap bitmap = BitmapFactory.decodeResource(m_context.getResources(), imageFileResourceId, options);

        int[] texture = new int[1];

        // create a texture object to apply to model
        GLES32.glGenTextures(1, texture, 0);

        // indicate that pixel rows are tightly packed
        // defauls to stride of 4 which is kind of only good for RGBA or FLOAT types
        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);

        //bind with the texture
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);

        //set up filter and wrap modes for this texture object
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

        // load the bitmap into the bound texture
        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);

        // generate mipmap
        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);

        return texture[0];
    }

    private void initialize(GL10 gl)
    {
        //  ------------------------ Vertex Shader ------------------------------------- //
        m_vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        // vertex shader source code
        final String vertexShaderSourceCode = String.format(
            "#version 320 es\n" +
            "in vec4 vPosition;\n" +
            "in vec3 vNormal;\n"+
            "uniform mat4 u_modelView_matrix;\n"+
            "uniform mat4 u_projectionMatrix;\n"+
            "uniform vec3 u_Ld;\n"+
            "uniform vec3 u_Kd;\n"+
            "uniform vec4 u_lightPosition;\n"+
            "uniform mediump int u_lightMode;\n"+
            "out vec3 diffused_light;\n"+
            "out vec4 vPosition2FS;\n"+
            "out vec3 vNormal2FS;\n"+
            "void main(void)\n" +
            "{\n" +
            "   if (u_lightMode == 1)\n"+
            "   {\n"+
            "       vec4 eyeCoordinates = u_modelView_matrix * vPosition;\n"+
            "       mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));\n"+
            "       vec3 tnorm = normalize(normal_matrix * vNormal);\n"+
            "       vec3 s = normalize(vec3(u_lightPosition - eyeCoordinates));\n"+
            "       diffused_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);\n"+
            "   }\n"+
            "   gl_Position = u_projectionMatrix * u_modelView_matrix * vPosition;\n"+
            "   vPosition2FS = vPosition;\n"+
            "   vNormal2FS = vNormal\n;"+
            "}"
        );

        // provide shader source code
        GLES32.glShaderSource(m_vertexShaderObject, vertexShaderSourceCode);
        // compile shader and check for errors
        GLES32.glCompileShader(m_vertexShaderObject);
        int[] iShaderCompilationStatus = new int[1];
        GLES32.glGetShaderiv(m_vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if (iShaderCompilationStatus[0] == GLES32.GL_FALSE)
        {
            int[] iShaderInfoLogLength = new int[1];
            GLES32.glGetShaderiv(m_vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iShaderInfoLogLength, 0);
            if (iShaderInfoLogLength[0] > 0)
            {
                System.out.println("Vertex shader compilation log");
                System.out.println(GLES32.glGetShaderInfoLog(m_vertexShaderObject));
                uninitialize();
                System.exit(0);
            }
        }

        //  ------------------------ Fragment Shader ------------------------------------- //
        m_fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSource = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;\n" +
            "in vec3 diffused_light;\n"+
            "in vec4 vPosition2FS;\n"+
            "in vec3 vNormal2FS;\n"+
            "uniform mat4 u_modelView_matrix;\n"+
            "uniform mat4 u_projectionMatrix;\n"+
            "uniform vec3 u_Ld;\n"+
            "uniform vec3 u_Kd;\n"+
            "uniform mediump int u_lightMode;\n"+
            "uniform vec4 u_lightPosition;\n"+
            "out vec4 FragColor;\n"+
            "void main(void)\n"+
            "{\n"+
            "   if (u_lightMode == 1)\n"+
            "   {\n"+
            "       FragColor = vec4(diffused_light, 1.0);\n"+
            "   }\n"+
            "   else if (u_lightMode == 2)\n"+
            "   {\n"+
            "       vec4 eyeCoordinates = u_modelView_matrix * vPosition2FS;\n"+
            "       mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));\n"+
            "       vec3 tnorm = normalize(normal_matrix * vNormal2FS);\n"+
            "       vec3 s = normalize(vec3(u_lightPosition - eyeCoordinates));\n"+
            "       FragColor = vec4(u_Ld * u_Kd * max(dot(s, tnorm), 0.0), 1.0);\n"+
            "   }\n"+
            "   else\n"+
            "   {\n"+
            "       FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"+
            "   }\n"+
            "}"
        );

        GLES32.glShaderSource(m_fragmentShaderObject, fragmentShaderSource);
        GLES32.glCompileShader(m_fragmentShaderObject);
        GLES32.glGetShaderiv(m_fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if (iShaderCompilationStatus[0] == GLES32.GL_FALSE)
        {
            int[] iShaderInfoLength = new int[1];
            GLES32.glGetShaderiv(m_fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iShaderInfoLength, 0);
            if (iShaderInfoLength[0] > 0)
            {
                System.out.println("Fragment shader compilation log:");
                System.out.println(GLES32.glGetShaderInfoLog(m_fragmentShaderObject));
                uninitialize();
                System.exit(0);
            }
        }

        //  ------------------------ Shader Program ------------------------------------- //
        m_shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(m_shaderProgramObject, m_vertexShaderObject);
        GLES32.glAttachShader(m_shaderProgramObject, m_fragmentShaderObject);

        GLES32.glBindAttribLocation(m_shaderProgramObject, GLESMacros.ATTRIBUTE_VERTEX, "vPosition");
        GLES32.glBindAttribLocation(m_shaderProgramObject, GLESMacros.ATTRIBUTE_NORMAL, "vNormal");

        // link program
        GLES32.glLinkProgram(m_shaderProgramObject);
        int[] iProgramLinkStatus = new int[1];
        GLES32.glGetProgramiv(m_shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            int[] iProgramInfoLength = new int[1];
            GLES32.glGetProgramiv(m_shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iProgramInfoLength, 0);
            if (iProgramInfoLength[0] > 0)
            {
                System.out.println("Shader program link failed. Link status");
                System.out.println(GLES32.glGetProgramInfoLog(m_shaderProgramObject));
                uninitialize();
                System.exit(0);
            }
        }

        // capture uniforms location for handy use
        m_modelViewMatrixUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_modelView_matrix");
        m_projectionMatrixUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_projectionMatrix");

        m_lightingModeUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_lightMode");
        m_LdUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_Ld");
        m_KdUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_Kd");
        m_lightPositionUniform = GLES32.glGetUniformLocation(m_shaderProgramObject, "u_lightPosition");

        // ---------------------- vertices, colors, shader attribs, vbo, vao initializations -----//
        loadCubeData();

        // ------------------------------- Drawing initialization ------------------------------- //
        // enable depth testing
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        // enable backface culling to improve performance
        GLES32.glEnable(GLES20.GL_CULL_FACE);
        // set background color
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // initialize projection matrix to identity
        Matrix.setIdentityM(m_perspectiveProjectionMatrix, 0);
    }

    private void loadCubeData()
    {
        final float cubeVertices[] = new float[]
        {
            // front
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            // right
            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            // left
            -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            // back
            -1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            // top
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            // bottom
            1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f
        };

        GLES32.glGenVertexArrays(1, m_vaoCube, 0);
        GLES32.glBindVertexArray(m_vaoCube[0]);

        GLES32.glGenBuffers(2, m_vboCube, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, m_vboCube[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4); // zeroed-out buffer
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(cubeVertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_VERTEX);

        // unbind vbo 0
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // bind vbo 1 which is a normals buffer
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, m_vboCube[1]);

        final float cubeNormals[] = new float[]
        {
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,

            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,

            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,

            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,
            0.0f, 0.0f, -1.0f,

            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,

            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
            0.0f, -1.0f, 0.0f,
        };

        byteBuffer = ByteBuffer.allocateDirect(cubeNormals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer normalsBuffer = byteBuffer.asFloatBuffer();
        normalsBuffer.put(cubeNormals);
        normalsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeNormals.length * 4, normalsBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
        GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_NORMAL);

        // unbind vbo 0 followed by vao
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);
    }

    private void uninitialize()
    {
        // destroy vao
        if (m_vaoCube[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, m_vaoCube, 0);
            m_vaoCube[0] = 0;
        }

        // delete vbo
        if (m_vboCube[1] != 0)
        {
            GLES32.glDeleteBuffers(1, m_vboCube, 1);
            m_vboCube[1] = 0;
        }

        // delete vbo
        if (m_vboCube[0] != 0)
        {
            GLES32.glDeleteBuffers(1, m_vboCube, 0);
            m_vboCube[0] = 0;
        }

        if (m_shaderProgramObject != 0)
        {
            // detach and destroy shaders
            GLES32.glDetachShader(m_shaderProgramObject, m_vertexShaderObject);
            GLES32.glDetachShader(m_shaderProgramObject, m_fragmentShaderObject);

            // delete shader program
            GLES32.glDeleteProgram(m_shaderProgramObject);
            m_shaderProgramObject = 0;
        }

        if (m_fragmentShaderObject != 0)
        {
            GLES32.glDeleteShader(m_fragmentShaderObject);
            m_fragmentShaderObject = 0;
        }

        if (m_vertexShaderObject != 0)
        {
            GLES32.glDeleteShader(m_vertexShaderObject);
            m_vertexShaderObject = 0;
        }
    }

    private void resize(int width, int height)
    {
        GLES32.glViewport(0, 0, width, height);

        if (height == 0)
            height = 1;

        float aspect = (float)width / (float)height;
        Matrix.perspectiveM(
                m_perspectiveProjectionMatrix,
                0,
                45.0f,
                aspect,
                0.1f,
                100.0f);
    }

    private void drawCube()
    {
        float modelViewMatrix[] = new float[16];
        Matrix.setIdentityM(modelViewMatrix, 0);

        // translate matrix
        Matrix.translateM(modelViewMatrix, 0,0.0f, 0.0f, -5.0f);

        if (m_enableAnimation)
        {
            Matrix.rotateM(modelViewMatrix, 0, m_angleCube, 1.0f, 0.0f, 0.0f);
            Matrix.rotateM(modelViewMatrix, 0, m_angleCube, 0.0f, 1.0f, 0.0f);
            Matrix.rotateM(modelViewMatrix, 0, m_angleCube, 0.0f, 0.0f, 1.0f);
        }

        m_angleCube += 1.0f;
        if (m_angleCube == 360.0f)
            m_angleCube = 0.0f;

        GLES32.glUniformMatrix4fv(m_modelViewMatrixUniform, 1, false, modelViewMatrix, 0);
        GLES32.glUniformMatrix4fv(m_projectionMatrixUniform, 1, false, m_perspectiveProjectionMatrix, 0);

        // lighting specific variables
        GLES32.glUniform1i(m_lightingModeUniform, m_lightingMode);
        GLES32.glUniform3f(m_LdUniform, 1.0f, 1.0f, 1.0f);
        GLES32.glUniform3f(m_KdUniform, 0.5f, 0.5f, 0.5f);

        float lightPosition[] = { 0.0f, 0.0f, 5.0f, 1.0f };
        GLES32.glUniform4fv(m_lightPositionUniform, 1, lightPosition, 0);

        GLES32.glBindVertexArray(m_vaoCube[0]);

        //draw either by glDrawArrays, glDrawTraiangles or glDrawElements
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(m_shaderProgramObject);

        drawCube();

        GLES32.glUseProgram(0);

        // render/flush
        requestRender();
    }
}
