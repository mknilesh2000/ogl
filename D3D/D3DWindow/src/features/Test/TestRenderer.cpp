#pragma once

#include "TestRenderer.h"

namespace Features
{
    using namespace Interfaces;

#define EXIT_IF_ERROR(x) if (x == GL_FALSE) { __debugbreak(); return RENDERER_RESULT_ERROR; }
#define CALL_AND_RESET_IF_VALID(x, y) if (x != NULL) { y;  x = NULL; }

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

    enum ATTRIBUTE
    {
        POSITION = 0,
        COLOR,
        NORMAL,
        TEXCOORD0
    };

    TestRenderer::TestRenderer() :
        m_fp(nullptr),
        m_pIDxgiSwapChain(nullptr),
        m_pID3dDevice(nullptr),
        m_pID3dDeviceContext(nullptr),
        m_pID3d11RenderTargetView(nullptr),
        m_clearColor{0.0f, 0.0f, 1.0f, 1.0f}
    {
        fopen_s(&m_fp, "output.log", "w+");
        if (m_fp == nullptr)
            __debugbreak();
        fprintf(m_fp, "Opened log-file\n");
    }

    void TestRenderer::RefreshLogFile()
    {
        if (m_fp != nullptr)
        {
            fclose(m_fp);
            fopen_s(&m_fp, "output.log", "a+");
            if (m_fp == nullptr)
                __debugbreak();
        }
    }

    TestRenderer::~TestRenderer()
    {
        if (m_fp != nullptr)
        {
            fprintf(m_fp, "Closing log-file\n");
            fclose(m_fp);
            m_fp = nullptr;
        }
    }

    /// Method for retrieving name of the renderer
    /// @param rendererName buffer to be filled with the renderer name
    /// @return RENDERER_RESULT_SUCCESS if succeded, rendererName to contain null-terminated name
    ///         RENDERER_RESULT_ERROR if failed, renderName is ignored
    const char* TestRenderer::GetName()
    {
        static const char name[] = "TestRenderer";
        return name;
    }

    /// Method for performing one-time renderer initialization.
    /// Renderer can initialize global/static instances as part of this method
    /// @param window identifier of the window where drawing is directed
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::Initialize(Window window)
    {
        RendererResult result = RENDERER_RESULT_ERROR;
        HRESULT hr = S_FALSE;
        D3D_DRIVER_TYPE d3dDriverType = {};
        D3D_DRIVER_TYPE d3dDriverTypes[] = {
            D3D_DRIVER_TYPE_HARDWARE,
            D3D_DRIVER_TYPE_WARP,
            D3D_DRIVER_TYPE_REFERENCE
        };
        D3D_FEATURE_LEVEL d3dFeatureLevelRequired = D3D_FEATURE_LEVEL_11_0;
        D3D_FEATURE_LEVEL d3dFeatureLevelAcquired = D3D_FEATURE_LEVEL_10_0;
        UINT createDeviceflags = 0;
        UINT numDriverTypes = _countof(d3dDriverTypes);
        UINT numFeatureLevels = 1;

        DXGI_SWAP_CHAIN_DESC dxgiSwapcChainDesc = {};
        dxgiSwapcChainDesc.BufferCount = 1;
        dxgiSwapcChainDesc.BufferDesc.Width = WIN_WIDTH;
        dxgiSwapcChainDesc.BufferDesc.Height = WIN_HEIGHT;
        dxgiSwapcChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        dxgiSwapcChainDesc.BufferDesc.RefreshRate.Numerator = 60;
        dxgiSwapcChainDesc.BufferDesc.RefreshRate.Denominator = 1;
        dxgiSwapcChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        dxgiSwapcChainDesc.OutputWindow = window;
        dxgiSwapcChainDesc.SampleDesc.Count = 1;
        dxgiSwapcChainDesc.SampleDesc.Quality = 0;
        // dxgiSwapcChainDesc.SwapEffect = ;
        dxgiSwapcChainDesc.Windowed = TRUE;

        for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; ++driverTypeIndex)
        {
            d3dDriverType = d3dDriverTypes[driverTypeIndex];
            hr = D3D11CreateDeviceAndSwapChain(
                NULL,   // pAdapter
                d3dDriverType,
                NULL,   // Software
                createDeviceflags,
                &d3dFeatureLevelRequired,
                numFeatureLevels,
                D3D11_SDK_VERSION,
                &dxgiSwapcChainDesc,
                &m_pIDxgiSwapChain,
                &m_pID3dDevice,
                &d3dFeatureLevelAcquired,
                &m_pID3dDeviceContext);
            if (SUCCEEDED(hr))
            {
                fprintf_s(m_fp, "D3D11CreateDeviceAndSwapChain succeded \n");
                fprintf_s(m_fp, "The chosen driver is of: ");
                switch (d3dDriverType)
                {
                case D3D_DRIVER_TYPE_HARDWARE:
                    fprintf_s(m_fp, "Hardware Type \n");
                    break;

                case D3D_DRIVER_TYPE_WARP:
                    fprintf_s(m_fp, "Warp Type \n");
                    break;

                case D3D_DRIVER_TYPE_REFERENCE:
                    fprintf_s(m_fp, "Reference Type \n");
                    break;

                default:
                    fprintf_s(m_fp, "Unknown Type \n");
                    break;
                }
                fprintf_s(m_fp, "Supported Highest feature level is: ");
                switch (d3dFeatureLevelAcquired)
                {
                case D3D_FEATURE_LEVEL_11_0:
                    fprintf_s(m_fp, "11.0 \n");
                    break;

                case D3D_FEATURE_LEVEL_10_0:
                    fprintf_s(m_fp, "10.0 \n");
                    break;

                default:
                    fprintf_s(m_fp, "Unknown \n");
                    break;
                }
                RefreshLogFile();
                break;
            }
        }

        if (SUCCEEDED(hr))
        {
            OnResize(WIN_WIDTH, WIN_HEIGHT);
            result = RENDERER_RESULT_SUCCESS;
        }
        else
        {
            fprintf_s(m_fp, "D3D11CreateDeviceAndSwapChain failed with %u \n", hr);
        }

        return result;
    }

    /// Method for performing one-time renderer un-initialization before it is unloaded
    /// Renderer can perform global cleanup as part of this method
    void TestRenderer::Uninitialize(void)
    {
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host before rendering a scene to the active renderer.
    /// Renderer should do initialization of scene specific things as part of this method
    /// @param scene Identifier of a scene to be initialized
    /// @return RENDERER_RESULT_SUCCESS if succeded
    ///         RENDERER_RESULT_ERROR if failed.
    RendererResult TestRenderer::InitializeScene(SceneType scene)
    {
        return RENDERER_RESULT_SUCCESS;
    }

    /// Method for performing scene-specific initialization
    /// This method will be called by the host after rendering a scene to the active renderer
    /// Renderer should do cleanup of scene specific things done as part of scene initialize.
    /// @param scene Identifier of a scene to be cleaned-up
    void TestRenderer::UninitializeScene(SceneType scene)
    {
    }

    /// Method for rendering a frame in a scene
    /// This method will be called by the host per frame of a scene only to the active renderer
    /// @param params describes the parameters curresponding to this render
    /// @return RENDERER_RESULT_SUCCESS if succeded in building the frame
    ///         RENDERER_RESULT_ERROR if failed in building the frame
    ///         RENDERER_RESULT_FINISHED if renderer has finished building its last frame of the scene.
    ///                                   in such cases no further frame calls would be made for this scene
    ///                                   to the renderer.
    RendererResult TestRenderer::Render(const RenderParams &params)
    {
        // clear render target with the specified color
        m_pID3dDeviceContext->ClearRenderTargetView(m_pID3d11RenderTargetView, m_clearColor);

        // switch between front and back buffers
        m_pIDxgiSwapChain->Present(0, 0);

        return RENDERER_RESULT_SUCCESS;
    }

    /// Generic method to notify active renderer about a message posted to host window. The message can be
    /// from system or initiated by the host itself.
    /// @param message OS dependent structure that describes the system message being processed.
    void TestRenderer::OnMessage(const Message &message)
    {
    }

    /// Generic method to notify active renderer about the change in the dimensions of the host window
    /// @param width New width of the window
    /// @param height New height of the window
    void TestRenderer::OnResize(unsigned int width, unsigned int height)
    {
        HRESULT hr = S_OK;
        if (m_pID3d11RenderTargetView != nullptr)
        {
            m_pID3d11RenderTargetView->Release();
            m_pID3d11RenderTargetView = nullptr;
        }

        // resize buffers according to requested width and height
        m_pIDxgiSwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
        fprintf_s(m_fp, "ResizeBuffers returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();

        // get the back buffer from the swapchain
        ID3D11Texture2D *pID3D11Texture2D_BackBuffer = nullptr;
        hr = m_pIDxgiSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);
        fprintf_s(m_fp, "GetBuffer pID3D11Texture2D_BackBuffer returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();

        // get render target view from d3d11 device using above back buffer
        hr = m_pID3dDevice->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, nullptr, &m_pID3d11RenderTargetView);
        fprintf_s(m_fp, "CreateRenderTargetView pID3D11Texture2D_BackBuffer returned %u: Result %s\n", hr, SUCCEEDED(hr) ? "succeded" : "failed");
        RefreshLogFile();
        pID3D11Texture2D_BackBuffer->Release();
        pID3D11Texture2D_BackBuffer = nullptr;

        m_pID3dDeviceContext->OMSetRenderTargets(1, &m_pID3d11RenderTargetView, nullptr);

        // set viewport
        D3D11_VIEWPORT d3dViewport;
        ZeroMemory(&d3dViewport, sizeof(d3dViewport));
        d3dViewport.TopLeftX = 0;
        d3dViewport.TopLeftY = 0;
        d3dViewport.Width = (FLOAT)width;
        d3dViewport.Height = (FLOAT)height;
        d3dViewport.MinDepth = 0.0f;
        d3dViewport.MaxDepth = 1.0f;
        m_pID3dDeviceContext->RSSetViewports(1, &d3dViewport);
    }
}
