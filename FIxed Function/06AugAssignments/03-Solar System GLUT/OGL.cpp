#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen
#define WIN_WIDTH 600
#define WIN_HEIGHT 600

static int year = 0, day = 0;

int main(int argc, char** argv)
{
    //function prototypes
    void display(void);
    void resize(int, int);
    void keyboard(unsigned char, int, int);
    void mouse(int, int, int, int);
    void initialize(void);
    void uninitialize(void);

    int width = 0, height = 0;
    width = GetSystemMetrics(SM_CXSCREEN);
    height = GetSystemMetrics(SM_CYSCREEN);

    //code
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT); //to declare initial window size
    glutInitWindowPosition((width - WIN_WIDTH) / 2, (height - WIN_HEIGHT) / 2); //to declare initial window position
    glutCreateWindow("GLUT:: Solar System"); //open the window with "OpenGL First Window : Hello World" in the title bar

    initialize();

    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutCloseFunc(uninitialize);

    glutMainLoop();

    return(0);
}

void display(void)
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3f(1.0f, 1.0f, 0.0f);

    glPushMatrix();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glutWireSphere(1.0f, 100, 100);//sun
    glRotatef((GLfloat)year, 0.0f, 1.0f, 0.0f);
    glTranslatef(2.0f, 0.0f, 0.0f);
    glRotatef((GLfloat)day, 0.0f, 1.0f, 0.0f);

    glColor3f(0.0f, 0.0f, 1.0f);
    glutWireSphere(0.2, 100, 80);//planet
    glPopMatrix();

    //to process buffered OpenGL Routines
    glutSwapBuffers();
}

void initialize(void)
{
    //code
    //to select clearing (background) clear
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //black
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void keyboard(unsigned char key, int x, int y)
{
    //code
    switch (key)
    {
    case 27: // Escape
        glutLeaveMainLoop();
        break;
    case 'F':
    case 'f':
        if (bFullscreen == false)
        {
            glutFullScreen();
            bFullscreen = true;
        }
        else
        {
            glutLeaveFullScreen();
            bFullscreen = false;
        }
        break;
    case 'd':
        day = (day + 10) % 360;
        glutPostRedisplay();
        break;
    case 'D':
        day = (day - 10) % 360;
        glutPostRedisplay();
        break;
    case 'y':
        year = (year + 10) % 360;
        glutPostRedisplay();
        break;
    case 'Y':
        year = (year - 10) % 360;
        glutPostRedisplay();
        break;

    default:
        break;
    }
}

void mouse(int button, int state, int x, int y)
{
    //code
    switch (button)
    {
    case GLUT_LEFT_BUTTON:
        break;
    default:
        break;
    }
}

void resize(int width, int height)
{
    // code
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (GLfloat)width / (GLfloat)height, 1.0f, 20.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
}

void uninitialize(void)
{
    // code
}

