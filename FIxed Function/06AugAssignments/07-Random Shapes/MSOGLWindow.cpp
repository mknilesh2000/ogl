#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 800

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    //function prototype
    void initialize(void);
    void uninitialize(void);
    void display(void);

    //variable declaration
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szClassName[] = TEXT("RTROGL");
    bool bDone = false;

    int width = 0, height = 0;
    width = GetSystemMetrics(SM_CXSCREEN);
    height = GetSystemMetrics(SM_CYSCREEN);

    //code
    //initializing members of struct WNDCLASSEX
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szClassName;
    wndclass.lpszMenuName = NULL;

    //Registering Class
    RegisterClassEx(&wndclass);

    //Create Window
    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szClassName,
        TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : Random Shapes"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        (width - WIN_WIDTH) / 2,
        (height - WIN_HEIGHT) / 2,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd = hwnd;

    //initialize
    initialize();

    ShowWindow(hwnd, SW_SHOW);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    //Message Loop
    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                    bDone = true;
                display();
            }
        }
    }

    uninitialize();
    return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //function prototype
    void resize(int, int);
    void ToggleFullscreen(void);
    void uninitialize(void);

    //code
    switch (iMsg)
    {
    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0)
            gbActiveWindow = true;
        else
            gbActiveWindow = false;
        break;
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            gbEscapeKeyIsPressed = true;
            break;
        case 0x46: //for 'f' or 'F'
            if (gbFullscreen == false)
            {
                ToggleFullscreen();
                gbFullscreen = true;
            }
            else
            {
                ToggleFullscreen();
                gbFullscreen = false;
            }
            break;
        default:
            break;
        }
        break;
    case WM_LBUTTONDOWN:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        break;
    }
    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    //variable declarations
    MONITORINFO mi;

    //code
    if (gbFullscreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi.cbSize = sizeof(MONITORINFO);
            if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
    }

    else
    {
        //code
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }
}

void initialize(void)
{
    //function prototypes
    void resize(int, int);

    //variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    //code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    //Initialization of structure 'PIXELFORMATDESCRIPTOR'
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;

    ghdc = GetDC(ghwnd);

    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if (iPixelFormatIndex == 0)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    resize(WIN_WIDTH, WIN_HEIGHT);
}

void DrawFigure1()
{
    int i = 0, j = 0;
    float x = 0.0f, y = 0.0f;

    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_POINTS);
    glPointSize(4);
    for (i = 0; i < 4; i++)
    {
        y = 0.0f;
        for (j = 0; j < 4; j++)
        {
            glVertex3f(x, y, 0.0f);
            y += 0.1f;
        }
        x += 0.1f;
    }
    glEnd();
}

void DrawFigure2()
{
    void DrawFigure4(float, float);

    DrawFigure4(-0.15f, 0.2f);
    glLoadIdentity();
    glTranslatef(-0.15f, 0.2f, 0.0f);
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.3f, 0.0f, 0.0f);

    glVertex3f(0.3f, 0.0f, 0.0f);
    glVertex3f(0.3f, 0.3f, 0.0f);
    glEnd();
}

void DrawFigure3()
{
    int i = 0;
    float x = 0.0f, y = 0.0f;

    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_LINES);
    for (i = 0; i < 4; i++)
    {
        glVertex3f(0.0f, y, 0.0f);
        glVertex3f(0.3f, y, 0.0f);

        glVertex3f(x, 0.0f, 0.0f);
        glVertex3f(x, 0.3f, 0.0f);

        x += 0.1f;
        y += 0.1f;
    }
    glEnd();
}

void DrawSmallSquareAndDiagonal()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_LINE_STRIP);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.1f, 0.0f);
    glVertex3f(0.1f, 0.1f, 0.0f);
    glVertex3f(0.1f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.1f, 0.1f, 0.0f);
    glEnd();
}

void DrawFigure4(float translateX, float translateY)
{
    int i = 0;
    int j = 0;
    float x = 0.0f;
    float y = 0.0f;

    for (i = 0; i < 3; i++)
    {
        y = 0.0f;
        for (j = 0; j < 3; j++)
        {
            glLoadIdentity();
            glTranslatef(translateX, translateY, 0.0f);
            glTranslatef(x, y, 0.0f);
            DrawSmallSquareAndDiagonal();
            y += 0.1f;
        }
        x += 0.1f;
    }
}


void DrawFigure5()
{
    int i = 0;
    float x = 0.0f, y = 0.0f;
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_LINES);

    for (i = 0; i < 4; i++)
    {
        glVertex3f(0.0f, 0.3f, 0.0f);
        glVertex3f(x, 0.0f, 0.0f);

        glVertex3f(0.0f, 0.3f, 0.0f);
        glVertex3f(0.3f, y, 0.0f);

        x += 0.1f;
        y += 0.1f;
    }
    glEnd();

    glLoadIdentity();
    glTranslatef(-0.15f, -0.5f, 0.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.3f, 0.0f, 0.0f);

    glVertex3f(0.3f, 0.0f, 0.0f);
    glVertex3f(0.3f, 0.3f, 0.0f);
    glEnd();

}


void DrawColorRect(float r, float g, float b)
{
    glColor3f(r, g, b);
    glBegin(GL_POLYGON);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.1f, 0.0f, 0.0f);
    glVertex3f(0.1f, 0.3f, 0.0f);
    glVertex3f(0.0f, 0.3f, 0.0f);
    glEnd();

    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.1f, 0.0f);
    glVertex3f(0.1f, 0.1f, 0.0f);

    glVertex3f(0.0f, 0.2f, 0.0f);
    glVertex3f(0.1f, 0.2f, 0.0f);
    glEnd();
}

void DrawFigure6()
{
    int i = 0;

    float x = 0.0f;

    float r = 1.0f;
    float g = 0.0f;
    float b = 0.0f;

    for (i = 0; i < 3; i++)
    {
        glLoadIdentity();
        glTranslatef(0.5f, -0.5f, 0.0f);
        glTranslatef(x, 0.0f, 0.0f);
        DrawColorRect(r, g, b);
        if (i == 0)
        {
            r = 0.0f;
            g = 1.0f;
        }
        else if (i == 1)
        {
            r = 0.0f;
            g = 0.0f;
            b = 1.0f;
        }
        x += 0.1f;
    }
}
void display(void)
{
    //code
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    glTranslatef(-0.8f, 0.2f, 0.0f);
    DrawFigure1();

    glLoadIdentity();
    glTranslatef(-0.15f, 0.2f, 0.0f);
    DrawFigure2();

    glLoadIdentity();
    glTranslatef(0.5f, 0.2f, 0.0f);
    DrawFigure3();

    DrawFigure4(-0.8f, -0.5f);

    glLoadIdentity();
    glTranslatef(-0.15f, -0.5f, 0.0f);
    DrawFigure5();

    DrawFigure6();

    SwapBuffers(ghdc);
}

void resize(int width, int height)
{
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (width <= height)
        glOrtho(-1.0f, 1.0f, -1.0f*(GLfloat)height / (GLfloat)width, 1.0f*(GLfloat)height / (GLfloat)width, -1.0f, 1.0f);
    else
        glOrtho(-1.0f*(GLfloat)width / (GLfloat)height, 1.0f*(GLfloat)width / (GLfloat)height, -1.0f, 1.0f, -1.0f, 1.0f);
}

void uninitialize(void)
{
    //UNINITIALIZATION CODE

    if (gbFullscreen == true)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    wglMakeCurrent(NULL, NULL);

    wglDeleteContext(ghrc);
    ghrc = NULL;

    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;

    DestroyWindow(ghwnd);
    ghwnd = NULL;
}
