#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>

#define WIN_WIDTH 600
#define WIN_HEIGHT 600
#define SAFFRON_COLOR_R 1.0f
#define SAFFRON_COLOR_G 0.6f
#define SAFFRON_COLOR_B 0.2f

#define GREEN_COLOR_R 0.074f
#define GREEN_COLOR_G 0.533f
#define GREEN_COLOR_B 0.031f

#define SPEED 0.005f

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gbMoveStart = true;

int giCurrentLetter = 1;

float gfX = -1.0f;
float gfY = 0.0f;

float gfSaffronR = 0.0f;
float gfSaffronG = 0.0f;
float gfSaffronB = 0.0f;

float gfGreenR = 0.0f;
float gfGreenG = 0.0f;
float gfGreenB = 0.0f;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    //function prototype
    void initialize(void);
    void uninitialize(void);
    void display(void);

    //variable declaration
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szClassName[] = TEXT("RTROGL");
    bool bDone = false;

    int width = 0, height = 0;
    width = GetSystemMetrics(SM_CXSCREEN);
    height = GetSystemMetrics(SM_CYSCREEN);

    //code
    //initializing members of struct WNDCLASSEX
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szClassName;
    wndclass.lpszMenuName = NULL;

    //Registering Class
    RegisterClassEx(&wndclass);

    //Create Window
    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szClassName,
        TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : INDIA"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        (width - WIN_WIDTH) / 2,
        (height - WIN_HEIGHT) / 2,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd = hwnd;

    //initialize
    initialize();

    ShowWindow(hwnd, SW_SHOW);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    //Message Loop
    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                    bDone = true;
                display();
            }
        }
    }

    uninitialize();
    return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //function prototype
    void resize(int, int);
    void ToggleFullscreen(void);
    void uninitialize(void);

    //code
    switch (iMsg)
    {
    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0)
            gbActiveWindow = true;
        else
            gbActiveWindow = false;
        break;
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            gbEscapeKeyIsPressed = true;
            break;
        case 0x46: //for 'f' or 'F'
            if (gbFullscreen == false)
            {
                ToggleFullscreen();
                gbFullscreen = true;
            }
            else
            {
                ToggleFullscreen();
                gbFullscreen = false;
            }
            break;
        default:
            break;
        }
        break;
    case WM_LBUTTONDOWN:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        break;
    }
    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    //variable declarations
    MONITORINFO mi;

    //code
    if (gbFullscreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi.cbSize = sizeof(MONITORINFO);
            if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
    }

    else
    {
        //code
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }
}

void initialize(void)
{
    //function prototypes
    void resize(int, int);

    //variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    //code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    //Initialization of structure 'PIXELFORMATDESCRIPTOR'
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;

    ghdc = GetDC(ghwnd);

    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if (iPixelFormatIndex == 0)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    resize(WIN_WIDTH, WIN_HEIGHT);
}

void PaintGreen()
{
    glColor3f(GREEN_COLOR_R, GREEN_COLOR_G, GREEN_COLOR_B);
}

void PaintSaffron()
{
    glColor3f(SAFFRON_COLOR_R, SAFFRON_COLOR_G, SAFFRON_COLOR_B);
}

void PaintWhite()
{
    glColor3f(1.0f, 1.0f, 1.0f);
}

void DrawI()
{
    glBegin(GL_LINES);
    PaintSaffron();
    glVertex3f(0.0f, 0.7f, 0.0f);
    PaintGreen();
    glVertex3f(0.0f, -0.7f, 0.0f);
    glEnd();
}

void DrawN()
{
    glBegin(GL_LINE_STRIP);
    PaintGreen();
    glVertex3f(-0.1f, -0.7f, 0.0f);
    PaintSaffron();
    glVertex3f(-0.1f, 0.7f, 0.0f);
    PaintGreen();
    glVertex3f(0.1f, -0.7f, 0.0f);
    PaintSaffron();
    glVertex3f(0.1f, 0.7f, 0.0f);
    glEnd();
}

void DrawD()
{
    glBegin(GL_LINES);
    glColor3f(gfSaffronR, gfSaffronG, gfSaffronB);
    glVertex3f(-0.1f, 0.7f, 0.0f);

    glColor3f(gfGreenR, gfGreenG, gfGreenB);
    glVertex3f(-0.1f, -0.7f, 0.0f);
    glEnd();

    glBegin(GL_LINE_STRIP);
    glColor3f(gfSaffronR, gfSaffronG, gfSaffronB);
    glVertex3f(-0.12f, 0.7f, 0.0f);
    glVertex3f(0.1f, 0.7f, 0.0f);

    glColor3f(gfGreenR, gfGreenG, gfGreenB);
    glVertex3f(0.1f, -0.7f, 0.0f);
    glVertex3f(-0.12f, -0.7f, 0.0f);
    glEnd();
}

void DrawI2()
{
    glBegin(GL_LINES);
    if (gfY < 0.4f)
    {
        PaintSaffron();
        glVertex3f(0.0f, gfY, 0.0f);
        PaintGreen();
        glVertex3f(0.0f, -1.0f, 0.0f);
    }
    else
    {
        PaintSaffron();
        glVertex3f(0.0f, gfY, 0.0f);
        PaintGreen();
        glVertex3f(0.0f, gfY - (0.7f*2.0f), 0.0f);
    }

    if (gfY > 0.7f)
    {
        giCurrentLetter++;
        gfX = 1.0f;
    }
    else
    {
        gfY += SPEED;
    }
    glEnd();
}

void DrawA()
{
    glBegin(GL_LINE_STRIP);
    PaintGreen();
    glVertex3f(-0.15f, -0.7f, 0.0f);

    PaintSaffron();
    glVertex3f(0.0f, 0.7f, 0.0f);

    PaintGreen();
    glVertex3f(0.15f, -0.7f, 0.0f);
    glEnd();
}

void DrawTriColor()
{
    glBegin(GL_LINES);

    if (gfX > 0.075f && gbMoveStart)
    {
        gbMoveStart = false;
        gfX = -1.65f;
    }

    if (gbMoveStart)
    {
        PaintSaffron();
        glVertex3f(-1.65f, 0.01f, 0.0f);
        glVertex3f(gfX, 0.01f, 0.0f);

        PaintWhite();
        glVertex3f(-1.65f, 0.0f, 0.0f);
        glVertex3f(gfX, 0.0f, 0.0f);

        PaintGreen();
        glVertex3f(-1.65f, -0.01f, 0.0f);
        glVertex3f(gfX, -0.01f, 0.0f);
    }
    else
    {
        PaintSaffron();
        glVertex3f(gfX, 0.01f, 0.0f);
        glVertex3f(0.075f, 0.01f, 0.0f);

        PaintWhite();
        glVertex3f(gfX, 0.0f, 0.0f);
        glVertex3f(0.075f, 0.0f, 0.0f);

        PaintGreen();
        glVertex3f(gfX, -0.01f, 0.0f);
        glVertex3f(0.075f, -0.01f, 0.0f);
    }

    glEnd();

}


void DrawINDIA()
{
    switch (giCurrentLetter)
    {
    case 1:// 'I'
        glLoadIdentity();
        glTranslatef(gfX, gfY, 0.0f);
        if (gfX > -0.7f)
        {
            giCurrentLetter++;
            gfX = -0.4f;
            gfY = 1.0f;
        }
        else
            gfX += SPEED;
        DrawI();
        break;

    case 2://'N'
        glLoadIdentity();
        glTranslatef(-0.7f, 0.0f, 0.0f);
        DrawI();

        glLoadIdentity();
        glTranslatef(gfX, gfY, 0.0f);
        if (gfY < 0.0f)
        {
            giCurrentLetter++;
            gfX = -0.4f;
            gfY = 0.0f;
        }
        else
            gfY -= SPEED;
        DrawN();
        break;

    case 3://'D'
        glLoadIdentity();
        glTranslatef(-0.7f, 0.0f, 0.0f);
        DrawI();

        glLoadIdentity();
        glTranslatef(-0.4f, 0.0f, 0.0f);
        DrawN();

        glLoadIdentity();
        if (gfSaffronR > 1.0f)
        {
            giCurrentLetter++;
            gfY = -1.0f;
        }
        else
        {
            gfSaffronR += (1.0f * SPEED);
            gfSaffronG += (0.6f * SPEED);
            gfSaffronB += (0.2f * SPEED);

            gfGreenR += (0.074f * SPEED);
            gfGreenG += (0.533f * SPEED);
            gfGreenB += (0.031f * SPEED);
        }
        DrawD();
        break;

    case 4://'I'
        glLoadIdentity();
        glTranslatef(-0.7f, 0.0f, 0.0f);
        DrawI();

        glLoadIdentity();
        glTranslatef(-0.4f, 0.0f, 0.0f);
        DrawN();

        glLoadIdentity();
        DrawD();

        glLoadIdentity();
        glTranslatef(0.3f, 0.0f, 0.0f);
        DrawI2();
        break;
    case 5://'A'
        glLoadIdentity();
        glTranslatef(-0.7f, 0.0f, 0.0f);
        DrawI();

        glLoadIdentity();
        glTranslatef(-0.4f, 0.0f, 0.0f);
        DrawN();

        glLoadIdentity();
        DrawD();

        glLoadIdentity();
        glTranslatef(0.3f, 0.0f, 0.0f);
        DrawI();

        glLoadIdentity();
        glTranslatef(gfX, 0.0f, 0.0f);
        if (gfX < 0.65f)
        {
            gfX = -1.65f;
            giCurrentLetter++;
        }
        else
        {
            gfX -= SPEED;
        }
        DrawA();
        break;

    case 6://TriColor
        glLoadIdentity();
        glTranslatef(-0.7f, 0.0f, 0.0f);
        DrawI();

        glLoadIdentity();
        glTranslatef(-0.4f, 0.0f, 0.0f);
        DrawN();

        glLoadIdentity();
        DrawD();

        glLoadIdentity();
        glTranslatef(0.3f, 0.0f, 0.0f);
        DrawI();

        glLoadIdentity();
        glTranslatef(0.65f, 0.0f, 0.0f);
        DrawA();

        DrawTriColor();
        if (gfX > -0.075f && !gbMoveStart)
            ;
        else
            gfX += SPEED;
    }

}
void display(void)
{
    //code
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLineWidth(4);

    DrawINDIA();
    SwapBuffers(ghdc);
}

void resize(int width, int height)
{
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(0.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
    //UNINITIALIZATION CODE

    if (gbFullscreen == true)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    wglMakeCurrent(NULL, NULL);

    wglDeleteContext(ghrc);
    ghrc = NULL;

    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;

    DestroyWindow(ghwnd);
    ghwnd = NULL;
}
