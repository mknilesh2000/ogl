#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen=false; //variable to toggle for fullscreen

int main(int argc,char** argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(800,800); //to declare initial window size
	glutInitWindowPosition(100,100); //to declare initial window position
	glutCreateWindow("GLUT: Nilesh's Window !!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

//	return(0); 
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    // draw a red colored horizontal line of width 3.0
    glLineWidth(3.0f);
    glBegin(GL_LINES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(-1.0f, 0.0f, 0.0f);
        glVertex3f(1.0f, 0.0f, 0.0f);
    glEnd();

    // draw 20 equally spaced blue lines of width 1.0 above and below red line
    glLineWidth(1.0f);
    glBegin(GL_LINES);
        glColor3f(0.0f, 0.0f, 1.0f);
        float totalParts = 21.0f;
        for (float lineIdx = 1.0f; lineIdx < totalParts; lineIdx += 1.0f)
        {
            float thisYMag = lineIdx / totalParts;
            // draw line above
            glVertex3f(-1.0f, thisYMag, 0.0f);
            glVertex3f(1.0f, thisYMag, 0.0f);
            // draw line below
            glVertex3f(-1.0f, -thisYMag, 0.0f);
            glVertex3f(1.0f, -thisYMag, 0.0f);
        }
    glEnd();

    glutSwapBuffers();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f,0.0f,0.0f,0.0f); //blue 
}

void keyboard(unsigned char key,int x,int y)
{
	//code
	switch(key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if(bFullscreen==false)
		{
			glutFullScreen();
			bFullscreen=true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen=false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button,int state,int x,int y)
{
	//code
	switch(button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width,int height)
{
	// code
    glViewport(0, 0, width, height);
}

void uninitialize(void)
{
	// code
}

