#pragma once
#include <gl/GL.h>
#include <math.h>
#define M_PI acos(-1.0)

struct Vertex3f
{
    GLfloat x;
    GLfloat y;
    GLfloat z;
};

struct Color3f
{
    GLfloat r;
    GLfloat g;
    GLfloat b;
};

void drawGrid();
bool drawSolidTriangle(size_t numColors, const Color3f *pColorArray, const Vertex3f *pVertices);
bool drawSolidQuad(size_t numColors, const Color3f *pColorArray, const Vertex3f *pVertices);
bool drawLineLoop(GLfloat lineWidth, size_t numVertices, const Vertex3f *pVertexArray, size_t numColors, const Color3f *pColorArray);
void drawCircleWithPoints(Color3f borderColor, Vertex3f center, GLfloat radius, bool drawWithAspectCorrection = true);
void drawCircleWithLines(Color3f borderColor, Vertex3f center, GLfloat radius, bool drawWithAspectCorrection = true);
void drawEquilateralTriangle(GLfloat radius, GLfloat borderWidth, Color3f borderColor, bool drawWithAspectCorrection = true);
