#include <windows.h>
#include "DrawHelper.h"

void drawGrid()
{
    glLineWidth(3.0f);
    glBegin(GL_LINES);
        // draw a red colored horizontal line of width 3.0
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(-1.0f, 0.0f, 0.0f);
        glVertex3f(1.0f, 0.0f, 0.0f);
        // draw a green colored vertical line of width 3.0
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3f(0.0f, -1.0f, 0.0f);
        glVertex3f(0.0f, 1.0f, 0.0f);
    glEnd();

    // draw grid of 20 equally spaced blue lines of width 1.0
    glLineWidth(1.0f);
    glBegin(GL_LINES);
        glColor3f(0.0f, 0.0f, 1.0f);
        float totalParts = 21.0f;
        for (float lineIdx = 1.0f; lineIdx < totalParts; lineIdx += 1.0f)
        {
            float thisMag = lineIdx / totalParts;
            // draw line before
            glVertex3f(-thisMag, -1.0f, 0.0f);
            glVertex3f(-thisMag, 1.0f, 0.0f);
            // draw line after
            glVertex3f(thisMag, -1.0f, 0.0f);
            glVertex3f(thisMag, 1.0f, 0.0f);
            // draw line above
            glVertex3f(-1.0f, thisMag, 0.0f);
            glVertex3f(1.0f, thisMag, 0.0f);
            // draw line below
            glVertex3f(-1.0f, -thisMag, 0.0f);
            glVertex3f(1.0f, -thisMag, 0.0f);
        }
    glEnd();
}

bool drawLineLoop(GLfloat lineWidth, size_t numVertices, const Vertex3f *pVertexArray, size_t numColors, const Color3f *pColorArray)
{
    bool successful = false;
    if (pVertexArray != nullptr && numVertices > 0 &&
        pColorArray != nullptr && numColors > 0)
    {
        glLineWidth(lineWidth);
        glBegin(GL_LINE_LOOP);
            for (size_t i = 0; i < numVertices; ++i)
            {
                glColor3f(pColorArray[i%numColors].r, pColorArray[i%numColors].g, pColorArray[i%numColors].b);
                glVertex3f(pVertexArray[i].x, pVertexArray[i].y, pVertexArray[i].z);
            }
        glEnd();
        successful = true;
    }
    return successful;
}

bool drawSolidTriangle(size_t numColors, const Color3f *pColorArray, const Vertex3f *pVertices)
{
    bool succeded = false;
    if (numColors > 0 && pColorArray != nullptr && pVertices != nullptr)
    {
        glBegin(GL_TRIANGLES);
        for (size_t i = 0; i < 3; ++i)
        {
            glColor3f(pColorArray[i%numColors].r, pColorArray[i%numColors].g, pColorArray[i%numColors].b);
            glVertex3f(pVertices[i].x, pVertices[i].y, pVertices[i].z);
        }
        glEnd();
        succeded = true;
    }
    return succeded;
}

bool drawSolidQuad(size_t numColors, const Color3f *pColorArray, const Vertex3f *pVertices)
{
    bool succeded = false;
    if (numColors > 0 && pColorArray != nullptr && pVertices != nullptr)
    {
        glBegin(GL_QUADS);
        for (size_t i = 0; i < 4; ++i)
        {
            glColor3f(pColorArray[i%numColors].r, pColorArray[i%numColors].g, pColorArray[i%numColors].b);
            glVertex3f(pVertices[i].x, pVertices[i].y, pVertices[i].z);
        }
        glEnd();
        succeded = true;
    }
    return succeded;
}

void drawCircleWithPoints(Color3f borderColor, Vertex3f center, GLfloat radius, bool drawWithAspectCorrection/* = true*/)
{
    GLfloat aspect = 1;
    if (drawWithAspectCorrection)
    {
        // retrieve the aspect ratio to apply screen correction.
        aspect = (GLfloat)GetSystemMetrics(SM_CXSCREEN) / (GLfloat)GetSystemMetrics(SM_CYSCREEN);
    }

    // draw yelllow bordered circle with half width diameter
    glBegin(GL_POINTS);
    glColor3f(borderColor.r, borderColor.g, borderColor.b);
    for (GLfloat angle = 0.0; angle <= 2 * M_PI; angle += 0.01f)
        glVertex3f(0.5f * (GLfloat)cos(angle), 0.5f * aspect * (GLfloat)sin(angle), 0.0f);
    glEnd();
}

void drawCircleWithLines(Color3f borderColor, Vertex3f center, GLfloat radius, bool drawWithAspectCorrection/* = true*/)
{
    GLfloat aspect = 1;
    if (drawWithAspectCorrection)
    {
        // retrieve the aspect ratio to apply screen correction.
        aspect = (GLfloat)GetSystemMetrics(SM_CXSCREEN) / (GLfloat)GetSystemMetrics(SM_CYSCREEN);
    }

    glBegin(GL_LINE_LOOP);
    glColor3f(borderColor.r, borderColor.g, borderColor.b);
    for (GLfloat angle = 0.0; angle <= 2 * M_PI; angle += 0.01f)
        glVertex3f(radius * (GLfloat)cos(angle), radius * aspect * (GLfloat)sin(angle), 0.0f);
    glEnd();
}

void drawEquilateralTriangle(GLfloat radius, GLfloat borderWidth, Color3f borderColor, bool drawWithAspectCorrection/* = true*/)
{
    GLfloat aspect = 1.0;
    if (drawWithAspectCorrection)
        aspect = (GLfloat)GetSystemMetrics(SM_CXSCREEN) / (GLfloat)GetSystemMetrics(SM_CYSCREEN);

    Vertex3f vertices[] = {
        { 0, radius, 0 },
        { - (GLfloat)cos(M_PI / 6) * radius, - (GLfloat)sin(M_PI / 6) * radius, 0.0f },
        { (GLfloat)cos(M_PI / 6) * radius, -(GLfloat)sin(M_PI / 6) * radius, 0.0f }
    };

    //Vertex3f vertices[] = {
    //    { apex.x, apex.y, 0.0f },
    //    { apex.x -((GLfloat)cos(M_PI / 6) * radius), aspect * (apex.y -((GLfloat)sin(M_PI / 6) * radius)), 0.0f },
    //    { apex.x + ((GLfloat)cos(M_PI / 6) * radius), aspect * (apex.y -((GLfloat)sin(M_PI / 6) * radius)), 0.0f }
    //};

    // draw triangle
    drawLineLoop(borderWidth, 3, vertices, 1, &borderColor);
}