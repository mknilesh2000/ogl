#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "DrawHelper.h"
#include "DrawState.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 800

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
GLfloat angleChakra = 0.0f;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
Vertex3f gvMax;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    //function prototype
    void initialize(void);
    void uninitialize(void);
    void display(void);
    void update(void);

    //variable declaration
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szClassName[] = TEXT("RTROGL");
    bool bDone = false;

    //code
    //initializing members of struct WNDCLASSEX
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szClassName;
    wndclass.lpszMenuName = NULL;

    //Registering Class
    RegisterClassEx(&wndclass);

    //Create Window
    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szClassName,
        TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : First Window"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        0,
        0,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd = hwnd;

    //initialize
    initialize();

    ShowWindow(hwnd, SW_SHOW);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    //Message Loop
    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                    bDone = true;
                update();
                display();
            }
        }
    }

    uninitialize();
    return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //function prototype
    void resize(int, int);
    void ToggleFullscreen(void);
    void uninitialize(void);

    //code
    switch (iMsg)
    {
    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0)
            gbActiveWindow = true;
        else
            gbActiveWindow = false;
        break;
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            gbEscapeKeyIsPressed = true;
            break;
        case 0x46: //for 'f' or 'F'
            if (gbFullscreen == false)
            {
                ToggleFullscreen();
                gbFullscreen = true;
            }
            else
            {
                ToggleFullscreen();
                gbFullscreen = false;
            }
            break;
        default:
            break;
        }
        break;
    case WM_LBUTTONDOWN:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        break;
    }
    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    //variable declarations
    MONITORINFO mi;

    //code
    if (gbFullscreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi = { sizeof(MONITORINFO) };
            if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
    }

    else
    {
        //code
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }
}

void initialize(void)
{
    //function prototypes
    void resize(int, int);

    //variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    //code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    //Initialization of structure 'PIXELFORMATDESCRIPTOR'
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;

    ghdc = GetDC(ghwnd);

    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if (iPixelFormatIndex == 0)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    resize(WIN_WIDTH, WIN_HEIGHT);
}

void update(void)
{
    angleChakra -= 2.0f;
    if (angleChakra <= -360.0f)
        angleChakra = 0.0f;
}

static std::shared_ptr<IDrawState> flagState;
static std::shared_ptr<IDrawState> chakraState;

void constructFlagState()
{
    DrawOperation thisOp = { 0 };
    thisOp.operation = GL_QUADS;
    thisOp.flags |= OPERATION_COLOR_VALID | OPERATION_VALID;

    // orange color
    thisOp.color = Color3f(239.0f / 255.0f, 126.0f / 255.0f, 24.0f / 255.0f);
    thisOp.vertices.push_back(Vertex3f(-gvMax.x, gvMax.y, 0.0f));
    thisOp.vertices.push_back(Vertex3f(-gvMax.x, gvMax.y / 3.0f, 0.0f));
    thisOp.vertices.push_back(Vertex3f(gvMax.x, gvMax.y / 3.0f, 0.0f));
    thisOp.vertices.push_back(Vertex3f(gvMax.x, gvMax.y, 0.0f));
    flagState->queueOperation(thisOp);

    // white color
    thisOp.vertices.clear();
    thisOp.color = Color3f(1.0f, 1.0f, 1.0f);
    thisOp.vertices.push_back(Vertex3f(-gvMax.x, gvMax.y / 3.0f, 0.0f));
    thisOp.vertices.push_back(Vertex3f(-gvMax.x, -gvMax.y / 3.0f, 0.0f));
    thisOp.vertices.push_back(Vertex3f(gvMax.x, -gvMax.y / 3.0f, 0.0f));
    thisOp.vertices.push_back(Vertex3f(gvMax.x, gvMax.y / 3.0f, 0.0f));
    flagState->queueOperation(thisOp);

    // green color
    thisOp.vertices.clear();
    thisOp.color = Color3f(0.0f, 1.0f, 0.0f);
    thisOp.vertices.push_back(Vertex3f(-gvMax.x, -gvMax.y / 3.0f, 0.0f));
    thisOp.vertices.push_back(Vertex3f(-gvMax.x, -gvMax.y, 0.0f));
    thisOp.vertices.push_back(Vertex3f(gvMax.x, -gvMax.y, 0.0f));
    thisOp.vertices.push_back(Vertex3f(gvMax.x, -gvMax.y / 3.0f, 0.0f));
    flagState->queueOperation(thisOp);
}

void constructChakraState()
{
    DrawOperation thisOp = { 0 };
    thisOp.operation = GL_QUADS;
    thisOp.flags |= OPERATION_COLOR_VALID | OPERATION_VALID;

    thisOp.vertices.clear();
    thisOp.color = Color3f(0.0f, 0.0f, 1.0f);

    const GLfloat count = 26.0f;
    const GLfloat gapAngle = M_PI / 50.0f;
    GLfloat totalAngle = 2.0f * M_PI - (count * gapAngle);
    for (GLfloat i = 0.0f; i < count; i += 1.0f)
    {
        GLfloat startAngle = ((i * totalAngle) / count) + i * gapAngle;
        GLfloat endAngle = ((i + 1) * totalAngle / count) + i * gapAngle;
        thisOp.vertices.push_back(Vertex3f(0.0f, 0.0f, 0.0f));
        thisOp.vertices.push_back(Vertex3f(0.5f * 0.3f * (GLfloat)cos(startAngle), 0.5f * 0.3f * (GLfloat)sin(startAngle), 0.0f));
        thisOp.vertices.push_back(Vertex3f(0.3f * (GLfloat)cos((startAngle + endAngle) / 2.0f), 0.3f * (GLfloat)sin((startAngle + endAngle) / 2.0f), 0.0f));
        thisOp.vertices.push_back(Vertex3f(0.5f * 0.3f * (GLfloat)cos(endAngle), 0.5f * 0.3f * (GLfloat)sin(endAngle), 0.0f));
    }
    chakraState->queueOperation(thisOp);

    thisOp.vertices.clear();
    thisOp.color = Color3f(0.0f, 0.0f, 1.0f);
    thisOp.operation = GL_POINTS;
    thisOp.flags |= OPERATION_ATTRIB_SIZE_VALID;
    thisOp.attribSize = 4.0f;
    for (GLfloat i = 0.0f; i <= 2 * M_PI; i += 0.00001f)
    {
        thisOp.vertices.push_back(Vertex3f(0.3f * (GLfloat)cos(i), 0.3f * (GLfloat)sin(i), 0.0f));
        thisOp.vertices.push_back(Vertex3f(0.301f * (GLfloat)cos(i), 0.301f * (GLfloat)sin(i), 0.0f));
    }
    chakraState->queueOperation(thisOp);
}

void display(void)
{
    // do the initialization only once
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -1.0f);

    // draws
    flagState->draw();

    // rotate the chakra by angleChakra
    glRotatef(angleChakra, 0.0f, 0.0f, 1.0f);
    chakraState->draw();

    SwapBuffers(ghdc);
}

void resize(int width, int height)
{
    //code
    if (height == 0)
        height = 1;

    GLfloat aspect = (GLfloat)width / (GLfloat)height;

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    ZeroMemory(&gvMax, sizeof(gvMax));

    if (aspect >= 1.0f)
    {
        // extend the left and right clipping planes with the aspect ratio
        gvMax.x = 1.0f * aspect;
        gvMax.y = 1.0f;
    }
    else
    {
        // extend the top and bottom clipping planes with the aspect ratio
        gvMax.x = 1.0f;
        gvMax.y = 1.0f / aspect;
    }
    glFrustum(-gvMax.x, gvMax.x, -gvMax.y, gvMax.y, 1.0f, 1000.0f);

    // always re-create the flag state on resize
    flagState = createDrawState();
    constructFlagState();

    if (!chakraState)
    {
        chakraState = createDrawState();
        constructChakraState();
    }
}

void uninitialize(void)
{
    //UNINITIALIZATION CODE
    if (gbFullscreen == true)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }
    wglMakeCurrent(NULL, NULL);

    wglDeleteContext(ghrc);
    ghrc = NULL;

    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;

    DestroyWindow(ghwnd);
    ghwnd = NULL;
}
