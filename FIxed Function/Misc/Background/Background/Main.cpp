
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <memory>
#include "Utils.h"
#include "resource.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool g_bLightEnable = false;

GLfloat g_lightPosition[] = { 0.0f, 0.0f, 1.0f };
GLfloat g_ambientComponent[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat g_diffusedComponent[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat g_specularComponent[] = { 1.0f, 1.0f, 1.0f, 1.0f };

GLfloat g_material[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat g_materialShinyness = 50.0f;

using namespace Utils;

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

double g_eyeX = 0.0, g_eyeY = 0.0, g_eyeZ = 300.0;
double g_upX = 0.0, g_upY = 1.0, g_upZ = 0.0;

GLuint g_texMoon = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    //local variable declarations
    WNDCLASSEX wndclass;
    TCHAR szAppName[] = TEXT("RTROG");
    HWND hwnd;
    bool bDone = false;
    MSG msg;

    //function prototypes
    void initialize(void);
    void uninitialize(void);
    void display(void);

    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.lpfnWndProc = WndProc;
    wndclass.hInstance = hInstance;
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.lpszClassName = szAppName;
    wndclass.lpszMenuName = NULL;
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

    RegisterClassEx(&wndclass);

    int width = GetSystemMetrics(SM_CXFULLSCREEN);
    int height = GetSystemMetrics(SM_CYFULLSCREEN);

    width = width / 2 - 400;   // width To get Window at Center.
    height = height / 2 - 300; // heignt To get Window at Center.

    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szAppName,
        TEXT("RTROGL: DEMO : Background"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        width,
        height,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd = hwnd;

    initialize();

    ShowWindow(hwnd, SW_SHOW);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    //game loop
    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                    bDone = true;

                display();
            }
        }
    }

    uninitialize();
    return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //function prototpyes
    void resize(int, int);
    void ToggleFullscreen(void);
    void uninitialize(void);

    switch (iMsg)
    {
    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0)
            gbActiveWindow = true;
        else
            gbActiveWindow = false;
        break;
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYDOWN:
        switch (LOWORD(wParam))
        {
        case VK_ESCAPE:
            gbEscapeKeyIsPressed = true;
            break;

        case VK_LEFT:
            g_eyeX -= 1.0;
            break;

        case VK_RIGHT:
            g_eyeX += 1.0f;
            break;

        case VK_UP:
            g_eyeY += 1.0f;
            break;

        case VK_DOWN:
            g_eyeY -= 1.0f;
            break;

        case 0x5A:   // Z key
            g_eyeZ += 1.0f;
            break;

        case 0x41:   // A key
            g_eyeZ -= 1.0f;
            break;

        case 0x59:     // Y key
            g_upY += 1.0f;
            break;

        case 0x48:       // H key
            g_upY -= 1.0f;
            break;

        case 0x47:     // G key
            g_upX -= 1.0f;
            break;

        case 0x4A:      // J key
            g_upX += 1;
            break;

        case 0x46: //'F' or 'f' Key
            if (gbFullscreen == false)
            {
                ToggleFullscreen();
                gbFullscreen = true;
            }
            else
            {
                ToggleFullscreen();
                gbFullscreen = false;
            }
            break;
        default:
            break;
        }
        break;
    case WM_CHAR:
        switch (wParam)
        {
        case 'L':
        case 'l':
            g_bLightEnable = !g_bLightEnable;
            if (g_bLightEnable)
            {
                glEnable(GL_LIGHTING);
                glEnable(GL_LIGHT0);
                // set components
                glLightfv(GL_LIGHT0, GL_AMBIENT, g_ambientComponent);
                glLightfv(GL_LIGHT0, GL_DIFFUSE, g_diffusedComponent);
                glLightfv(GL_LIGHT0, GL_SPECULAR, g_specularComponent);
                glMaterialfv(GL_FRONT, GL_SPECULAR, g_material);
                glMaterialf(GL_FRONT, GL_SHININESS, g_materialShinyness);
            }
            else
            {
                glDisable(GL_LIGHT0);
                glDisable(GL_LIGHTING);
            }
            break;

            // material-experimental
        case 'M':
        case 'm':
        {
            static bool enablematerial = false;
            enablematerial = !enablematerial;

            if (enablematerial)
            {
                float materialColor[] = { 0.0f, 1.0f, 0.0f, 1.0f };
                glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, materialColor);

                // enable color tracking
                glEnable(GL_COLOR_MATERIAL);
                glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
            }
            else
            {
                glDisable(GL_COLOR_MATERIAL);
            }
        }
        break;
        }
        break;
    case WM_LBUTTONDOWN:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    }
    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    //local variable declarations
    bool bIsWindowPlacement = false;
    bool bIsMonitorInfo = false;
    HMONITOR hMonitor;
    MONITORINFO mi;

    if (gbFullscreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        bIsWindowPlacement = GetWindowPlacement(ghwnd, &wpPrev) == TRUE;

        mi.cbSize = sizeof(MONITORINFO);
        hMonitor = MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY);
        bIsMonitorInfo = GetMonitorInfo(hMonitor, &mi) == TRUE;

        if (bIsWindowPlacement == true && bIsMonitorInfo == true)
        {
            SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
            SetWindowPos(ghwnd,
                HWND_TOP,
                mi.rcMonitor.left,
                mi.rcMonitor.top,
                (mi.rcMonitor.right - mi.rcMonitor.left),
                (mi.rcMonitor.bottom - mi.rcMonitor.top),
                SWP_NOZORDER | SWP_FRAMECHANGED);
            ShowCursor(FALSE);
        }
    }
    else
    {
        if (gbFullscreen == true)
        {
            SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPED);
            SetWindowPlacement(ghwnd, &wpPrev);
            SetWindowPos(ghwnd,
                HWND_TOP,
                0,
                0,
                0,
                0,
                SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
            ShowCursor(TRUE);
        }
    }
}

void initialize(void)
{
    bool LoadGLTexture(GLuint &texture, TCHAR imageResourceId[]);

    //local variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    //function prototypes
    void resize(int, int);

    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;
    pfd.cDepthBits = 32;

    ghdc = GetDC(ghwnd);

    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if (iPixelFormatIndex == 0)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (wglMakeCurrent(ghdc, ghrc) == false)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_LINE_STIPPLE);

    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //black color for background
    resize(WIN_WIDTH, WIN_HEIGHT);

    glEnable(GL_TEXTURE_2D);
    LoadGLTexture(g_texMoon, MAKEINTRESOURCE(IDI_TEXTURE_MOON));

    void BuildVertices();
    BuildVertices();
}

bool LoadGLTexture(GLuint &texture, TCHAR imageResourceId[])
{
    bool result = false;

    // generate 1 texture block
    glGenTextures(1, &texture);
    HBITMAP hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (hBitmap != NULL)
    {
        BITMAP bitmapInfo = { 0 };
        if (GetObject(hBitmap, sizeof(bitmapInfo), &bitmapInfo) != 0)
        {
            // define pixel store format
            glPixelStorei(GL_UNPACK_ALIGNMENT, 4); // word alignment of size 4 bytes
            glBindTexture(GL_TEXTURE_2D, texture);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            // generate mipmapped texture
            gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bitmapInfo.bmWidth, bitmapInfo.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bitmapInfo.bmBits);
            DeleteObject(hBitmap);
            result = true;
        }
    }
    return result;
}

#define MAX_X 128
#define MAX_Z 128

struct InterleavedBuffer
{
    // color
    GLfloat cr;
    GLfloat cg;
    GLfloat cb;
    // vertex
    GLfloat vx;
    GLfloat vy;
    GLfloat vz;
};

static InterleavedBuffer buffer[MAX_X*MAX_Z];
static std::shared_ptr<IObjLoader> objLoader(createObjLoader());

void BuildVertices()
{
    std::shared_ptr<IBitmapHelper> helper(createBitmapHelper());
    helper->Load(L"./rtr.bmp");

    for (size_t z = 0; z < MAX_Z; ++z)
    {
        for (size_t x = 0; x < MAX_X; ++x)
        {
            buffer[x*MAX_X + z].cr = (GLfloat)helper->getImageData()[(z*MAX_Z + x) * 3 + 2] / 255.0f;
            buffer[x*MAX_X + z].cg = (GLfloat)helper->getImageData()[(z*MAX_Z + x) * 3 + 1] / 255.0f;
            buffer[x*MAX_X + z].cb = (GLfloat)helper->getImageData()[(z*MAX_Z + x) * 3] / 255.0f;

            buffer[x*MAX_X + z].vx = -0.5f + ((GLfloat)x / MAX_X);
            buffer[x*MAX_X + z].vz = - (GLfloat)z / MAX_Z;
            buffer[x*MAX_X + z].vy = -0.5f + buffer[x*MAX_X + z].cr;
        }
    }
    objLoader->Load(L"./moon.obj");
    return;
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glLightfv(GL_LIGHT0, GL_POSITION, g_lightPosition);
    gluLookAt(g_eyeX, g_eyeY, g_eyeZ, 0, 0, 0, g_upX, g_upY, g_upZ);

//    glTranslatef(0.0f, 0.0f, -400.0f);

    glFrontFace(GL_CCW);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if (objLoader->getFaceVertexIndices().size() > 0)
    {
        const std::vector<IObjLoader::Vec3>& vertices = objLoader->getVertices();
        const std::vector<IObjLoader::Vec2>& textCords = objLoader->getTexCords();
        const std::vector<IObjLoader::Vec3>& normals = objLoader->getNormals();
        for (size_t elementId = 0; elementId < objLoader->getFaceVertexIndices().size(); ++elementId)
        {
            const std::vector<unsigned int>& vIndices = objLoader->getFaceVertexIndices()[elementId];
            const std::vector<unsigned int>& tIndices = objLoader->getFaceTexIndices()[elementId];
            const std::vector<unsigned int>& nIndices = objLoader->getFaceNormalIndices()[elementId];
            glBegin(GL_POLYGON);
            for (size_t i = 0; i < vIndices.size(); ++i)
            {
                glNormal3d(normals[nIndices[i]].x, normals[nIndices[i]].y, normals[nIndices[i]].z);
                glTexCoord2d(textCords[tIndices[i]].x, textCords[tIndices[i]].y);
                glVertex3d(vertices[vIndices[i]].x, vertices[vIndices[i]].y, vertices[vIndices[i]].z);
            }
            glEnd();
        }
    }
    else
    {
        __debugbreak();
    }

    SwapBuffers(ghdc);
}

void resize(int width, int height)
{
    if (height == 0)
        height = 1;

    if (width == 0)
        width = 1;

   glViewport(0, 0, (GLsizei)width, (GLsizei)height);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();

   gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 1000.0f);

}

void uninitialize(void)
{
    if (gbFullscreen == true)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd,
            HWND_TOP,
            0,
            0,
            0,
            0,
            SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    wglMakeCurrent(NULL, NULL);

    wglDeleteContext(ghrc);
    ghrc = NULL;

    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;

    DestroyWindow(ghwnd);
    ghwnd = NULL;
}

