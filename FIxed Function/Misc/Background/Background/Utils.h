#pragma once

#include <vector>

namespace Utils
{
    class IBitmapHelper
    {
    public:
        virtual bool Load(const wchar_t *filePath) = 0;
        virtual size_t getImageDataSize() const = 0;
        virtual size_t getImageWidth() const = 0;
        virtual size_t getImageHeight() const = 0;
        virtual const unsigned char* getImageData() const = 0;
    };

    class IObjLoader
    {
    public:
        struct Vec3
        {
            double x;
            double y;
            double z;
        };

        struct Vec2
        {
            double x;
            double y;
        };

        virtual bool Load(const wchar_t *filePath) = 0;
        virtual const std::vector<std::vector<unsigned int>>& getFaceVertexIndices() const = 0;
        virtual const std::vector<std::vector<unsigned int>>& getFaceTexIndices() const = 0;
        virtual const std::vector<std::vector<unsigned int>>& getFaceNormalIndices() const = 0;
        virtual const std::vector<Vec3>& getVertices() const = 0;
        virtual const std::vector<Vec2>& getTexCords() const = 0;
        virtual const std::vector<Vec3>& getNormals() const = 0;
    };

    IBitmapHelper* createBitmapHelper();
    IObjLoader* createObjLoader();
};

