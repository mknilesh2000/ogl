#include <windows.h>
#include <stdio.h>
#include "Utils.h"

namespace Utils
{
#define BITMAP_ID 0x4D42

    class BitmapHelper : public IBitmapHelper
    {
    public:
        BitmapHelper() :
            m_imageData(nullptr)
        {
            memset(&m_bitmapInfoHeader, 0, sizeof(m_bitmapInfoHeader));
            memset(&m_bitmapFileHeader, 0, sizeof(m_bitmapFileHeader));
        }

        ~BitmapHelper()
        {
            if (m_imageData != nullptr)
            {
                delete m_imageData;
                m_imageData = NULL;
            }
        }

        bool Load(const wchar_t *filepath)
        {
            FILE *fp;
            bool isLoaded = false;

            if (0 == _wfopen_s(&fp, filepath, L"rb") && fp != nullptr)
            {
                //read file header
                if (fread_s(&m_bitmapFileHeader, sizeof(m_bitmapFileHeader), sizeof(m_bitmapFileHeader), 1, fp) > 0)
                {
                    //read bitmapinfoheader
                    if (m_bitmapFileHeader.bfType == BITMAP_ID &&
                        fread_s(&m_bitmapInfoHeader, sizeof(m_bitmapInfoHeader), sizeof(m_bitmapInfoHeader), 1, fp) > 0)
                    {
                        //allocate 
                        m_imageData = new unsigned char[m_bitmapInfoHeader.biSizeImage];

                        //capture image file data
                        fseek(fp, m_bitmapFileHeader.bfOffBits, SEEK_SET);

                        if (m_bitmapInfoHeader.biSizeImage == fread_s(m_imageData, m_bitmapInfoHeader.biSizeImage, 1, m_bitmapInfoHeader.biSizeImage, fp))
                        {
                            isLoaded = true;
                        }
                        else
                        {
                            delete m_imageData;
                            m_imageData = NULL;
                        }
                    }
                }
                fclose(fp);
            }
            return isLoaded;
        }

        size_t getImageDataSize() const
        {
            return (m_imageData != nullptr) ? m_bitmapInfoHeader.biSizeImage : 0;
        }

        size_t getImageWidth() const
        {
            return (m_imageData != nullptr) ? m_bitmapInfoHeader.biWidth : 0;
        }

        size_t getImageHeight() const
        {
            return (m_imageData != nullptr) ? m_bitmapInfoHeader.biHeight : 0;
        }

        const unsigned char* getImageData() const
        {
            return m_imageData;
        }

    private:
        BITMAPINFOHEADER m_bitmapInfoHeader;
        BITMAPFILEHEADER m_bitmapFileHeader;

        unsigned char * m_imageData;
    };

    IBitmapHelper* createBitmapHelper()
    {
        return new BitmapHelper;
    }

    class CObjLoader : public IObjLoader
    {
    public:
        virtual bool Load(const wchar_t *filePath)
        {
            FILE *fp;
            bool isLoaded = true;

            if (0 == _wfopen_s(&fp, filePath, L"r") && fp != nullptr)
            {
                while (true)
                {
                    char line[128] = { 0 };
                    if (fscanf_s(fp, "%s", line, (unsigned int)_countof(line)) == EOF)
                        break;

                    if (strcmp(line, "v") == 0)
                    {
                        // vertices
                        Vec3 vec;
                        if (3 == fscanf_s(fp, "%lf %lf %lf\n", &vec.x, &vec.y, &vec.z))
                        {
                            m_vertices.push_back(vec);
                        }
                        else
                        {
                            isLoaded = false;
                            break;
                        }
                    }
                    else if (strcmp(line, "vt") == 0)
                    {
                        // tex
                        Vec2 vec;
                        if (2 == fscanf_s(fp, "%lf %lf\n", &vec.x, &vec.y))
                        {
                            m_texCords.push_back(vec);
                        }
                        else
                        {
                            isLoaded = false;
                            break;
                        }
                    }
                    else if (strcmp(line, "vn") == 0)
                    {
                        // normals
                        Vec3 vec;
                        if (3 == fscanf_s(fp, "%lf %lf %lf\n", &vec.x, &vec.y, &vec.z))
                        {
                            m_normals.push_back(vec);
                        }
                        else
                        {
                            isLoaded = false;
                            break;
                        }
                    }
                    else if (strcmp(line, "f") == 0)
                    {
                        // faces
                        std::vector<unsigned int> vIndices;
                        std::vector<unsigned int> tIndices;
                        std::vector<unsigned int> nIndices;

                        unsigned int vIndex, tIndex, nIndex;
                        while (3 == fscanf_s(fp, "%d/%d/%d", &vIndex, &tIndex, &nIndex))
                        {
                            vIndices.push_back(vIndex-1);
                            tIndices.push_back(tIndex-1);
                            nIndices.push_back(nIndex-1);
                        }
                        // push only if the indices are valid and matching
                        if (vIndices.size() > 0 &&
                            (vIndices.size() == tIndices.size()) &&
                            (tIndices.size() == nIndices.size()))
                        {
                            m_vIndices.push_back(vIndices);
                            m_tIndices.push_back(tIndices);
                            m_nIndices.push_back(nIndices);
                        }
                        else
                        {
                            isLoaded = false;
                            break;
                        }
                    }
                }
                fclose(fp);
            }
            else
            {
                isLoaded = false;
            }

            if (!isLoaded)
            {
                clear();
            }

            return isLoaded;
        }

        virtual const std::vector<std::vector<unsigned int>>& getFaceVertexIndices() const
        {
            return m_vIndices;
        }

        virtual const std::vector<std::vector<unsigned int>>& getFaceTexIndices() const
        {
            return m_tIndices;
        }

        virtual const std::vector<std::vector<unsigned int>>& getFaceNormalIndices() const
        {
            return m_nIndices;
        }

        virtual const std::vector<Vec3>& getVertices() const
        {
            return m_vertices;
        }

        virtual const std::vector<Vec2>& getTexCords() const
        {
            return m_texCords;
        }

        virtual const std::vector<Vec3>& getNormals() const
        {
            return m_normals;
        }

    private:
        void clear()
        {
            m_vIndices.clear();
            m_tIndices.clear();
            m_nIndices.clear();

            m_vertices.clear();
            m_texCords.clear();
            m_normals.clear();
        }

        std::vector<std::vector<unsigned int>> m_vIndices;
        std::vector<std::vector<unsigned int>> m_tIndices;
        std::vector<std::vector<unsigned int>> m_nIndices;

        std::vector<Vec3> m_vertices;
        std::vector<Vec2> m_texCords;
        std::vector<Vec3> m_normals;
    };

    IObjLoader* createObjLoader()
    {
        return new CObjLoader;
    }
};