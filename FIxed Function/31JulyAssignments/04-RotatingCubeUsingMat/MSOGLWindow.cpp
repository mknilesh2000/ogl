#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "DrawHelper.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 800

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
Vertex3f gvMax = {1.0f, 1.0f, 1.0f};
GLfloat g_angleCube = 0.0f;
std::shared_ptr<GLfloat> g_identityMatrix = getIdentityMatrix();
std::shared_ptr<GLfloat> g_translationMatrix = getTranslationMatrix(0.0f, 0.0f, -3.0f);
std::shared_ptr<GLfloat> g_scaleMatrix = getScaleMatrix(0.75f, 0.75f, 0.75f);
std::shared_ptr<GLfloat> g_projectionMatrix = getPerspectiveProjectionMatrix(-gvMax.x, gvMax.x, -gvMax.y, gvMax.y, 1.0f, 1000.0f);

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    //function prototype
    void initialize(void);
    void uninitialize(void);
    void display(void);
    void update(void);

    //variable declaration
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szClassName[] = TEXT("RTROGL");
    bool bDone = false;

    //code
    //initializing members of struct WNDCLASSEX
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szClassName;
    wndclass.lpszMenuName = NULL;

    //Registering Class
    RegisterClassEx(&wndclass);

    //Create Window
    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szClassName,
        TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : First Window"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        0,
        0,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd = hwnd;

    //initialize
    initialize();

    ShowWindow(hwnd, SW_SHOW);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    //Message Loop
    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                    bDone = true;

                update();
                display();
            }
        }
    }

    uninitialize();
    return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //function prototype
    void resize(int, int);
    void ToggleFullscreen(void);
    void uninitialize(void);

    //code
    switch (iMsg)
    {
    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0)
            gbActiveWindow = true;
        else
            gbActiveWindow = false;
        break;
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            gbEscapeKeyIsPressed = true;
            break;
        case 0x46: //for 'f' or 'F'
            if (gbFullscreen == false)
            {
                ToggleFullscreen();
                gbFullscreen = true;
            }
            else
            {
                ToggleFullscreen();
                gbFullscreen = false;
            }
            break;
        default:
            break;
        }
        break;
    case WM_LBUTTONDOWN:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        break;
    }
    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    //variable declarations
    MONITORINFO mi;

    //code
    if (gbFullscreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi = { sizeof(MONITORINFO) };
            if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
    }

    else
    {
        //code
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }
}

void initialize(void)
{
    //function prototypes
    void resize(int, int);

    //variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    //code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    //Initialization of structure 'PIXELFORMATDESCRIPTOR'
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;
    pfd.cDepthBits = 32;

    ghdc = GetDC(ghwnd);

    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if (iPixelFormatIndex == 0)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    resize(WIN_WIDTH, WIN_HEIGHT);
}

void update(void)
{
    Sleep(10);
    g_angleCube += 1.0f;
    if (g_angleCube >= 360.0f)
        g_angleCube = 0.0f;
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(g_identityMatrix.get());
    glMultMatrixf(g_translationMatrix.get());
    glMultMatrixf(g_scaleMatrix.get());
    glMultMatrixf(getRotationMatrixX(g_angleCube).get());
    glMultMatrixf(getRotationMatrixY(g_angleCube).get());
    glMultMatrixf(getRotationMatrixZ(g_angleCube).get());

    Vertex3f cubeVertices[] = {
        {-1.0f, 1.0f, -1.0f},
        {-1.0f, -1.0f, -1.0f},
        {1.0f, -1.0f, -1.0f},
        {1.0f, 1.0f, -1.0f},

        { -1.0f, 1.0f, 1.0f },
        { -1.0f, -1.0f, 1.0f },
        { -1.0f, -1.0f, -1.0f },
        { -1.0f, 1.0f, -1.0f },

        { -1.0f, 1.0f, 1.0f },
        { -1.0f, -1.0f, 1.0f },
        { 1.0f, -1.0f, 1.0f },
        { 1.0f, 1.0f, 1.0f },

        { 1.0f, 1.0f, -1.0f },
        { 1.0f, -1.0f, -1.0f },
        { 1.0f, -1.0f, 1.0f },
        { 1.0f, 1.0f, 1.0f },

        { -1.0f, 1.0f, 1.0f },
        { -1.0f, 1.0f, -1.0f },
        { 1.0f, 1.0f, -1.0f },
        { 1.0f, 1.0f, 1.0f },

        { -1.0f, -1.0f, 1.0f },
        { -1.0f, -1.0f, -1.0f },
        { 1.0f, -1.0f, -1.0f },
        { 1.0f, -1.0f, 1.0f },
    };

    Color3f colorsCube[] = {
        { 1.0f, 0.0f, 0.0f },
        { 0.0f, 1.0f, 0.0f },
        { 0.0f, 0.0f, 1.0f },
        { 1.0f, 1.0f, 0.0f },
        { 0.0f, 1.0f, 1.0f },
        { 1.0f, 0.0f, 1.0f },
    };

    // Draw triangles for the pyramid
    drawSolidQuad(1, &colorsCube[0], &cubeVertices[0]);
    drawSolidQuad(1, &colorsCube[1], &cubeVertices[4]);
    drawSolidQuad(1, &colorsCube[2], &cubeVertices[8]);
    drawSolidQuad(1, &colorsCube[3], &cubeVertices[12]);
    drawSolidQuad(1, &colorsCube[4], &cubeVertices[16]);
    drawSolidQuad(1, &colorsCube[5], &cubeVertices[20]);

    SwapBuffers(ghdc);
}

void resize(int width, int height)
{
    //code
    if (height == 0)
        height = 1;

    GLfloat aspect = (GLfloat)width / (GLfloat)height;

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(g_identityMatrix.get());

    ZeroMemory(&gvMax, sizeof(gvMax));

    if (aspect >= 1.0f)
    {
        // extend the left and right clipping planes with the aspect ratio
        gvMax.x = 1.0f * aspect;
        gvMax.y = 1.0f;
    }
    else
    {
        // extend the top and bottom clipping planes with the aspect ratio
        gvMax.x = 1.0f;
        gvMax.y = 1.0f / aspect;
    }
    glMultMatrixf(g_projectionMatrix.get());
}

void uninitialize(void)
{
    //UNINITIALIZATION CODE
    if (gbFullscreen == true)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }
    wglMakeCurrent(NULL, NULL);

    wglDeleteContext(ghrc);
    ghrc = NULL;

    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;

    DestroyWindow(ghwnd);
    ghwnd = NULL;
}
