cwd=$(pwd)
cd ../../drawHelper.lib
./build_lib.sh
echo "Copying drawhelper to build directory"
cp drawhelper.a "$cwd"
echo "Building xwindow"
cd "$cwd"
rm -rf *.png
rm -rf xwindow
rm -rf xwindow.log
g++ -I../../drawHelper.lib "./xwindow.cpp" "drawhelper.a" -o "./xwindow" -lX11 -lGL -lGLU -std=c++1y
echo "deleting intermidiate files"
rm -rf drawhelper.a
echo "launching xwindow"
./xwindow&
sleep 3
gedit ./xwindow.log&
echo "capturing screenshot"
sleep 3
import -window root -delay 5000 screenshot.png



