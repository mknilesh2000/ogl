// To build, used... g++ "./xwindow.cpp" -o "./xwindow" -lX11 -lGL -lGLU -std=c++1y

#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include "DrawHelper.h"
#include <stdio.h>

using namespace std;

Display *g_pDisplay = NULL;
XVisualInfo *g_pXVisualInfo = NULL;
Colormap g_colormap;
Window g_window;
int giWindowWidth = 800;
int giWindowHeight = 600;

bool g_bFullscreen = false;
FILE *g_logfile = nullptr;

static int year = 0;
static int day = 0;

GLUquadric *quadric = NULL;

GLXContext g_GLXContext;

int main(void)
{
	g_logfile = fopen("./xwindow.log", "w+");
	if (g_logfile == nullptr)
		return -1;

	// forwards
	void Resize(int width, int height, int startX=0, int startY=0);
	void CreateWindow(void);
	void Initialize(void);
	void UnInitialize(void);
	void HandleXKeyEvent(const XKeyEvent &xkey);
	void HandleXButtonEvent(const XButtonEvent &xbutton);
	void DisplayContent(void);
	void Spin(void);

	// create window
	CreateWindow();

	// initialize GL
	Initialize();

	// 	/*
	//  * this union is defined so Xlib can always use the same sized
	//  * event structure internally, to avoid memory fragmentation.
	//  */
	// typedef union _XEvent {
	//         int type;		/* must not be changed; first element */
	// 	XAnyEvent xany;
	// 	XKeyEvent xkey;
	// 	XButtonEvent xbutton;
	// 	XMotionEvent xmotion;
	// 	XCrossingEvent xcrossing;
	// 	XFocusChangeEvent xfocus;
	// 	XExposeEvent xexpose;
	// 	XGraphicsExposeEvent xgraphicsexpose;
	// 	XNoExposeEvent xnoexpose;
	// 	XVisibilityEvent xvisibility;
	// 	XCreateWindowEvent xcreatewindow;
	// 	XDestroyWindowEvent xdestroywindow;
	// 	XUnmapEvent xunmap;
	// 	XMapEvent xmap;
	// 	XMapRequestEvent xmaprequest;
	// 	XReparentEvent xreparent;
	// 	XConfigureEvent xconfigure;
	// 	XGravityEvent xgravity;
	// 	XResizeRequestEvent xresizerequest;
	// 	XConfigureRequestEvent xconfigurerequest;
	// 	XCirculateEvent xcirculate;
	// 	XCirculateRequestEvent xcirculaterequest;
	// 	XPropertyEvent xproperty;
	// 	XSelectionClearEvent xselectionclear;
	// 	XSelectionRequestEvent xselectionrequest;
	// 	XSelectionEvent xselection;
	// 	XColormapEvent xcolormap;
	// 	XClientMessageEvent xclient;
	// 	XMappingEvent xmapping;
	// 	XErrorEvent xerror;
	// 	XKeymapEvent xkeymap;
	// 	XGenericEvent xgeneric;
	// 	XGenericEventCookie xcookie;
	// 	long pad[24];
	// } XEvent;

	XEvent event;

	// Message Loop
	bool bDone = false;
	while (!bDone)
	{
		while (XPending(g_pDisplay))
		{
			XNextEvent(g_pDisplay, &event);
			switch (event.type)
			{
				case MapNotify:
					break;

				case KeyPress: // keyboard
					HandleXKeyEvent(event.xkey);
					break;

				case ButtonPress: // mouse buttons
					HandleXButtonEvent(event.xbutton);
					break;

				case MotionNotify: // mouse move
					break;

				case ConfigureNotify: // similar to WM_SIZE
					Resize(event.xconfigure.width, event.xconfigure.height);
					break;

				case Expose: // equivalent to WM_PAINT
					break;

				case DestroyNotify: // equivalent to WM_DESTROY
					break;

				case 33:  // this will come when user tries to close the window
					bDone = true;
					break;

				default:
					break;
			}
		}

		Spin();
		DisplayContent();
	}

	UnInitialize();
	fclose(g_logfile);

	return(0);
}

void CreateWindow(void)
{
	fprintf(g_logfile, "Entering %s\n", __FUNCTION__);
	// forwards
	void UnInitialize(void);

	g_pDisplay = XOpenDisplay(NULL);
	if (g_pDisplay == NULL)
	{
		fprintf(g_logfile,"ERROR: Unable to open XDisplay. Exitting ....\n");
		UnInitialize();
		exit(1);
	}

	int defaultScreen = XDefaultScreen(g_pDisplay);
	static int frameBufferAttributes[] = 
	{
		GLX_RGBA,
		GLX_RED_SIZE, 1,
		GLX_GREEN_SIZE, 1,
		GLX_BLUE_SIZE, 1,
		GLX_ALPHA_SIZE, 1,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 8,
		None
	};
	g_pXVisualInfo = glXChooseVisual(g_pDisplay, defaultScreen, frameBufferAttributes);

	XSetWindowAttributes winAttributes = {0};
	winAttributes.border_pixel 		= 0;
	winAttributes.background_pixmap = 0;
	winAttributes.colormap 			= XCreateColormap(g_pDisplay, RootWindow(g_pDisplay, g_pXVisualInfo->screen), g_pXVisualInfo->visual, AllocNone);
	winAttributes.background_pixel 	= BlackPixel(g_pDisplay, defaultScreen);
	winAttributes.event_mask 		= ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	// store color map in global variable
	g_colormap = winAttributes.colormap;

	g_window = XCreateWindow(
							    g_pDisplay /* display */,
							    RootWindow(g_pDisplay, g_pXVisualInfo->screen) /* parent */,
							    0			/* x */,
							    0			/* y */,
							    giWindowWidth	/* width */,
							    giWindowHeight	/* height */,
							    0	/* border_width */,
							    g_pXVisualInfo->depth			/* depth */,
							    InputOutput	/* class */,
							    g_pXVisualInfo->visual		/* visual */,
							    CWBorderPixel | CWBackPixel | CWEventMask | CWColormap	/* valuemask */,
							    &winAttributes	/* attributes */
							);

	if (!g_window)
	{
		fprintf(g_logfile, "ERROR: failed to create main window. Exitting ....\n");
		UnInitialize();
		exit(1);
	}

	// store name to uniquely identify the window. This can be used remotely
	XStoreName(
			    g_pDisplay		/* display */,
			    g_window		/* w */,
			    "Nilesh's XWindow OGL"	/* window_name */
			);

	// register a protocol for deleting the window
	Atom windowManagerDelete = XInternAtom(
										    g_pDisplay		/* display */,
										    "WM_DELETE_WINDOW"	/* atom_name */,
										    True		/* only_if_exists */
										  );
	XSetWMProtocols(
				    g_pDisplay		/* display */,
				    g_window		/* w */,
				    &windowManagerDelete		/* protocols */,
				    1			/* count */
				);

	// show window
	XMapWindow(
			    g_pDisplay		/* display */,
			    g_window		/* w */
			);
	fprintf(g_logfile, "Exitting %s\n", __FUNCTION__);
}

void DisplayContent(void)
{
	//fprintf(g_logfile, "Entering %s\n", __FUNCTION__);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW); // select modelview matrix
	glLoadIdentity(); // reset current modelview matrix

    // view transformation
	gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// large sphere like sun
	// model transformations
	glPushMatrix();

	glRotatef(90.0f, 1.0f, 0.0f, .0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(1.0f, 1.0f, 0.0f);
    // 3rd param i.e. slices (like longitudes) and 4th param i.e. stacks (like latitudes)
	// more the 3rd and the 4th params, i.e. more the subdivisions, more circular
	gluSphere(quadric, 0.75, 30, 30);

	glPopMatrix();

	// small sphere like earth
	glPushMatrix();

	glRotatef((GLfloat)year, 0.0f, 1.0f, 0.0f);

	glTranslatef(1.5f, 0.0f, 0.0f);

	glRotatef(90.0f, 1.0f, 0.0f, .0f);

	glRotatef((GLfloat)day, 0.0f, 0.0f, 1.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	quadric = gluNewQuadric();
	glColor3f(0.4f, 0.9f, 1.0f);
	gluSphere(quadric, 0.2, 20, 20);

	glPopMatrix();

	glXSwapBuffers(g_pDisplay, g_window);
	//fprintf(g_logfile, "Exitting %s\n", __FUNCTION__);
}

void Spin(void)
{
	// fprintf(g_logfile, "Entering %s\n", __FUNCTION__);

    // fprintf(g_logfile, "Exitting %s\n", __FUNCTION__);
}

void Resize(int width, int height, int startX=0, int startY=0)
{
	fprintf(g_logfile, "Entering %s\n", __FUNCTION__);

	if (height == 0)
		height = 1;

	/* GLAPI void GLAPIENTRY glViewport( GLint x, GLint y,
                                    GLsizei width, GLsizei height ); */
	glViewport(startX, startY, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f); // calculate the aspect ratio of the view

	fprintf(g_logfile, "Exitting %s\n", __FUNCTION__);
}

void Initialize(void)
{
	fprintf(g_logfile, "Entering %s\n", __FUNCTION__);

	// declarations
	void Resize(int width, int height, int startX=0, int startY=0);

	g_GLXContext = glXCreateContext(
									g_pDisplay,     // Display *dpy, 
									g_pXVisualInfo, // XVisualInfo *vis,
				    				NULL, 			// GLXContext shareList, <= valid if we want to share this context with multiple monitors
				    				GL_TRUE 		// Bool direct       < = Use HW Driver
				    			);
	/*Bool*/ glXMakeCurrent( 
							g_pDisplay, // Display *dpy, 
							g_window, // GLXDrawable drawable,
							g_GLXContext // GLXContext ctx
			    		);

	glShadeModel(GL_SMOOTH);

	// set background color to black
	/* GLAPI void GLAPIENTRY */ glClearColor( 
											 0.0f, // GLclampf red, 
											 0.0f, // GLclampf green, 
											 0.0f, // GLclampf blue, 
											 0.0f  // GLclampf alpha
										);
	// set-up depth buffer
	glClearDepth(1.0f);

	// enable depth-testing
	glEnable(GL_DEPTH_TEST);

	// depth-test to do
	glDepthFunc(GL_LEQUAL);

	// set nice perspective calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	Resize(giWindowWidth, giWindowHeight);
	fprintf(g_logfile, "Exitting %s\n", __FUNCTION__);
}

void UnInitialize(void)
{
	fprintf(g_logfile, "Entering %s\n", __FUNCTION__);

	GLXContext currentGLXContext = glXGetCurrentContext();
	
	if ((currentGLXContext != NULL) && (currentGLXContext == g_GLXContext))
	{
		glXMakeCurrent(g_pDisplay, 0, 0);
	}

	if (g_GLXContext)
	{
		glXDestroyContext(g_pDisplay, g_GLXContext);
	}

	if (g_window)
	{
		XDestroyWindow(g_pDisplay, g_window);
	}

	XFreeColormap(g_pDisplay, g_colormap);

	if (g_pXVisualInfo)
	{
		free(g_pXVisualInfo);
		g_pXVisualInfo = NULL;
	}

	if (g_pDisplay)
	{
		XCloseDisplay(g_pDisplay);
		g_pDisplay = NULL;
	}

	fprintf(g_logfile, "Exitting %s\n", __FUNCTION__);
}

void ToggleFullscreen()
{
	fprintf(g_logfile, "Entering %s\n", __FUNCTION__);

	// Registers XServer specific protocols. These are of types -
	// - XServer specific
	// - Window manager specific
	// - Network Window manager specific
	XEvent xev = {0};
	xev.type = ClientMessage;
	xev.xclient.window = g_window;
	xev.xclient.message_type = XInternAtom(g_pDisplay,"_NET_WM_STATE", False);
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = g_bFullscreen ? 0 : 1;
	xev.xclient.data.l[1] = XInternAtom(g_pDisplay,"_NET_WM_STATE_FULLSCREEN", False);

	// Status XSendEvent(
	//     Display*		/* display */,
	//     Window		/* w */,
	//     Bool		/* propagate */,
	//     long		/* event_mask */,
	//     XEvent*		/* event_send */
	// );
	XSendEvent(g_pDisplay, 
			   RootWindow(g_pDisplay, g_pXVisualInfo->screen),
			   False,  // whether to propagate this event to other monitors window managers
			   StructureNotifyMask,
			   &xev);

	// negate the fullscreen flag
	g_bFullscreen = !g_bFullscreen;

	fprintf(g_logfile, "Exitting %s\n", __FUNCTION__);
}

void HandleXKeyEvent(const XKeyEvent &xkey)
{
	fprintf(g_logfile, "Entering %s\n", __FUNCTION__);

	void Resize(int width, int height, int startX=0, int startY=0);

	// forwards
	void UnInitialize(void);

	int xreturn = 0, yreturn = 0;
	unsigned int width = 0, height=0, borderwidth=0, depth=0;
	Window root;

	XGetGeometry(g_pDisplay, g_window, &root, &xreturn, &yreturn, &width, &height, &borderwidth, &depth);
	fprintf(g_logfile, "XGetGeometry returned width=%u height=%u \n", width, height);

	// KeySym XkbKeycodeToKeysym(
	// 					 		Display *	/* dpy */,
	// 					 #if NeedWidePrototypes
	// 					 		 unsigned int 	/* kc */,
	// 					 #else
	// 					 		 KeyCode 	/* kc */,
	// 					 #endif
	// 					 		 int 		/* group */,
	// 					 		 int		/* level */
	// 					);
	KeySym keysym = XkbKeycodeToKeysym(g_pDisplay, xkey.keycode, 0, 0);
	fprintf(g_logfile, "Pressed key %lu\n", keysym);
	switch (keysym)
	{
		case XK_Escape:
			UnInitialize();
			exit(0);
			break;

		case XK_D:
			day = (day + 6) % 360;
			break;
		case XK_d:
			day = (day - 6) % 360;
			break;
		case XK_Y:
			year = (year + 3) % 360;
			break;
		case XK_y:
			year = (year - 3) % 360;
			break;

		default:
			break;
	}

	fprintf(g_logfile, "Exitting %s\n", __FUNCTION__);
}

void HandleXButtonEvent(const XButtonEvent &xbutton)
{
	switch (xbutton.button)
	{
		case 1:
			break;

		case 2:
			break;

		case 3:
			break;

		default:
			break;
	}
}
