#include <iostream>
#include <stdio.h>
#include <stdlib.h> // for exit function
#include <memory.h> // for memset

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

using namespace std;

bool g_bFullscreen = false;
Display *g_pDisplay = NULL;
XVisualInfo *g_pVisualInfo = NULL;
Colormap g_Colormap;
Window g_Window;
int g_iWindowHeight = 800;
int g_iWindowWidth = 600;

int main(void)
{
	// forwards
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	int winWidth = g_iWindowWidth;
	int winHeight = g_iWindowHeight;

	CreateWindow();

	// Message Loop
	XEvent event;
	KeySym keysym;

	while (true)
	{
		XNextEvent(g_pDisplay, &event);
		switch (event.type)
		{
		case MapNotify:
			break;

		case KeyPress:
			keysym = XkbKeycodeToKeysym(g_pDisplay,
				event.xkey.keycode,
				0,
				0);
			switch (keysym)
			{
			case XK_Escape:
				UnInitialize();
				exit(0);
			case XK_F:
			case XK_f:
				ToggleFullscreen();
				g_bFullscreen = !g_bFullscreen;
				break;
			default:
				break;
			}
			break;

		case ButtonPress:
			switch (event.xbutton.button)
			{
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			default:
				break;
			}
			break;

		case MotionNotify:
			break;

		case ConfigureNotify:
			winWidth = event.xconfigure.width;
			winHeight = event.xconfigure.height;
			break;

		case Expose:
			break;

		case DestroyNotify:
			break;
			
		case 33:
			UnInitialize();
			exit(0);
			break;

		default:
			break;
		}
	}

	UnInitialize();
	return 0;
}

void CreateWindow(void)
{
	// function prototypes
	void UnInitialize(void);

	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	g_pDisplay = XOpenDisplay(NULL); // displayName = NULL
	if (g_pDisplay == NULL)
	{
		printf("ERROR: Unable to open XDisplay. Exitting \n");
		UnInitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(g_pDisplay);
	defaultDepth = DefaultDepth(g_pDisplay, defaultScreen);
	g_pVisualInfo = (XVisualInfo*) malloc(sizeof(XVisualInfo));
	if (g_pVisualInfo == NULL)
	{
		printf("ERROR: Unable to get a visual. Exitting \n");
		UnInitialize();
		exit(1);
	}

	XMatchVisualInfo(g_pDisplay, defaultScreen, defaultDepth, TrueColor, g_pVisualInfo);
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(g_pDisplay,
		RootWindow(g_pDisplay, g_pVisualInfo->screen),
		g_pVisualInfo->visual,
		AllocNone);
	g_Colormap = winAttribs.colormap;
	winAttribs.background_pixel = BlackPixel(g_pDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask |
		KeyPressMask | PointerMotionMask | StructureNotifyMask;
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	g_Window = XCreateWindow(g_pDisplay,
		RootWindow(g_pDisplay, g_pVisualInfo->screen),
		0,
		0,
		g_iWindowWidth,
		g_iWindowHeight,
		0,
		g_pVisualInfo->depth,
		InputOutput,
		g_pVisualInfo->visual,
		styleMask,
		&winAttribs);

	if (!g_Window)
	{
		printf("ERROR: Failed to create main window. Exitting \n");
		UnInitialize();
		exit(1);
	}

	XStoreName(g_pDisplay, g_Window, "Nilesh's first XWindow");
	Atom windowManagerDelete = XInternAtom(g_pDisplay
		, "WM_DELETE_WINDOW",
		True);
	XSetWMProtocols(g_pDisplay,
		g_Window,
		&windowManagerDelete,
		1);

	XMapWindow(g_pDisplay, g_Window);
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	wm_state = XInternAtom(g_pDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));
	xev.type = ClientMessage;
	xev.xclient.window = g_Window;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = g_bFullscreen ? 0 : 1;

	fullscreen = XInternAtom(g_pDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(g_pDisplay,
		RootWindow(g_pDisplay, g_pVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);	
}

void UnInitialize(void)
{
	if (g_Window)
	{
		XDestroyWindow(g_pDisplay, g_Window);
		g_Window = 0;
	}

	XFreeColormap(g_pDisplay, g_Colormap);

	if (g_pVisualInfo != NULL)
	{
		free(g_pVisualInfo);
		g_pVisualInfo = NULL;
	}

	if (g_pDisplay != NULL)
	{
		XCloseDisplay(g_pDisplay);
		g_pDisplay = NULL;
	}
}




