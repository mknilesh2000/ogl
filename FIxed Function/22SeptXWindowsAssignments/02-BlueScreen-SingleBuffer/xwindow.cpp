// To build, used... g++ "./XWindow3D.cpp" -o "./XWindow3D" -lX11 -lGL -lGLU

#include <iostream>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

using namespace std;

Display *g_pDisplay = NULL;
XVisualInfo *g_pXVisualInfo = NULL;
Colormap g_colormap;
Window g_window;
int giWindowWidth = 800;
int giWindowHeight = 600;
bool g_bFullscreen = false;

GLXContext g_GLXContext;

int main(void)
{
	// forwards
	void Resize(int width, int height);
	void CreateWindow(void);
	void Initialize(void);
	void UnInitialize(void);
	void HandleXKeyEvent(const XKeyEvent &xkey);
	void HandleXButtonEvent(const XButtonEvent &xbutton);
	void DisplayContent(void);
	void Spin(void);

	// create window
	CreateWindow();

	// initialize GL
	Initialize();

	// 	/*
	//  * this union is defined so Xlib can always use the same sized
	//  * event structure internally, to avoid memory fragmentation.
	//  */
	// typedef union _XEvent {
	//         int type;		/* must not be changed; first element */
	// 	XAnyEvent xany;
	// 	XKeyEvent xkey;
	// 	XButtonEvent xbutton;
	// 	XMotionEvent xmotion;
	// 	XCrossingEvent xcrossing;
	// 	XFocusChangeEvent xfocus;
	// 	XExposeEvent xexpose;
	// 	XGraphicsExposeEvent xgraphicsexpose;
	// 	XNoExposeEvent xnoexpose;
	// 	XVisibilityEvent xvisibility;
	// 	XCreateWindowEvent xcreatewindow;
	// 	XDestroyWindowEvent xdestroywindow;
	// 	XUnmapEvent xunmap;
	// 	XMapEvent xmap;
	// 	XMapRequestEvent xmaprequest;
	// 	XReparentEvent xreparent;
	// 	XConfigureEvent xconfigure;
	// 	XGravityEvent xgravity;
	// 	XResizeRequestEvent xresizerequest;
	// 	XConfigureRequestEvent xconfigurerequest;
	// 	XCirculateEvent xcirculate;
	// 	XCirculateRequestEvent xcirculaterequest;
	// 	XPropertyEvent xproperty;
	// 	XSelectionClearEvent xselectionclear;
	// 	XSelectionRequestEvent xselectionrequest;
	// 	XSelectionEvent xselection;
	// 	XColormapEvent xcolormap;
	// 	XClientMessageEvent xclient;
	// 	XMappingEvent xmapping;
	// 	XErrorEvent xerror;
	// 	XKeymapEvent xkeymap;
	// 	XGenericEvent xgeneric;
	// 	XGenericEventCookie xcookie;
	// 	long pad[24];
	// } XEvent;

	XEvent event;

	// Message Loop
	bool bDone = false;
	while (!bDone)
	{
		while (XPending(g_pDisplay))
		{
			XNextEvent(g_pDisplay, &event);
			switch (event.type)
			{
				case MapNotify:
					break;

				case KeyPress: // keyboard
					HandleXKeyEvent(event.xkey);
					break;

				case ButtonPress: // mouse buttons
					HandleXButtonEvent(event.xbutton);
					break;

				case MotionNotify: // mouse move
					break;

				case ConfigureNotify: // similar to WM_SIZE
					Resize(event.xconfigure.width, event.xconfigure.height);
					break;

				case Expose: // equivalent to WM_PAINT
					break;

				case DestroyNotify: // equivalent to WM_DESTROY
					break;

				case 33:  // this will come when user tries to close the window
					bDone = true;
					break;

				default:
					break;
			}
		}

		Spin();
		DisplayContent();
	}

	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	// forwards
	void UnInitialize(void);

	g_pDisplay = XOpenDisplay(NULL);
	if (g_pDisplay == NULL)
	{
		cout << "ERROR: Unable to open XDisplay. Exitting ....";
		UnInitialize();
		exit(1);
	}

	int defaultScreen = XDefaultScreen(g_pDisplay);
	static int frameBufferAttributes[] = 
	{
		GLX_RGBA,
		GLX_RED_SIZE, 1,
		GLX_GREEN_SIZE, 1,
		GLX_BLUE_SIZE, 1,
		GLX_ALPHA_SIZE, 1,
		None
	};

	g_pXVisualInfo = glXChooseVisual(g_pDisplay, defaultScreen, frameBufferAttributes);

	XSetWindowAttributes winAttributes = {0};
	winAttributes.border_pixel 		= 0;
	winAttributes.background_pixmap = 0;
	winAttributes.colormap 			= XCreateColormap(g_pDisplay, RootWindow(g_pDisplay, g_pXVisualInfo->screen), g_pXVisualInfo->visual, AllocNone);
	winAttributes.background_pixel 	= BlackPixel(g_pDisplay, defaultScreen);
	winAttributes.event_mask 		= ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	// store color map in global variable
	g_colormap = winAttributes.colormap;

	g_window = XCreateWindow(
							    g_pDisplay /* display */,
							    RootWindow(g_pDisplay, g_pXVisualInfo->screen) /* parent */,
							    0			/* x */,
							    0			/* y */,
							    giWindowWidth	/* width */,
							    giWindowHeight	/* height */,
							    0	/* border_width */,
							    g_pXVisualInfo->depth			/* depth */,
							    InputOutput	/* class */,
							    g_pXVisualInfo->visual		/* visual */,
							    CWBorderPixel | CWBackPixel | CWEventMask | CWColormap	/* valuemask */,
							    &winAttributes	/* attributes */
							);

	if (!g_window)
	{
		cout << "ERROR: failed to create main window. Exitting ....";
		UnInitialize();
		exit(1);
	}

	// store name to uniquely identify the window. This can be used remotely
	XStoreName(
			    g_pDisplay		/* display */,
			    g_window		/* w */,
			    "Nilesh's XWindow OGL"	/* window_name */
			);

	// register a protocol for deleting the window
	Atom windowManagerDelete = XInternAtom(
										    g_pDisplay		/* display */,
										    "WM_DELETE_WINDOW"	/* atom_name */,
										    True		/* only_if_exists */
										  );
	XSetWMProtocols(
				    g_pDisplay		/* display */,
				    g_window		/* w */,
				    &windowManagerDelete		/* protocols */,
				    1			/* count */
				);

	// show window
	XMapWindow(
			    g_pDisplay		/* display */,
			    g_window		/* w */
			);
}

void DisplayContent(void)
{
	// GLAPI void GLAPIENTRY glClear( GLbitfield mask );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glFlush();
}

void Spin(void)
{
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;
	/* GLAPI void GLAPIENTRY glViewport( GLint x, GLint y,
                                    GLsizei width, GLsizei height ); */
	glViewport(0, 0, static_cast<GLsizei>(width), static_cast<GLsizei>(height));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, static_cast<GLfloat>(width/height), 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Initialize(void)
{
	// declarations
	void Resize(int width, int height);

	g_GLXContext = glXCreateContext(
									g_pDisplay,     // Display *dpy, 
									g_pXVisualInfo, // XVisualInfo *vis,
				    				NULL, 			// GLXContext shareList, <= valid if we want to share this context with multiple monitors
				    				GL_TRUE 		// Bool direct       < = Use HW Driver
				    			);
	/*Bool*/ glXMakeCurrent( 
							g_pDisplay, // Display *dpy, 
							g_window, // GLXDrawable drawable,
							g_GLXContext // GLXContext ctx
			    		);

	glShadeModel(GL_SMOOTH);

	// set background color to black
	/* GLAPI void GLAPIENTRY */ glClearColor( 
											 0.0f, // GLclampf red, 
											 0.0f, // GLclampf green, 
											 1.0f, // GLclampf blue, 
											 0.0f  // GLclampf alpha
										);
	Resize(giWindowWidth, giWindowHeight);
}

void UnInitialize(void)
{
	GLXContext currentGLXContext = glXGetCurrentContext();
	
	if ((currentGLXContext != NULL) && (currentGLXContext == g_GLXContext))
	{
		glXMakeCurrent(g_pDisplay, 0, 0);
	}

	if (g_GLXContext)
	{
		glXDestroyContext(g_pDisplay, g_GLXContext);
	}

	if (g_window)
	{
		XDestroyWindow(g_pDisplay, g_window);
	}

	XFreeColormap(g_pDisplay, g_colormap);

	if (g_pXVisualInfo)
	{
		free(g_pXVisualInfo);
		g_pXVisualInfo = NULL;
	}

	if (g_pDisplay)
	{
		XCloseDisplay(g_pDisplay);
		g_pDisplay = NULL;
	}
}

void ToggleFullscreen()
{
	// Registers XServer specific protocols. These are of types -
	// - XServer specific
	// - Window manager specific
	// - Network Window manager specific
	XEvent xev = {0};
	xev.type = ClientMessage;
	xev.xclient.window = g_window;
	xev.xclient.message_type = XInternAtom(g_pDisplay,"_NET_WM_STATE", False);
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = g_bFullscreen ? 0 : 1;
	xev.xclient.data.l[1] = XInternAtom(g_pDisplay,"_NET_WM_STATE_FULLSCREEN", False);

	// Status XSendEvent(
	//     Display*		/* display */,
	//     Window		/* w */,
	//     Bool		/* propagate */,
	//     long		/* event_mask */,
	//     XEvent*		/* event_send */
	// );
	XSendEvent(g_pDisplay, 
			   RootWindow(g_pDisplay, g_pXVisualInfo->screen),
			   False,  // whether to propagate this event to other monitors window managers
			   StructureNotifyMask,
			   &xev);

	// negate the fullscreen flag
	g_bFullscreen = !g_bFullscreen;
}

void HandleXKeyEvent(const XKeyEvent &xkey)
{
	// forwards
	void UnInitialize(void);

	// KeySym XkbKeycodeToKeysym(
	// 					 		Display *	/* dpy */,
	// 					 #if NeedWidePrototypes
	// 					 		 unsigned int 	/* kc */,
	// 					 #else
	// 					 		 KeyCode 	/* kc */,
	// 					 #endif
	// 					 		 int 		/* group */,
	// 					 		 int		/* level */
	// 					);
	KeySym keysym = XkbKeycodeToKeysym(g_pDisplay, xkey.keycode, 0, 0);
	switch (keysym)
	{
		case XK_Escape:
			UnInitialize();
			exit(0);
			break;

		case XK_F:
		case XK_f:
			ToggleFullscreen();
			break;

		default:
			break;
	}
}

void HandleXButtonEvent(const XButtonEvent &xbutton)
{
	switch (xbutton.button)
	{
		case 1:
			break;

		case 2:
			break;

		case 3:
			break;

		default:
			break;
	}
}
