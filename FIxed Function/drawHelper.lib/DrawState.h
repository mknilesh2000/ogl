#pragma once

#include <vector>
#include <memory>
#include "DrawCommon.h"
#ifndef _WIN32
#include <cstring>
#endif

#define OPERATION_VALID               0x00000001
#define OPERATION_COLOR_VALID         0x00000002
#define OPERATION_ATTRIB_SIZE_VALID   0x00000004

using namespace std;

struct DrawOperation
{
    unsigned long flags;
    GLenum operation;
    Color3f color;
    GLfloat attribSize;
    std::vector<Vertex3f> vertices;

    DrawOperation& operator=(const DrawOperation &other)
    {
        if (this != &other)
        {
            this->operation = other.operation;
            memcpy(&this->color, &other.color, sizeof(Color3f));
            this->flags = other.flags;
            this->vertices = other.vertices;
        }
        return *this;
    }
};

// interface to expose functionality of recording draw operations along with the vertices.
class IDrawState
{
public:
    // queues the operation at the end of the operations and returns the
    // identifier of the operation
    virtual size_t queueOperation(const DrawOperation &operation) = 0;

    // returns the operation at the specified position
    virtual DrawOperation& getOperation(size_t opId) = 0;

    // draws the stored state
    virtual void draw() = 0;
};

// creates a new DrawState and returns its shared_ptr
std::shared_ptr<IDrawState> createDrawState();

