#pragma once
#include <math.h>
#include "DrawCommon.h"
#include <memory>

#ifdef _WIN32
#define M_PI ((GLfloat)acos(-1.0))
#endif
#define sinf(x) ((GLfloat)sin(x))
#define cosf(x) ((GLfloat)cos(x))

using namespace std;

void drawGrid();
bool drawSolidTriangle(size_t numColors, const Color3f *pColorArray, const Vertex3f *pVertices);
bool drawTexturedTriangle(const Vertex3f *pVertices, const TexCoord2f *pTexCoords);
bool drawSolidQuad(size_t numColors, const Color3f *pColorArray, const Vertex3f *pVertices);
bool drawTexturedQuad(const Vertex3f *pVertices, const TexCoord2f *pTexCoords);
bool drawLineLoop(GLfloat lineWidth, size_t numVertices, const Vertex3f *pVertexArray, size_t numColors, const Color3f *pColorArray);
void drawCircleWithPoints(Color3f borderColor, Vertex3f center, GLfloat radius, bool drawWithAspectCorrection = true);
void drawCircleWithLines(Color3f borderColor, Vertex3f center, GLfloat radius, bool drawWithAspectCorrection = true);
void drawEquilateralTriangle(GLfloat radius, GLfloat borderWidth, Color3f borderColor, bool drawWithAspectCorrection = true);
Vertex3f* getArcVertices(Vertex3f(&vertices)[3], size_t &numVertices);

// various matrices
std::shared_ptr<GLfloat> getIdentityMatrix();
std::shared_ptr<GLfloat> getTranslationMatrix(GLfloat tx, GLfloat ty, GLfloat tz);
std::shared_ptr<GLfloat> getScaleMatrix(GLfloat sx, GLfloat sy, GLfloat sz);
std::shared_ptr<GLfloat> getRotationMatrixX(GLfloat rotationDegrees);
std::shared_ptr<GLfloat> getRotationMatrixY(GLfloat rotationDegrees);
std::shared_ptr<GLfloat> getRotationMatrixZ(GLfloat rotationDegrees);
std::shared_ptr<GLfloat> getOrthographicProjectionMatrix(GLfloat left, GLfloat right, GLfloat top, GLfloat bottom, GLfloat nr, GLfloat fr);
std::shared_ptr<GLfloat> getPerspectiveProjectionMatrix(GLfloat left, GLfloat right, GLfloat top, GLfloat bottom, GLfloat nr, GLfloat fr);
