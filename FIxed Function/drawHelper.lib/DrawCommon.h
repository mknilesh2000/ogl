#pragma once

#ifdef _WIN32
#include <Windows.h>
#else
#define _countof(x) (sizeof(x)/sizeof(x[0]))
#endif

#include <GL/gl.h>

struct Vertex3f
{
    GLfloat x;
    GLfloat y;
    GLfloat z;

    Vertex3f(GLfloat x, GLfloat y, GLfloat z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    Vertex3f()
        : x(0.0f), y(0.0f), z(0.0f)
    {

    }
};

struct Color3f
{
    GLfloat r;
    GLfloat g;
    GLfloat b;

    Color3f(GLfloat r, GLfloat g, GLfloat b)
    {
        this->r = r;
        this->g = g;
        this->b = b;
    }

    Color3f() :
        r(0.0f), g(0.0f), b(0.0f)
    {
    }
};

struct TexCoord2f
{
    GLfloat s;
    GLfloat t;

    TexCoord2f(GLfloat s, GLfloat t)
    {
        this->s = s;
        this->t = t;
    }

    TexCoord2f() :
        s(0.0f),
        t(0.0f)
    {
    }
};
