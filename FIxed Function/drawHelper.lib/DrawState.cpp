#include "DrawState.h"
#include <shared_mutex>
#include <algorithm>

class CDrawState : public IDrawState
{
    std::vector<DrawOperation> m_operations;
public:
    // queues the operation at the end of the operations and returns the
    // identifier of the operation
    size_t queueOperation(const DrawOperation &operation) override
    {
        size_t currentIdx = m_operations.size();
        m_operations.push_back(operation);
        return currentIdx;
    }

    // returns the operation at the specified position
    DrawOperation& getOperation(size_t opId) override
    {
        return m_operations.at(opId);
    }

    // draws the saved operation
    void draw() override
    {
        std::for_each(
            m_operations.begin(),
            m_operations.end(),
            std::bind(&CDrawState::executeOperation, this, std::placeholders::_1)
        );
    }

private:
    // executes an operation
    void executeOperation(const DrawOperation &operation)
    {
        if (operation.flags | OPERATION_VALID)
        {
            // set color if requested
            if (operation.flags | OPERATION_COLOR_VALID)
                glColor3f(operation.color.r, operation.color.g, operation.color.b);
            // set point size if the attrib size is valid
            if (operation.flags | OPERATION_ATTRIB_SIZE_VALID)
                glPointSize(operation.attribSize);
            // draw vertices
            glBegin(operation.operation);
                std::for_each(operation.vertices.begin(), operation.vertices.end(), std::bind(&CDrawState::drawVertex, this, std::placeholders::_1));
            glEnd();
        }
    }

    void drawVertex(const Vertex3f &vertex)
    {
        glVertex3f(vertex.x, vertex.y, vertex.z);
    }
};

std::shared_ptr<IDrawState> createDrawState()
{
    return std::shared_ptr<IDrawState>(new CDrawState);
}
