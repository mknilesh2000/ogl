#ifdef _WIN32
#include <windows.h>
#endif

#include "DrawHelper.h"

void drawGrid()
{
    glLineWidth(3.0f);
    glBegin(GL_LINES);
        // draw a red colored horizontal line of width 3.0
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(-1.0f, 0.0f, 0.0f);
        glVertex3f(1.0f, 0.0f, 0.0f);
        // draw a green colored vertical line of width 3.0
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3f(0.0f, -1.0f, 0.0f);
        glVertex3f(0.0f, 1.0f, 0.0f);
    glEnd();

    // draw grid of 20 equally spaced blue lines of width 1.0
    glLineWidth(1.0f);
    glBegin(GL_LINES);
        glColor3f(0.0f, 0.0f, 1.0f);
        float totalParts = 21.0f;
        for (float lineIdx = 1.0f; lineIdx < totalParts; lineIdx += 1.0f)
        {
            float thisMag = lineIdx / totalParts;
            // draw line before
            glVertex3f(-thisMag, -1.0f, 0.0f);
            glVertex3f(-thisMag, 1.0f, 0.0f);
            // draw line after
            glVertex3f(thisMag, -1.0f, 0.0f);
            glVertex3f(thisMag, 1.0f, 0.0f);
            // draw line above
            glVertex3f(-1.0f, thisMag, 0.0f);
            glVertex3f(1.0f, thisMag, 0.0f);
            // draw line below
            glVertex3f(-1.0f, -thisMag, 0.0f);
            glVertex3f(1.0f, -thisMag, 0.0f);
        }
    glEnd();
}

bool drawLineLoop(GLfloat lineWidth, size_t numVertices, const Vertex3f *pVertexArray, size_t numColors, const Color3f *pColorArray)
{
    bool successful = false;
    if (pVertexArray != nullptr && numVertices > 0 &&
        pColorArray != nullptr && numColors > 0)
    {
        glLineWidth(lineWidth);
        glBegin(GL_LINE_LOOP);
            for (size_t i = 0; i < numVertices; ++i)
            {
                glColor3f(pColorArray[i%numColors].r, pColorArray[i%numColors].g, pColorArray[i%numColors].b);
                glVertex3f(pVertexArray[i].x, pVertexArray[i].y, pVertexArray[i].z);
            }
        glEnd();
        successful = true;
    }
    return successful;
}

bool drawSolidTriangle(size_t numColors, const Color3f *pColorArray, const Vertex3f *pVertices)
{
    bool succeded = false;
    if (numColors > 0 && pColorArray != nullptr && pVertices != nullptr)
    {
        glBegin(GL_TRIANGLES);
        for (size_t i = 0; i < 3; ++i)
        {
            glColor3f(pColorArray[i%numColors].r, pColorArray[i%numColors].g, pColorArray[i%numColors].b);
            glVertex3f(pVertices[i].x, pVertices[i].y, pVertices[i].z);
        }
        glEnd();
        succeded = true;
    }
    return succeded;
}

bool drawTexturedTriangle(const Vertex3f *pVertices, const TexCoord2f *pTexCoords)
{
    bool succeded = false;
    if (pVertices != nullptr && pTexCoords != nullptr)
    {
        glBegin(GL_TRIANGLES);
        for (size_t i = 0; i < 3; ++i)
        {
            glTexCoord2f(pTexCoords[i].s, pTexCoords[i].t);
            glVertex3f(pVertices[i].x, pVertices[i].y, pVertices[i].z);
        }
        glEnd();
        succeded = true;
    }
    return succeded;
}

bool drawSolidQuad(size_t numColors, const Color3f *pColorArray, const Vertex3f *pVertices)
{
    bool succeded = false;
    if (numColors > 0 && pColorArray != nullptr && pVertices != nullptr)
    {
        glBegin(GL_QUADS);
        for (size_t i = 0; i < 4; ++i)
        {
            glColor3f(pColorArray[i%numColors].r, pColorArray[i%numColors].g, pColorArray[i%numColors].b);
            glVertex3f(pVertices[i].x, pVertices[i].y, pVertices[i].z);
        }
        glEnd();
        succeded = true;
    }
    return succeded;
}

bool drawTexturedQuad(const Vertex3f *pVertices, const TexCoord2f *pTexCoords)
{
    bool succeded = false;
    if (pVertices != nullptr && pTexCoords != nullptr)
    {
        glBegin(GL_QUADS);
        for (size_t i = 0; i < 4; ++i)
        {
            glTexCoord2f(pTexCoords[i].s, pTexCoords[i].t);
            glVertex3f(pVertices[i].x, pVertices[i].y, pVertices[i].z);
        }
        glEnd();
        succeded = true;
    }
    return succeded;
}

void drawCircleWithPoints(Color3f borderColor, Vertex3f center, GLfloat radius, bool drawWithAspectCorrection/* = true*/)
{
    GLfloat aspect = 1;
#ifdef _WIN32
    if (drawWithAspectCorrection)
    {
        // retrieve the aspect ratio to apply screen correction.
        aspect = (GLfloat)GetSystemMetrics(SM_CXSCREEN) / (GLfloat)GetSystemMetrics(SM_CYSCREEN);
    }
#endif
    // draw yelllow bordered circle with half width diameter
    glBegin(GL_POINTS);
    glColor3f(borderColor.r, borderColor.g, borderColor.b);
    for (GLfloat angle = 0.0; angle <= 2 * M_PI; angle += 0.01f)
        glVertex3f(0.5f * (GLfloat)cos(angle), 0.5f * aspect * (GLfloat)sin(angle), 0.0f);
    glEnd();
}

void drawCircleWithLines(Color3f borderColor, Vertex3f center, GLfloat radius, bool drawWithAspectCorrection/* = true*/)
{
    GLfloat aspect = 1;
#ifdef _WIN32
    if (drawWithAspectCorrection)
    {
        // retrieve the aspect ratio to apply screen correction.
        aspect = (GLfloat)GetSystemMetrics(SM_CXSCREEN) / (GLfloat)GetSystemMetrics(SM_CYSCREEN);
    }
#endif

    glBegin(GL_LINE_LOOP);
    glColor3f(borderColor.r, borderColor.g, borderColor.b);
    for (GLfloat angle = 0.0; angle <= 2 * M_PI; angle += 0.01f)
        glVertex3f(center.x + radius * (GLfloat)cos(angle), center.y + radius * aspect * (GLfloat)sin(angle), 0.0f);
    glEnd();
}

void drawEquilateralTriangle(GLfloat radius, GLfloat borderWidth, Color3f borderColor, bool drawWithAspectCorrection/* = true*/)
{
    GLfloat aspect = 1.0;
#ifdef _WIN32
    if (drawWithAspectCorrection)
        aspect = (GLfloat)GetSystemMetrics(SM_CXSCREEN) / (GLfloat)GetSystemMetrics(SM_CYSCREEN);
#endif
    Vertex3f vertices[] = {
        { 0, radius, 0 },
        { - (GLfloat)cos(M_PI / 6) * radius, - (GLfloat)sin(M_PI / 6) * radius, 0.0f },
        { (GLfloat)cos(M_PI / 6) * radius, -(GLfloat)sin(M_PI / 6) * radius, 0.0f }
    };

    //Vertex3f vertices[] = {
    //    { apex.x, apex.y, 0.0f },
    //    { apex.x -((GLfloat)cos(M_PI / 6) * radius), aspect * (apex.y -((GLfloat)sin(M_PI / 6) * radius)), 0.0f },
    //    { apex.x + ((GLfloat)cos(M_PI / 6) * radius), aspect * (apex.y -((GLfloat)sin(M_PI / 6) * radius)), 0.0f }
    //};

    // draw triangle
    drawLineLoop(borderWidth, 3, vertices, 1, &borderColor);
}

GLfloat solveDeterminant(GLfloat *det, size_t rows)
{
    if (rows == 1)
        return det[0];

    size_t newRows = rows - 1;
    GLfloat solved = 0.0f;
    for (size_t i = 0; i < rows; ++i)
    {
        GLfloat *newDet = new GLfloat[newRows*newRows];
        size_t thisElement = 0;
        for (size_t j = 1; j < rows; ++j)
        {
            for (size_t k = 0; k < rows; ++k)
            {
                if (i != k)
                {
                    newDet[thisElement] = det[j*rows + k];
                    thisElement++;
                }
            }
        }

        if (i % 2 == 0)
            solved += det[i] * solveDeterminant(newDet, newRows);
        else
            solved -= det[i] * solveDeterminant(newDet, newRows);

        delete newDet;
    }

    return solved;
}

//Let (h,k) be the coordinates of the center of the circle, and r its 
//radius. Then the equation of the circle is:
//
//     (x-h)^2 + (y-k)^2 = r^2
//
//Since the three points all lie on the circle, their coordinates will 
//satisfy this equation. That gives you three equations:
//
//     (x1-h)^2 + (y1-k)^2 = r^2
//     (x2-h)^2 + (y2-k)^2 = r^2
//     (x3-h)^2 + (y3-k)^2 = r^2
//
//in the three unknowns h, k, and r. To solve these, subtract the first 
//from the other two. That will eliminate r, h^2, and k^2 from the last 
//two equations, leaving you with two simultaneous linear equations in 
//the two unknowns h and k. Solve these, and you'll have the coordinates 
//(h,k) of the center of the circle. Finally, set:
//
//     r = sqrt[(x1-h)^2+(y1-k)^2]
//
//and you'll have everything you need to know about the circle.
//
//This can all be done symbolically, of course, but you'll get some 
//pretty complicated expressions for h and k. The simplest forms of 
//these involve determinants, if you know what they are:
//
//         |x1^2+y1^2  y1  1|        |x1  x1^2+y1^2  1|
//         |x2^2+y2^2  y2  1|        |x2  x2^2+y2^2  1|
//         |x3^2+y3^2  y3  1|        |x3  x3^2+y3^2  1|
//     h = ------------------,   k = ------------------
//             |x1  y1  1|               |x1  y1  1|
//           2*|x2  y2  1|             2*|x2  y2  1|
//             |x3  y3  1|               |x3  y3  1|
//
//Example: Suppose a circle passes through the points (4,1), (-3,7), and 
//(5,-2). Then we know that:
//
//     (h-4)^2 + (k-1)^2 = r^2
//     (h+3)^2 + (k-7)^2 = r^2
//     (h-5)^2 + (k+2)^2 = r^2
//
//Subtracting the first from the other two, you get:
//
//     (h+3)^2 - (h-4)^2 + (k-7)^2 - (k-1)^2 = 0
//     (h-5)^2 - (h-4)^2 + (k+2)^2 - (k-1)^2 = 0
//
//     h^2+6*h+9 - h^2+8*h-16 + k^2-14*k+49 - k^2+2*k-1 = 0
//     h^2-10*h+25 - h^2+8*h-16 + k^2+4*k+4 - k^2+2*k-1 = 0
//
//     14*h - 12*k + 41 = 0
//     -2*h + 6*k + 12 = 0
//
//     10*h + 65 = 0
//     30*k + 125 = 0
//
//     h = -13/2
//     k = -25/6
//
//Then
//
//     r = sqrt[(4+13/2)^2 + (1+25/6)^2]
//       = sqrt[4930]/6
//
//Thus the equation of the circle is:
//
//     (x+13/2)^2 + (y+25/6)^2 = 4930/36
//
// co-ordinates of any point in the circle having center at (h,k) and radius 'r' can be given as (h + r cos(t), k + r sin(t))
// where t is the angle of the parameteric point
//
Vertex3f* getArcVertices(Vertex3f(&vertices)[3], size_t &numVertices)
{
    // find out center of the arc
    GLfloat hDetNum[] = {
        vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y, vertices[0].y, 1,
        vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y, vertices[1].y, 1,
        vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y, vertices[2].y, 1,
    };
    GLfloat kDetNum[] = {
        vertices[0].x, vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y, 1,
        vertices[1].x, vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y, 1,
        vertices[2].x, vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y, 1,
    };
    GLfloat detDenom[] = {
        vertices[0].x, vertices[0].y, 1,
        vertices[1].x, vertices[1].y, 1,
        vertices[2].x, vertices[2].y, 1,
    };

    GLfloat h = 0.5f * solveDeterminant(hDetNum, 3) / solveDeterminant(detDenom, 3);
    GLfloat k = 0.5f * solveDeterminant(kDetNum, 3) / solveDeterminant(detDenom, 3);
    GLfloat radius = (GLfloat)sqrt((vertices[0].x - h)*(vertices[0].x - h) + (vertices[0].y - k)*(vertices[0].y - k));

    GLfloat startAngle = (GLfloat)acos((vertices[0].x - h) / radius);
    GLfloat endAngle = (GLfloat)acos((vertices[0].x - h) / radius);

    for (size_t i = 1; i < _countof(vertices); ++i)
    {
        GLfloat thisAngle = (GLfloat)acos((vertices[i].x - h) / radius);
        startAngle = min(startAngle, thisAngle);
        endAngle = max(endAngle, thisAngle);
    }

    numVertices = (size_t)(((endAngle - startAngle) / 0.00001f) + 1);
    Vertex3f *pVertexArray = new Vertex3f[numVertices];
    for (size_t i = 0; i < numVertices; ++i)
    {
        pVertexArray[i].x = h + radius * (GLfloat)cos(startAngle + i*0.00001f);
        pVertexArray[i].y = k + radius * (GLfloat)sin(startAngle + i*0.00001f);
        pVertexArray[i].z = 0.0f;
        glVertex3f(pVertexArray[i].x, pVertexArray[i].y, pVertexArray[i].z);
    }

    return pVertexArray;
}

std::shared_ptr<GLfloat> getIdentityMatrix()
{
    GLfloat *pIdentityMatrix = new GLfloat[16];
    pIdentityMatrix[0] = 1.0f; pIdentityMatrix[4] = 0.0f; pIdentityMatrix[8] = 0.0f;  pIdentityMatrix[12] = 0.0f;
    pIdentityMatrix[1] = 0.0f; pIdentityMatrix[5] = 1.0f; pIdentityMatrix[9] = 0.0f;  pIdentityMatrix[13] = 0.0f;
    pIdentityMatrix[2] = 0.0f; pIdentityMatrix[6] = 0.0f; pIdentityMatrix[10] = 1.0f; pIdentityMatrix[14] = 0.0f;
    pIdentityMatrix[3] = 0.0f; pIdentityMatrix[7] = 0.0f; pIdentityMatrix[11] = 0.0f; pIdentityMatrix[15] = 1.0f;
    return std::shared_ptr<GLfloat>(pIdentityMatrix);
}

std::shared_ptr<GLfloat> getTranslationMatrix(GLfloat tx, GLfloat ty, GLfloat tz)
{
    GLfloat *pTranslationMatrix = new GLfloat[16];
    pTranslationMatrix[0] = 1.0f; pTranslationMatrix[4] = 0.0f; pTranslationMatrix[8] = 0.0f;  pTranslationMatrix[12] = tx;
    pTranslationMatrix[1] = 0.0f; pTranslationMatrix[5] = 1.0f; pTranslationMatrix[9] = 0.0f;  pTranslationMatrix[13] = ty;
    pTranslationMatrix[2] = 0.0f; pTranslationMatrix[6] = 0.0f; pTranslationMatrix[10] = 1.0f; pTranslationMatrix[14] = tz;
    pTranslationMatrix[3] = 0.0f; pTranslationMatrix[7] = 0.0f; pTranslationMatrix[11] = 0.0f; pTranslationMatrix[15] = 1.0f;
    return std::shared_ptr<GLfloat>(pTranslationMatrix);
}

std::shared_ptr<GLfloat> getScaleMatrix(GLfloat sx, GLfloat sy, GLfloat sz)
{
    GLfloat *pScaleMatrix = new GLfloat[16];
    pScaleMatrix[0] = sx;       pScaleMatrix[4] = 0.0f;     pScaleMatrix[8] = 0.0f;     pScaleMatrix[12] = 0.0f;
    pScaleMatrix[1] = 0.0f;     pScaleMatrix[5] = sy;       pScaleMatrix[9] = 0.0f;     pScaleMatrix[13] = 0.0f;
    pScaleMatrix[2] = 0.0f;     pScaleMatrix[6] = 0.0f;     pScaleMatrix[10] = sz;      pScaleMatrix[14] = 0.0f;
    pScaleMatrix[3] = 0.0f;     pScaleMatrix[7] = 0.0f;     pScaleMatrix[11] = 0.0f;    pScaleMatrix[15] = 1.0f;
    return std::shared_ptr<GLfloat>(pScaleMatrix);
}

std::shared_ptr<GLfloat> getRotationMatrixX(GLfloat rotationDegrees)
{
    GLfloat rotationRad = rotationDegrees * M_PI / 180.0f;
    GLfloat *pRotationMatrixX = new GLfloat[16];
    pRotationMatrixX[0] = 1;        pRotationMatrixX[4] = 0.0f;                 pRotationMatrixX[8] = 0.0f;                 pRotationMatrixX[12] = 0.0f;
    pRotationMatrixX[1] = 0.0f;     pRotationMatrixX[5] = cosf(rotationRad);    pRotationMatrixX[9] = -sinf(rotationRad);   pRotationMatrixX[13] = 0.0f;
    pRotationMatrixX[2] = 0.0f;     pRotationMatrixX[6] = sinf(rotationRad);    pRotationMatrixX[10] = cosf(rotationRad);   pRotationMatrixX[14] = 0.0f;
    pRotationMatrixX[3] = 0.0f;     pRotationMatrixX[7] = 0.0f;                 pRotationMatrixX[11] = 0.0f;                pRotationMatrixX[15] = 1.0f;
    return std::shared_ptr<GLfloat>(pRotationMatrixX);
}

std::shared_ptr<GLfloat> getRotationMatrixY(GLfloat rotationDegrees)
{
    GLfloat rotationRad = rotationDegrees * M_PI / 180.0f;
    GLfloat *pRotationMatrixY = new GLfloat[16];
    pRotationMatrixY[0] = cosf(rotationRad);    pRotationMatrixY[4] = 0.0f;     pRotationMatrixY[8] = sinf(rotationRad);    pRotationMatrixY[12] = 0.0f;
    pRotationMatrixY[1] = 0.0f;                 pRotationMatrixY[5] = 1.0f;     pRotationMatrixY[9] = 0.0f;                 pRotationMatrixY[13] = 0.0f;
    pRotationMatrixY[2] = -sinf(rotationRad);   pRotationMatrixY[6] = 0.0f;     pRotationMatrixY[10] = cosf(rotationRad);   pRotationMatrixY[14] = 0.0f;
    pRotationMatrixY[3] = 0.0f;                 pRotationMatrixY[7] = 0.0f;     pRotationMatrixY[11] = 0.0f;                pRotationMatrixY[15] = 1.0f;
    return std::shared_ptr<GLfloat>(pRotationMatrixY);
}

std::shared_ptr<GLfloat> getRotationMatrixZ(GLfloat rotationDegrees)
{
    GLfloat rotationRad = rotationDegrees * M_PI / 180.0f;
    GLfloat *pRotationMatrixZ = new GLfloat[16];
    pRotationMatrixZ[0] = cosf(rotationRad);    pRotationMatrixZ[4] = -sinf(rotationRad);   pRotationMatrixZ[8] = 0.0f;     pRotationMatrixZ[12] = 0.0f;
    pRotationMatrixZ[1] = sinf(rotationRad);    pRotationMatrixZ[5] = cosf(rotationRad);    pRotationMatrixZ[9] = 0.0f;     pRotationMatrixZ[13] = 0.0f;
    pRotationMatrixZ[2] = 0.0f;                 pRotationMatrixZ[6] = 0.0f;                 pRotationMatrixZ[10] = 1.0f;    pRotationMatrixZ[14] = 0.0f;
    pRotationMatrixZ[3] = 0.0f;                 pRotationMatrixZ[7] = 0.0f;                 pRotationMatrixZ[11] = 0.0f;    pRotationMatrixZ[15] = 1.0f;
    return std::shared_ptr<GLfloat>(pRotationMatrixZ);
}

std::shared_ptr<GLfloat> getOrthographicProjectionMatrix(GLfloat left, GLfloat right, GLfloat top, GLfloat bottom, GLfloat nr, GLfloat fr)
{
    GLfloat *pOrthographicMatrix = new GLfloat[16];
    pOrthographicMatrix[0] = 2.0f / (right-left);   pOrthographicMatrix[4] = 0.0f;                  pOrthographicMatrix[8] = 0.0f;                  pOrthographicMatrix[12] = (right + left)/(right-left);
    pOrthographicMatrix[1] = 0.0f;                  pOrthographicMatrix[5] = 2.0f / (top - bottom); pOrthographicMatrix[9] = 0.0f;                  pOrthographicMatrix[13] = (top+bottom)/(top-bottom);
    pOrthographicMatrix[2] = 0.0f;                  pOrthographicMatrix[6] = 0.0f;                  pOrthographicMatrix[10] = -2.0f / (fr - nr);    pOrthographicMatrix[14] = (fr+nr)/(fr-nr);
    pOrthographicMatrix[3] = 0.0f;                  pOrthographicMatrix[7] = 0.0f;                  pOrthographicMatrix[11] = 0.0f;                 pOrthographicMatrix[15] = 1.0f;
    return std::shared_ptr<GLfloat>(pOrthographicMatrix);
}

std::shared_ptr<GLfloat> getPerspectiveProjectionMatrix(GLfloat left, GLfloat right, GLfloat top, GLfloat bottom, GLfloat nr, GLfloat fr)
{
    GLfloat *pMat = new GLfloat[16];
    pMat[0] = (2.0f * nr) / (right - left); pMat[4] = 0.0f;                         pMat[8] = (right+left)/(right-left);    pMat[12] = 0.0f;
    pMat[1] = 0.0f;                         pMat[5] = (2.0f * nr) / (top - bottom); pMat[9] = (top+bottom)/(top-bottom);    pMat[13] = 0.0f;
    pMat[2] = 0.0f;                         pMat[6] = 0.0f;                         pMat[10] = -(fr + nr)/(fr-nr);          pMat[14] = (-2.0f * fr * nr)/(fr-nr);
    pMat[3] = 0.0f;                         pMat[7] = 0.0f;                         pMat[11] = -1.0f;                       pMat[15] = 0.0f;
    return std::shared_ptr<GLfloat>(pMat);
}
