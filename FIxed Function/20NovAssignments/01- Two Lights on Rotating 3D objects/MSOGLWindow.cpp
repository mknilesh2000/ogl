#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include "DrawHelper.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 800

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLUquadric *g_gluQuadric = nullptr;
bool g_bLightEnable = true;

GLfloat g_angleCube = 0.0f;
GLfloat g_anglePyramid = 0.0f;
GLfloat g_angleSphere = 0.0f;

// light 0 - Right
GLfloat g_light0Position[] = {2.0f, 1.0f, 1.0f, 0.0f};
GLfloat g_light0AmbientComponent[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat g_light0DiffusedComponent[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat g_light0SpecularComponent[] = { 1.0f, 0.0f, 0.0f, 0.0f };

// light 1 - Left
GLfloat g_light1Position[] = { -2.0f, 1.0f, 1.0f, 0.0f };
GLfloat g_light1AmbientComponent[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat g_light1DiffusedComponent[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat g_light1SpecularComponent[] = { 0.0f, 0.0f, 1.0f, 0.0f };

// material properties
GLfloat g_materialAmbientComponent[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat g_materialDiffusedComponent[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat g_materialSpecularComponent[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat g_materialShinyness = 50.0f;

enum GeometryEnable
{
    Sphere,
    Cube,
    Pyramid
};

GeometryEnable g_currentGeometry = Pyramid;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    //function prototype
    void initialize(void);
    void display(void);
    void uninitialize(void);
    void spin(void);

    //variable declaration
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szClassName[] = TEXT("RTROGL");
    bool bDone = false;

    //code
    //initializing members of struct WNDCLASSEX
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szClassName;
    wndclass.lpszMenuName = NULL;

    //Registering Class
    RegisterClassEx(&wndclass);

    //Create Window
    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szClassName,
        TEXT("OGL Fixed function. Lights on Rotating Objects"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        0,
        0,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd = hwnd;

    //initialize
    initialize();

    ShowWindow(hwnd, SW_SHOW);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    //Message Loop
    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                    bDone = true;

                spin();
                // render
                display();
            }
        }
    }

    uninitialize();
    return((int)msg.wParam);
}

void spin(void)
{
    g_angleCube += 1.0f;
    if (g_angleCube >= 360.0f)
        g_angleCube = 1.0f;

    g_anglePyramid += 1.0f;
    if (g_anglePyramid >= 360.0f)
        g_anglePyramid = 1.0f;

    g_angleSphere += 1.0f;
    if (g_angleSphere >= 360.0f)
        g_angleSphere = 1.0f;
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //function prototype
    void resize(int, int);
    void ToggleFullscreen(void);
    void uninitialize(void);

    //code
    switch (iMsg)
    {
    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0)
            gbActiveWindow = true;
        else
            gbActiveWindow = false;
        break;
    case WM_ERASEBKGND:
        return(0);
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            gbEscapeKeyIsPressed = true;
            break;
        }
        break;
    case WM_CHAR:
        switch(wParam)
        {
        case 'F':
        case 'f':
            if (gbFullscreen == false)
            {
                ToggleFullscreen();
                gbFullscreen = true;
            }
            else
            {
                ToggleFullscreen();
                gbFullscreen = false;
            }
            break;

        case 'L':
        case 'l':
            g_bLightEnable = !g_bLightEnable;
            if (g_bLightEnable)
            {
                glEnable(GL_LIGHTING);
            }
            else
            {
                glDisable(GL_LIGHTING);
            }
            break;

        case 'P':
        case 'p':
            g_currentGeometry = Pyramid;
            break;

        case 'C':
        case 'c':
            g_currentGeometry = Cube;
            break;

        case 'S':
        case 's':
            g_currentGeometry = Sphere;
            break;

        default:
            break;
        }
        break;
    case WM_LBUTTONDOWN:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        break;
    }
    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    //variable declarations
    MONITORINFO mi;

    //code
    if (gbFullscreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi = { sizeof(MONITORINFO) };
            if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
    }

    else
    {
        //code
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }
}

void initialize(void)
{
    //function prototypes
    void resize(int, int);

    //variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    //code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    //Initialization of structure 'PIXELFORMATDESCRIPTOR'
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;
    pfd.cDepthBits = 32;

    ghdc = GetDC(ghwnd);

    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if (iPixelFormatIndex == 0)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    glClearDepth(1.0f); // set depth buffer
    glEnable(GL_DEPTH_TEST); // enable depth testing
    glDepthFunc(GL_LEQUAL); // type of depth testing

    // set components for light0
    glLightfv(GL_LIGHT0, GL_AMBIENT, g_light0AmbientComponent);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, g_light0DiffusedComponent);
    glLightfv(GL_LIGHT0, GL_SPECULAR, g_light0SpecularComponent);
    glLightfv(GL_LIGHT0, GL_POSITION, g_light0Position);
    glEnable(GL_LIGHT0);

    // set components for light1
    glLightfv(GL_LIGHT1, GL_AMBIENT, g_light1AmbientComponent);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, g_light1DiffusedComponent);
    glLightfv(GL_LIGHT1, GL_SPECULAR, g_light1SpecularComponent);
    glLightfv(GL_LIGHT1, GL_POSITION, g_light1Position);
    glEnable(GL_LIGHT1);

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, g_materialAmbientComponent);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, g_materialDiffusedComponent);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, g_materialSpecularComponent);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, g_materialShinyness);

    if (g_gluQuadric == nullptr)
        g_gluQuadric = gluNewQuadric();

    if (g_bLightEnable)
        glEnable(GL_LIGHTING);
    else
        glDisable(GL_LIGHTING);

    resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW); // select modelview matrix
    glLoadIdentity(); // reset current modelview matrix

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if (g_currentGeometry == Pyramid)
    {
        glTranslatef(0.0f, 0.0f, -6.0f);
        glRotatef(g_anglePyramid, 0.0f, 1.0f, 0.0f);

        Vertex3f pyramidVertices[] = {
            // front
            { 0.0f, 1.0f, 0.0f },
            { -1.0f, -1.0f, 1.0f },
            { 1.0f, -1.0f, 1.0f },

            // left
            { 0.0f, 1.0f, 0.0f },
            { -1.0f, -1.0f, -1.0f },
            { -1.0f, -1.0f, 1.0f },

            //back
            { 0.0f, 1.0f, 0.0f },
            { -1.0f, -1.0f, -1.0f },
            { 1.0f, -1.0f, -1.0f },

            // right
            { 0.0f, 1.0f, 0.0f },
            { 1.0f, -1.0f, 1.0f },
            { 1.0f, -1.0f, -1.0f },
        };

        // Draw triangles for the pyramid
        glBegin(GL_TRIANGLES);
            glNormal3f(0.0f, 0.447214f, 0.894427f);
            glVertex3fv((GLfloat*)&pyramidVertices[0]);
            glVertex3fv((GLfloat*)&pyramidVertices[1]);
            glVertex3fv((GLfloat*)&pyramidVertices[2]);

            glNormal3f(-0.894427f, 0.447214f, 0.0f);
            glVertex3fv((GLfloat*)&pyramidVertices[3]);
            glVertex3fv((GLfloat*)&pyramidVertices[4]);
            glVertex3fv((GLfloat*)&pyramidVertices[5]);

            glNormal3f(0.0f, 0.447214f, -0.894427f);
            glVertex3fv((GLfloat*)&pyramidVertices[6]);
            glVertex3fv((GLfloat*)&pyramidVertices[7]);
            glVertex3fv((GLfloat*)&pyramidVertices[8]);

            glNormal3f(0.894427f, 0.447214f, 0.0f);
            glVertex3fv((GLfloat*)&pyramidVertices[9]);
            glVertex3fv((GLfloat*)&pyramidVertices[10]);
            glVertex3fv((GLfloat*)&pyramidVertices[11]);
        glEnd();
    }
    else if (g_currentGeometry == Cube)
    {
        glTranslatef(0.0f, 0.0f, -6.0f);
        glScalef(0.75f, 0.75f, 0.75f);
        glRotatef(g_angleCube, 0.0f, 1.0f, 0.0f);

        Vertex3f cubeVertices[] = {
            // front
            { 1.0f, 1.0f, 1.0f },
            { -1.0f, 1.0f, 1.0f },
            { -1.0f, -1.0f, 1.0f },
            { 1.0f, -1.0f, 1.0f },

            // left
            { -1.0f, 1.0f, 1.0f },
            { -1.0f, 1.0f, -1.0f },
            { -1.0f, -1.0f, -1.0f },
            { -1.0f, -1.0f, 1.0f },

            // back
            { 1.0f, 1.0f, -1.0f },
            { -1.0f, 1.0f, -1.0f },
            { -1.0f, -1.0f, -1.0f },
            { 1.0f, -1.0f, -1.0f },

            // right
            { 1.0f, 1.0f, -1.0f },
            { 1.0f, 1.0f, 1.0f },
            { 1.0f, -1.0f, 1.0f },
            { 1.0f, -1.0f, -1.0f },

            // top
            { 1.0f, 1.0f, -1.0f },
            { -1.0f, 1.0f, -1.0f },
            { -1.0f, 1.0f, 1.0f },
            { 1.0f, 1.0f, 1.0f },

            // bottom
            { 1.0f, -1.0f, -1.0f },
            { -1.0f, -1.0f, -1.0f },
            { -1.0f, -1.0f, 1.0f },
            { 1.0f, -1.0f, 1.0f },
        };

        glBegin(GL_QUADS);
            glNormal3f(0.0f, 0.0f, 1.0f);
            glVertex3fv((GLfloat*)&cubeVertices[0]);
            glVertex3fv((GLfloat*)&cubeVertices[1]);
            glVertex3fv((GLfloat*)&cubeVertices[2]);
            glVertex3fv((GLfloat*)&cubeVertices[3]);

            glNormal3f(-1.0f, 0.0f, 0.0f);
            glVertex3fv((GLfloat*)&cubeVertices[4]);
            glVertex3fv((GLfloat*)&cubeVertices[5]);
            glVertex3fv((GLfloat*)&cubeVertices[6]);
            glVertex3fv((GLfloat*)&cubeVertices[7]);

            glNormal3f(0.0f, 0.0f, -1.0f);
            glVertex3fv((GLfloat*)&cubeVertices[8]);
            glVertex3fv((GLfloat*)&cubeVertices[9]);
            glVertex3fv((GLfloat*)&cubeVertices[10]);
            glVertex3fv((GLfloat*)&cubeVertices[11]);

            glNormal3f(1.0f, 0.0f, 0.0f);
            glVertex3fv((GLfloat*)&cubeVertices[12]);
            glVertex3fv((GLfloat*)&cubeVertices[13]);
            glVertex3fv((GLfloat*)&cubeVertices[14]);
            glVertex3fv((GLfloat*)&cubeVertices[15]);

            glNormal3f(0.0f, 1.0f, 0.0f);
            glVertex3fv((GLfloat*)&cubeVertices[16]);
            glVertex3fv((GLfloat*)&cubeVertices[17]);
            glVertex3fv((GLfloat*)&cubeVertices[18]);
            glVertex3fv((GLfloat*)&cubeVertices[19]);

            glNormal3f(0.0f, -1.0f, 0.0f);
            glVertex3fv((GLfloat*)&cubeVertices[20]);
            glVertex3fv((GLfloat*)&cubeVertices[21]);
            glVertex3fv((GLfloat*)&cubeVertices[22]);
            glVertex3fv((GLfloat*)&cubeVertices[23]);
        glEnd();
    }
    else if (g_currentGeometry == Sphere)
    {
        glTranslatef(0.0f, 0.0f, -4.0f);
        glRotatef(g_angleSphere, 0.0f, 1.0f, 0.0f);
        gluSphere(g_gluQuadric, 0.75, 30, 30);
    }

    SwapBuffers(ghdc);
}

void resize(int width, int height)
{
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    glMatrixMode(GL_PROJECTION); // select projection matrix
    glLoadIdentity(); // reset projection matrix

    gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f); // calculate the aspect ratio of the view
}

void uninitialize(void)
{
    if (gbFullscreen == true)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }

    wglMakeCurrent(NULL, NULL);

    wglDeleteContext(ghrc);
    ghrc = NULL;

    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;

    DestroyWindow(ghwnd);
    ghwnd = NULL;

    if (g_gluQuadric != nullptr)
    {
        gluDeleteQuadric(g_gluQuadric);
        g_gluQuadric = nullptr;
    }
}
