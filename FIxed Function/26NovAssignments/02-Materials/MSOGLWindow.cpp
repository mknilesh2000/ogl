#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include "DrawHelper.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 800

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLUquadric *g_gluQuadric = nullptr;
bool g_bLightEnable = false;
GLfloat xMultiplier = 1.0f;
GLfloat yMultiplier = 1.0f;

enum RotationAxis
{
    NoRotation,
    X_axis,
    Y_axis,
    Z_axis
};

// light 0
GLfloat g_lightPosition[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat g_lightAmbientComponent[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat g_lightDiffusedComponent[] = { 1.0f, 1.0f, 1.0f, 0.0f };
GLfloat g_lightSpecularComponent[] = { 1.0f, 1.0f, 1.0f, 0.0f };
GLfloat g_rotationAngle = 0.0f;
RotationAxis g_lightRotationAxis = NoRotation;

// light model
GLfloat g_lightModelAmbient[] = { 0.2f, 0.2f, 0.2f, 0.0f };
GLfloat g_lightModelLocalViewer = 0.0f;

struct SphereProps
{
    GLfloat ambientMaterial[4];
    GLfloat diffuseMaterial[4];
    GLfloat specularMaterial[4];
    GLfloat shinyness;
    GLfloat rowMultiplier;
    GLfloat colMultiplier;

    void draw()
    {
        glPushMatrix();
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambientMaterial);
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseMaterial);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularMaterial);
            glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shinyness);
            glTranslatef(colMultiplier*xMultiplier, rowMultiplier*yMultiplier, -10.0f);
            gluSphere(g_gluQuadric, 0.75, 120, 120);
        glPopMatrix();
    }
};

SphereProps g_spheres[] = {
    {   // emerald
        {0.0215f, 0.1745f, 0.0215f, 1.0f},
        {0.07568f, 0.61424f, 0.07568f, 1.0f},
        {0.633f, 0.727811f, 0.633f, 1.0f},
        0.6f*128.0f,
        5.0f,
        -3.0f
    },
    {   // jade
        {0.135f, 0.2225f, 0.1575f, 1.0f},
        {0.54f, 0.89f, 0.63f, 1.0f},
        {0.316228f, 0.316228f, 0.316228f, 1.0f},
        0.1f * 128.0f,
        3.0f,
        -3.0f
    },
    {   // obsidian
        {0.05375f, 0.05f, 0.06625f, 1.0f},
        {0.18275f, 0.17f, 0.22525f, 1.0f},
        {0.332741f, 0.328634f, 0.346435f, 1.0f},
        0.3f * 128.0f,
        1.0f,
        -3.0f
    },
    {   // pearl
        { 0.25f, 0.20725f, 0.20725f, 1.0f },
        { 1.0f, 0.829f, 0.829f, 1.0f },
        { 0.296648f, 0.296648f, 0.296648f, 1.0f },
        0.088f*128.0f,
        -1.0f,
        -3.0f
    },
    {   // ruby
        { 0.1745f, 0.01175f, 0.01175f, 1.0f },
        { 0.61424f, 0.04136f, 0.04136f, 1.0f },
        { 0.727811f, 0.626959f, 0.626959f, 1.0f },
        0.6f * 128.0f,
        -3.0f,
        -3.0f
    },
    {   // turquoise
        { 0.1f, 0.18725f, 0.1745f, 1.0f },
        { 0.396f, 0.74151f, 0.69102f, 1.0f },
        { 0.297254f, 0.30829f, 0.306678f, 1.0f },
        0.1f * 128.0f,
        -5.0f,
        -3.0f
    },

    // 2nd column
    {   // brass
        { 0.329412f, 0.223529f, 0.027451f, 1.0f },
        { 0.780392f, 0.568627f, 0.113725f, 1.0f },
        { 0.992157f, 0.941176f, 0.807843f, 1.0f },
        0.21794872f*128.0f,
        5.0f,
        -1.0f
    },
    {   // bronze
        { 0.2125f, 0.1275f, 0.054f, 1.0f },
        { 0.714f, 0.4284f, 0.18144f, 1.0f },
        { 0.393548f, 0.271906f, 0.166721f, 1.0f },
        0.2f * 128.0f,
        3.0f,
        -1.0f
    },
    {   // chrome
        { 0.25f, 0.25f, 0.25f, 1.0f },
        { 0.4f, 0.4f, 0.4f, 1.0f },
        { 0.774597f, 0.774597f, 0.774597f, 1.0f },
        0.6f * 128.0f,
        1.0f,
        -1.0f
    },
    {   // copper
        { 0.19125f, 0.0735f, 0.0225f, 1.0f },
        { 0.7038f, 0.27048f, 0.828f, 1.0f },
        { 0.256777f, 0.137622f, 0.086014f, 1.0f },
        0.1f*128.0f,
        -1.0f,
        -1.0f
    },
    {   // gold
        { 0.24725f, 0.1995f, 0.0745f, 1.0f },
        { 0.75164f, 0.60648f, 0.22648f, 1.0f },
        { 0.628281f, 0.555802f, 0.366065f, 1.0f },
        0.4f * 128.0f,
        -3.0f,
        -1.0f
    },
    {   // silver
        { 0.19225f, 0.19225f, 0.19225f, 1.0f },
        { 0.50754f, 0.50754f, 0.50754f, 1.0f },
        { 0.508273f, 0.508273f, 0.508273f, 1.0f },
        0.4f * 128.0f,
        -5.0f,
        -1.0f
    },

    // 3rd column
    {   // black
        { 0.0f, 0.0f, 0.0f, 1.0f },
        { 0.01f, 0.01f, 0.01f, 1.0f },
        { 0.50f, 0.50f, 0.50f, 1.0f },
        0.25f*128.0f,
        5.0f,
        1.0f
    },
    {   // cyan
        { 0.0f, 0.1f, 0.06f, 1.0f },
        { 0.0f, 0.50980392f, 0.50980392f, 1.0f },
        { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f },
        0.25f * 128.0f,
        3.0f,
        1.0f
    },
    {   // green
        { 0.0f, 0.0f, 0.0f, 1.0f },
        { 0.1f, 0.35f, 0.1f, 1.0f },
        { 0.45f, 0.55f, 0.45f, 1.0f },
        0.25f * 128.0f,
        1.0f,
        1.0f
    },
    {   // red
        { 0.0f, 0.0f, 0.0f, 1.0f },
        { 0.5f, 0.0f, 0.0f, 1.0f },
        { 0.7f, 0.6f, 0.6f, 1.0f },
        0.25f*128.0f,
        -1.0f,
        1.0f
    },
    {   // white
        { 0.0f, 0.0f, 0.0f, 1.0f },
        { 0.55f, 0.55f, 0.55f, 1.0f },
        { 0.7f, 0.7f, 0.7f, 1.0f },
        0.25f*128.0f,
        -3.0f,
        1.0f
    },
    {   // yellow plastic
        { 0.0f, 0.0f, 0.0f, 1.0f },
        { 0.5f, 0.5f, 0.0f, 1.0f },
        { 0.6f, 0.6f, 0.5f, 1.0f },
        0.25f*128.0f,
        -5.0f,
        1.0f
    },

    // 4th column
    {   // black
        { 0.02f, 0.02f, 0.02f, 1.0f },
        { 0.01f, 0.01f, 0.01f, 1.0f },
        { 0.40f, 0.40f, 0.40f, 1.0f },
        0.078125f*128.0f,
        5.0f,
        3.0f
    },
    {   // cyan
        { 0.0f, 0.05f, 0.05f, 1.0f },
        { 0.4f, 0.5f, 0.5f, 1.0f },
        { 0.04f, 0.7f, 0.7f, 1.0f },
        0.078125f*128.0f,
        3.0f,
        3.0f
    },
    {   // green
        { 0.0f, 0.05f, 0.0f, 1.0f },
        { 0.4f, 0.5f, 0.4f, 1.0f },
        { 0.04f, 0.7f, 0.04f, 1.0f },
        0.078125f*128.0f,
        1.0f,
        3.0f
    },
    {   // red
        { 0.05f, 0.0f, 0.0f, 1.0f },
        { 0.5f, 0.4f, 0.4f, 1.0f },
        { 0.7f, 0.04f, 0.04f, 1.0f },
        0.078125f*128.0f,
        -1.0f,
        3.0f
    },
    {   // white
        { 0.05f, 0.05f, 0.05f, 1.0f },
        { 0.5f, 0.5f, 0.5f, 1.0f },
        { 0.7f, 0.7f, 0.7f, 1.0f },
        0.078125f*128.0f,
        -3.0f,
        3.0f
    },
    {   // yellow rubber
        { 0.05f, 0.05f, 0.0f, 1.0f },
        { 0.5f, 0.5f, 0.4f, 1.0f },
        { 0.7f, 0.7f, 0.04f, 1.0f },
        0.078125f*128.0f,
        -5.0f,
        3.0f
    },
};

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    //function prototype
    void initialize(void);
    void display(void);
    void uninitialize(void);
    void spin(void);

    //variable declaration
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szClassName[] = TEXT("RTROGL");
    bool bDone = false;

    //code
    //initializing members of struct WNDCLASSEX
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szClassName;
    wndclass.lpszMenuName = NULL;

    //Registering Class
    RegisterClassEx(&wndclass);

    //Create Window
    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szClassName,
        TEXT("OGL Fixed function. Lights on Rotating Objects"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        0,
        0,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd = hwnd;

    //initialize
    initialize();

    ShowWindow(hwnd, SW_SHOW);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    //Message Loop
    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                    bDone = true;

                spin();
                // render
                display();
            }
        }
    }

    uninitialize();
    return((int)msg.wParam);
}

void spin(void)
{
    g_rotationAngle += 1.0f;
    if (g_rotationAngle >= 360.0f)
        g_rotationAngle = 1.0f;
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //function prototype
    void resize(int, int);
    void ToggleFullscreen(void);
    void uninitialize(void);

    //code
    switch (iMsg)
    {
    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0)
            gbActiveWindow = true;
        else
            gbActiveWindow = false;
        break;
    case WM_ERASEBKGND:
        return(0);
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            gbEscapeKeyIsPressed = true;
            break;
        }
        break;
    case WM_CHAR:
        switch(wParam)
        {
        case 'F':
        case 'f':
            if (gbFullscreen == false)
            {
                ToggleFullscreen();
                gbFullscreen = true;
            }
            else
            {
                ToggleFullscreen();
                gbFullscreen = false;
            }
            break;

        case 'L':
        case 'l':
            g_bLightEnable = !g_bLightEnable;
            if (g_bLightEnable)
            {
                glEnable(GL_LIGHTING);
            }
            else
            {
                glDisable(GL_LIGHTING);
            }
            break;

        case 'X':
        case 'x':
            g_lightRotationAxis = X_axis;
            g_rotationAngle = 0.0f;
            break;

        case 'Y':
        case 'y':
            g_lightRotationAxis = Y_axis;
            g_rotationAngle = 0.0f;
            break;

        case 'Z':
        case 'z':
            g_lightRotationAxis = Z_axis;
            g_rotationAngle = 0.0f;
            break;

        default:
            break;
        }
        break;
    case WM_LBUTTONDOWN:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        break;
    }
    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    //variable declarations
    MONITORINFO mi;

    //code
    if (gbFullscreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi = { sizeof(MONITORINFO) };
            if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
    }

    else
    {
        //code
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }
}

void initialize(void)
{
    //function prototypes
    void resize(int, int);

    //variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    //code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    //Initialization of structure 'PIXELFORMATDESCRIPTOR'
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;
    pfd.cDepthBits = 32;

    ghdc = GetDC(ghwnd);

    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if (iPixelFormatIndex == 0)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_NORMALIZE);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, g_lightModelAmbient);
    glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, g_lightModelLocalViewer);

    // set components for light0
    glLightfv(GL_LIGHT0, GL_AMBIENT, g_lightAmbientComponent);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, g_lightDiffusedComponent);
    glLightfv(GL_LIGHT0, GL_SPECULAR, g_lightSpecularComponent);
    glEnable(GL_LIGHT0);

    if (g_gluQuadric == nullptr)
        g_gluQuadric = gluNewQuadric();

    if (g_bLightEnable)
        glEnable(GL_LIGHTING);
    else
        glDisable(GL_LIGHTING);

    resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    if (g_lightRotationAxis != NoRotation)
    {
        glRotatef(g_rotationAngle,
            (g_lightRotationAxis == X_axis) ? 1.0f : 0.0f,
            (g_lightRotationAxis == Y_axis) ? 1.0f : 0.0f,
            (g_lightRotationAxis == Z_axis) ? 1.0f : 0.0f
        );
        switch (g_lightRotationAxis)
        {
        case X_axis:
            g_lightPosition[0] = 0.0f;
            g_lightPosition[1] = g_rotationAngle;
            g_lightPosition[2] = 0.0f;
            break;
        case Y_axis:
        case Z_axis:
            g_lightPosition[0] = g_rotationAngle;
            g_lightPosition[1] = 0.0f;
            g_lightPosition[2] = 0.0f;
            break;
        }
    }
    glLightfv(GL_LIGHT0, GL_POSITION, g_lightPosition);
    glPopMatrix();

    for (size_t i = 0; i < _countof(g_spheres); ++i)
    {
        g_spheres[i].draw();
    }

    SwapBuffers(ghdc);
}

void resize(int width, int height)
{
    //code
    if (height == 0)
        height = 1;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    glMatrixMode(GL_PROJECTION); // select projection matrix
    glLoadIdentity(); // reset projection matrix

    GLfloat aspect = (GLfloat)width / (GLfloat)height;

    if (aspect >= 1.0f)
    {
        // extend the left and right clipping planes with the aspect ratio
        xMultiplier = aspect;
        yMultiplier = 1.0f;
    }
    else
    {
        // extend the top and bottom clipping planes with the aspect ratio
        xMultiplier = 1.0f;
        yMultiplier = aspect;
    }
    glFrustum(-xMultiplier, xMultiplier, -yMultiplier, yMultiplier, 1.0f, 10000.0f);
}

void uninitialize(void)
{
    if (gbFullscreen == true)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }

    wglMakeCurrent(NULL, NULL);

    wglDeleteContext(ghrc);
    ghrc = NULL;

    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;

    DestroyWindow(ghwnd);
    ghwnd = NULL;

    if (g_gluQuadric != nullptr)
    {
        gluDeleteQuadric(g_gluQuadric);
        g_gluQuadric = nullptr;
    }
}
