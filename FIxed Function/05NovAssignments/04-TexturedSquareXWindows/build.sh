cwd=$(pwd)
cd ../../drawHelper.lib
./build_lib.sh
echo "Copying drawhelper to build directory"
cp drawhelper.a "$cwd"
echo "Building xwindow"
cd "$cwd"
rm -rf *.png
rm -rf xwindow
rm -rf xwindow.log
g++ -I../../drawHelper.lib "./xwindow.cpp" "drawhelper.a" -o "./xwindow" -lX11 -lGL -lGLU -lSOIL -std=c++1y
echo "deleting intermidiate files"
rm -rf drawhelper.a
echo "launching xwindow"
./xwindow&
sleep 3
echo "capturing screenshot"
sleep 2
import -window root -delay 5000 screenshot.png



