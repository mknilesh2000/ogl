#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "DrawHelper.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 800

#define NUM_0 0x30
#define NUM_1 0x31
#define NUM_2 0x32
#define NUM_3 0x33
#define NUM_4 0x34
#define NUM_5 0x35
#define NUM_6 0x36
#define NUM_7 0x37
#define NUM_8 0x38
#define NUM_9 0x39

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
Vertex3f gvMax = {1.0f, 1.0f, 1.0f};
GLfloat g_angleTri = 0.0f;
GLfloat g_angleQuad = 0.0f;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    //function prototype
    void initialize(void);
    void uninitialize(void);
    void display(void);
    void update(void);

    //variable declaration
    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szClassName[] = TEXT("RTROGL");
    bool bDone = false;

    //code
    //initializing members of struct WNDCLASSEX
    wndclass.cbSize = sizeof(WNDCLASSEX);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szClassName;
    wndclass.lpszMenuName = NULL;

    //Registering Class
    RegisterClassEx(&wndclass);

    //Create Window
    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szClassName,
        TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : First Window"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        0,
        0,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd = hwnd;

    //initialize
    initialize();

    ShowWindow(hwnd, SW_SHOW);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    //Message Loop
    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = true;
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow == true)
            {
                if (gbEscapeKeyIsPressed == true)
                    bDone = true;
                update();
                display();
            }
        }
    }

    uninitialize();
    return((int)msg.wParam);
}

void update(void)
{
    g_angleTri += 1.0f;
    g_angleQuad += 1.0f;

    if (g_angleTri >= 360.0f)
        g_angleTri = 0.0f;

    if (g_angleQuad >= 360.0f)
        g_angleQuad = 0.0f;
}

void processKeyDownMsg(WPARAM wParam, LPARAM lParam)
{
    void resize(int width, int height, int startX = 0, int startY = 0);
    void ToggleFullscreen(void);

    // only press when last state was up
    if ((lParam & 0x40000000) == 0)
    {
        RECT rect;
        GetWindowRect(ghwnd, &rect);

        UINT width = rect.right - rect.left;
        UINT height = rect.bottom - rect.top;

        switch (wParam)
        {
        case VK_ESCAPE:
            gbEscapeKeyIsPressed = true;
            break;
        case 0x46: //for 'f' or 'F'
            if (gbFullscreen == false)
            {
                ToggleFullscreen();
                gbFullscreen = true;
            }
            else
            {
                ToggleFullscreen();
                gbFullscreen = false;
            }
            break;

        case NUM_0: // left-top
            resize(width, height);
            break;
        case NUM_1: // left-top
            resize(width / 2, height / 2, 0, height / 2);
            break;
        case NUM_2: // right-top
            resize(width/2, height/2, width / 2, height / 2);
            break;
        case NUM_3: // left-bottom
            resize(width/2, height/2, 0, 0);
            break;
        case NUM_4: // right-bottom
            resize(width / 2, height / 2, width/2, 0);
            break;
        case NUM_5: // left-half
            resize(width/2, height, 0, 0);
            break;
        case NUM_6: // right-half
            resize(width / 2, height, height/2, 0);
            break;
        case NUM_7: // bottom-half
            resize(width, height/2, 0, 0);
            break;
        case NUM_8: // top-half
            resize(width, height / 2, 0, height/2);
            break;
        case NUM_9: // center
            resize(width / 2, height / 2, width / 4, height / 4);
            break;
        }
    }
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //function prototype
    void resize(int width, int height, int startX = 0, int startY = 0);
    void uninitialize(void);

    //code
    switch (iMsg)
    {
    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0)
            gbActiveWindow = true;
        else
            gbActiveWindow = false;
        break;
    case WM_SIZE:
        resize(LOWORD(lParam), HIWORD(lParam));
        break;
    case WM_KEYDOWN:
        processKeyDownMsg(wParam, lParam);
        break;
    case WM_LBUTTONDOWN:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        break;
    }
    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
    //variable declarations
    MONITORINFO mi;

    //code
    if (gbFullscreen == false)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        if (dwStyle & WS_OVERLAPPEDWINDOW)
        {
            mi = { sizeof(MONITORINFO) };
            if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
    }

    else
    {
        //code
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

        ShowCursor(TRUE);
    }
}

void initialize(void)
{
    //function prototypes
    void resize(int width, int height, int startX = 0, int startY = 0);

    //variable declarations
    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    //code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    //Initialization of structure 'PIXELFORMATDESCRIPTOR'
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;

    ghdc = GetDC(ghwnd);

    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    if (iPixelFormatIndex == 0)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
        ReleaseDC(ghwnd, ghdc);
        ghdc = NULL;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-1.5f * 1.0f, 0.0f, -6.0f);

    Vertex3f triVertices[] = {
        { 0.0f, 1.0f, 0.0f },
        { -1.0f, -1.0f, 0.0f },
        { 1.0f, -1.0f, 0.0f }
    };

    Color3f colorsTri[] = {
        { 1.0f, 0.0f, 0.0f },
        { 0.0f, 1.0f, 0.0f },
        { 0.0f, 0.0f, 1.0f }
    };

    glRotatef(g_angleTri, 0.0f, 1.0f, 0.0f);
    drawSolidTriangle(_countof(colorsTri), colorsTri, triVertices);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(1.5f * 1.0f, 0.0f, -6.0f);

    Vertex3f quadVertices[] = {
        { -1.0f, 1.0f, 0.0f },
        { -1.0f, -1.0f, 0.0f },
        { 1.0f, -1.0f, 0.0f },
        { 1.0f, 1.0f, 0.0f }
    };

    // corn-flour color
    Color3f colorsQuad[] = {
        { (GLfloat)249 / 255, (GLfloat)253 / 255, (GLfloat)164 / 255 },
    };

    glRotatef(g_angleQuad, 1.0f, 0.0f, 0.0f);
    drawSolidQuad(_countof(colorsQuad), colorsQuad, quadVertices);

    SwapBuffers(ghdc);
}

void resize(int width, int height, int startX=0, int startY=0)
{
    //code
    if (height == 0)
        height = 1;

    GLfloat aspect = (GLfloat)width / (GLfloat)height;

    glViewport(startX, startY, (GLsizei)width, (GLsizei)height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    ZeroMemory(&gvMax, sizeof(gvMax));

    if (aspect >= 1.0f)
    {
        // extend the left and right clipping planes with the aspect ratio
        gvMax.x = 1.0f * aspect;
        gvMax.y = 1.0f;
    }
    else
    {
        // extend the top and bottom clipping planes with the aspect ratio
        gvMax.x = 1.0f;
        gvMax.y = 1.0f / aspect;
    }
    glFrustum(-gvMax.x, gvMax.x, -gvMax.y, gvMax.y, 1.0f, 1000.0f);
}

void uninitialize(void)
{
    //UNINITIALIZATION CODE
    if (gbFullscreen == true)
    {
        dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
        SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(ghwnd, &wpPrev);
        SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }
    wglMakeCurrent(NULL, NULL);

    wglDeleteContext(ghrc);
    ghrc = NULL;

    ReleaseDC(ghwnd, ghdc);
    ghdc = NULL;

    DestroyWindow(ghwnd);
    ghwnd = NULL;
}
