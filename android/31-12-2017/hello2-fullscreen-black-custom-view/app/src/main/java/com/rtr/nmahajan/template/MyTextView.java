package com.rtr.nmahajan.template;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

public class MyTextView extends TextView {
    public MyTextView(Context thisContext) {
        super(thisContext);
        setText("Hello Android from Nilesh!!");
        setTextColor(Color.GREEN);
        setGravity(Gravity.CENTER);
    }
}
