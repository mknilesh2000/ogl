package com.rtr.nmahajan.template;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView textView = new TextView(this);
        textView.setText("Hello Android from Nilesh!!");
        textView.setTextColor(Color.GREEN);
        textView.setGravity(Gravity.CENTER);
        setContentView(textView);
    }
}
